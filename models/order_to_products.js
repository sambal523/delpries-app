const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema
const OrderToProductsSchema = new Schema ({
    order_id : {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    product_id : {
        type: String,
        required: true
    },
    amount_ordered : {
        type: Number,
        required: true
    },
    product_price : {
        type: Number,
        required: true
    },
    product_name : {
        type: String,
        required: true
    },
    special_instructions: {
        type: String,
        required: false
    },
    addons_total: {
        type: Number,
        required: false
    },
    addons_description: {
        type: String,
        required: false
    }
});


//export to have access
module.exports = OrderToProducts = mongoose.model('order_to_products', OrderToProductsSchema)