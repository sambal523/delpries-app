const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema
const ServicesCategorySchema = new Schema ({
    service_category_name : {
        type: String,
        required: true
    }, 
    service_category_date_created : {
        type: Date,
        required: true,
        default: Date.now
    },
    service_category_more_info : {
        type: String,
        required: true
    },
    service_category_nice_color : {
        type: String,
        required: true
    },
    service_category_image_name : {
        type: String,
        required: true
    },
    service_category_image_icon: {
        type: String,
        required: true
    },
    is_active : {
        type: Boolean,
        required: true
    }
});


//export to have access
module.exports = ServicesCategory = mongoose.model('services_category', ServicesCategorySchema)