const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema
const ProductVendorMoreInfoSchema = new Schema ({
    product_vendor_postal_code : {
        type: String,
        required: true
    }, 
    product_vendor_address : {
        type: String,
        required: true
    }, 
    product_vendor_province : {
        type: String,
        required: true
    },    
    product_vendor_city : {
        type: String,
        required: true
    }, 
    product_vendor_store : {
        type: String,
        required: true
    }, 
    product_vendor_phone_number : {
        type: String,
        required: true
    }, 
    product_vendor_text_description : {
        type: String,
        required: true
    },
    product_vendor_id : {
        type: String,
        required: true
    }, 
});


//export to have access
module.exports = ProductVendorMoreInfo = mongoose.model('product_vendor_more_info', ProductVendorMoreInfoSchema)