const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const VendorScheduleSpecialtySchema = new Schema ({
    vendor_id : {
        type: String,
        required: true
    },
    special_date : {
        type: String,
        required: true
    },
    start_time : {
        type: String,
        required: true
    },
    end_time : {
        type: String,
        required: true
    },
    notes: {
        type: String,
        required: true
    }
})


//export to have accessx
module.exports = VendorScheduleSpecialty = mongoose.model('vendor_schedule_specialty', VendorScheduleSpecialtySchema)