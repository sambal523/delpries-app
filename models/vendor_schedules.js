const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const VendorScheduleSchema = new Schema ({
    vendor_id : {
        type: String,
        required: true
    },
    monday_start_time : {
        type: String,
        required: true,
    },
    monday_end_time : {
        type: String,
        required: true,
    },
    tuesday_start_time : {
        type: String,
        required: true,
    },
    tuesday_end_time : {
        type: String,
        required: true,
    },
    wednesday_start_time : {
        type: String,
        required: true,
    },
    wednesday_end_time : {
        type: String,
        required: true,
    },
    thursday_start_time : {
        type: String,
        required: true,
    },
    thursday_end_time : {
        type: String,
        required: true,
    },
    friday_start_time : {
        type: String,
        required: true,
    },
    friday_end_time : {
        type: String,
        required: true,
    },
    saturday_start_time : {
        type: String,
        required: true,
    },
    saturday_end_time : {
        type: String,
        required: true,
    },
    sunday_start_time : {
        type: String,
        required: true,
    },
    sunday_end_time : {
        type: String,
        required: true,
    }
})


//export to have accessx
module.exports = VendorSchedule = mongoose.model('vendor_schedule', VendorScheduleSchema)