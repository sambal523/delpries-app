const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const UserSchema = new Schema ({
    user_name : {
        type: String,
        required: true
    },
    first_name : {
        type: String,
        required: true
    },
    last_name : {
        type: String,
        required: true
    },
    date_joined : {
        type: Date,
        required: true,
        default: Date.now,
    },
    password : {
        type: String,
        required: true
    },
    user_type_id: {
        type: String,
        required: true
    },
    user_email : {
        type: String,
		lowercase: true,
        unique: true,
		required: true
    }, 
    mobile_no : {
        type: String,
        required: true
    }, 
    is_account_locked: {
        type: Boolean,
		required: true
    },
    verification_code: {
        type: String,
		required: false
    },
    can_access_order_details_page :{
        type: Boolean,
        required: true,
        default: false
    },
    can_access_vendors_page :{
        type: Boolean,
        required: true,
        default: false
    },
    can_access_drivers_page :{
        type: Boolean,
        required: true,
        default: false
    },
    can_make_an_order :{
        type: Boolean,
        required: true,
        default: false
    },
    deactivated: {
        type: Boolean,
        required: true,
        default: false
    }
});


//export to have accessx
module.exports = User = mongoose.model('user', UserSchema)