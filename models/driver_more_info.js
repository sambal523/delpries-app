const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema
const DriverMoreInfoSchema = new Schema ({
    user_id : {
        type: String,
        required: true
    }, 
    drivers_license_number : {
        type: String,
        required: true
    }, 
    on_an_order : {
        type: Boolean,
        required: true
    }, 
    is_currently_active : {
        type: Boolean,
        required: true
    }
});


//export to have access
module.exports = DriverMoreInfo = mongoose.model('driver_more_info', DriverMoreInfoSchema)
