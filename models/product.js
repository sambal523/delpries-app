const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema
const addonsSchema = new Schema ({
    name: String,
    price: Number
})

const groupAddons = new Schema ({
    group_name: String,
    how_many_must_be_chosen: Number,
    addons: [addonsSchema]
})

const ProductSchema = new Schema ({
    product_name : {
        type: String,
        required: true
    }, 
    product_price : {
        type: Number,
        required: true
    },
    service_category_id : {
        type: String,
        required: true
    }, 
    product_vendor_id : {
        type: String,
        required: true
    },
    product_description : {
        type: String,
        required: true
    },
    product_number_available : {
        type: Number,
        required: true
    },
    is_active : {
        type: Boolean,
        required: true
    },
    uncountable_product_type : {
        type: Boolean,
        required: true
    },
    uncountable_product_type_out_of_stock : {
        type: Boolean,
        required: true
    },
    add_on_groups: [groupAddons]
});

/**
 *         {
            id: String,
            group_name: String,
            how_many_must_be_chosen: Number,
            add_ons: [
                {
                    name: String,
                    price: String
                }
            ]
        }
 */


//export to have access
module.exports = Product = mongoose.model('product', ProductSchema)