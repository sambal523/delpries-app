const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema
const ProductVendorSchema = new Schema ({
    product_vendor_name : {
        type: String,
        required: true
    }, 
    product_vendor_image_name : {
        type: String,
        required: true
    },
    product_vendor_icon_name: {
        type: String,
        required: true
    },
    product_vendor_ratings: {
        type: String,
        required: true
    },
    is_active: {
        type: Boolean,
		required: true
    },
    user_id: {
        type: String,
        required: true
    },
    is_currently_active: {
        type: Boolean,
		required: true
    },
    admin_password: {
        type: String,
        required: true
    }
});


//export to have access
module.exports = ProductVendor = mongoose.model('product_vendor', ProductVendorSchema)