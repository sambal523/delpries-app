const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema
const UserTypeSchema = new Schema ({
    user_type_name : {
        type: String,
        required: true
    }
});


//export to have access
module.exports = UserType = mongoose.model('user_type', UserTypeSchema)