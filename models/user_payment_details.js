const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema
const UserPaymentDetailsSchema = new Schema ({
    card_no : {
        type: String,
        required: true
    },  
    name_on_card : {
        type: String,
        required: true
    },
    sec_no : {
        type: String,
        required: true
    },
    expiry_dat : {
        type: String,
        required: true
    },
    card_type : {
        type: String,
        required: true
    },
    user_id : {
        type: String,
        required: true
    }
});


//export to have access
module.exports = UserPaymentDetails = mongoose.model('user_payment_details', UserPaymentDetailsSchema)