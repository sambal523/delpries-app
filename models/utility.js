const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema
const UtilitySchema = new Schema ({
    city : {
        type: String,
        required: true
    }, 
    province : {
        type: String,
        required: true
    }, 
    delivery_fee_for_client : {
        type: Number,
        required: true
    },
    drivers_money : {
        type: Number,
        required: true
    }, 
    service_charge_on_vendors_percentage : {
        type: Number,
        required: true
    }, 
    delivery_fee_on_stores : {
        type: Number,
        required: true
    }, 
    tax : {
        type: Number,
        required: true
    },
    percent_delpries_take_for_tip: {
        type: Number,
        required: true
    },
    percent_drivers_take_for_tip: {
        type: Number,
        required: true
    },
    i_need_driver_delivery_fee: {
        type: Number,
        required: true
    }
});


//export to have access
module.exports = Utility = mongoose.model('utility', UtilitySchema)
