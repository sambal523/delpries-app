const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema
const OrderSchema = new Schema ({
    order_name : {
        type: String,
        required: true
    }, 
    order_date : {
        type: Date,
        required: true
    }, 

    
    payment_id : {
        type: String,
        required: true
    }, 
    user_id : {
        type: String,
        required: true
    }, 
    user_name : {
        type: String,
        required: true
    }, 


    order_price_total_with_tax: {
        type: Number,
        required: true
    }, 
    product_vendor_id : {
        type: String,
        required: false
    },
    just_ordered : {
        type: Boolean,
        required: true
    },
    vendor_approved : {
        type: Boolean,
        required: true
    },


    time_to_get_ready_milli_sec : {
        type: String
    },
    order_needs_driver : {
        type: Boolean,
        required: true
    },


    driver_has_started_order : {
        type: Boolean,
        required: true
    },
    driver_has_picked_up : {
        type: Boolean,
        required: true
    },    
    delivered : {
        type: Boolean,
        required: true
    }, 

    drivers_total_money: {
        type: Number,
        required: true
    }, 
    vendors_total_money: {
        type: Number,
        required: true
    },
    driver_id : {
        type: String,
        required: false
    },
    address : {
        type: String,
        required: true
    },
    tip: {
        type: Number,
        required: true
    },
    order_num_for_view: {
        type: String,
        required: true
    },

    vendor_requested_order:  {
        type: Boolean,
        required: true
    },
    vendor_wants_direct_transfer:  {
        type: Boolean,
        required: true
    },
    vendor_wants_cash:  {
        type: Boolean,
        required: true
    },
    clients_would_give_cash:  {
        type: Boolean,
        required: true
    },
    clients_would_give_card:  {
        type: Boolean,
        required: true
    },


    cancelled:  {
        type: Boolean,
        required: true
    },

    
    vendor_approved_at : {
        type: Date,
        required: false
    }, 
    driver_accepted_at : {
        type: Date,
        required: false
    },     
    driver_has_started_order_at : {
        type: Date,
        required: false
    }, 
    driver_has_picked_up_at : {
        type: Date,
        required: false
    }, 
    delivered_at : {
        type: Date,
        required: false
    }
});


//export to have access
module.exports = Order = mongoose.model('order', OrderSchema)
