
/**
 * Date Created: 24th November 2019.
 * This file would hold all the routes related to the Driver Compoment
 */

//system defined imports
const config = require('config');
const express = require('express');
const router = express.Router();
const _ = require('lodash');


const Order = require('../../models/order');
const Product = require('../../models/product');
const ProductVendor = require('../../models/product_vendors');
const ProductVendorMoreInfo = require('../../models/product_vendor_more_info');
const Users = require('../../models/user');
const auth = require('../../middleware/auth');
const DriverMoreInfo = require('../../models/driver_more_info');

/**
 * Get all order that the driver has made
 */
router.get('/get_orders_this_driver_has_made/:id', auth, (req, res) => {
    var driverId = req.params.id;

    if(!driverId) {
        return res.json({msg: 'Please enter all fields!', productAdded: false})
    } 

    //get the orders
    Order.find({driver_id: driverId, delivered: true})
    .select('-order_price_total_with_tax -tip -vendors_total_money')
    .then(orders => {
        /* add vendor name and location to the data that has been returned  */
        //get the order ids
        var idsOfVendor = orders.map(function(order) { return order.product_vendor_id; });

        //get the OrderToProducts data that match the orders
        ProductVendor.find({_id: {$in: idsOfVendor}})
        .select('')
        .then(vendors => {
            //map add the vendor data to the order data
            var compilationOfOrderAndVendorsData = [];

            //iterate through the orders
            for(var i=0; i<orders.length; i++) {
                //iterate through the orderitems
                for(var j=0; j<vendors.length; j++) {
                    //if the order id matches the order id in the order items
                    if(vendors[j]._id.equals(orders[i].product_vendor_id)) {
                        compilationOfOrderAndVendorsData.push(Object.assign({}, orders[i].toObject(), {vendor_name : vendors[j].product_vendor_name } ));

                        continue;
                    }
                }
            }

            return res.send(compilationOfOrderAndVendorsData.sort((a, b) => (a.order_date < b.order_date) ? 1 : -1));
        })
        .catch(err => {});
    })
    .catch(err => {});
});

/**
 * get more info for an order
 */
router.get('/get_more_info_on_an_order/:id', auth, (req, res) => {
    var orderId = req.params.id;

    if(!orderId) {
        return res.json({msg: 'Please enter all fields!', productFound: false})
    } 

    //get the order details and the the order to Product details and then merge them and send them to the front end
    Order.findOne({_id: orderId})
    .select('-product_vendor_id -vendors_total_money -tip -order_price_total_with_tax')
    .then(order => {
        OrderToProducts.find({order_id: orderId})
        .select('')
        .then(orderToProducts => {
            return res.json({order, orderToProducts});
        })
        .catch(err => {})
    })
    .catch(err => {})
 });

 /**
 * The vendor sends this when they have the order items
 */
router.get('/driver_will_take_order/:id/:id2', auth, (req, res) => {
    var orderId = req.params.id;
    var driverId = req.params.id2;

    //date data
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var orderDate = date+' '+time;
    
    //get the order and update the details
    Order.findOne({_id: orderId})
    .select('-drivers_total_money -order_price_total_with_tax -tip -order_name')
    .then(order => {
        if(!order) return res.json({msg: 'Not updated', SuccessInUpdate: false});
        if(order.driver_id) return res.json({msg: 'Order is already Taken', SuccessInUpdate: false});
        
        order.driver_id = driverId;
        order.driver_accepted_at = orderDate;
        order.save() 
        .then(order => {
            DriverMoreInfo.findOne({user_id: driverId})
            .then(driverMoreInfo => {
                if(!driverMoreInfo) return res.json({msg: 'Not updated', SuccessInUpdate: false});
                driverMoreInfo.on_an_order = true;
                driverMoreInfo.save()
                .then(driverMoreInfo => {
                    return res.json({msg: 'updated', SuccessInUpdate: true});
                });
            })
        });
    })
    .catch(err => {})
});



/**
 * get all the orders that this driver has accepted
 */
router.get('/get_orders_driver_accepted_to_make/:id', auth, (req, res) => {
    var driverId = req.params.id;

    if(!driverId) {
        return res.json({msg: 'Please enter all fields!'})
    } 

    //get the order details and the the order to Product details and then merge them and send them to the front end
    Order.find({driver_id: driverId, vendor_approved: true, delivered: false})
    .select('')
    .then(orders => {
        //get the ids of all the vendors related to the orders
        var idsOfVendors = orders.map(function(order) { return order.product_vendor_id; });
        var idsOfClients = orders.map(function(order) { return order.user_id; });

        //get the product vendors data
        ProductVendor.find({_id: {$in: idsOfVendors}})
        .select('')
        .then(vendors => {

            //get the clients data ProductVendorMoreInfo
            Users.find({_id: {$in: idsOfClients}})
            .select('')
            .then(clients => {

                ProductVendorMoreInfo.find({product_vendor_id: {$in: idsOfVendors}})
                .select('')
                .then(productVendorMoreInfos => {

                    var compilationOfOrderVendorsAndClientsData = [];
                    var vendorObj = null;
                    var clientObj = null;
                    var vendorMoreInfoObj = null;
                    
                    //loop through the orders, get the appopriate vendors data and also the clients data and merge it to create a larger object that gets appened to the list that should be returned
                    for(var i=0; i<orders.length; i++) {
                        vendorObj = _.find(vendors, function(vend) {
                            return vend._id.equals(orders[i].product_vendor_id)
                        });
                        clientObj = _.find(clients, function(cli) {
                            return cli._id.equals(orders[i].user_id)
                        });
                        vendorMoreInfoObj = _.find(productVendorMoreInfos, {'product_vendor_id': orders[i].product_vendor_id});

                        //if we found the right data
                        if(vendorObj && clientObj && vendorMoreInfoObj) {
                            compilationOfOrderVendorsAndClientsData.push({
                                                                            order_id: orders[i]._id,
                                                                            order_num_for_view: orders[i].order_num_for_view,
                                                                            vendor_name : vendorObj.product_vendor_name,
                                                                            vendor_address : vendorMoreInfoObj.product_vendor_address,
                                                                            clients_adress: orders[i].address,
                                                                            clients_name: orders[i].user_name,
                                                                            clients_mobile_number: clientObj.mobile_no,
                                                                            driver_has_started_order: orders[i].driver_has_started_order,
                                                                            driver_has_picked_up: orders[i].driver_has_picked_up,
                                                                            driver_id: orders[i].driver_id
                                                                        });
                        } 
                    }
    
                    DriverMoreInfo.findOne({user_id: driverId})
                    .select('')
                    .then(driverMoreInfoData => {
                        return res.json({ orders : compilationOfOrderVendorsAndClientsData.sort((a, b) => (a.driver_has_started_order < b.driver_has_started_order) ? 1 : -1),
                                   driver: {driverCurrentAvailabilityStatus: driverMoreInfoData.is_currently_active}});
                    });
                });
            });
        });
    })
    .catch(err => {})
 });


/**
 * This gets called when the driver wants to start an order
 */
router.get('/start_order/:id/:id2', auth, (req, res) => {
    var driverId = req.params.id;
    var orderId = req.params.id2;

    if(!driverId && !orderId) {
        return res.json({msg: 'Please enter all fields!', orderStarted: false})
    }

    //date data
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var orderDate = date+' '+time;

    //find the order and then modify the driver has started order value if the driver is the one that has that order
    Order.findOne({_id : orderId})
    .select('')
    .then(order => {
        if(!order) return res.json({msg: 'Could not find the order!', orderStarted: false});

        //get all the orders that are for this vendor and check if any order is started and not delivered, if so dont allow the driver start a new order  -- this is just extra caution
        Order.find({driver_id: driverId, driver_has_started_order: true, delivered: false})
        .then(orderData => {
            if(orderData.length > 0) { 
                return res.json({msg: 'Please finish your current order!', orderStarted: false});
            } else {
                if(order.driver_id === driverId) {
                    order.driver_has_started_order = true;
                    order.driver_has_started_order_at = orderDate;
                    order.save()
                    .then(order => {
                        return res.json({msg: 'order has started', orderStarted: true})
                    });
                } else {
                    return res.json({msg: 'Invalid driver for this order', orderStarted: false})
                }
            }
        })
    });
});

/**
 * This gets called when the driver has picked up an order
 */
router.get('/picked_up_order/:id/:id2', auth, (req, res) => {
    var driverId = req.params.id;
    var orderId = req.params.id2;

    if(!driverId && !orderId) {
        return res.json({msg: 'Please enter all fields!', orderUpdated: false})
    } 

    //date data
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var orderDate = date+' '+time;

    //find the order and then modify the driver has started order value if the driver is the one that has that order
    Order.findOne({_id : orderId})
    .select('')
    .then(order => {
        if(!order) return res.json({msg: 'Could not find the order!', orderUpdated: false});

        if(order.driver_id === driverId) {
            order.driver_has_picked_up = true;
            order.driver_has_picked_up_at = orderDate;
            order.save()
            .then(order => {
                return res.json({msg: 'order has been picked up', orderUpdated: true})
            });
        } else {
            return res.json({msg: 'Invalid driver for this order', orderUpdated: false})
        }
    });
});




/**
 * This gets called when the driver has delivered an order
 */
router.get('/deliver_order/:id/:id2', auth, (req, res) => {
    var driverId = req.params.id;
    var orderId = req.params.id2;

    if(!driverId && !orderId) {
        return res.json({msg: 'Please enter all fields!', orderUpdated: false})
    } 

    //date data
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var orderDate = date+' '+time;

    //find the order and then modify the driver has started order value if the driver is the one that has that order
    Order.findOne({_id : orderId})
    .select('')
    .then(order => {
        if(!order) res.json({msg: 'Could not find the order!', orderUpdated: false});

        if(order.driver_id === driverId) {
            order.delivered = true;
            order.delivered_at = orderDate;
            order.save() 
            .then(order => {
                return res.json({msg: 'order has been delivered', orderUpdated: true})
            });
        } else {
            return res.json({msg: 'Invalid driver for this order', orderUpdated: false})
        }
    });
});


/**
 * This gets called when the driver updates availability status
 */
router.get('/current_availability/:id/:id2', auth, (req, res) => {
    var driverId = req.params.id;
    var availabilityStatus = req.params.id2;

    if(!driverId && !availabilityStatus) {
        return res.json({msg: 'Please enter all fields!', successful: false})
    } 

    DriverMoreInfo.findOne({user_id: driverId})
    .select('')
    .then(driverMoreInfoData => {
        if(driverMoreInfoData) {
            driverMoreInfoData.is_currently_active = availabilityStatus;
            driverMoreInfoData.save()
            .then(driverMoreInfoData => {
                return res.json({msg: 'status updated', successful: true})
            });
        } else {
            return res.json({msg: 'status not updated', successful: false})
        }
    });
});



module.exports = router;