
/**
 * Date Created: 2nd November 2019.
 * This file would hold all the routes related to the Vendor Compoment
 */

//system defined imports
const config = require('config');
const express = require('express');
const router = express.Router();
const _ = require('lodash');var mongoose = require('mongoose');
const moment = require('moment');



const Order = require('../../models/order');
const Product = require('../../models/product');
const ProductVendor = require('../../models/product_vendors');
const OrderToProducts = require('../../models/order_to_products.js');
const ServiceCategory = require('../../models/services_category.js');
const auth = require('../../middleware/auth');
const VendorSchedule = require('../../models/vendor_schedules');
const VendorScheduleSpecialty = require('../../models/vendor_schedules_specialty');


function orderNumber() {
    let now = Date.now().toString() // '1492341545873'

    // pad with extra random digit
    now += now + Math.floor(Math.random() * 10)
    
    return  [now.slice(4, 10), now.slice(10, 14)].join('')
}

/**
 * Get all order that have products of that service category --called in front end
 */
router.get('/get_orders_made_to_vendor/:id', auth, (req, res) => {
    var vendorId = req.params.id;

    if(!vendorId) {
        return res.json({msg: 'Please enter all fields!', productFound: false})
    }
    
    //get the orders
    Order.find({product_vendor_id: vendorId, vendor_approved: true, delivered: true, cancelled: false})
    .select('-user_id -drivers_total_money -driver_id')
    .then(orders => {
        return res.send(orders.sort((a, b) => (a.order_date < b.order_date) ? 1 : -1));
    })
    .catch(err => {});
});

/**
 * Get all pending orders waiting for vendor to approve their availability made to this vendor
 */
router.get('/get_pending_orders_made_to_vendor/:id', auth, (req, res) => {
    var vendorId = req.params.id;
        
    if(!vendorId) {
        return res.json({msg: 'Please enter all fields!', productFound: false})
    } 

    //get the orders
    Order.find({product_vendor_id: vendorId, vendor_approved: false, cancelled: false})
    .select('-drivers_total_money -order_price_total_with_tax -tip -order_name')
    .then(orders => {
        //orders.sort((a, b) => (a.order_date < b.order_date) ? 1 : -1)

        //get the order ids
        var idOfOrders = orders.map(function(order) { return order._id; });

        //get the OrderToProducts data that match the orders
        OrderToProducts.find({order_id: {$in: idOfOrders}})
        .select('')
        .then(orderItems => {
            //map add the vendor data to the order data
            var compilationOfOrderAndOrderItems = [];
            var itemsForAnOrder = [];

            //iterate through the orders
            for(var i=0; i<orders.length; i++) {

                //iterate through the orderitems
                for(var j=0; j<orderItems.length; j++) {

                    //if the order id matches the order id in the order items
                    if(orders[i]._id.equals(orderItems[j].order_id)) {
                        itemsForAnOrder.push(orderItems[j].toObject());

                        continue;
                    }
                }
                compilationOfOrderAndOrderItems.push(Object.assign({}, orders[i].toObject(), {cartItems : JSON.stringify(itemsForAnOrder)} ));
                itemsForAnOrder = [];
            }

            return res.json(compilationOfOrderAndOrderItems.sort((a, b) => (a.order_date < b.order_date) ? 1 : -1));
        })
        .catch(err => {})
    })
    .catch(err => {});
});


/**
 * get more info for an order -- i am not sure if this is being used
 */
/**
 * get more info for an order
 */
router.get('/get_more_info_on_an_order/:id', auth, (req, res) => {
    var orderId = req.params.id;

    if(!orderId) {
        return res.json({msg: 'Please enter all fields!', productFound: false})
    } 

    //get the order details and the the order to Product details and then merge them and send them to the front end
    Order.findOne({_id: orderId})
    .select('-driver_id -drivers_total_money -tip -order_price_total_with_tax')
    .then(order => {

        OrderToProducts.find({order_id: orderId})
        .select('')
        .then(orderToProducts => {
            return res.json({order, orderToProducts});
        })
        .catch(err => {})
    })
    .catch(err => {})
 });


//this would be used for deleting an addon group
 router.get('/del_addon_group/:id/:id2', auth, (req, res) => {
    var addonGroupId = req.params.id;
    var productId = req.params.id2;

    if(!addonGroupId || !productId) {
        return res.json({deleted: false, msg: "Could Not Delete"})
    }

    Product.updateOne(
        { _id: productId },
        { $pull: { add_on_groups: { _id: addonGroupId } } },
        { multi: true}
    ).then(product => {
        Product.findOne({ _id: productId }, function (err, prod){
            if(err) {
                return res.json({msg: "Error Occured", productUpdated: false});
            } 
            return res.json({deleted: true, updatedProduct : prod, msg: "Delete Successful"})
        });
    })
    .catch(err => {});
 });

/**
* Add a product data
*/
 router.post('/add_product', auth, (req, res) => {
    const {productName, serviceCategoryId, price, productDescription, amountLeft, vendorId, productIsCountable } = req.body;

    if(!productName || !serviceCategoryId || !price || !productDescription || (amountLeft === null) || !vendorId || (productIsCountable === null)) {
        return res.json({msg: 'Please enter all fields!', productAdded: false})
    } 

    var product = new Product({
        product_name : productName,
        service_category_id : serviceCategoryId,
        product_vendor_id : vendorId,
        product_price : price,
        product_description : productDescription,
        product_number_available : amountLeft,
        is_active : true,
        product_vendor_id : vendorId,
        uncountable_product_type: !productIsCountable,
        uncountable_product_type_out_of_stock: false
    });

    product.save()                                
    .then(product => {
        return res.json({msg: 'Product Added', productAdded: true, newAdded : product});
    });
 });

 /**
  * update a product data
  */
 router.post('/update_product', auth, (req, res) => {
    const {productId, productName, serviceCategoryId, price, productDescription, amountLeft, productIsCountable, thereAreAddons, addonsInfo } = req.body;

    if(!productId || !productName || !serviceCategoryId || !price || !productDescription || (amountLeft === null) || (productIsCountable === null) || (thereAreAddons === null)) {
        return res.json({msg: 'Please enter all fields!', productUpdated: false})
    } 

    Product.findOne({ _id: productId }, function (err, product){
        if(err) {
            return res.json({msg: "Error Occured", productUpdated: false});
        } 

        //create addons or edit addons - search for an addonGroup with the same title, if none then edit the current addon
        if(thereAreAddons) {
            if(product.add_on_groups) {
                //test to see if the group Id id is valid
                if(addonsInfo.addonGroupId !== '' && addonsInfo.addonGroupId.length !== 24) {
                    return res.json({msg: "Invalid addon Id", productUpdated: false});
                }

                var idx = -1;
                //if we have a group Id then try to find it
                if(addonsInfo.addonGroupId !== '') {
                    idx = _.findIndex(product.add_on_groups, { '_id': mongoose.Types.ObjectId(addonsInfo.addonGroupId)}); //id for object id
                }
                var idx2 = _.findIndex(product.add_on_groups, { 'group_name': (addonsInfo.title).toLowerCase()}); //id for group name

                if(idx !== -1) { //this means we are to edit
                    //check to see if the addon group name is already being used
                    //if the group_name found has its id to be different from the id of the coming group name then return an error because you cannot 
                    //edit choosing a name that already exist if you are not the owner of that group name
                    if(idx2 !== -1 && !(product.add_on_groups[idx2]._id.equals(addonsInfo.addonGroupId))) {
                        return res.json({msg: "Addon Group name is already used", productUpdated: false});
                    } else {
                        var addons = [];
                        _.forEach(addonsInfo.addonsInfo, function(value) {
                            addons.push({name: value.name, price: value.price})
                        });

                        var addonGroup = {
                            group_name: (addonsInfo.title).toLowerCase(),
                            how_many_must_be_chosen: addonsInfo.amountUserMustChoose,
                            addons
                        }
                        product.add_on_groups[idx] = addonGroup;
                    }
                } else { //this means we are to create a new one
                    if(idx2 === -1) {
                        var addons = [];
                        _.forEach(addonsInfo.addonsInfo, function(value) {
                            addons.push({name: value.name, price: value.price})
                        });
        
                        var addonGroup = {
                            group_name: (addonsInfo.title).toLowerCase(),
                            how_many_must_be_chosen: addonsInfo.amountUserMustChoose,
                            addons
                        }
        
                        product.add_on_groups.push(addonGroup);
                    } else {
                        return res.json({msg: "Addon Group name is already used", productUpdated: false});
                    }
                }
            }
        }

        product.product_name = productName;
        product.service_category_id = serviceCategoryId;
        product.product_price = price;
        product.product_description = productDescription;
        product.product_number_available = amountLeft;
        product.uncountable_product_type = !productIsCountable;

        product.save()                                
        .then(product => {
            return res.json({msg: 'Product Updated', productUpdated: true, updatedProduct : product});
        });
    });
 });


  /**
  * specify that a non countable product is out of stock
  */
 router.post('/product_in_stock_status', auth, (req, res) => {
    const {productId, productIsOutOfStock } = req.body;

    if(!productId || (productIsOutOfStock === undefined)) {
        return res.json({msg: 'Please enter all fields!', productUpdated: false})
    } 

    Product.findOne({ _id: productId }, function (err, product){
        if(err) {
            return res.json({msg: "Error Occured", productUpdated: false});
        } 
        if(!product.uncountable_product_type) {
            return res.json({msg: "Invalid product Type", productUpdated: false});
        }

        product.uncountable_product_type_out_of_stock = productIsOutOfStock;
        product.save()                                
        .then(product => {
            return res.json({msg: 'Product Updated', productUpdated: true, updatedProduct : product});
        });
    });
 });
 
  /**
  * deactivate a product data
  */
 router.post('/deactivate_product', auth, (req, res) => {
    const { productId } = req.body;

    if(!productId) {
        return res.json({msg: 'Please enter all data', productDeactivated: false})
    } 

    Product.findOne({ _id: productId }, function (err, product){
        if(err) {
            return res.json({msg: "Error Occured", productDeactivated: false});
        } 
        product.is_active = !product.is_active;
        product.save()                                
        .then(product => {
            return res.json({msg: product.is_active ? 'Product Activated' : 'Product Deactivated', productDeactivated: true, deactivatedProduct : product});
        });
    });
 });


/**
 * Get all products for this vendor
 */
router.get('/get_all_products_for_this_vendor/:id', auth, (req, res) => {
    var vendorId = req.params.id;
    
    //get the service categories
    ServiceCategory.find({is_active: true})
    .select('-service_category_date_created -service_category_more_info -service_category_nice_color')
    .then(serviceCategories => {

        //get the products
        Product.find({product_vendor_id: vendorId})
        .then(products => {
            return res.send({products, serviceCategories});
        })
        .catch(res => {});
    })
    .catch(res => {});

});


/**
 * The vendor sends this when they have the order items
 */
router.get('/vendor_yes_we_have_order/:id/:time', auth, (req, res) => {
    var orderId = req.params.id;
    var time = req.params.time;
    
    //date data
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var orderDate = date+' '+time;
    
    //get the order and update the details
    Order.findOne({_id: orderId})
    .select('-drivers_total_money -order_price_total_with_tax -tip -order_name')
    .then(order => {
        if(!order) return res.status(400).json({msg: 'Invalid Order', SuccessInUpdate: false});

        order.vendor_approved = true;
        order.vendor_approved_at = orderDate;
        order.time_to_get_ready_milli_sec = time;
        order.save() 
        .then(order => {
            return  res.json({msg: 'updated', SuccessInUpdate: true, order});
        });
    })
    .catch(err => {})
});


/**
 * The vendor sends this when they have the order items
 */
router.get('/cancel_order/:id', auth, (req, res) => {
    var orderId = req.params.id;

    //get the order and update the details
    Order.findOne({_id: orderId})
    .then(order => {
        if(order) {
            order.cancelled = true;
            order.save() 
            .then(order => {
                return  res.json({msg: 'updated', SuccessInUpdate: true, order});
            });
        } else {
            return  res.json({msg: 'not updated', SuccessInUpdate: false});
        }
    })
    .catch(err => {})
});







/**
 * Vendors
 */
/**
 * This gets called when the vendor updates availability status
 */

router.post('/current_availability', auth, (req, res) => {
    var dayOfWeek = '';
    const {userId, availabilityStatus, date, vendorId} = req.body;
    var time = moment(date).format("HH:mm");

    switch(moment(date).weekday()) {
        case 0:
            dayOfWeek = 'sunday';
            break;
        case 1:
            dayOfWeek = 'monday';
            break;
        case 2:
            dayOfWeek = 'tuesday';
            break;
        case 3:
            dayOfWeek = 'wednesday';
            break;
        case 4:
            dayOfWeek = 'thursday';
            break;
        case 5:
            dayOfWeek = 'friday';
            break;
        case 6:
            dayOfWeek = 'sunday';
            break;
    }

    if(!userId && !availabilityStatus) {
        return res.json({msg: 'Please enter all fields!', successful: false})
    } 

    //get the vendor details
    //check the time frame and current time to know if the vendor should be closed by this time
    VendorScheduleSpecialty.findOne({vendor_id: vendorId, special_date: moment(date).format('L')})
    .select('-_id -vendor_id')
    .then(vendorScheduleSpecialty => {
        if(vendorScheduleSpecialty) {
            var openingTime = moment(vendorScheduleSpecialty.start_time, 'h:mma');
            var closingTime = moment(vendorScheduleSpecialty.end_time, 'h:mma');
            var currentTime = moment(time, 'h:mma');

            //if you are trying to open then we  need to be sure you are opening between your scheduled window
            if(currentTime.isSameOrAfter(openingTime) && currentTime.isSameOrBefore(closingTime)) {
                ProductVendor.findOne({user_id: userId})
                .select('')
                .then(productVendor => {
                    if(productVendor) {
                        productVendor.is_currently_active = availabilityStatus;

                        productVendor.save()
                        .then(() => {
                            return res.json({msg: 'status updated', successful: true});
                        });
                    } else {
                        return res.json({msg: 'Could not find vendor', successful: false});
                    }
                });
            } else {
                return res.json({msg: `Your schedule does not allow you to ${availabilityStatus ? 'open' : 'close'} at this time`, successful: false});
            }
        } else {
            VendorSchedule.findOne({vendor_id: vendorId})
            .select(`${dayOfWeek}_start_time ${dayOfWeek}_end_time -_id`)
            .then(vendorSchedule => {
                if(vendorSchedule) {
                    var openingTime = moment(vendorSchedule[`${dayOfWeek}_start_time`], 'h:mma');
                    var closingTime = moment(vendorSchedule[`${dayOfWeek}_end_time`], 'h:mma');
                    var currentTime = moment(time, 'h:mma');

                    //if you are trying to open then we  need to be sure you are opening between your scheduled window
                    if(currentTime.isSameOrAfter(openingTime) && currentTime.isSameOrBefore(closingTime)) {
                        ProductVendor.findOne({user_id: userId})
                        .select('')
                        .then(productVendor => {
                            if(productVendor) {
                                productVendor.is_currently_active = availabilityStatus;
        
                                productVendor.save()
                                .then(() => {
                                    return res.json({msg: 'status updated', successful: true});
                                });
                            } else {
                                return res.json({msg: 'Could not find vendor', successful: false});
                            }
                        });
                    } else {
                        return res.json({msg: `Your schedule does not allow you to ${availabilityStatus ? 'open' : 'close'} at this time`, successful: false});
                    }
                } else {
                    return res.json({msg: 'status not updated', successful: false})
                }
            });
        }
    });
});

/**
 * This gets called when the vendor updates availability status
 */
router.get('/get_current_availability/:id', auth, (req, res) => {
    var userId = req.params.id;

    if(!userId) {
        return res.json({msg: 'Please enter all fields!', gotten: false})
    } 

    ProductVendor.findOne({user_id: userId})
    .select('')
    .then(productVendor => {
        if(productVendor) {
            return res.json({msg: 'gotten', gotten: true, status: productVendor.is_currently_active})
        } else {
            return res.json({msg: 'not gotten', gotten: false})
        }
    });
});

/**
 * i need a driver
 */
router.post('/i_need_a_driver', auth, (req, res) => {
    const {userId, 
           productVendorId, 
           customerName, 
           customerAddress, 
           customerPhoneNumber, 
           totalPriceWithTax, 
           totalIncome, 
           paymentMethodToVendor, 
           paymentMethodFromClient,
           city,
           province } = req.body;

    if(!customerName || !productVendorId || !customerAddress || !customerPhoneNumber || !totalPriceWithTax || !totalIncome || !paymentMethodToVendor || !paymentMethodFromClient || !city || !province) {
        return res.json({msg: 'Please enter all fields!', orderCreated: false})
    } 


    Utility.findOne({city, province}, function(err, utility) {
        if (err) throw err;
        if (!utility) return res.json({msg:'Please enter all values', orderCreated: false});

        //date data
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var orderDate = date+' '+time;

        //get payment types
        var vendorWantsDirectTransfer = false;
        var vendorWantsCash = false;
        var clientWouldGiveCash = false;
        var clientWouldGiveCard = false;

        if(paymentMethodToVendor === 'directTransfer') {
            vendorWantsDirectTransfer = true;
        } else if(paymentMethodToVendor === 'cash') {
            vendorWantsCash = true;
        }

        if(paymentMethodFromClient === 'cash') {
            clientWouldGiveCash = true;
        } else if(paymentMethodFromClient === 'card') {
            clientWouldGiveCard = true;
        }

        var order = new Order({
            order_name : `Order for ${customerName}, Mobile Number ${customerPhoneNumber}`,
            order_date : orderDate,
            
            user_id : userId,
            user_name: customerName,

            order_price_total_with_tax : totalPriceWithTax,
            product_vendor_id : productVendorId,
            just_ordered: true,
            vendor_approved: true,

            time_to_get_ready_milli_sec: '',
            order_needs_driver: true,
            // vendor_has_given_driver_order: false,
            
            driver_has_started_order: false,
            driver_has_picked_up: false,
            
            delivered: false,
            tip: 0.0,
            drivers_total_money: utility.drivers_money,
            vendors_total_money: totalIncome,
            driver_id: '',
            address: customerAddress,
            order_num_for_view: orderNumber(),
            vendor_requested_order: true,
            vendor_wants_direct_transfer: vendorWantsDirectTransfer,
            vendor_wants_cash: vendorWantsCash,
            clients_would_give_cash: clientWouldGiveCash,
            clients_would_give_card: clientWouldGiveCard,

            vendor_approved_at: orderDate,
            driver_accepted_at: "",
            driver_has_started_order_at: "",
            driver_has_picked_up_at: "",
            delivered_at: ""
        });

        order.save().then(order => {
            return res.json({msg: 'Order Created', 
                             orderCreated: true, 
                             order:{
                                _id: order._id,
                                user_id: order.user_id,
                                address: order.address,
                                product_vendor_id: order.product_vendor_id
                             }})
        });
    });
});





/**
 * Vendor Ongoing Orders
 */
router.get('/get_ongoing_orders/:id', auth, (req, res) => {
    var vendorId = req.params.id;

    if(!vendorId) {
        return res.json({msg: 'Please enter all fields!', gotten: false})
    } 
    
    //get the orders -> if vendors have not said we have given the driver the order then the order needs to be returned
    Order.find({product_vendor_id: vendorId, vendor_approved: true, delivered: false, cancelled: false})
    .select('-drivers_total_money -driver_id -clients_would_give_card -clients_would_give_cash -delivered -driver_has_picked_up -driver_has_started_order -just_ordered -order_price_total_with_tax')
    .then(orders => {
        //get the order ids
        var idOfOrders = orders.map(function(order) { return order._id; });

        //get the OrderToProducts data that match the orders
        OrderToProducts.find({order_id: {$in: idOfOrders}})
        .select('')
        .then(orderItems => {
            //map add the vendor data to the order data
            var compilationOfOrderAndOrderItems = [];
            var itemsForAnOrder = [];

            //iterate through the orders
            for(var i=0; i<orders.length; i++) {

                //iterate through the orderitems
                for(var j=0; j<orderItems.length; j++) {

                    //if the order id matches the order id in the order items
                    if(orders[i]._id.equals(orderItems[j].order_id)) {
                        itemsForAnOrder.push(orderItems[j].toObject());

                        continue;
                    }
                }
                compilationOfOrderAndOrderItems.push(Object.assign({}, orders[i].toObject(), {cartItems : JSON.stringify(itemsForAnOrder)} ));
                itemsForAnOrder = [];
            }

            return res.json({orders:compilationOfOrderAndOrderItems.sort((a, b) => (a.order_date < b.order_date) ? 1 : -1),
                             msg: 'gotten orders', 
                             status: true});
        })
        .catch(err => {})

        // return res.send({orders: orders.sort((a, b) => (a.order_date < b.order_date) ? 1 : -1), 
        //                  msg: 'gotten orders', 
        //                  status: true});
    })
    .catch(err => {});
});



router.get('/get_amount_of_ongoing_orders/:id', auth, (req, res) => {
    var vendorId = req.params.id;

    if(!vendorId) {
        return res.json({msg: 'Please enter all fields!', gotten: false})
    } 
    
    //get the orders -> if vendors have not said we have given the driver the order then the order needs to be returned
    Order.find({product_vendor_id: vendorId, vendor_approved: true, delivered: false}) //Order.find({product_vendor_id: vendorId, vendor_approved: true, vendor_has_given_driver_order: false})
    .select('-user_id -drivers_total_money -driver_id -address -clients_would_give_card -clients_would_give_cash -delivered -driver_has_picked_up -driver_has_started_order -just_ordered -order_price_total_with_tax')
    .then(orders => {
        return res.json({amount: orders.length, gotten: true});
    });
});


router.get('/order_is_ready_from_vendor/:id/:id2', auth, (req, res) => {
    var vendorId = req.params.id;
    var orderId = req.params.id2;

    if(!vendorId || !orderId) {
        return res.json({msg: 'Please enter all fields!', updated: false})
    } 
    
    Order.findOne({product_vendor_id: vendorId, _id: orderId})
    .select('')
    .then(order => {
        if(order) {
            order.order_is_ready_from_vendor = true;
            order.save()                                
            .then(order => {
                return res.json({msg:'Updated', updated: true});
            });
        } else {
            return res.json({msg:'Could not find the order', updated: false});
        }
    });
});








router.get('/get_schedule_data/:id', auth, (req, res) => {
    var vendorId = req.params.id;

    if(!vendorId) {
        return res.json({msg: 'Please enter all fields!', gottenData: false})
    } 
    
    VendorSchedule.findOne({vendor_id: vendorId})
    .select('-_id -vendor_id')
    .then(vendorSchedule => {
        if(vendorSchedule) {
            return res.json({gottenData: true, schedule: vendorSchedule});
        } else {
            return res.json({msg:'Could not find the Schedule', gottenData: false});
        }
    });
});

/**
 * This is used to update the vendors schedule data
 */
router.post('/update_schedule_data', auth, (req, res) => {
    const {
        monday_start_time,
        monday_end_time,
        tuesday_start_time,
        tuesday_end_time,
        wednesday_start_time,
        wednesday_end_time,
        thursday_start_time,
        thursday_end_time,
        friday_start_time,
        friday_end_time,
        saturday_start_time,
        saturday_end_time,
        sunday_start_time,
        sunday_end_time,
        vendor_id
     } = req.body;

    if(!monday_start_time || 
       !monday_end_time || 
       !tuesday_start_time || 
       !tuesday_end_time || 
       !wednesday_start_time || 
       !wednesday_end_time || 
       !thursday_start_time || 
       !thursday_end_time || 
       !friday_start_time || 
       !friday_end_time|| 
       !saturday_start_time || 
       !saturday_end_time || 
       !sunday_start_time || 
       !sunday_end_time || 
       !vendor_id) {
        return res.json({msg: 'Please enter all fields!', updated: false})
    } 

    VendorSchedule.findOne({vendor_id})
    .select('')
    .then(vendorSchedule => {
        if(vendorSchedule) {
            vendorSchedule.monday_start_time = monday_start_time;
            vendorSchedule.monday_end_time = monday_end_time;
            vendorSchedule.tuesday_start_time = tuesday_start_time;
            vendorSchedule.tuesday_end_time = tuesday_end_time;
            vendorSchedule.wednesday_start_time = wednesday_start_time;
            vendorSchedule.wednesday_end_time = wednesday_end_time;
            vendorSchedule.thursday_start_time = thursday_start_time;
            vendorSchedule.thursday_end_time = thursday_end_time;
            vendorSchedule.friday_start_time = friday_start_time;
            vendorSchedule.friday_end_time = friday_end_time;
            vendorSchedule.saturday_start_time = saturday_start_time;
            vendorSchedule.saturday_end_time = saturday_end_time;
            vendorSchedule.sunday_start_time = sunday_start_time;
            vendorSchedule.sunday_end_time = sunday_end_time;

            vendorSchedule.save()                                
            .then(vendorSchedule => {
                return res.json({msg: 'Updated Succesful', updated: true});
            });
        } else {
            return res.json({msg:'Could not find the Schedule', updated: false});
        }
    });
});

/**
 * this is used to get a special dates ofo if any
 */
router.post('/get_special_date_info', auth, (req, res) => {
    const {
        vendorId,
        date,
        weekDay
    } = req.body;

    var startTimeNeeded = `${weekDay}_start_time`;
    var endTimeNeeded = `${weekDay}_end_time`;

    if(!vendorId || !date || !weekDay) {
        return res.json({msg: 'Please enter all fields!', foundData: false})
    } 


    VendorScheduleSpecialty.findOne({vendor_id: vendorId, special_date: date})
    .select('-_id -vendor_id')
    .then(vendorScheduleSpecialty => {
        if(vendorScheduleSpecialty) {
            return res.json({foundData: true,
                             data:{start_time: vendorScheduleSpecialty.start_time, 
                                   end_time: vendorScheduleSpecialty.end_time, 
                                   notes:vendorScheduleSpecialty.notes},
                             foundSpecialty: true});
        } else {
            VendorSchedule.findOne({vendor_id: vendorId})
            .select(`${startTimeNeeded} ${endTimeNeeded} -_id`)
            .then(vendorSchedule => {
                if(vendorSchedule) {
                    return res.json({foundData: true,
                                     data:{start_time: vendorSchedule[startTimeNeeded], end_time: vendorSchedule[endTimeNeeded]},
                                     foundSpecialty: false});
                } else {
                    return res.json({msg: 'Did not find schedule for specified Day', foundData: false})
                }
            });
        }
    });
});

/**
 * this is used to get a special dates ofo if any
 */
router.post('/set_special_date_info', auth, (req, res) => {
    const {
        vendorId,
        specialOpeningTime,
        specialClosingTime,
        note,
        date
    } = req.body;

    if(!vendorId || !specialOpeningTime || !specialClosingTime || !note || !date) {
        return res.json({msg: 'Please enter all fields!', setData: false})
    } 

    VendorScheduleSpecialty.findOne({vendor_id: vendorId, special_date: date})
    .select('')
    .then(vendorScheduleSpecialty => {
        if(vendorScheduleSpecialty) {
            vendorScheduleSpecialty.start_time = specialOpeningTime;
            vendorScheduleSpecialty.end_time = specialClosingTime;
            vendorScheduleSpecialty.notes = note;
            vendorScheduleSpecialty.save()                                
            .then(vendorScheduleSpecialtyResult => {
                if(vendorScheduleSpecialtyResult) {
                    return res.json({msg: 'Updated Succesful', setData: true});
                } else {
                    return res.json({msg: 'Could Not Update Specialty', setData: false});
                }
            });
        } else {
            var newSpecialty = new VendorScheduleSpecialty({
                vendor_id: vendorId,
                special_date: date,
                start_time : specialOpeningTime,
                end_time : specialClosingTime,
                notes : note
            });
            newSpecialty.save()                                
            .then(newSpecialtyResult => {
                if(newSpecialtyResult) {
                    return res.json({msg: 'Succesfully Added Specialty', setData: true});
                } else {
                    return res.json({msg: 'Could Not Added Specialty', setData: false});
                }
            });
        }
    });
});



router.get('/does_order_need_driver/:id/:id2', auth, (req, res) => {
    var orderId = req.params.id;
    var vendorId = req.params.id2;

    if(!orderId || !vendorId ) {
        return res.json({msg: 'Please enter all fields!', error: true})
    }

    
    Order.findOne({product_vendor_id: vendorId, _id: orderId})
    .select('')
    .then(order => {
        if(order) {
            //if this is true then we dont need to send for a driver, obe has already been sent for
            if(order.order_needs_driver) { 
                return res.json({error:false, send_for_driver: false});
            } else {
                order.order_needs_driver = true;
                order.save()                                
                .then(order => {
                    return res.json({error:false, send_for_driver: true});
                });
            }
        } else {
            return res.json({msg:'Could not find the order', error: true});
        }
    });
});

module.exports = router;