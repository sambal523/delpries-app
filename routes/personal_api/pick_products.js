
/**
 * Date Created: 23th September 2019.
 * This file would hold all the routes related to the PickProducts Compoment
 */

//system defined imports
const config = require('config');
const express = require('express');
const router = express.Router();
const _ = require('lodash');

//custom imports
const ProductVendorMoreInfo = require('../../models/product_vendor_more_info.js');
const Product = require('../../models/product');
const ProductVendor = require('../../models/product_vendors');

//get all products for the product vendor
router.get('/get_all_prod_for_this_vendor_for/:id', (req, res) => {
    var productVendorId = req.params.id;
    
    //get all the products that belongs to the desired service category
    Product.find({product_vendor_id: productVendorId, is_active: true}, function(err, products) {
        if (err) throw err;

        // docs contains your answer
        return res.json(products);
    });
});

//get product vendor more info and info, join them together
router.get('/get_product_vendor_info_and_more_info/:id', (req, res) => {
    var productVendorId = req.params.id;
    
    //get all the products that belongs to the desired service category
    ProductVendor.findOne({_id: productVendorId}, function(err, productVendor) {
        if (err) throw err;

        // Get the product vendors that match
        ProductVendorMoreInfo.findOne({product_vendor_id : productVendorId}, function(err, productVendorMoreInfo) {
            if (err) throw err;

            // docs contains your answer
            return res.json({productVendor, productVendorMoreInfo});
        });
    });
});


/**
 * add manyp
 */
/*
router.post('/add_many_product_vendor_more_info', (req, res) => {
    //get the objects
    allProductVendorMoreInfo = req.body.all_product_vendor_more_info_data;

    //add them all
    ProductVendorMoreInfo.create(allProductVendorMoreInfo, function (err, productVendorsMoreInfos) {
        if (err) {
            res.send(err);
        } else {
            res.send(`finished adding all`);
        }
    });
});
*/

/**
 * delete all product
 */
/*
router.delete('/add_many_product_vendor_more_info', (req, res) => {
    ProductVendorMoreInfo.deleteMany(function(err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(`finished deleting all, deleted ${data.deletedCount}`);
        }
    });
});
*/


module.exports = router;