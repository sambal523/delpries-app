
/**
 * Date Created: 3rd September 2019.
 * This file would hold all the routes related to the Home Component
 */

//system defined imports
const config = require('config');
const express = require('express');
const router = express.Router();
const _ = require('lodash');

//custom imports
const ServiceCategory = require('../../models/services_category');
const Utility = require('../../models/utility');
const Users = require('../../models/user');

/**
 * This is used to add a service_category to the databse
 */
/*
router.post('/add_a_serv_cat', (req, res) => {
    ServiceCategory.save().then(ServiceCategory => res.json("single item added"));
});
*/

/**
 * This is used to add multiple service_category to the databse
 */
/*
router.post('/add_many_serv_cat', (req, res) => {
    //get the objects
    allServiceCategories = req.body.all_the_service_categories;

    //add them all
    ServiceCategory.create(allServiceCategories, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.send(`finished adding all`);
        }
    });
});
*/

/**
 * This is used to delete all
 */
/*
router.delete('/all_serv_cat', (req, res) => {
    ServiceCategory.deleteMany(function(err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(`finished deleting all, deleted ${data.deletedCount}`);
        }
    });
})
*/


/**
 * This is used to get all -- called in the front end
 */
router.get('/all_serv_cat', (req, res) => {
    ServiceCategory.find({is_active: true})
    .select('-is_active')
    .then(serviceCategories => res.json(serviceCategories));
});


/**
 * Add utility for London Ontario
 */
/*
router.post('/add_utility', (req, res) => {
    //add them all
    utility.create({city:'London', 
                    province:'Ontario', 
                    delivery_fee_for_client: 4.00, 
                    drivers_money: 6.50, 
                    service_charge_on_vendors_percentage : 0.15, 
                    delivery_fee_on_stores: 3.00,
                    tax: 0.13 }, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.send(`finished adding all`);
        }
    });
});
*/

/**
 * Get the utility details for london  ontario
 */
router.get('/get_utility/:city/:province/:userId', (req, res) => {
    var city = req.params.city;
    var province = req.params.province;
    var userId = req.params.userId;

    if(userId === 'undefined') {
        Utility.findOne({city, province})
        .select('-drivers_money -service_charge_on_vendors_percentage -delivery_fee_on_stores -percent_delpries_take_for_tip -percent_drivers_take_for_tip -i_need_driver_delivery_fee')
        .then(utility => { 
            return res.json(utility)
        });
    } else {
        //get the user type
        Users.findOne({_id: userId})
        .then(user => {
            if(!user) return res,json('invalid user');

            //if the user is a vendor then get the vendor specific info else get the generic one
            if(user.can_access_vendors_page) {
                Utility.findOne({city, province})
                .select('-drivers_money -service_charge_on_vendors_percentage -delivery_fee_on_stores -percent_delpries_take_for_tip -percent_drivers_take_for_tip')
                .then(utility => res.json(utility));
            } else {
                Utility.findOne({city, province})
                .select('-drivers_money -service_charge_on_vendors_percentage -delivery_fee_on_stores -percent_delpries_take_for_tip -percent_drivers_take_for_tip -i_need_driver_delivery_fee')
                .then(utility => res.json(utility));
            }
        });
    }
});

module.exports = router;