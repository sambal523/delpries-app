
/**
 * Date Created: 12th October 2019.
 * This file would hold all the routes related to the PickProducts Compoment
 */

//system defined imports
const config = require('config');
const express = require('express');
const router = express.Router();
const _ = require('lodash');

//custom imports
const Order = require('../../models/order');
const Product = require('../../models/product');
const OrderToProducts = require('../../models/order_to_products.js');
const ProductVendor = require('../../models/product_vendors');
const ProductVendorMoreInfo = require('../../models/product_vendor_more_info.js');
const Users = require('../../models/user');
const auth = require('../../middleware/auth');

/**
 * Get all the order data for a user
 */
router.get('/get_all_order_data_for_user/:id', auth, (req, res) => {
    var userId = req.params.id;

    if(!userId) {
        return res.json({msg: 'Please enter all fields!', gotten: false})
    } 


    //get the orders
    Order.find({user_id: userId})
    .select('-drivers_total_money -is_active -vendors_total_money -driver_id')
    .then(orders => {
        // Map the docs into an array of just the product vendor ids
        var productVendorIdOfOrders = orders.map(function(order) { return order.product_vendor_id; });

        // Get the product vendors that match
        ProductVendor.find({_id: {$in: productVendorIdOfOrders}})
        .select('-is_active -product_vendor_id')
        .then(productVendors => {
            //map add the vendor data to the order data
            var compilationOfOrderAndVendor = [];

            //iterate through the orders
            for(var i=0; i<orders.length; i++) {
                //iterate through the product vendors
                for(var j=0; j<productVendors.length; j++) {
                    //if this order matches the product vendor id then we want to join the product vendor data to that order data
                    if(productVendors[j]._id.equals(orders[i].product_vendor_id)) {
                        compilationOfOrderAndVendor.push(Object.assign({}, productVendors[j].toObject(), orders[i].toObject()));
                        continue;
                    }
                }
            }
            
            return res.json(compilationOfOrderAndVendor.sort((a, b) => (a.order_date < b.order_date) ? 1 : -1));
        })
        .catch(err => {})
    })
    .catch(err => {})
});


/**
 * get more info for an order
 */
 router.get('/get_more_info_on_an_order/:id', auth, (req, res) => {
    var orderId = req.params.id;

    if(!orderId) {
        return res.json({msg: 'Please enter all fields!', productFound: false})
    } 

    //get the order details and the the order to Product details and then merge them and send them to the front end
    Order.findOne({_id: orderId})
    .select('-drivers_total_money -vendors_total_money')
    .then(order => {

        OrderToProducts.find({order_id: orderId})
        .select('')
        .then(orderToProducts => {
            ProductVendorMoreInfo.findOne({product_vendor_id: order.product_vendor_id})
            .select('')
            .then(productVendorMoreInfo => {
                var vendorLocation = productVendorMoreInfo.product_vendor_address;

                order.product_vendor_id = '';

                //if there is a driver assigned to this order then get the drivers details
                if(order.driver_id !== '' && order.delivered === false) {
                    Users.findOne({_id: order.driver_id}) 
                    .select('first_name last_name')
                    .then(driver => {
                        order.driver_id = '';
                        return res.json({order, orderToProducts, vendorLocation, driver : {first_name: driver.first_name, last_name: driver.last_name} });
                    })
                } else {
                    order.driver_id = '';
                    return res.json({order, orderToProducts, vendorLocation });
                }
            });
        })
        .catch(err => {})
    })
    .catch(err => {})
 });

module.exports = router;