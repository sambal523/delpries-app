
/**
 * Date Created: 23th September 2019.
 * This file would hold all the routes related to the PickProducts Compoment
 */

//system defined imports
const config = require('config');
const express = require('express');
const router = express.Router();
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const multer = require('multer');

//custom imports
const UserType = require('../../models/user_type');
const User = require('../../models/user');
const ProductVendor = require('../../models/product_vendors');
const ProductVendorMoreInfo = require('../../models/product_vendor_more_info');
const DriverMoreInfo = require('../../models/driver_more_info');
const VendorSchedule = require('../../models/vendor_schedules');

const storage = multer.diskStorage({
    destination : function(req, file, cb) {
        cb(null, 'client/src/images/vendor_images/');
    },

    filename: function(req, file, cb) {
        cb(null, file.originalname)
    }
});

const upload = multer({
    storage: storage,
    limits : {
        fileSize : 1024 * 1024 * 5
    }
});


randomString = (length, chars) => {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}


/**
 * add a driver 
 */
router.post('/add_driver', (req, res) => {
    const {firstName, lastName, email, dob, age, driversLicenseNumber, phoneNo, password } = req.body;
    const userType = 'driver';

    if(!firstName || !lastName || !email || !dob || !age || !driversLicenseNumber || !phoneNo || !password) {
        return res.json({msg: 'Please enter all fields!', accountCreated: false})
    } 

    //check for existing user
    User.findOne({user_email: email})
    .then(user => {
        if(user) return res.json({msg: 'Email is already being used', accountCreated: false});
        if(email === config.get('companyEmail')) return res.json({msg: 'Using Company Email would not Work', accountCreated: false});


        UserType.findOne({user_type_name: userType})
        .then(typeFound => {
            if(!typeFound) return res.status(400).json({msg: 'The type specified does not exist', accountCreated: false});

            //we need a salt to create a hash so we can encrypt the password
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(password, salt, (err, hash) => {
                    if(err) throw err;
                    var today = new Date();
                    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                    var todaysDate = date+' '+time;
                    var verificationCode = randomString(8, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');

                    const newUser = new User({
                        user_name: firstName + ' '+lastName,
                        first_name : firstName,
                        last_name: lastName,
                        date_joined: todaysDate,
                        password: hash,
                        user_type_id : typeFound._id,
                        user_email: email,
                        is_account_locked: 1,
                        verification_code: verificationCode,
                        can_access_order_details_page: false,
                        can_access_vendors_page: false,
                        can_access_drivers_page: true,
                        can_make_an_order: false,
                        mobile_no: phoneNo,
                        deactivated: false
                    });

                    //send the email of the verification code to the user
                    nodemailer.createTestAccount((err, account) => {
                        const htmlEmail = `
                            <div style="width: 500px;font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                                <div style="background-color: #f2f2f2; height: 100px" >
                                    <div style="font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; padding:28px 15px;">
                                        <b style="color:#4db57c;font-size: 35px; ">DELP</b><b style="color:#F7C544;font-size: 35px;">RIES</b>
                                        <div style="font-size: 14px;">delpries.ca</div>
                                    </div>
                                </div>
                    
                                <div style="background-color:#4db57c; height:350px; padding:50px 15px; color:#ffffff; position: relative;">
                                    <div style="font-size:25px;">Hello ${firstName + ' '+lastName},</div>
                    
                                    <div style='margin-top:50px;'>Thank you for creating an account with Delpries as a Driver. Please verify your account and after that we will contact you for your Orientation</div>
                    
                                    <div style='margin-top:10px;'>Your Verification code is ${verificationCode}.</div>
                    
                                    <div style='font-family: cursive; position: absolute; bottom: 15px; right: 20px; font-size: 20px;'>Thank You!</div>
                                </div>
                    
                                <div style="background-color:#F7C544; height:100px;">
                                    <div style="font-size:20px; text-align: center; padding-top:40px; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; color: #ffffff;">Thank you for Creating An Account</div>
                                </div>
                            </div>
                        `;
                
                        let transporter = nodemailer.createTransport({
                            host: 'smtp.gmail.com',
                            port: 587,
                            secure: false, // true for 465, false for other ports
                            auth: {
                                user: config.get('companyEmail'), // generated ethereal user
                                pass: config.get('companyPassword') // generated ethereal password
                            }
                        });
                
                        let mailOptions = {
                            from: config.get('companyEmail'), // sender address
                            to: email, // list of receivers
                            subject: 'Delpries Verification Code', // Subject line
                            html: htmlEmail // html body
                        };
                
                        transporter.sendMail(mailOptions, (err, info) => {
                            if(err) {
                                return res.json({msg: "Email Error: We couldn't send you the verification code", grantedAccess: false});
                            } else {
                                //add the user to the database if email was sent
                                newUser.save()
                                .then(user => {
                                    //make the drivers more info aspect
                                    const newDriverMoreInfo = new DriverMoreInfo({
                                        user_id: user._id,
                                        drivers_license_number : driversLicenseNumber,
                                        on_an_order: false,
                                        is_currently_active: false
                                    });

                                    newDriverMoreInfo.save()
                                    .then(driverMoreInfo => {
                                        return res.json({msg: 'Successful Sign Up, Please check email for Verification Code', grantedAccess: true});
                                    })
                                });
                            }
                        });
                    }); //end of nodemailer
                });
            });
        });
    });
});



/**
 * add a user 
 */
router.post('/add_user', (req, res) => {
    const {firstName, lastName, userName, email, mobileNo, password, userType } = req.body;

    if(!firstName || !lastName || !userName || !email || !mobileNo || !password || !userType) {
        return res.json({msg: 'Please enter all fields!', accountCreated: false})
    } 

    //check for existing user
    User.findOne({user_email: email})
    .then(user => {
        if(user) return res.json({msg: 'Email is already being used', accountCreated: false});
        if(email === config.get('companyEmail')) return res.json({msg: 'Using Company Email would not Work', accountCreated: false});

        UserType.findOne({user_type_name: userType})
        .then(typeFound => {
            if(!typeFound) return res.status(400).json({msg: 'The type specified does not exist', accountCreated: false});

            //we need a salt to create a hash so we can encrypt the password
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(password, salt, (err, hash) => {
                    if(err) throw err;
                    var today = new Date();
                    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                    var todaysDate = date+' '+time;
                    var verificationCode = randomString(8, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        
                    const newUser = new User({
                        user_name: userName,
                        first_name : firstName,
                        last_name: lastName,
                        date_joined: todaysDate,
                        password: hash,
                        user_type_id : typeFound._id,
                        user_email: email,
                        mobile_no: mobileNo,
                        is_account_locked: 1,
                        verification_code: verificationCode,
                        can_access_order_details_page: true,
                        can_access_vendors_page: false,
                        can_access_drivers_page: false,
                        can_make_an_order: true,
                        deactivated: false
                    });

                    //send the email of the verification code to the user
                    nodemailer.createTestAccount((err, account) => {
                        const htmlEmail = `
                            <div style="width: 500px;font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                                <div style="background-color: #f2f2f2; height: 100px" >
                                    <div style="font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; padding:28px 15px;">
                                        <b style="color:#4db57c;font-size: 35px; ">DELP</b><b style="color:#F7C544;font-size: 35px;">RIES</b>
                                        <div style="font-size: 14px;">delpries.ca</div>
                                    </div>
                                </div>
                    
                                <div style="background-color:#4db57c; height:350px; padding:50px 15px; color:#ffffff; position: relative;">
                                    <div style="font-size:25px;">Hello ${userName},</div>
                    
                                    <div style='margin-top:50px;'>Thank you for creating an account with Delpries.</div>
                    
                                    <div style='margin-top:10px;'>Your Verification code is ${verificationCode}.</div>
                    
                                    <div style='font-family: cursive; position: absolute; bottom: 15px; right: 20px; font-size: 20px;'>Thank You!</div>
                                </div>
                    
                                <div style="background-color:#F7C544; height:100px;">
                                    <div style="font-size:20px; text-align: center; padding-top:40px; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; color: #ffffff;">Thank you for Creating An Account</div>
                                </div>
                            </div>
                        `;
                
                        let transporter = nodemailer.createTransport({
                            host: 'smtp.gmail.com',
                            port: 587,
                            secure: false, // true for 465, false for other ports
                            auth: {
                                user: config.get('companyEmail'), // generated ethereal user
                                pass: config.get('companyPassword') // generated ethereal password
                            }
                        });
                
                        let mailOptions = {
                            from: config.get('companyEmail'), // sender address
                            to: email, // list of receivers
                            subject: 'Delpries Verification Code', // Subject line
                            html: htmlEmail // html body
                        };
                
                        transporter.sendMail(mailOptions, (err, info) => {
                            if(err) {
                                return res.json({msg: "Email Error: We couldn't send you the verification code", grantedAccess: false});
                            } else {
                                //add the user to the database if email was sent
                                newUser.save()
                                .then(user => {
                                    return res.json({msg: 'Successful Sign Up, Please check email for Verification Code', grantedAccess: true});
                                });
                            }
                        });
                    })
                })
            })
        })
        
    })

});

/**
 * This function is used to send email  -- its not being used for now because its async but we need to call it in sync
 */
sendEmail = (messageToBeSent, recipientsEmail) => {
    //send the email of the verification code to the user
    nodemailer.createTestAccount((err, account) => {  
        let transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: config.get('companyEmail'), // generated ethereal user
                pass: config.get('companyPassword') // generated ethereal password
            }
        });

        let mailOptions = {
            from: config.get('companyEmail'), // sender address
            to: recipientsEmail, // list of receivers
            subject: 'Delpries Verification Code', // Subject line
            html: messageToBeSent // html body
        };

        transporter.sendMail(mailOptions, (err, info) => {
            if(err) {
                return false;
            } else {
                return true;
            }
        });
    });
}

/**
 * this is to check if the email exists
 */
router.post('/validate_email', (req, res) => {
    const { accountManagerEmail } = req.body;

    //check for existing user
    User.findOne({user_email: accountManagerEmail})
    .then(user => {
        if(user) 
            return res.json({msg: 'Email is already being used', accountExists: true});
        else 
            return res.json({msg: 'Email is valid', accountExists: false});
    });
})


/**
 * add a vendor
 */
router.post('/add_vendor', upload.array('companyImages'), (req, res) => {
    //the email has already been validated if this runs so no need to validate the user
    var storeName = req.body.storeName;
    var storeAddress = req.body.storeAddress;
    var postalCode = req.body.postalCode;
    var province = req.body.province;
    var city = req.body.city;
    var accountManagerName = req.body.accountManagerName;
    var accountManagerFirstName = req.body.accountManagerFirstName;
    var accountManagerLastName = req.body.accountManagerLastName;
    var accountManagerEmail = req.body.accountManagerEmail;
    var companyIconImgName = req.body.companyIconImgName;
    var companyBgImageName = req.body.companyBgImageName;
    var contactNo = req.body.contactNo;
    var textDesciption = req.body.textDesciption;
    var password = req.body.password;
    var reTypePassword = req.body.reTypePassword;
    var adminPassword = req.body.adminPassword;
    var userType = req.body.userType;


    //check if the specified user type exists
    UserType.findOne({user_type_name: userType})
    .then(typeFound => {
        if(!typeFound) return res.status(400).json({msg: 'The type specified does not exist', accountCreated: false});

        //we need a salt to create a hash so we can encrypt the password
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(password, salt, (err, hash) => {
                if(err) throw err;

                bcrypt.hash(adminPassword, salt, (err2, hash2) => {
                    if(err2) throw err2;

                    var today = new Date();
                    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                    var todaysDate = date+' '+time;
                    var verificationCode = randomString(8, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');

                    //add the user id
                    const newVendor = new ProductVendor({
                        product_vendor_name: storeName,
                        product_vendor_image_name: companyBgImageName,
                        product_vendor_icon_name: companyIconImgName,
                        product_vendor_ratings: 0,
                        is_active: 0,
                        is_currently_active: false,
                        admin_password: hash2
                    });

                    //add the vendor id later
                    const newVendorMoreInfo = new ProductVendorMoreInfo({
                        product_vendor_postal_code: postalCode,
                        product_vendor_address: storeAddress,
                        product_vendor_province: province,
                        product_vendor_city: city,
                        product_vendor_store: 'custom data can be inserted here',
                        product_vendor_phone_number: contactNo,
                        product_vendor_text_description: textDesciption
                    });

                    const newUser = new User({
                        user_name: accountManagerName,
                        first_name : accountManagerFirstName,
                        last_name: accountManagerLastName,
                        date_joined: todaysDate,
                        password: hash,
                        user_type_id : typeFound._id,
                        user_email: accountManagerEmail,
                        is_account_locked: 1,
                        verification_code: verificationCode,
                        can_access_order_details_page: false,
                        can_access_vendors_page: true,
                        can_access_drivers_page: false,
                        can_make_an_order: false,
                        mobile_no: contactNo,
                        deactivated: false
                    });

                    //we use military time
                    const vendorSchedule = new VendorSchedule ({
                        monday_start_time: '11:00',
                        monday_end_time: '6:00',
                        tuesday_start_time: '11:00',
                        tuesday_end_time: '6:00',
                        wednesday_start_time: '11:00',
                        wednesday_end_time: '6:00',
                        thursday_start_time: '11:00',
                        thursday_end_time: '6:00',
                        friday_start_time: '11:00',
                        friday_end_time: '6:00',
                        saturday_start_time: '11:00',
                        saturday_end_time: '6:00',
                        sunday_start_time: '11:00',
                        sunday_end_time: '6:00',
                    })
                    //send the email of the verification code to the user
                    nodemailer.createTestAccount((err, account) => {
                        const htmlEmail = `
                            <div style="width: 500px;font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                                <div style="background-color: #f2f2f2; height: 100px" >
                                    <div style="font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; padding:28px 15px;">
                                        <b style="color:#4db57c;font-size: 35px; ">DELP</b><b style="color:#F7C544;font-size: 35px;">RIES</b>
                                        <div style="font-size: 14px;">delpries.ca</div>
                                    </div>
                                </div>
                    
                                <div style="background-color:#4db57c; height:350px; padding:50px 15px; color:#ffffff; position: relative;">
                                    <div style="font-size:25px;">Hello ${accountManagerName} from ${storeName},</div>
                    
                                    <div style='margin-top:50px;'>Thank you for creating an account with Delpries.</div>
                    
                                    <div style='margin-top:10px;'>Your Verification code is ${verificationCode}.</div>
                    
                                    <div style='font-family: cursive; position: absolute; bottom: 15px; right: 20px; font-size: 20px;'>Thank You!</div>
                                </div>
                    
                                <div style="background-color:#F7C544; height:100px;">
                                    <div style="font-size:20px; text-align: center; padding-top:40px; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; color: #ffffff;">Thank you for Creating An Account</div>
                                </div>
                            </div>
                        `;
                
                        let transporter = nodemailer.createTransport({
                            host: 'smtp.gmail.com',
                            port: 587,
                            secure: false, // true for 465, false for other ports
                            auth: {
                                user: config.get('companyEmail'), // generated ethereal user
                                pass: config.get('companyPassword') // generated ethereal password
                            }
                        });
                
                        let mailOptions = {
                            from: config.get('companyEmail'), // sender address
                            to: accountManagerEmail, // list of receivers
                            subject: 'Delpries Verification Code', // Subject line
                            html: htmlEmail // html body
                        };
                
                        transporter.sendMail(mailOptions, (err, info) => {
                            if(err) {
                                return res.json({msg: "Email Error: We couldn't send you the verification code", accountHasBeenSetUp: false});
                            } else {
                                //add the user to the database if email was sent
                                newUser.save()
                                .then(user => {
                                    newVendor.user_id = user._id;

                                    if (err) {
                                        return res.json({msg: "Error Occured", accountHasBeenSetUp: false});
                                    } else {

                                        //we can save the vendor(store) if the user was saved
                                        newVendor.save()
                                        .then(vendor => {
                                            newVendorMoreInfo.product_vendor_id = vendor._id;

                                            if (err) {
                                                return res.json({msg: "Error Occured", accountHasBeenSetUp: false});
                                            } else {

                                                //we can save the vendorMoreInfo if the vendor was saved
                                                newVendorMoreInfo.save()
                                                .then(vendorMoreInfo => {

                                                    //save the scheduling details
                                                    vendorSchedule.vendor_id = vendor._id;
                                                    vendorSchedule.save()
                                                    .then(vendorScheduleInfo => {
                                                        if (err) {
                                                            return res.json({msg: "Error Occured", accountHasBeenSetUp: false});
                                                        } else {
                                                            return res.json({msg: "You account has been setup, please check email for Verification Code", accountHasBeenSetUp: true});
                                                        }
                                                    });
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    })
                });
            }); //inner bcrypt.hash
        });
    });
});


/**
 * this is to validate a user with a validation code
 */
router.post('/validate_user', (req, res) => {
    const {email, verificationCode} = req.body;

    if(!email || !verificationCode) {
        return res.json({msg: 'Please enter all fields!', accountValidated: false})
    } 

    //check for existing user
    User.findOne({user_email: email})
    .then(user => {
        if(!user) return res.json({msg: 'User does not exist', accountValidated: false});

        if(user.is_account_locked) {
            if(user.verification_code === verificationCode) {
                //modify the data
                user.verification_code = '';
                user.is_account_locked = false;
    
                //update the user
                user.save()
                .then(user => {
                    return res.json({msg: 'Account Verified', accountValidated: true});
                })
            } else {
                return res.json({msg: 'Wrong Verification Code', accountValidated: false});
            }
        } else {
            res.json({msg: 'Account has already been Verified', accountValidated: true});
        }

    })
});

/**
 * this is to ask for a new verification code
 */
router.post('/request_new_code', (req, res) => {
    const {email} = req.body;

    if(!email) {
        return res.json({msg: 'Please enter all fields!', successfullySent: false})
    } 

    //check for existing user
    User.findOne({user_email: email})
    .then(user => {
        if(!user) return res.json({msg: 'User does not exist', successfullySent: false});

        if(user.is_account_locked) {
            //send the verification code
            var verificationCode = randomString(8, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');

            //send the email of the verification code to the user
            nodemailer.createTestAccount((err, account) => {
                const htmlEmail = `
                    <div style="width: 500px;font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                        <div style="background-color: #f2f2f2; height: 100px" >
                            <div style="font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; padding:28px 15px;">
                                <b style="color:#4db57c;font-size: 35px; ">DELP</b><b style="color:#F7C544;font-size: 35px;">RIES</b>
                                <div style="font-size: 14px;">delpries.ca</div>
                            </div>
                        </div>
            
                        <div style="background-color:#4db57c; height:350px; padding:50px 15px; color:#ffffff; position: relative;">
                            <div style="font-size:25px;">Hello ${user.first_name + ' '+user.last_name},</div>
            
                            <div style='margin-top:50px;'>You requested a new verification code. If you did not request this, disregard the email </div>
            
                            <div style='margin-top:10px;'>Your Verification code is ${verificationCode}.</div>
            
                            <div style='font-family: cursive; position: absolute; bottom: 15px; right: 20px; font-size: 20px;'>Thank You!</div>
                        </div>
            
                        <div style="background-color:#F7C544; height:100px;">
                            <div style="font-size:20px; text-align: center; padding-top:40px; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; color: #ffffff;">Thank you using Delpries</div>
                        </div>
                    </div>
                `;

                let transporter = nodemailer.createTransport({
                    host: 'smtp.gmail.com',
                    port: 587,
                    secure: false, // true for 465, false for other ports
                    auth: {
                        user: config.get('companyEmail'), // generated ethereal user
                        pass: config.get('companyPassword') // generated ethereal password
                    }
                });
        
                let mailOptions = {
                    from: config.get('companyEmail'), // sender address
                    to: email, // list of receivers
                    subject: 'Delpries Verification Code', // Subject line
                    html: htmlEmail // html body
                };
        
                transporter.sendMail(mailOptions, (err, info) => {
                    if(err) {
                        return res.json({msg: "Email Error: We couldn't send you the verification code", successfullySent: false});
                    } else {
                        //add the user to the database if email was sent
                        user.verification_code = verificationCode;
                        user.save()
                        .then(() => {
                            return res.json({msg: "Verification Code was Sent", successfullySent: true});
                        });
                    }
                });
            }); //end of nodemailer
        } else {
            res.json({msg: 'Account has already been Verified', accountValidated: true});
        }

    })
});

/**
 * add all user types
 */
router.post('/add_user_types', (req, res) => {
    var userTypes = req.body;

    UserType.create(userTypes, function (err) {
        if (err) {
            return res.send(err);
        } else {
            return res.send(`finished adding all`);
        }
    });
});




module.exports = router;