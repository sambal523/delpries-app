
/**
 * Date Created: 19th September 2019.
 * This file would hold all the routes related to the PickItems Compoment
 */

//system defined imports
const config = require('config');
const express = require('express');
const router = express.Router();
const _ = require('lodash');

//custom imports
const ProductVendor = require('../../models/product_vendors');
const Product = require('../../models/product');

/**
 * Get all product vendors that have products of that service category --called in front end
 * for the pagination feature :/get_prod_vendors/:id/:incVal/:srchCount
 */
router.get('/get_prod_vendors/:id', (req, res) => {
    var idOfServceCategory = req.params.id;
    // var incrementVal = req.params.incVal; //this represents the amount of data that should be gotten per set
    // var searchCount = req.params.srchCount; //thins represents the range to wheich the data should be gotten from
    
    //get all the products that belongs to the desired service category
    Product.find({service_category_id: idOfServceCategory}, function(err, products) {
        if (err) throw err;

        // Map the docs into an array of just the product vendor ids
        var productVendorIdOfTheProducts = products.map(function(product) { return product.product_vendor_id; });
    
        // Get the product vendors that match 
        ProductVendor.find({_id: {$in: productVendorIdOfTheProducts}, is_active: true})
        .select('-user_id -is_active -admin_password')
        .then(productVendors => {
            // var startVal = searchCount === 0 ? 
            //                     1 
            //                 :   
            //                     (searchCount * incrementVal) + 1;
            // var endVal = ((++searchCount) * incrementVal);

            // res.json({productVendors: _.slice(productVendors, startVal-1, endVal), lastSet:false});

            res.json({productVendors});
        })
        .catch(err)
    });
});

//add many product vendors
/*
router.post('/add_many_product_vendors', (req, res) => {
    //get the objects
    allProductVendors = req.body.all_product_vendors;

    //add them all
    var count = 0;
    ProductVendor.create(allProductVendors, function (err, productVendors) {
        if (err) {
            res.send(err);
        } else {
            res.send(`finished adding all`);
        }
    });
});
*/

/**
 * add many products
 */
/*
router.post('/add_many_products', (req, res) => {
    //get the objects
    allProducts = req.body.all_products;

    //add them all
    var count = 0;
    Product.create(allProducts, function (err, products) {
        if (err) {
            res.send(err);
        } else {
            res.send(`finished adding all`);
        }
    });
});
*/

/**
 * delete all product
 */
/*
router.delete('/all_prods', (req, res) => {
    Product.deleteMany(function(err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(`finished deleting all, deleted ${data.deletedCount}`);
        }
    });
});
*/


/**
 * delete all product vendors
 */
/*
router.delete('/all_prod_vendors', (req, res) => {
    ProductVendor.deleteMany(function(err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(`finished deleting all, deleted ${data.deletedCount}`);
        }
    });
});
*/


module.exports = router;