
/**
 * Date Created: 23th September 2019.
 * This file would hold all the routes related to the PickProducts Compoment
 */

//system defined imports
const config = require('config');
const express = require('express');
const router = express.Router();
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const auth = require('../../middleware/auth');
const nodemailer = require('nodemailer');

//custom imports
const User = require('../../models/user');
const UserType = require('../../models/user_type');
const ProductVendor = require('../../models/product_vendors');
const ProductVendorMoreInfo = require('../../models/product_vendor_more_info');
const DriverMoreInfo = require('../../models/driver_more_info');

/**
 * Sign In the user
 */
router.post('/sign_in', (req, res) => {
    const {email, password} = req.body;

    if(!email || !password) {
        return res.json({msg: 'Please enter all fields!', canLogIn: false})
    } 

    //check for existing user
    User.findOne({user_email: email})
    .then(user => {
        if(!user) return res.status(400).json({msg: 'Email does not exixst', canLogIn: false});
        
        if(user.is_account_locked === true ) return res.status(400).json({msg: 'Go to the Sign Up Page and Click Verify Account', canLogIn: false});
        if(user.deactivated === true ) return res.status(400).json({msg: 'Your account has been deactivated - Please contact our support team support@delpries.ca', canLogIn: false});

        //we need to authenticate the user
        //validate the password
        bcrypt.compare(password, user.password)
        .then(isMatch => {
            if(!isMatch) return res.status(400).json({msg: 'Invalid Credentials', canLogIn: false});

            UserType.findOne({_id: user.user_type_id})
            .then(userType => {
                if(!userType) return res.status(400).json({msg: 'Invalid User type', canLogIn: false});

                if(userType.user_type_name === 'client') {
                    //parts of jwt, header payload and signature, iat is when it was created
                    jwt.sign(
                        { id: user._id, user_type: userType.user_type_name},
                        config.get('jwtSecret'),
                        {expiresIn: 3600}, //set for one hour
                        (err, token) => {
                            if(err) return res.status(400).json({msg: 'Error Occured while trying to login', canLogIn: false});
    
                            //token is the same thing as doing "token": token
                            return res.json({
                                token,
                                user: {
                                    _id: user._id,
                                    name: user.user_name,
                                    email: user.user_email,
                                    can_access_order_details_page: user.can_access_order_details_page,
                                    can_access_vendors_page: user.can_access_vendors_page,
                                    can_access_drivers_page: user.can_access_drivers_page,
                                    can_make_an_order: user.can_make_an_order
                                },
                                user_specifics: {
                                    _id: userType._id,
                                    type_name: userType.user_type_name
                                },
                                msg: 'You can log in',
                                canLogIn: true
                            })
                    }); 
                } else if(userType.user_type_name === 'vendor') {
                    ProductVendor.findOne({user_id: user._id}, function(err, productVendor) {
                        ProductVendorMoreInfo.findOne({product_vendor_id: productVendor._id}, function(err, productVendorMoreInfo) {
                            if(err) return res.status(400).json({msg: 'Error Occured while trying to login', canLogIn: false});

                            //parts of jwt, header payload and signature, iat is when it was created
                            jwt.sign(
                                { id: user._id, user_type: userType.user_type_name},
                                config.get('jwtSecret'),
                                {expiresIn: 3600}, //set for one hour
                                (err, token) => {
                                    if(err) return res.status(400).json({msg: 'Error Occured while trying to login', canLogIn: false});
                                    
                                    //token is the same thing as doing "token": token
                                    return res.json({
                                        token,
                                        user: {
                                            _id: user._id,
                                            name: user.user_name,
                                            email: user.user_email,
                                            vendor_id_for_vendor: productVendor._id,
                                            can_access_order_details_page: user.can_access_order_details_page,
                                            can_access_vendors_page: user.can_access_vendors_page,
                                            can_access_drivers_page: user.can_access_drivers_page,
                                            can_make_an_order: user.can_make_an_order
                                        },
                                        user_specifics: {
                                            _id: userType._id,
                                            type_name: userType.user_type_name,
                                            vendorName: productVendor.product_vendor_name,
                                            vendorAddress: productVendorMoreInfo.product_vendor_address
                                        },
                                        msg: 'You can log in',
                                        canLogIn: true,
                                    })
                            }); 
                        });
                    });
                } else if(userType.user_type_name === 'driver') {
                    DriverMoreInfo.findOne({user_id: user._id}, function(err, driverMoreInfo) {
                        if(err) return res.status(400).json({msg: 'Error Occured while trying to login', canLogIn: false});

                        //parts of jwt, header payload and signature, iat is when it was created
                        jwt.sign(
                            { id: user._id, user_type: userType.user_type_name},
                            config.get('jwtSecret'),
                            {expiresIn: 3600}, //set for one hour
                            (err, token) => {
                                if(err) return res.status(400).json({msg: 'Error Occured while trying to login', canLogIn: false});
                                
                                //token is the same thing as doing "token": token
                                return res.json({
                                    token,
                                    user: {
                                        _id: user._id,
                                        name: user.user_name,
                                        first_name: user.first_name,
                                        email: user.user_email,
                                        on_an_order : driverMoreInfo.on_an_order,
                                        can_access_order_details_page: user.can_access_order_details_page,
                                        can_access_vendors_page: user.can_access_vendors_page,
                                        can_access_drivers_page: user.can_access_drivers_page,
                                        can_make_an_order: user.can_make_an_order
                                    },
                                    user_specifics: {
                                        _id: userType._id,
                                        type_name: userType.user_type_name
                                    },
                                    msg: 'You can log in',
                                    canLogIn: true
                                })
                        }); 
                    });
                }//end driver
            })
        });
        

    })
});

/**
 * This is used to send the verification code to the user when the user forgot their passwords
 */
router.post('/send_user_verification_code_to_email', (req, res) => {
    const {email} = req.body;

    if(!email) {
        return res.json({msg: 'Please enter all fields!', sentCode: false})
    } 

    User.findOne({user_email: email})
    .select('')
    .then(user => {
        if(!user) {
            return res.json({msg: 'User does not exist', sentCode: false})
        } 

        var verificationCode = randomString(8, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');

        //send the email of the verification code to the user
        nodemailer.createTestAccount((err, account) => {
            const htmlEmail = `
                <div style="width: 500px;font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                    <div style="background-color: #f2f2f2; height: 100px" >
                        <div style="font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; padding:28px 15px;">
                            <b style="color:#4db57c;font-size: 35px; ">DELP</b><b style="color:#F7C544;font-size: 35px;">RIES</b>
                            <div style="font-size: 14px;">delpries.ca</div>
                        </div>
                    </div>
        
                    <div style="background-color:#4db57c; height:350px; padding:50px 15px; color:#ffffff; position: relative;">
                        <div style="font-size:25px;">Hello ${user.first_name + ' '+user.last_name},</div>
        
                        <div style='margin-top:50px;'>You requested a verification code to change Password</div>
        
                        <div style='margin-top:10px;'>Your Verification code is ${verificationCode}.</div>
        
                        <div style='font-family: cursive; position: absolute; bottom: 15px; right: 20px; font-size: 20px;'>Thank You!</div>
                    </div>
        
                    <div style="background-color:#F7C544; height:100px;">
                        <div style="font-size:20px; text-align: center; padding-top:40px; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; color: #ffffff;">Thank you for using Delpries</div>
                    </div>
                </div>
            `;
        
            let transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 587,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: config.get('companyEmail'), // generated ethereal user
                    pass: config.get('companyPassword') // generated ethereal password
                }
            });
        
            let mailOptions = {
                from: config.get('companyEmail'), // sender address
                to: email, // list of receivers
                subject: 'Delpries Change Password Verification Code', // Subject line
                html: htmlEmail // html body
            };
        
            transporter.sendMail(mailOptions, (err, info) => {
                if(err) {
                    return res.json({msg: "Email Error: We couldn't send you the verification code", sentCode: false});
                } 


                user.verification_code = verificationCode;
                user.save()
                .then(user => {
                    return res.json({msg: "Verification code has been sent to your Email", sentCode: true});
                });
            })
        });//end of nodemailer
    });
});


/**
 * change the password with email and verification code
 */
router.post('/update_password_with_verification_code_email', (req, res) => {
    const {verificationCode, newPassword, email } = req.body;

    if(!verificationCode || !newPassword || !email) {
        return res.json({msg: 'Please enter all fields!', updated: false})
    } 

    User.findOne({user_email: email})
    .select('')
    .then(user => {
        if(!user) {
            return res.json({msg: 'User does not exist', updated: false})
        } 

        if(!(user.verification_code === verificationCode)) {
            return res.json({msg: 'Invalid Verification Code', updated: false})
        } else {
            //we need a salt to create a hash so we can encrypt the password
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newPassword, salt, (err, hash) => {
                    if(err) throw err;
        
                    user.password = hash;
                    user.verification_code = '';
                    user.save()
                    .then(user => {
                        return res.json({msg:'Password Changed', updated: true})
                    });
                })
            });
        }
    });
});


module.exports = router;