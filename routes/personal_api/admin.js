
/**
 * Date Created: 23rd July 2020.
 * This file would hold all the routes related to the Admin Components
 */

//system defined imports
const config = require('config');
const express = require('express');
const router = express.Router();
const _ = require('lodash');
var mongoose = require('mongoose');
ObjectId = require('mongodb').ObjectID;



const Order = require('../../models/order');
const ProductVendor = require('../../models/product_vendors');
const Users = require('../../models/user');
const OrderToProducts = require('../../models/order_to_products.js');
const ProductVendorMoreInfo = require('../../models/product_vendor_more_info.js');


/**
 * Get all the ongoing orders - orders that have not been delivered
 */
router.get('/get_all_ongoing_orders', (req, res) => {
    var result = [];

    Order.find({delivered : false, cancelled: false})
    .select('order_num_for_view vendor_approved order_needs_driver driver_has_started_order driver_has_picked_up product_vendor_id driver_id')
    .then(orders => {
        //get the ids
        var idOfStores = orders.map(function(order) { return order.product_vendor_id});
        var idOfDrivers = [];
        _.forEach(orders, function(order) {
            if(order.driver_id){
                idOfDrivers.push(order.driver_id);
            }
        });

        //get the vendors Name
        ProductVendor.find({_id: {$in: idOfStores}})
        .select('product_vendor_name')
        .then(productVendors => {
            //get the drivers Name
            Users.find({_id: {$in: idOfDrivers}})
            .select('first_name last_name')
            .then(drivers => {
                _.forEach(orders, function(order) {
                    var storeName = '';
                    var driverName = '';

                    if(order.driver_id) {
                        var driver = _.find(drivers, {'_id': ObjectId(order.driver_id)});
                        if(driver) {
                            driverName = driver.first_name + ", " + driver.last_name;
                        }
                    }

                    if(order.product_vendor_id) {
                        var store = _.find(productVendors, {'_id': ObjectId(order.product_vendor_id)});
                        if(store) {
                            storeName = store.product_vendor_name;
                        }
                    }

                    result.push(Object.assign({}, order.toObject(), {store_name : storeName, drivers_name: driverName} ));
                });

                res.send(result);
            });
        });
    });
});

/**
 * this would gbe used to get info on a single ongoing order by an admin
 */
router.get('/get_order_info/:id', (req, res) => {
    var orderId = req.params.id;

    if(!orderId) {
        return res.json({msg: 'Please enter all data', refreshed: false})
    } 

    Order.findOne({_id : orderId})
    .select('order_num_for_view vendor_approved order_needs_driver driver_has_started_order driver_has_picked_up product_vendor_id driver_id cancelled delivered')
    .then(order => {
        if(order) {
            //get the vendor Name
            ProductVendor.findOne({_id: order.product_vendor_id})
            .select('product_vendor_name')
            .then(productVendor => {
                if(productVendor) {
                    if(order.driver_id) {
                        Users.findOne({_id: order.driver_id})
                        .select('first_name last_name')
                        .then(driver => {
                            if(driver) {
                                return res.json(Object.assign({}, order.toObject(), {store_name : productVendor.product_vendor_name, drivers_name: driver.first_name + ", " + driver.last_name, refreshed: true} ));
                            } else {
                                return res.json({msg: 'Driver cannot be found', refreshed: false})
                            }
                        });
                    } else {
                        return res.json(Object.assign({}, order.toObject(), {store_name : productVendor.product_vendor_name, drivers_name:"", refreshed: true} ));
                    }
                } else {
                    return res.json({msg: 'Vendor cannot be found', refreshed: false})
                }
            });
        } else {
            return res.json({msg: 'Order cannot be found', refreshed: false})
        }
    })
});

/**
 * this would gbe used to get info on a single ongoing order by an admin
 */
router.get('/get_order_more_info/:id', (req, res) => {
    var orderId = req.params.id;

    if(!orderId) {
        return res.json({msg: 'Please enter all data', refreshed: false})
    } 

    Order.findOne({_id : orderId})
    .select('order_num_for_view vendor_approved order_needs_driver driver_has_started_order driver_has_picked_up product_vendor_id driver_id cancelled delivered order_name order_date address order_price_total_with_tax user_id vendor_approved_at driver_accepted_at driver_has_started_order_at driver_has_picked_up_at delivered_at')
    .then(order => {
        if(order) {
            ProductVendorMoreInfo.findOne({product_vendor_id : order.product_vendor_id})
            .select('product_vendor_address product_vendor_phone_number')
            .then(productVendorMoreInfo => {
                if(productVendorMoreInfo) {            
                    //get the products that were made for this order
                    OrderToProducts.find({order_id : orderId})
                    .select('-addons_total -order_id -addons_description')
                    .then(productsOrdered => {
                        //if we have a driver then get the driver info
                        var userAndDriverIds = [];
                        userAndDriverIds.push(order.user_id);
                        if(order.driver_id)
                            userAndDriverIds.push(order.driver_id);
        
                        //get the drivers details if any and that of the users
                        Users.find({_id: {$in: userAndDriverIds}})
                        .select('first_name last_name user_email mobile_no')
                        .then(users => { 
                            if(users) {
                                var usersInfo = undefined;
                                var driversInfo = undefined;
                                
                                //distinguish between the users and drivers info
                                if(users.length === 1) {
                                    usersInfo = users[0];
                                } else {
                                    if(users[0]._id.equals(order.user_id)) {
                                        usersInfo = users[0];
                                        driversInfo = users[1];
                                    } else {
                                        usersInfo = users[1];
                                        driversInfo = users[0];
                                    }
                                }
        
                                var result = Object.assign(
                                    {}, 
                                    { orderInfo: order.toObject() }, 
                                    { productsOrdered },
                                    { usersInfo },
                                    { driversInfo : driversInfo || false },
                                    { storeInfo : productVendorMoreInfo},
                                    {refreshed: true, msg: 'Refreshed'}
                                );
                                
                                return res.json(result);
                            } else {
                                return res.json({msg: 'Drivers info cannot be found', refreshed: false})
                            }
                        })
                    });
                } else {
                    return res.json({msg: 'Stores info cannot be found', refreshed: false})
                }
            })
        } else {
            return res.json({msg: 'Order cannot be found', refreshed: false})
        }
    })
});







/**
 * Get all the delivered orders - orders that have been delivered
 */
router.get('/get_add_orders', (req, res) => {
    var result = [];

    Order.find({delivered : true, cancelled: true, cancelled: false})
    .select('order_num_for_view product_vendor_id driver_id order_date user_id vendor_requested_order delivered_at order_price_total_with_tax drivers_total_money vendors_total_money cancelled address')
    .then(orders => {
        //get the ids
        var idOfStores = orders.map(function(order) { return order.product_vendor_id});
        var idsOfOrders = orders.map(function(order) { return order._id});
        var idOfDrivers = orders.map(function(order) { return order.driver_id});
        var idOfUsers = orders.map(function(order) { return order.user_id});
        var idOfDriversAndUsers = idOfDrivers.concat(idOfUsers);

        //get the vendors Name
        ProductVendor.find({_id: {$in: idOfStores}})
        .select('product_vendor_name')
        .then(productVendorInfo => {
            //get the products that were made for this order
            OrderToProducts.find({order_id: {$in: idsOfOrders}})
            .select('-addons_total')
            .then(productsOrdered => {           
                //get the drivers Name
                Users.find({_id: {$in: idOfDriversAndUsers}})
                .select('first_name last_name mobile_no user_email')
                .then(users => {
                    if(users) {
                        var result = [];

                        _.forEach(orders, function(order) {
                            result.push(Object.assign(
                                {}, 
                                { orderInfo: order.toObject() }, 
                                { productsOrdered: _.filter(productsOrdered, {'order_id': ObjectId(order._id)}) },
                                { usersInfo: _.find(users, {'_id': ObjectId(order.user_id)}) },
                                { driversInfo : _.find(users, {'_id': ObjectId(order.driver_id)})},
                                { storeInfo : _.find(productVendorInfo, {'_id': ObjectId(order.product_vendor_id)})}
                            ));
                        });

                        res.json(result);
                    } else {
                        return res.json({msg: 'Drivers info cannot be found', refreshed: false})
                    }
                });
            })
        });
    });
});






/**
 * Get all the users 
 */
router.get('/get_all_users', (req, res) => {
    Users.find({})
    .select('first_name last_name mobile_no user_email date_joined deactivated can_access_vendors_page can_access_drivers_page can_access_order_details_page')
    .then(users => {
        return res.json(users);
    });
});






/**
 * Get the busin ess data within the specified time frame 
 */
router.get('/get_bus_data/:startDate/:endDate', (req, res) => {
    var startDate = req.params.startDate;
    var endDate = req.params.endDate;

    if(!startDate || !endDate ) {
        return res.json({msg: 'Please enter all data', gotten: false})
    } 
    
    Order.aggregate([
        {$match: {"order_date": {"$gte": new Date(startDate), "$lt": new Date(endDate+' 23:59:59')}, delivered: true}},
        {
            $group: { 
                _id: null,
                total: { $sum: { $subtract: [ "$order_price_total_with_tax", { $add: [ "$drivers_total_money", "$vendors_total_money" ] } ] } },
                count: { $sum: 1 }
            }
        }
    ])
    .then(data => {
        if(data.length > 0) {
            return res.json(
                Object.assign(
                    {},
                    {total: (data[0].total).toFixed(2), count: data[0].count, gotten: true}
                )
            );
        } else {
            return res.json(
                Object.assign(
                    {},
                    {total: 0, count: 0, gotten: true}
                )
            );
        }
    })
});



module.exports = router;