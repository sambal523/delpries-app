
/**
 * Date Created: 23th September 2019.
 * This file would hold all the routes related to the PickProducts Compoment
 */

//system defined imports
const express = require('express');
const router = express.Router();
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const nodemailer = require('nodemailer');
const multer = require('multer');
const fs = require('fs');
const Cryptr = require('cryptr');

//custom imports
const ProductVendor = require('../../models/product_vendors');
const ProductVendorMoreInfo = require('../../models/product_vendor_more_info.js');
const DriverMoreInfo = require('../../models/driver_more_info');
const Users = require('../../models/user');
const UserPaymentDetails = require('../../models/user_payment_details');
const auth = require('../../middleware/auth');
const path = 'client/src/images/vendor_images/';

const storage = multer.diskStorage({
    destination : function(req, file, cb) {
        cb(null, path);
    },

    filename: function(req, file, cb) {
        cb(null, file.originalname)
    }
});

const upload = multer({
    storage: storage,
    limits : {
        fileSize : 1024 * 1024 * 5
    }
});


randomString = (length, chars) => {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

/**
 * Get all the order data for a user
 */
router.get('/get_user_data/:id', auth, (req, res) => {
    var userId = req.params.id;    
    
    if(!userId) {
        return res.json({msg: 'Please enter all fields!', gotten: false})
    } 


    Users.findOne({_id: userId})
    .select('')
    .then(user => {
        //if the user is a client just return the data else get the other findings to return the data for the vendor and driver
        if(user.can_access_order_details_page) {
            UserPaymentDetails.find({user_id: userId})
            .select('-sec_no -user_id') //card_no name_on_card expiry_dat card_type 
            .then(userPaymentDetails => {
                //get the secret to decrypt the card number
                const cardNoSec = new Cryptr(config.get('cardNoSecret'));
                var decryptedCardNo = '';

                //decrypt every card number in the array and update the array with the last 4 digits
                for(var i=0; i<userPaymentDetails.length; i++) {
                    decryptedCardNo = cardNoSec.decrypt(userPaymentDetails[i].card_no);
                    userPaymentDetails[i].card_no = decryptedCardNo.substr(decryptedCardNo.length - 4);
                }

                return res.json({
                    userInfo: {
                        firstName: user.first_name,
                        lastName: user.last_name,
                        userName: user.user_name,
                        mobileNo: user.mobile_no,
                        cardInfo: userPaymentDetails
                    },
                    gotten: true
                })
            });
        }else if(user.can_access_drivers_page) {
            DriverMoreInfo.findOne({user_id: userId})
            .select('')
            .then(driverMoreInfo => {
                if(!driverMoreInfo) return res.json({msg:'Driver data not found', gotten: false})

                return res.json({
                    userInfo: {
                        firstName: user.first_name,
                        lastName: user.last_name,
                        userName: user.user_name,
                        mobileNo: user.mobile_no,
                        driversLiscenceNumber: driverMoreInfo.drivers_license_number
                    },
                    gotten: true
                })
            });
        }else if(user.can_access_vendors_page) {
            ProductVendor.findOne({user_id: userId})
            .select('')
            .then(productVendor => {
                if(!productVendor) return res.json({msg: 'Could not get complete data!', gotten: false});

                ProductVendorMoreInfo.findOne({product_vendor_id: productVendor._id})
                .select('')
                .then(productVendorMoreInfo => {
                    if(!productVendorMoreInfo) return res.json({msg: 'Could not get complete data!', gotten: false});

                    return res.json({
                        userInfo: {
                            firstName: user.first_name,
                            lastName: user.last_name,
                            userName: user.user_name,
                            mobileNo: user.mobile_no,
                            storeName: productVendor.product_vendor_name,
                            storeAddress: productVendorMoreInfo.product_vendor_address,
                            postalCode: productVendorMoreInfo.product_vendor_postal_code,
                            province: productVendorMoreInfo.product_vendor_province,
                            city: productVendorMoreInfo.product_vendor_city,
                            description: productVendorMoreInfo.product_vendor_text_description
                        },
                        gotten: true
                    })
                });
            });
        }else {
            return res.json({msg: 'Invalid user type', gotten: false})
        }
    });
});


/**
 * update the user data
 */
router.post('/update_user_data', auth, (req, res) => {
    const {firstName, lastName, userName, mobileNo, userId } = req.body;

    if(!firstName || !lastName || !userName || !mobileNo || !userId) {
        return res.json({msg: 'Please enter all fields!', updated: false})
    } 

    Users.findOne({_id: userId})
    .select('')
    .then(user => {
        if(!user) {
            return res.json({msg: 'User Not Found', updated: false})
        } 

        user.first_name = firstName;
        user.last_name = lastName;
        user.user_name = userName;
        user.mobile_no = mobileNo;

        user.save()
        .then(user => {
            return res.json({msg:'User Data Updated', updated: true})
        });
    });
});




/**
 * update the password data
 */
router.post('/update_password_data', auth, (req, res) => {
    const {oldPassword, newPassword, userId } = req.body;

    if(!oldPassword || !newPassword || !userId) {
        return res.json({msg: 'Please enter all fields!', updated: false})
    } 

    Users.findOne({_id: userId})
    .select('')
    .then(user => {
        if(!user) {
            return res.json({msg: 'User Not Found', updated: false})
        } 

        //test to see if the new password matches
        bcrypt.compare(oldPassword, user.password)
        .then(isMatch => {
            //update password if it matches else return with the appopriate message
            if(!isMatch) return res.json({msg:'Old Password is Wrong', updated: false})

            //we need a salt to create a hash so we can encrypt the password
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newPassword, salt, (err, hash) => {
                    if(err) throw err;

                    user.password = hash;
                    user.save()
                    .then(user => {
                        return res.json({msg:'Password Updated', updated: true})
                    });
                })
            });
        });
    });
});


/**
 * 
 */
router.post('/send_user_verification_code', auth, (req, res) => {
    const {userId } = req.body;

    if(!userId) {
        return res.json({msg: 'Please enter all fields!', sentCode: false})
    } 

    Users.findOne({_id: userId})
    .select('')
    .then(user => {
        if(!user) {
            return res.json({msg: 'User Not Found', sentCode: false})
        } 

        var verificationCode = randomString(8, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');

        //send the email of the verification code to the user
        nodemailer.createTestAccount((err, account) => {
            const htmlEmail = `
                <div style="width: 500px;font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                    <div style="background-color: #f2f2f2; height: 100px" >
                        <div style="font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; padding:28px 15px;">
                            <b style="color:#4db57c;font-size: 35px; ">DELP</b><b style="color:#F7C544;font-size: 35px;">RIES</b>
                            <div style="font-size: 14px;">delpries.ca</div>
                        </div>
                    </div>
        
                    <div style="background-color:#4db57c; height:350px; padding:50px 15px; color:#ffffff; position: relative;">
                        <div style="font-size:25px;">Hello ${user.first_name + ' '+user.last_name},</div>
        
                        <div style='margin-top:50px;'>You requested a verification code to change password</div>
        
                        <div style='margin-top:10px;'>Your Verification code is ${verificationCode}.</div>
        
                        <div style='font-family: cursive; position: absolute; bottom: 15px; right: 20px; font-size: 20px;'>Thank You!</div>
                    </div>
        
                    <div style="background-color:#F7C544; height:100px;">
                        <div style="font-size:20px; text-align: center; padding-top:40px; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; color: #ffffff;">Thank you for using Delpries</div>
                    </div>
                </div>
            `;
        
            let transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 587,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: config.get('companyEmail'), // generated ethereal user
                    pass: config.get('companyPassword') // generated ethereal password
                }
            });
        
            let mailOptions = {
                from: config.get('companyEmail'), // sender address
                to: user.user_email, // list of receivers
                subject: 'Delpries Change Password Verification Code', // Subject line
                html: htmlEmail // html body
            };
        
            transporter.sendMail(mailOptions, (err, info) => {
                if(err) {
                    return res.json({msg: "Email Error: We couldn't send you the verification code, Try again later!", sentCode: false});
                } 

                user.verification_code = verificationCode;
                user.save()
                .then(user => {
                    return res.json({msg: "Verification code has been sent to your Email", sentCode: true});
                });
            })
        });//end of nodemailer
    });
});


/**
 * update the password data
 */
router.post('/update_password_with_verification_code', auth, (req, res) => {
    const {verificationCode, newPassword, userId } = req.body;

    if(!verificationCode || !newPassword || !userId) {
        return res.json({msg: 'Please enter all fields!', updated: false})
    } 

    Users.findOne({_id: userId})
    .select('')
    .then(user => {
        if(!user) {
            return res.json({msg: 'User Not Found', updated: false})
        } 

        if(!(user.verification_code === verificationCode)) {
            return res.json({msg: 'Invalid Verification Code', updated: false})
        } else {
            //we need a salt to create a hash so we can encrypt the password
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newPassword, salt, (err, hash) => {
                    if(err) throw err;
        
                    user.password = hash;
                    user.verification_code = '';
                    user.save()
                    .then(user => {
                        return res.json({msg:'Password Updated', updated: true})
                    });
                })
            });
        }
    });
});





/**
 * this is used to update the email
 */
router.post('/update_email', auth, (req, res) => {
    const {email, verificationCode, password, userId } = req.body;

    if(!email || !verificationCode || !password || !userId) {
        return res.json({msg: 'Please enter all fields!', updated: false})
    } 

    Users.findOne({_id: userId})
    .select('')
    .then(user => {
        if(!user) {
            return res.json({msg: 'User Not Found', updated: false})
        } 

        if(!(user.verification_code === verificationCode)) {
            return res.json({msg: 'Invalid Verification Code', updated: false})
        } else {
            //we need a salt to create a hash so we can encrypt the password
            bcrypt.compare(password, user.password)
            .then(isMatch => {
                //update password if it matches else return with the appopriate message
                if(!isMatch) return res.json({msg:'Invalid Password', updated: false})
                
                Users.findOne({user_email: email})
                .select('')
                .then(userNew => {
                    if(userNew) return res.json({msg:'Email is already used', updated: false})

                    user.user_email = email;
                    user.verification_code = '';
                    user.save()
                    .then(user => {
                        return res.json({msg:'Email Updated', updated: true})
                    });
                });
            });
        }
    });
});


/**
 * 
 */
router.post('/send_user_verification_code_new_email', auth, (req, res) => {
    const {userId, email, firstName, lastName } = req.body;

    if(!userId || !email || !firstName || !lastName) {
        return res.json({msg: 'Please enter all fields!', sentCode: false})
    } 

    Users.findOne({user_email: email})
    .select('')
    .then(user => {
        if(user) {
            return res.json({msg: 'Email is already Being Used', sentCode: false})
        } 

        var verificationCode = randomString(8, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');

        //send the email of the verification code to the user
        nodemailer.createTestAccount((err, account) => {
            const htmlEmail = `
                <div style="width: 500px;font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                    <div style="background-color: #f2f2f2; height: 100px" >
                        <div style="font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; padding:28px 15px;">
                            <b style="color:#4db57c;font-size: 35px; ">DELP</b><b style="color:#F7C544;font-size: 35px;">RIES</b>
                            <div style="font-size: 14px;">delpries.ca</div>
                        </div>
                    </div>
        
                    <div style="background-color:#4db57c; height:350px; padding:50px 15px; color:#ffffff; position: relative;">
                        <div style="font-size:25px;">Hello ${firstName + ' '+lastName},</div>
        
                        <div style='margin-top:50px;'>You requested a verification code to change Email</div>
        
                        <div style='margin-top:10px;'>Your Verification code is ${verificationCode}.</div>
        
                        <div style='font-family: cursive; position: absolute; bottom: 15px; right: 20px; font-size: 20px;'>Thank You!</div>
                    </div>
        
                    <div style="background-color:#F7C544; height:100px;">
                        <div style="font-size:20px; text-align: center; padding-top:40px; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; color: #ffffff;">Thank you for using Delpries</div>
                    </div>
                </div>
            `;
        
            let transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 587,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: config.get('companyEmail'), // generated ethereal user
                    pass: config.get('companyPassword') // generated ethereal password
                }
            });
        
            let mailOptions = {
                from: config.get('companyEmail'), // sender address
                to: email, // list of receivers
                subject: 'Delpries Change Password Verification Code', // Subject line
                html: htmlEmail // html body
            };
        
            transporter.sendMail(mailOptions, (err, info) => {
                if(err) {
                    return res.json({msg: "Email Error: We couldn't send you the verification code", sentCode: false});
                } 


                Users.findOne({_id: userId}) 
                .select('')
                .then(user => {
                    user.verification_code = verificationCode;
                    user.save()
                    .then(user => {
                        return res.json({msg: "Verification code has been sent to your Email", sentCode: true});
                    });
                })
            })
        });//end of nodemailer
    });
});


router.post('/add_card', auth, (req, res) => {
    const {cardType, cardNumber, cardOwner, secNo, expDat, userId} = req.body;

    //validate payment info
    if(!cardType || !cardNumber || !cardOwner || !secNo || !expDat || !userId) {
        return res.json({msg: 'Please enter all fields!', saved: false})
    } 

    const cardNoSec = new Cryptr(config.get('cardNoSecret'));
    const cardSecuNoSec = new Cryptr(config.get('cardSecNoSecret'));

    Users.findOne({_id: userId})
    .select('')
    .then(user => {
        if(!user) {
            return res.json({msg: 'User Not Found', saved: false});
        } else {
            UserPaymentDetails.find({user_id: userId})
            .then(paymentMethods => {
                //test to ensure that maximum of 4 cards can be allowed entry
                if(paymentMethods.length == 4) {
                    return res.json({msg: 'Maximum of 4 cards allowed', saved: false});
                } else {
                    //ensure duplicate cards are not entered
                    var decryptedCardNo = '';
                    var foundDuplicate = false;
                    const cardNoSec = new Cryptr(config.get('cardNoSecret'));

                    //decrypt every card number in the array and update the array with the last 4 digits
                    for(var i=0; i<paymentMethods.length; i++) {
                        decryptedCardNo = cardNoSec.decrypt(paymentMethods[i].card_no);

                        if(decryptedCardNo === cardNumber) {
                            foundDuplicate = true;
                            break;
                        }
                    }

                    if(foundDuplicate) {
                        return res.json({msg: `Duplicate Card ending with ${cardNumber.substr(cardNumber.length - 4)}`, saved: false});
                    } else {
                        //store payment details
                        var paymentDetails = new UserPaymentDetails({
                            card_no: cardNoSec.encrypt(cardNumber),
                            name_on_card: cardOwner,
                            sec_no: cardSecuNoSec.encrypt(secNo),
                            expiry_dat: expDat,
                            card_type: cardType,
                            user_id: userId
                        });
        
                        UserPaymentDetails.create(paymentDetails, function (err) {
                            if (err) {
                                return res.json({msg: 'Error Occuured Please Try Again', saved: false});
                            } else {
                                return res.json({msg: 'Successful', saved: true});
                            }
                        });
                    }
                }
            })
        }
    });
});

/**
 * Get Cards Info
 */
router.get('/get_cards_info/:id', auth, (req, res) => {
    var userId = req.params.id;    
    
    if(!userId) {
        return res.json({msg: 'Please enter all fields!', gotten: false})
    } 

    Users.findOne({_id: userId})
    .select('')
    .then(user => {
        //if the user is a client just return the data else get the other findings to return the data for the vendor and driver
        if(user.can_access_order_details_page) {
            UserPaymentDetails.find({user_id: userId})
            .select('-sec_no -user_id')
            .then(userPaymentDetails => {
                //get the secret to decrypt the card number
                var decryptedCardNo = '';
                const cardNoSec = new Cryptr(config.get('cardNoSecret'));

                //decrypt every card number in the array and update the array with the last 4 digits
                for(var i=0; i<userPaymentDetails.length; i++) {
                    decryptedCardNo = cardNoSec.decrypt(userPaymentDetails[i].card_no);
                    userPaymentDetails[i].card_no = decryptedCardNo.substr(decryptedCardNo.length - 4);
                }

                return res.json({
                    cardsInfo: userPaymentDetails,
                    gotten: true
                })
            });
        }else {
            return res.json({msg: 'Invalid User Type', gotten: false})
        }
    });
});

//this would be used for deleting an addon group
router.get('/del_card/:id/:id2', auth, (req, res) => {
    var cardId = req.params.id;
    var userId = req.params.id2;

    if(!cardId || !userId) {
        return res.json({deleted: false, msg: "Could Not Delete"})
    }

    UserPaymentDetails.deleteOne({ _id: cardId, user_id: userId }, function (err) {
        if(err) {
            return res.json({deleted: false, msg: "Could Not Delete"})
        } else {
            return res.json({deleted: true, msg: "Deleted"})
        }
    });
 });







/**
 * this is used to update the email
 */
router.post('/update_driver_info', auth, (req, res) => {
    const {driversLicenceNumber, userId } = req.body;

    if(!driversLicenceNumber || !userId) {
        return res.json({msg: 'Please enter all fields!', updated: false})
    } 

    Users.findOne({_id: userId})
    .select('')
    .then(user => {
        if(!user) {
            return res.json({msg: 'User Not Found', updated: false})
        } 

        if(!user.can_access_drivers_page) {
            return res.json({msg: 'User is not a Driver', updated: false})
        }

        DriverMoreInfo.findOne({user_id: userId})
        .select('')
        .then(driverMoreInfo => {
            if(!driverMoreInfo) return res.json({msg:'Driver data not found', updated: false})

            driverMoreInfo.drivers_license_number = driversLicenceNumber;
            driverMoreInfo.save()
            .then(driverMoreInfo => {
                return res.json({msg: 'Information Updated', updated: true})
            });
        });
    });
});
















/**
 * this is used to update the vendors data
 */
router.post('/update_vendor_data_no_pic', auth, (req, res) => {
    var storeName = req.body.storeName;
    var storeAddress = req.body.storeAddress;
    var postalCode = req.body.postalCode;
    var province = req.body.province;
    var city = req.body.city;
    var textDesciption = req.body.textDesciption;
    var userId = req.body.userId;

    if(!storeName || !storeAddress || !postalCode || !province || !city || !textDesciption || !userId) {
        return res.json({msg: 'Please enter all fields!', sentCode: false})
    } 

    ProductVendor.findOne({user_id: userId})
    .select('')
    .then(productVendor => {
        if(!productVendor) return res.json({msg: 'Could not get complete data!', gotten: false});

        ProductVendorMoreInfo.findOne({product_vendor_id: productVendor._id})
        .select('')
        .then(productVendorMoreInfo => {
            if(!productVendorMoreInfo) return res.json({msg: 'Could not get complete data!', gotten: false});

            productVendor.product_vendor_name = storeName;
            productVendor.save()
            .then(productVendor => {
                productVendorMoreInfo.product_vendor_address = storeAddress;
                productVendorMoreInfo.product_vendor_postal_code = postalCode;
                productVendorMoreInfo.product_vendor_province = province;
                productVendorMoreInfo.product_vendor_city = city;
                productVendorMoreInfo.product_vendor_text_description = textDesciption;
                productVendorMoreInfo.save()
                .then(productVendorMoreInfo => {
                    return res.json({msg: 'Information Updated', updated: true})
                });
            });
        });
    });
})


/**
 * add a vendor
 */
router.post('/update_vendor_data_with_pic', upload.array('companyImages'), auth, (req, res) => {
    var storeName = req.body.storeName;
    var storeAddress = req.body.storeAddress;
    var postalCode = req.body.postalCode;
    var province = req.body.province;
    var city = req.body.city;
    var textDesciption = req.body.textDesciption;
    var userId = req.body.userId;
    var companyIconImgName = req.body.companyIconImgName;
    var companyBgImageName = req.body.companyBgImageName;

    if(!storeName || !storeAddress || !postalCode || !province || !city || !textDesciption || !userId || !(companyIconImgName || companyBgImageName)) {
        return res.json({msg: 'Please enter all fields!', sentCode: false})
    } 


    ProductVendor.findOne({user_id: userId})
    .select('')
    .then(productVendor => {
        if(!productVendor) return res.json({msg: 'Could not get complete data!', gotten: false});

        ProductVendorMoreInfo.findOne({product_vendor_id: productVendor._id})
        .select('')
        .then(productVendorMoreInfo => {
            if(!productVendorMoreInfo) return res.json({msg: 'Could not get complete data!', gotten: false});

            productVendor.product_vendor_name = storeName;
            if(companyIconImgName) {
                //remove the previous file
                fs.unlink(path+productVendor.product_vendor_icon_name+'.png', (err) => {
                    if (err) {
                        return
                    }
                
                })
                productVendor.product_vendor_icon_name = companyIconImgName;
            }
            if(companyBgImageName) {
                //remove the previous file
                fs.unlink(path+productVendor.product_vendor_image_name+'.jpg', (err) => {
                    if (err) {
                        return
                    }
                
                })
                productVendor.product_vendor_image_name = companyBgImageName;
            }
            productVendor.save()
            .then(productVendor => {
                productVendorMoreInfo.product_vendor_address = storeAddress;
                productVendorMoreInfo.product_vendor_postal_code = postalCode;
                productVendorMoreInfo.product_vendor_province = province;
                productVendorMoreInfo.product_vendor_city = city;
                productVendorMoreInfo.product_vendor_text_description = textDesciption;
                productVendorMoreInfo.save()
                .then(productVendorMoreInfo => {
                    return res.json({msg: 'Information Updated', updated: true})
                });
            });
        });
    });
});




/**
 * this is used to validate admin password for vendors
 */
router.post('/validate_admin', auth, (req, res) => {
    const {adminPassword, vendorId } = req.body;

    if(!adminPassword || !vendorId) {
        return res.json({msg: 'Please enter all fields!', validated: false})
    } 

    ProductVendor.findOne({_id: vendorId})
    .select('')
    .then(productVendor => {
        if(!productVendor) {
            return res.json({msg: 'Vendor Not Found', validated: false})
        } 

        bcrypt.compare(adminPassword, productVendor.admin_password)
        .then(isMatch => {
            if(!isMatch) {
                return res.json({msg: 'Invalid Password', validated: false});
            } else {
                return res.json({msg: 'Logged In', validated: true});
            }
        });
    });
});


/**
 * this is used to update admin password for vendors
 */
router.post('/change_admin_password', auth, (req, res) => {
    const {adminPassword, vendorId } = req.body;

    if(!adminPassword || !vendorId) {
        return res.json({msg: 'Please enter all fields!', updated: false})
    } 

    ProductVendor.findOne({_id: vendorId})
    .select('')
    .then(productVendor => {
        if(!productVendor) {
            return res.json({msg: 'Vendor Not Found', updated: false})
        } 

        //we need a salt to create a hash so we can encrypt the password
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(adminPassword, salt, (err, hash) => {
                if(err) throw err;
        
                productVendor.admin_password = hash;
                productVendor.save()
                .then(prodVend => {
                    return res.json({msg:'Password Updated', updated: true})
                });
            })
        });
    });
});

module.exports = router;