
/**
 * Date Created: 12th November 2019.
 * This file would be used to hold generic auth routes
 */

//system defined imports
const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');

//custom imports
const User = require('../../models/user');
const ProductVendor = require('../../models/product_vendors');
const DriverMoreInfo = require('../../models/driver_more_info');

/**
 * Authenticate this user In the user
 */
router.get('/validate_user', auth, (req, res) => {
    User.findOne({_id: req.user.id})
    .select('-password')
    .then( user => {
            if(user.is_account_locked) {
                return res.json({msg: 'Please validate your account', accountCreated: false})
            }

            if(!user) {
                return res.status(400).json({msg: 'Sign In', grantAccess: false});
            }

            //if our user is a user we just send the user details, if a vendor we send some vendor related info and also if the user is a driver
            if(user.can_access_order_details_page) {
                return res.json({user, user_specifics : ''})
            } else if(user.can_access_vendors_page) {
                // Get the product vendors that this user manages
                ProductVendor.findOne({user_id: user._id}, function(err, productVendor) {
                    ProductVendorMoreInfo.findOne({product_vendor_id: productVendor._id}, function(err, productVendorMoreInfo) {
                        if(err) return res.status(400).json({msg: 'Error Occured while trying to authenticate user', grantAccess: false});
            
                        // docs contains your answer
                        var userObj = Object.assign({} , user.toObject(), {'vendor_id_for_vendor': productVendor._id})

                        var user_specifics = {
                            vendorName: productVendor.product_vendor_name,
                            vendorAddress: productVendorMoreInfo.product_vendor_address
                        };

                        return res.json({user: userObj, user_specifics});
                    });
                });
            }else if(user.can_access_drivers_page) {
                // Get the product vendors that this user manages
                DriverMoreInfo.findOne({user_id: user._id}, function(err, driverMoreInfo) {
                    if(err) return res.status(400).json({msg: 'Error Occured while trying to authenticate user', grantAccess: false});
        
                    // docs contains your answer
                    var userObj = Object.assign({} , user.toObject(), {'on_an_order': driverMoreInfo.on_an_order})

                    var user_specifics = '';

                    return res.json({user: userObj, user_specifics});
                });
            }else {
                if(err) return res.status(400).json({msg: 'Invalid user type', grantAccess: false});
            }
        }
    )
    .catch(err => {
        
    })
});


module.exports = router;