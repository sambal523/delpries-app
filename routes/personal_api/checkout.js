
/**
 * Date Created: 23th September 2019.
 * This file would hold all the routes related to the PickProducts Compoment
 */

//system defined imports
const config = require('config');
const express = require('express');
const router = express.Router();
const _ = require('lodash');
const Cryptr = require('cryptr');

//custom imports
const Order = require('../../models/order');
const Product = require('../../models/product');
const OrderToProducts = require('../../models/order_to_products.js');
const Utility = require('../../models/utility.js');
const auth = require('../../middleware/auth');
const ProductVendor = require('../../models/product_vendors');
const Users = require('../../models/user');
const UserPaymentDetails = require('../../models/user_payment_details');



function orderNumber() {
    let now = Date.now().toString() // '1492341545873'

    // pad with extra random digit
    now += now + Math.floor(Math.random() * 10)
    
    return [now.slice(4, 10), now.slice(10, 14)].join('')
}


/**
 * Get Cards Info
 */
router.get('/get_cards_info/:id', auth, (req, res) => {
    var userId = req.params.id;    
    
    if(!userId) {
        return res.json({msg: 'Please enter all fields!', gotten: false})
    } 

    Users.findOne({_id: userId})
    .select('')
    .then(user => {
        //if the user is a client just return the data else get the other findings to return the data for the vendor and driver
        if(user.can_access_order_details_page) {
            UserPaymentDetails.find({user_id: userId})
            .select('-sec_no -user_id')
            .then(userPaymentDetails => {
                //get the secret to decrypt the card number
                var decryptedCardNo = '';
                const cardNoSec = new Cryptr(config.get('cardNoSecret'));

                //decrypt every card number in the array and update the array with the last 4 digits
                for(var i=0; i<userPaymentDetails.length; i++) {
                    decryptedCardNo = cardNoSec.decrypt(userPaymentDetails[i].card_no);
                    userPaymentDetails[i].card_no = decryptedCardNo.substr(decryptedCardNo.length - 4);
                }

                return res.json({
                    cardsInfo: userPaymentDetails,
                    gotten: true
                })
            });
        }else {
            return res.json({msg: 'Invalid User Type', gotten: false})
        }
    });
});

/**
 * add an order
 */
router.post('/add_an_order', auth, (req, res) => {
    if (!req.body.userId || !req.body.address || !req.body.userName || !req.body.paymentId) return res.json('Please enter all values');
    
    //get the objects
    var orderData = req.body.cartItems;
    var address = req.body.address;
    var productVendorId = orderData[0].product_vendor_id;
    var userName = req.body.userName;
    var fromOrderDetails = req.body.fromOrderDetails;
    var paymentId = req.body.paymentId;

    //get some data from the utility for this city
    Utility.findOne({city : req.body.city, province : req.body.province}, function(err, utility) {
        if (err) throw err;
        if (!utility) return res.json({msg:'Please enter all values', orderAdded: false});
        
        ProductVendor.findOne({_id: productVendorId})
        .select('')
        .then(prodVend=> {
            if(prodVend) {
                if(!prodVend.is_currently_active) {
                    return res.json({msg: `${prodVend.product_vendor_name} is currently closed`, orderAdded: false});
                }
                else {
                    //_id is the product id. we are building the data for the orderToProducts table
                    var orderToProducts = [];
                    var addonDescription = '';
                    var count = 0;
                    _.map(orderData, o =>  {
                        if(!fromOrderDetails) {
                            _.map(o.addons_groups_selected, oAdd =>  {
                                if(oAdd) {
                                    _.map(oAdd.addons_selected, oAddSingle =>  {
                                        addonDescription = addonDescription.concat(oAddSingle.name+',');
                                    });
                                }
                            });

                            orderToProducts.push({
                                product_id: o._id,
                                amount_ordered: o.amount_ordered,
                                product_price: o.product_price,
                                product_name: o.product_name,
                                special_instructions: o.special_instructions,
                                addons_total: o.addons_total,
                                addons_description: addonDescription
                            })

                            addonDescription = '';
                        } else {
                            o.addons_description = o.addons_description;

                            orderToProducts.push({
                                product_id: o.product_id,
                                amount_ordered: o.amount_ordered,
                                product_price: o.product_price,
                                product_name: o.product_name,
                                special_instructions: o.special_instructions,
                                addons_total: o.addons_total,
                                addons_description: o.addons_description
                            })
                        }
                    });

                    //build the order data
                    //date data
                    var today = new Date();
                    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                    
                    var orderName = `Ordered ${orderData.length} ${orderData.length === 1 ? 'item' : 'items'}`;
                    var orderDate = date+' '+time;
                    var userId = req.body.userId;
                    var productVendorId = orderData[0].product_vendor_id;
                    var vendorsMoney = 0.00;
                    var driversMoney = utility.drivers_money;
                    var tip = req.body.tip;
                    var deliveryFeeWeChargeClient = utility.delivery_fee_for_client;
                    var percentWeChargeVendors = utility.service_charge_on_vendors_percentage;
                    var deliveryFeeWeChargeStores = utility.delivery_fee_on_stores;
                    var tax = utility.tax;
                    var delpriesMoney = 0.00;
            
                    //calculate the total
                    var orderPriceTotalWithTax =  _.sumBy(orderData, function (order) {
                        return ((order.product_price + order.addons_total) * order.amount_ordered);
                    });
                    orderPriceTotalWithTax += (orderPriceTotalWithTax * tax);
            
                    //calculate the money delpries own
                    delpriesMoney = (orderPriceTotalWithTax * percentWeChargeVendors) + deliveryFeeWeChargeStores;
                    vendorsMoney = orderPriceTotalWithTax - delpriesMoney;
                    delpriesMoney -= 3; //we remove the dividend to cover up for the drivers money + $4 delivery fee to make up $7
                    driversMoney += tip * utility.percent_drivers_take_for_tip;
                    delpriesMoney += tip * utility.percent_delpries_take_for_tip;
            
                    //round the digits
                    driversMoney = driversMoney.toFixed(2);
                    vendorsMoney = vendorsMoney.toFixed(2);
                    delpriesMoney = delpriesMoney.toFixed(2);
            
                    
                    var order = new Order({
                        order_name : orderName,
                        order_date : orderDate,

                        payment_id: paymentId,
                        user_id : userId,
                        user_name: userName,

                        order_price_total_with_tax : (orderPriceTotalWithTax + deliveryFeeWeChargeClient + tip).toFixed(2),
                        product_vendor_id : productVendorId,
                        just_ordered: true,
                        vendor_approved: false,

                        time_to_get_ready_milli_sec: '',
                        order_needs_driver: false,
                        // vendor_has_given_driver_order: false,

                        driver_has_started_order: false,
                        driver_has_picked_up: false,

                        delivered: false,
                        tip: tip,
                        drivers_total_money: driversMoney,
                        vendors_total_money: vendorsMoney,
                        driver_id: '',
                        address: address,
                        order_num_for_view: orderNumber(),
                        vendor_requested_order: false, 
                        vendor_wants_direct_transfer: false,
                        vendor_wants_cash: false,
                        clients_would_give_cash: false,
                        clients_would_give_card: false,

                        cancelled: false,

                        vendor_approved_at: "",
                        driver_accepted_at: "",
                        driver_has_started_order_at: "",
                        driver_has_picked_up_at: "",
                        delivered_at: ""
                    });
                    
                    //insert the order and get the id
                    order.save().then(order => {
                        //update the products data and change the data for amount left in each
                        //add the order_id to the order to products before adding them to the database | update the products data and change the data for amount left in each
                        orderToProducts.forEach(function (element) {
                            Product.findOne({_id: element.product_id}, function(err, product) {
                                if(!product.uncountable_product_type) {
                                    product.product_number_available = product.product_number_available - element.amount_ordered;
                                    product.save();
                                }
                            });
            
                            element.order_id = order._id;
                        });
            
                        //insert the order to products
                        OrderToProducts.create(orderToProducts, function (err, orderToProduct) {
                            if (err) {
                                return res.send({ orderAdded: false, msg:'Error Occured'});
                            } else {
                                return res.send({ orderNumForView: order.order_num_for_view, orderId: order._id, orderAdded: true, vendors_total_money:order.vendors_total_money, msg: 'Order Added'});
                            }
                        });
                    });
                }
            }else {
                return res.json({msg: `Store Not Found`, orderAdded: false});
            }
        });
    })
});



/**
 * Delete all orders and order details
 */
router.delete('/all_orders_and_order_details', (req, res) => {
    //delete all order
    Order.deleteMany({}, function (err) {
        if(err) {
            res.send(false);
        }
    }).then(res1 => {
        //delete all order products
        OrderToProducts.deleteMany({}, function(err) {
            if(err) {
                res.send(false);
            }
        }).then(res2 => {
            res.send('done deleting all');
        })
    });
});


module.exports = router;