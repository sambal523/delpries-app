//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';
import {
    geocodeByAddress,
    getLatLng,
} from 'react-places-autocomplete';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {storeOngoingOrdersAmount, removeOneOngoingOrder, addOneOngoingOrder} from '../../redux/actions/driverActions';

//custom made component
import PageLoading from '../utility/PageLoading';
import GoogleApiWrapper from '../utility/GoogleApiWrapper';

//images
import map_icon from '../../images/icons/map.png';
import { MdClear } from 'react-icons/md'; 

//icons
import { FiMaximize } from "react-icons/fi";
import { FiMinimize } from "react-icons/fi";

//stylings
import '../../stylings/driver-ongoing-orders.css';

//define loadash
const _ = require('lodash');

class DriverOngoingOrders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPageLoading : true,
            allOngoingOrdersData: null,
            driverId: this.props.userData._id,
            ongoingOrdersComponents: [],
            startedOrderData: null,
            showModal: false,
            modalMessage: '',
            modalResponseStatus: false,
            startOrderContainerIsMaximized: false
        }


        this.getOngoingOrders();
    }

    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
            this.setState({modalMessage: ''});
        }
    
        //open the modal
        this.setState({showModal: !this.state.showModal});
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        userData: PropTypes.object,
        storeOngoingOrdersAmount: PropTypes.func.isRequired,
        removeOneOngoingOrder: PropTypes.func.isRequired,
        addOneOngoingOrder: PropTypes.func.isRequired,

        socketData: PropTypes.object,
    }

    getOngoingOrders = () => {
        //headers
        const config = {
            headers: {
                'x-auth-token': localStorage.getItem('token')
            }
        }

        //ToDo: get all orders that have their status to be pending   
        axios.get(`${apiURL}/api/drivers/get_orders_driver_accepted_to_make/${this.props.userData._id}`, config)
        .then(res => {
            //store the socket data
            this.props.storeOngoingOrdersAmount(res.data.orders.length);

            //build the orders data
            this.setState({allOngoingOrdersData : res.data.orders});
            this.buildOrderList(res.data.orders);

            //show the loading sign
            this.setState({showPageLoading : false });
        })
        .catch(err => {});
    }

    openStoreAddressInMap = () => {
        var address = this.state.startedOrderData.vendor_address;
        geocodeByAddress(address)
        .then(results => getLatLng(results[0]))
        .then(latLng => {
            //this.setState({center: {lat: latLng.lat, lng: latLng.lng}});
            window.open("https://maps.google.com?q="+latLng.lat+","+latLng.lng );
        })
        .catch(error => {});
    }

    openClientAddressInMap = () => {
        var address = this.state.startedOrderData.clients_adress;
        geocodeByAddress(address)
        .then(results => getLatLng(results[0]))
        .then(latLng => {
            //this.setState({center: {lat: latLng.lat, lng: latLng.lng}});
            window.open("https://maps.google.com?q="+latLng.lat+","+latLng.lng );
        })
        .catch(error => {});
    }

    removeOrderFromOngoingOrderList = (orderId) => {
        var currOrders = this.state.allOngoingOrdersData;
    
        //if the current order is not an object we want to parse it
        if(!(typeof currOrders === 'object')) {
            currOrders = JSON.parse(currOrders);
        }
        
        //we want to remove the order
        _.remove(currOrders, function(order) {
            return order.order_id === orderId;
        });
        
        //update our order data
        this.setState({allOngoingOrdersData : currOrders});
    
        //rebuild the list
        this.buildOrderList(currOrders);
    
        //add an order to the redux state
        this.props.removeOneOngoingOrder();
    }

    iHaveDeliveredUpTheOrder = () => {
        //send the response to the backEnd
        const config = {
            headers: {
                'x-auth-token': localStorage.getItem('token')
            }
        }

        //
        var orderData = this.state.startedOrderData;

        //ToDo: get all orders that have their status to be pending   
        axios.get(`${apiURL}/api/drivers/deliver_order/${orderData.driver_id}/${orderData.order_id}`, config)
        .then(res => {
            if(res.data.orderUpdated) {
                //update the started Order by removing it
                this.setState({startedOrderData : null});

                //notify the server through socket that this order has been delivered
                this.props.socketData.emit('driverDeliveredOrder', {orderOngoingAmount: this.state.allOngoingOrdersData.length, driverId: orderData.driver_id});
                
                //remove the order from the list of ongoing orders
                this.removeOrderFromOngoingOrderList(orderData.order_id);
            } else {
                //display in the modal that the order could not be started
                this.setState({modalMessage: res.data.msg});
                this.setState({modalResponseStatus: res.data.orderUpdated});

                //toggle the modal
                this.toggleModal();
            }
        })
        .catch(err => {});
    }

    iHavePickedUpTheOrder = () => {
        //send the response to the backEnd
        const config = {
            headers: {
                'x-auth-token': localStorage.getItem('token')
            }
        }

        //
        var orderData = this.state.startedOrderData;

        //ToDo: get all orders that have their status to be pending   
        axios.get(`${apiURL}/api/drivers/picked_up_order/${orderData.driver_id}/${orderData.order_id}`, config)
        .then(res => {
            if(res.data.orderUpdated) {
                //update the started Order
                var startedOrderDataTemp = this.state.startedOrderData;
                startedOrderDataTemp.driver_has_picked_up = true;
                this.setState({startedOrderData : startedOrderDataTemp});

                //notify the client through socket
                
            } else {
                //display in the modal that the order could not be started
                this.setState({modalMessage: res.data.msg});
                this.setState({modalResponseStatus: res.data.orderUpdated});

                //toggle the modal
                this.toggleModal();
            }
        })
        .catch(err => {});
    }

    startThisOrder = (orderData) => {
        //send the response to the backEnd
        const config = {
            headers: {
                'x-auth-token': localStorage.getItem('token')
            }
        }

        //ToDo: get all orders that have their status to be pending   
        axios.get(`${apiURL}/api/drivers/start_order/${orderData.driver_id}/${orderData.order_id}`, config)
        .then(res => {
            if(res.data.orderStarted) {
                //remove the order from the list
                var currOngoingOrders = this.state.allOngoingOrdersData;

                _.remove(currOngoingOrders, function(order) {
                    return order.order_id === orderData.order_id;
                });
                
                //update the state
                this.setState({allOngoingOrdersData: currOngoingOrders});

                //update the state data
                orderData.driver_has_started_order = true;
                this.setState({startedOrderData: orderData});

                //rebuild the list
                this.buildOrderList(currOngoingOrders);
            } else {
                //display in the modal that the order could not be started
                this.setState({modalMessage: res.data.msg});
                this.setState({modalResponseStatus: res.data.orderStarted});

                //toggle the modal
                this.toggleModal();
            }
        })
        .catch(err => {});
    }

    //this is calle to build the list of ongoing orders
    buildOrderList = (ongoingOrders) => {
        var foundAStartedOrder = false;

        if(ongoingOrders.length > 0) { 
            var count = 0;
            var ongoingOrdersComponentsTemp = [];
      
            ongoingOrders.forEach(singleOrder => {
                if(singleOrder.driver_has_started_order) {
                    foundAStartedOrder = true;

                    //update the state data
                    this.setState({startedOrderData: singleOrder});
                } else {
                    ongoingOrdersComponentsTemp.push(
                        <div key={count++} className='single-ongoing-order-container'>            
                            <div id='order-id' className={'margin-bottom10'}>
                                <b>#{singleOrder.order_num_for_view}</b>
                            </div>
                            <div className='single-ongoing-order-data-row'><b>Store Address:</b>  {singleOrder.vendor_name}</div>
                            <div className='single-ongoing-order-data-row'><b>Store Name:</b>  {singleOrder.vendor_address}</div>
                            
                            <div className='single-ongoing-order-data-row margin-top3-per'><b>Client Name:</b>  {singleOrder.clients_name}</div>
                            <div className='single-ongoing-order-data-row'><b>Client Address:</b>  {singleOrder.clients_adress}</div>
                            <div className='single-ongoing-order-data-row'><b>Client Mobile Number:</b>  {singleOrder.clients_mobile_number}</div>

                            <div className='start-order-btn-container'>
                                {
                                    foundAStartedOrder || this.state.startedOrderData ?
                                        <button className='start-order-btn custom-button default-bg-color'>Start Order</button>
                                    :
                                        <button className='start-order-btn custom-button' onClick={() => this.startThisOrder(singleOrder)}>Start Order</button>
                                }
                            </div>
                        </div>
                    );
                }
            });
      
            this.setState({ongoingOrdersComponents : ongoingOrdersComponentsTemp});
        } else {
            //remove every ongoing order components
            this.setState({ongoingOrdersComponents : []});

            if(!this.state.startedOrderData) {
                this.setState({ongoingOrdersComponents : <div key={'no-order-in-list'} id='no-items-available-in-list-vendor-order'>No Ongoing Order</div>});
            }
        }

        //show the loading sign
        this.setState({showPageLoading : false });
    }

    //
    resizeStartOrderContainer = () => {
        if(this.state.startOrderContainerIsMaximized) {
            //minimize it 
            document.getElementById("start-order-container-maximized").setAttribute("id", "start-order-container-minimized");
            this.setState({startOrderContainerIsMaximized : false})
        } else {
            //maximize it
            document.getElementById("start-order-container-minimized").setAttribute("id", "start-order-container-maximized");
            this.setState({startOrderContainerIsMaximized : true})
        }
    }

    render() {
        return (
            <div id='alpha-outgoing-orders-container'>
                <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/> 

                {/* the modal */}
                <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                    <div id='modal-container'>
                        <MdClear id='close-modal' onClick={() => this.toggleModal()}/>

                        
                        <div id='response-from-order'  className={this.state.modalResponseStatus ? 'valid-color' : 'bad-color'}>
                            {this.state.modalMessage}
                        </div>
                    </div>
                </div>

                <div id='all-ongoing-orders-container'>
                    {
                        this.state.startedOrderData ? 
                            <div id='start-order-container-minimized'>     
                                <div id='small-icons-containers'>
                                    {
                                        this.state.startOrderContainerIsMaximized ? 
                                            <FiMinimize className='icons-for-started-order' onClick={() => this.resizeStartOrderContainer()}/>
                                        :
                                            <FiMaximize className='icons-for-started-order' onClick={() => this.resizeStartOrderContainer()}/>
                                    }
                                </div>

                                <div className='map-icon-container'>
                                    <img src={map_icon} className='image-take-full-space' alt='map-icon'/>
                                </div>

                                {
                                    !this.state.startedOrderData.driver_has_picked_up  ?
                                        <div id='go-pick-up-order-container'>
                                            <div className='bad-color italic center margin-top20'>Go to the store's address and pick up the order and then click on PickedUpOrder</div>
         
                                            <div id='order-id' className={'single-ongoing-order-data-row'}>
                                                <b>Order Id#</b>{this.state.startedOrderData.order_num_for_view}
                                            </div>
                                            <div className='single-ongoing-order-data-row'><b>Client Name:</b>  {this.state.startedOrderData.clients_name}</div>
                                            <div className='single-ongoing-order-data-row margin-top20'><b>Store Name:</b>  {this.state.startedOrderData.vendor_name}</div>
                                            <div className='single-ongoing-order-data-row'><b>Store Address:</b>  {this.state.startedOrderData.vendor_address}</div>

                                            <div id='map-container'>
                                                <GoogleApiWrapper address={this.state.startedOrderData.vendor_address}/>
                                            </div>

                                            <div id='destination-button-container'>
                                                <button className='custom-button' id='open-store-address-btn' onClick={() => this.openStoreAddressInMap()}>Open Store Address In Map</button>
                                                <button className='custom-button' id='picked-up-order-btn' onClick={() => this.iHavePickedUpTheOrder()}>Picked Up Order</button>
                                            </div>
                                        </div>
                                    :
                                        <div id='go-deliver-order-container'>
                                            <div className='bad-color italic center margin-top20'>Go to the {this.state.startedOrderData.clients_name}'s Address and click on Delivered Order</div>
         
                                            <div id='order-id' className={'single-ongoing-order-data-row'}>
                                                <b>Order Id#</b>{this.state.startedOrderData.order_num_for_view}
                                            </div>
                                            <div className='single-ongoing-order-data-row margin-top20'><b>Client Name:</b>  {this.state.startedOrderData.clients_name}</div>
                                            <div className='single-ongoing-order-data-row'><b>Client Address:</b>  {this.state.startedOrderData.clients_mobile_number}</div>
                                            <div className='single-ongoing-order-data-row'><b>Client Phone Number:</b>  {this.state.startedOrderData.clients_adress}</div>

                                            <div id='map-container'>
                                                <GoogleApiWrapper address={this.state.startedOrderData.clients_adress}/>
                                            </div>

                                            <div id='destination-button-container'>
                                                <button className='custom-button' id='open-store-address-btn' onClick={() => this.openClientAddressInMap()}>Open Client's Address In Map</button>
                                                <button className='custom-button' id='picked-up-order-btn' onClick={() => this.iHaveDeliveredUpTheOrder()}>Delivered Order</button>
                                            </div>
                                        </div>
                                }
                            </div>
                        :
                            ''
                    }
                    
                    {this.state.ongoingOrdersComponents}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userData: state.auth.userData,
    socketData: state.auth.socketData
});

export default connect(mapStateToProps, {storeOngoingOrdersAmount, removeOneOngoingOrder, addOneOngoingOrder})(DriverOngoingOrders);
  