//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';

//custom made components
import Footer from '../utility/Footer';
import GoogleSearchLocator from '../utility/GoogleSearchLocator';
import PageLoading from '../utility/PageLoading';
import FormData from 'form-data'

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {storeAddressData} from '../../redux/actions/utilityActions';

//images
import company_logo from '../../images/icons/logo9.png';

//icons
import { MdClear, MdAddCircleOutline } from 'react-icons/md';
import { FaRegEyeSlash, FaRegEye, FaRegCheckCircle, FaTimesCircle, FaTrashAlt} from 'react-icons/fa';

//stylings
import '../../stylings/profile.css';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally, validatePhoneNumber, validateEmail, validatePassword, validatePostalCode} from '../utility/utilityFunctions';

class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            goodColor: '#258e25',
            badColor: '#ff3333',
            defaultColor: '#a7a7a7',
            showPasswordInText: false,
            
            userDetailsTypeError : {
                errorInFirstName: false,
                errorInLastName: false,
                errorInUserName : false,
                errorInPhoneNo : false
            },

            changePasswordDetailsTypeError: {
                errorInOldPassword: false,
                errorInNewPassword: false,
                errorInRetypePassword: false
            },
            forgotPasswordDetailsTypeError: {
                errorInVerificationCode: false,
                errorInNewPassword: false,
                errorInRetypePassword: false
            },

            changeEmailDetailsTypeError: {
                errorInEmail: false,
                errorInVerificationCode: false,
                errorInPassword: false
            },

            showAddNewCard: false,
            savedCardsComponents: [],
            cardIdToBeDeleted: '',
            deleteCardMessage: '',
            showDeleteCardData: false,
            deletionFromCard: false,

            drivingInformationError: {
                errorInDriversLiscenceNumber: false
            },

            storeDetailsTypeError : {
                errorInStoreName : false,
                errorInStoreAddress : false,
                errorInPostalCode : false,
                errorInProvince : false,
                errorInCity : false,
                errorInCompanyIcon : false,
                errorInCompanyBackgroundImage : false,
                errorInTextDescription : false,
            },

            changeAdminPasswordDetailsTypeError: {
                errorInNewPassword: false,
                errorInRetypePassword: false
            },

            ihaveGottenReduxUserData: false,
            showPageLoading : false,
            response : {
                status: false,
                msg: ''
            },
            responseDriver : {
                status: false,
                msg: ''
            },
            responseVendor : {
                status: false,
                msg: ''
            },
            adminLoginResponse: {
                status: false,
                msg: ''
            },
            showModal: false,
            showPostRequestDataInModal: false,
            showChangePasswordModalInfo: false,
            showForgotPassowrdDataInModal: false,
            showChangeEmailInfoInModal: false,
            showChangeAdminPasswordOpenModal: false,
            showAddNewCardModalData: false,

            validatedAsAdmin: this.props.userData.can_access_vendors_page ? false : true, //if i am a vendor then by default i am not validated as an admin but if im not then by default i am validated as admin

            cardNumberPaymentInfo: '',
            cardNumberPaymentInfoValid : false,
            nameOnCardPaymentInfo: '',
            nameOnCardPaymentInfoValid : false,
            securityNumberPaymentInfo: '',
            securityNumberPaymentInfoValid : false,
            expiryDatePaymentInfo: '',
            expiryDatePaymentInfoValid: false
        }
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        userData: PropTypes.object,
        destinationAddressData : PropTypes.object,
        storeAddressData : PropTypes.func.isRequired
    }

    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
            this.setState({showPostRequestDataInModal: false});
            this.setState({showChangePasswordModalInfo: false});
            this.setState({showForgotPassowrdDataInModal: false});
            this.setState({showChangeEmailInfoInModal: false});
            this.setState({showChangeAdminPasswordOpenModal: false});
            this.setState({showAddNewCardModalData: false});
            
            //reset the response for calls
            this.setState({response: {status: false, msg: ''}});
        }

        //open the modal
        this.setState({showModal: !this.state.showModal});

        //clear the response fields
        this.clearResponseFields();

        if(this.state.showAddNewCardModalData) {
            this.toggleShowAddNewCard();
            this.getSavedCards();
        }
    }

    clearModalData = () => {
        this.setState({showPostRequestDataInModal: false});
        this.setState({showChangePasswordModalInfo: false});
        this.setState({showForgotPassowrdDataInModal: false});
        this.setState({showChangeAdminPasswordOpenModal: false});
        this.setState({showAddNewCardModalData: false});
    }

    clearResponseFields = () => {
        //reset the response for calls
        this.setState({response: {status: false, msg: ''}});
        this.setState({responseDriver: {status: false, msg: ''}});
        this.setState({responseVendor: {status: false, msg: ''}});
    }

    componentDidMount = () => {
        this.boilerPlateCodeForComponentUpdateAndMount();
    }

    componentDidUpdate = () => {
        this.boilerPlateCodeForComponentUpdateAndMount();
    }
    
    boilerPlateCodeForComponentUpdateAndMount = () => {
        //check if the userData is present, if so then get the personal information that we need to upload
        if(!this.state.ihaveGottenReduxUserData && this.props.userData && this.state.validatedAsAdmin) {
            //add loading icon
            addLoadingIconProcedurally('alpha-container-profile-page');

            //headers
            const token = localStorage.getItem('token');
            const config = {
            headers: {
                    'x-auth-token': token
                }
            }

            axios.get(`${apiURL}/api/profile/get_user_data/${this.props.userData._id}`, config)
            .then(res => {
                if(res.data.gotten) {        
                    document.getElementById('first-name-input').value = res.data.userInfo.firstName;
                    document.getElementById('last-name-input').value = res.data.userInfo.lastName;
                    document.getElementById('username-input').value = res.data.userInfo.userName;
                    document.getElementById('mobile-number-input').value = res.data.userInfo.mobileNo;

                    if(this.props.userData.can_access_order_details_page) {
                        this.buildSavedCards(res.data.userInfo.cardInfo);
                    }
                    else if(this.props.userData.can_access_drivers_page) {
                        document.getElementById('drivers-licence-input').value = res.data.userInfo.driversLiscenceNumber;
                    }else if(this.props.userData.can_access_vendors_page) {
                        document.getElementById('store-name-input').value = res.data.userInfo.storeName;
                        this.props.storeAddressData({address:res.data.userInfo.storeAddress, addressIsValid: true});
                        document.getElementById('postal-code-input').value = res.data.userInfo.postalCode;
                        document.getElementById('province-input').value = res.data.userInfo.province;
                        document.getElementById('city-input').value = res.data.userInfo.city;
                        document.getElementById('text-description-input').value = res.data.userInfo.description;
                    }
                } else {
                    //show the error
                    this.setState({response: {status: res.data.gotten, msg: res.data.msg}});
                    this.setState({showPostRequestDataInModal: true});

                    //open the modal
                    this.toggleModal();
                }

                //remove the loading icon
                removeLoadingIconProcedurally();
            })
            .catch(err => {
            });

            this.setState({ihaveGottenReduxUserData : true});
        }
    }

    validateUserData = () => {
        //this is to reset the response
        this.clearResponseFields();
        
        var firstName = document.getElementById('first-name-input').value;
        var lastName = document.getElementById('last-name-input').value;
        var userName = document.getElementById('username-input').value;
        var mobileNo = document.getElementById('mobile-number-input').value;
        var noErrors = [{firstNameError: true, lastNameError: true, userNameError: true, mobileNoError: true}];

        //validate firstname
        if(firstName) {
            noErrors.firstNameError = false;
            document.getElementById('first-name-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.firstNameError = true;
            document.getElementById('first-name-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //validate lastname
        if(lastName) {
            noErrors.lastNameError = false;
            document.getElementById('last-name-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.lastNameError = true;
            document.getElementById('last-name-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //validate username
        if(userName && userName.length > 5) {
            noErrors.userNameError = false;
            document.getElementById('username-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.userNameError = true;
            document.getElementById('username-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //validate number
        if(mobileNo && validatePhoneNumber(mobileNo)) {
            noErrors.mobileNoError = false;
            document.getElementById('mobile-number-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.mobileNoError = true;
            document.getElementById('mobile-number-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //update the data
        this.setState({userDetailsTypeError : {
            errorInFirstName: noErrors.firstNameError, 
            errorInLastName: noErrors.lastNameError, 
            errorInUserName: noErrors.userNameError, 
            errorInPhoneNo: noErrors.mobileNoError
        }});

        return !noErrors.firstNameError && !noErrors.lastNameError && !noErrors.userNameError && !noErrors.mobileNoError;    
    }

    updateUserDetails = () => {
        //validate user data
        if(this.validateUserData()) {
            var firstName = document.getElementById('first-name-input').value;
            var lastName = document.getElementById('last-name-input').value;
            var userName = document.getElementById('username-input').value;
            var mobileNo = document.getElementById('mobile-number-input').value;
            var userId = this.props.userData._id;

            //headers        
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
        
            //make body
            const body = JSON.stringify({firstName, lastName, userName, mobileNo, userId});

            //add loading icon procedurally
            addLoadingIconProcedurally('alpha-container-profile-page');

            axios.post(`${apiURL}/api/profile/update_user_data`, body, config)
            .then(res => {
                this.setState({response: {status: res.data.updated, msg: res.data.msg}});

                //remove the loading icon
                removeLoadingIconProcedurally();
            });
        }
    }

    //this is called to change the password
    changePasswordOpenModal = () => {
        this.setState({showChangePasswordModalInfo: true});
        this.toggleModal();
    }

    validateChangePasswordData = () => {
        //this is to reset the response
        this.clearResponseFields();
        
        var oldPassword = document.getElementById('old-password-input').value;
        var newPassword = document.getElementById('password-input').value;
        var reTypePassword = document.getElementById('re-password-input').value;
        var noErrors = [{oldpasswordError: true, newPasswordError: true, reTypePasswordError: true}];
    
        if(validatePassword(oldPassword)) {
            noErrors.oldpasswordError = false;
            document.getElementById('old-password-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.oldpasswordError = true;
            document.getElementById('old-password-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(validatePassword(newPassword)) {
            noErrors.newPasswordError = false;
            document.getElementById('password-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.newPasswordError = true;
            document.getElementById('password-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(reTypePassword === newPassword) {
            noErrors.reTypePasswordError = false;
            document.getElementById('re-password-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.reTypePasswordError = true;
            document.getElementById('re-password-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //update the data
        this.setState({changePasswordDetailsTypeError : {
            errorInOldPassword: noErrors.oldpasswordError, 
            errorInNewPassword: noErrors.newPasswordError, 
            errorInRetypePassword: noErrors.reTypePasswordError
        }});

        return !noErrors.oldpasswordError && !noErrors.newPasswordError && !noErrors.reTypePasswordError;   
    }

    toggleShowPasswordInText = () => {
        this.setState({showPasswordInText: !this.state.showPasswordInText});
    }

    updatePassword = () => {
        //validate user data
        if(this.validateChangePasswordData()) {
            var oldPassword = document.getElementById('old-password-input').value;
            var newPassword = document.getElementById('password-input').value;
            var userId = this.props.userData._id;

            //headers
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
        
            //make body
            const body = JSON.stringify({oldPassword, newPassword, userId});

            //add loading icon procedurally
            addLoadingIconProcedurally('alpha-container-profile-page');

            axios.post(`${apiURL}/api/profile/update_password_data`, body, config)
            .then(res => {
                this.setState({response: {status: res.data.updated, msg: res.data.msg}});

                //remove the loading icon
                removeLoadingIconProcedurally();
            });
        }
    }

    forgotPasswordModal = () => {
        //change the modal to show what we need it to
        this.clearModalData();
        this.clearResponseFields();
        this.setState({showForgotPassowrdDataInModal: true})

        //send the user a new verification code
        var userId = this.props.userData._id;

        //headers
        const token = localStorage.getItem('token');
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token': token
            }
        }

        //make body
        const body = JSON.stringify({userId});

        //add loading icon procedurally
        addLoadingIconProcedurally('alpha-container-profile-page');

        axios.post(`${apiURL}/api/profile/send_user_verification_code`, body, config)
        .then(res => {
            this.setState({response: {status: res.data.sentCode, msg: res.data.msg}});

            //remove the loading icon
            removeLoadingIconProcedurally();
        });
    }

    validateForgotPasswordData = () => {
        //this is to reset the response
        this.clearResponseFields();
        
        var verificationCode = document.getElementById('verification-code').value;
        var newPassword = document.getElementById('password-input').value;
        var reTypePassword = document.getElementById('re-password-input').value;
        var noErrors = [{verificationCodeError: true, newPasswordError: true, reTypePasswordError: true}];
    
        if(verificationCode.length > 0) {
            noErrors.verificationCodeError = false;
            document.getElementById('verification-code').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.verificationCodeError = true;
            document.getElementById('verification-code').style.borderColor = `${this.state.defaultColor}`;
        }

        if(validatePassword(newPassword)) {
            noErrors.newPasswordError = false;
            document.getElementById('password-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.newPasswordError = true;
            document.getElementById('password-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(reTypePassword === newPassword) {
            noErrors.reTypePasswordError = false;
            document.getElementById('re-password-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.reTypePasswordError = true;
            document.getElementById('re-password-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //update the data
        this.setState({forgotPasswordDetailsTypeError : {
            errorInVerificationCode: noErrors.verificationCodeError, 
            errorInNewPassword: noErrors.newPasswordError, 
            errorInRetypePassword: noErrors.reTypePasswordError
        }});

        return !noErrors.verificationCodeError && !noErrors.newPasswordError && !noErrors.reTypePasswordError;   
    }

    updatePasswordWithVerification = () => {    
        if(this.validateForgotPasswordData()) {
            var verificationCode = document.getElementById('verification-code').value;
            var newPassword = document.getElementById('password-input').value;
            var userId = this.props.userData._id;

            //headers        
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
        
            //make body
            const body = JSON.stringify({verificationCode, newPassword, userId});

            //add loading icon procedurally
            addLoadingIconProcedurally('alpha-container-profile-page');

            axios.post(`${apiURL}/api/profile/update_password_with_verification_code`, body, config)
            .then(res => {
                this.setState({response: {status: res.data.updated, msg: res.data.msg}});

                //remove the loading icon
                removeLoadingIconProcedurally();
            });
        }
    }

    changeEmailOpenModal = () => {
        this.setState({showChangeEmailInfoInModal: true});
        this.toggleModal();
    }

    validateChangeEmailData = () => {
        //this is to reset the response
        this.clearResponseFields();
        
        var email = document.getElementById('email-input').value;
        var verificationCode = document.getElementById('verification-code').value;
        var password = document.getElementById('password-input').value;
        var noErrors = [{emailError: false, verificationCodeError: true, passwordError: true}];

        if(validateEmail(email)) {
            noErrors.emailError = false;
            document.getElementById('email-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.emailError = true;
            document.getElementById('email-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(verificationCode.length > 0) {
            noErrors.verificationCodeError = false;
            document.getElementById('verification-code').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.verificationCodeError = true;
            document.getElementById('verification-code').style.borderColor = `${this.state.defaultColor}`;
        }

        if(validatePassword(password)) {
            noErrors.passwordError = false;
            document.getElementById('password-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.passwordError = true;
            document.getElementById('password-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //update the data
        this.setState({changeEmailDetailsTypeError : {
            errorInEmail: noErrors.emailError, 
            errorInVerificationCode: noErrors.verificationCodeError, 
            errorInPassword: noErrors.passwordError
        }});

        return !noErrors.emailError && !noErrors.verificationCodeError && !noErrors.passwordError;   
    }

    requestVerificationCode = () => {
        //send the user a new verification code
        var userId = this.props.userData._id;
        var firstName = this.props.userData.first_name;
        var lastName = this.props.userData.last_name;
        var email = document.getElementById('email-input').value;

        if(validateEmail(email)) {
            //headers
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }

            //make body
            const body = JSON.stringify({firstName, lastName, email, userId});

            //add loading icon procedurally
            addLoadingIconProcedurally('alpha-container-profile-page');

            axios.post(`${apiURL}/api/profile/send_user_verification_code_new_email`, body, config)
            .then(res => {
                this.setState({response: {status: res.data.sentCode, msg: res.data.msg}});
                
                //remove the loading icon
                removeLoadingIconProcedurally();
            });
        } else {
            this.setState({response: {status: false, msg: 'Please enter a valid Email'}});
        }
    }

    updateEmail = () => {    
        if(this.validateChangeEmailData()) {
            var email = document.getElementById('email-input').value;
            var verificationCode = document.getElementById('verification-code').value;
            var password = document.getElementById('password-input').value;
            var userId = this.props.userData._id;

            //headers        
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
        
            //make body
            const body = JSON.stringify({email, verificationCode, password, userId});

            //add loading icon procedurally
            addLoadingIconProcedurally('alpha-container-profile-page');

            axios.post(`${apiURL}/api/profile/update_email`, body, config)
            .then(res => {
                this.setState({response: {status: res.data.updated, msg: res.data.msg}});
                this.setState({showAddNewCardModalData: true})

                //remove the loading icon
                removeLoadingIconProcedurally();
            });
        }
    }

    //handlers for card info
    handleRadioButtonCardType = (ev) => {
        var all = document.getElementsByClassName('radio-buttons-card-type-profile');
        for (var i = 0; i < all.length; i++) {
            all[i].style.color = `#000000`;
        }
    }

    handleCardNumberChange = (ev) => {
        this.setState({cardNumberPaymentInfo : ev.target.value});

        if(ev.target.value && ev.target.value.length === 16 && !isNaN(ev.target.value)) {
            document.getElementById('card-number-payment-details').style.borderColor = `${this.state.goodColor}`;
            this.setState({cardNumberPaymentInfoValid : true});
        } else {
            document.getElementById('card-number-payment-details').style.borderColor = `${this.state.badColor}`;
            this.setState({cardNumberPaymentInfoValid : false});
        }
    }

    handleCardNameOnCardChange = (ev) => {
        this.setState({nameOnCardPaymentInfo : ev.target.value});

        if(ev.target.value) {
            document.getElementById('name-on-card-details').style.borderColor = `${this.state.goodColor}`;
            this.setState({nameOnCardPaymentInfoValid : true});
        } else {
            document.getElementById('name-on-card-details').style.borderColor = `${this.state.badColor}`;
            this.setState({nameOnCardPaymentInfoValid : false});
        }
    }

    handleSecurityCode = (ev) => {
        this.setState({securityNumberPaymentInfo : ev.target.value});
        if(ev.target.value && ev.target.value.length === 3 && !isNaN(ev.target.value)) {
            document.getElementById('security-code-details').style.borderColor = `${this.state.goodColor}`;
            this.setState({securityNumberPaymentInfoValid : true});
        } else {
            document.getElementById('security-code-details').style.borderColor = `${this.state.badColor}`;
            this.setState({securityNumberPaymentInfoValid : true});
        }
    }

    handleExpiryDateChange = (ev) => {
        if(ev.target.value) {
            document.getElementById('expiry-date-details').style.borderColor = `${this.state.goodColor}`;
            this.setState({expiryDatePaymentInfoValid : true});
        } else {
            document.getElementById('expiry-date-details').style.borderColor = `${this.state.badColor}`;
            this.setState({expiryDatePaymentInfoValid : false});
        }
    }
    
    validateAddNewPaymentMethod = () => {
        //check to see if a card type has been chosen -- 
        if(document.getElementById('visa-debit-profile').checked || document.getElementById('master-card-profile').checked || document.getElementById('credit-card-profile').checked) {
            //check to see if there are any errors with the entered fields
            if(this.state.cardNumberPaymentInfoValid) 
                document.getElementById('card-number-payment-details').style.borderColor = `${this.state.goodColor}`;
            else 
                document.getElementById('card-number-payment-details').style.borderColor = `${this.state.badColor}`;
            
            if(this.state.nameOnCardPaymentInfoValid) 
                document.getElementById('name-on-card-details').style.borderColor = `${this.state.goodColor}`;
            else 
                document.getElementById('name-on-card-details').style.borderColor = `${this.state.badColor}`;

            if(this.state.securityNumberPaymentInfoValid) 
                document.getElementById('security-code-details').style.borderColor = `${this.state.goodColor}`;
            else 
                document.getElementById('security-code-details').style.borderColor = `${this.state.badColor}`;

            if(this.state.expiryDatePaymentInfoValid) 
                document.getElementById('expiry-date-details').style.borderColor = `${this.state.goodColor}`;
            else 
                document.getElementById('expiry-date-details').style.borderColor = `${this.state.badColor}`;
            
            if(this.state.cardNumberPaymentInfoValid && this.state.nameOnCardPaymentInfoValid && this.state.securityNumberPaymentInfoValid && this.state.expiryDatePaymentInfoValid) {
                var cardType = '';
                
                if(document.getElementById('visa-debit-profile').checked) {
                    cardType = 'visa';
                } else if(document.getElementById('master-card-profile').checked) {
                    cardType = 'master';
                } else if(document.getElementById('credit-card-profile').checked) {
                    cardType = 'credit';
                }
                
                //add loading icon
                addLoadingIconProcedurally('alpha-container-profile-page');
                
                //this is the token
                const token = localStorage.getItem('token');

                //headers
                const config = {
                    headers: {
                        'Content-Type': 'application/json',
                        'x-auth-token': token
                    }
                }

                const body = JSON.stringify({
                    cardType,
                    cardNumber: document.getElementById('card-number-payment-details').value,
                    cardOwner: document.getElementById('name-on-card-details').value,
                    secNo: document.getElementById('security-code-details').value,
                    expDat: document.getElementById('expiry-date-details').value,
                    userId: this.props.userData._id
                });
                
                axios.post(`${apiURL}/api/profile/add_card`, body, config)
                .then(res => {
                    removeLoadingIconProcedurally();

                    //open the modal
                    this.toggleModal();
                    
                    //show the error
                    this.setState({response: {status: res.data.saved, msg: res.data.msg}});
                    this.setState({showAddNewCardModalData: true});
                })
                .catch(err => {
                });
            }
        } else {
            var all = document.getElementsByClassName('radio-buttons-card-type-profile');
            for (var i = 0; i < all.length; i++) {
                all[i].style.color = `${this.state.badColor}`;
            }
        }
    }

    toggleShowAddNewCard = () => {
        if(!this.state.deletionFromCard) {
            this.setState({showAddNewCard: !this.state.showAddNewCard})
        
            //clear the entries
            document.getElementById('card-number-payment-details').value = '';
            document.getElementById('name-on-card-details').value = ''
            document.getElementById('security-code-details').value = ''
            document.getElementById('expiry-date-details').value = ''

            this.setState({cardNumberPaymentInfoValid : false});
            this.setState({nameOnCardPaymentInfoValid : false});
            this.setState({securityNumberPaymentInfoValid : false});
            this.setState({expiryDatePaymentInfoValid : false});
        }
    }

    getSavedCards = () => {
        //add loading icon
        addLoadingIconProcedurally('alpha-container-profile-page');

        //headers
        const token = localStorage.getItem('token');
        const config = {
            headers: {
                'x-auth-token' : token
            }
        }

        axios.get(`${apiURL}/api/profile/get_cards_info/${this.props.userData._id}`, config)
        .then(res => {
            if(res.data.gotten) {
                if(this.props.userData.can_access_order_details_page) {
                    this.buildSavedCards(res.data.cardsInfo);
                }
            }
        });

        removeLoadingIconProcedurally();
        this.setState({deletionFromCard: false});
    }

    buildSavedCards = (savedCards) => {
        var tempSavedCardsComponent = [];
        var count = 0;

        if(savedCards.length > 0) {
            savedCards.forEach(singleCard => {
                var cardType = singleCard.card_type;

                tempSavedCardsComponent.push(
                    <div className='single-saved-card-container' key={count++}>
                        <div className='delete-icon-single-card' onClick={() => this.openDeleteCard(singleCard._id, singleCard.card_no)}><FaTrashAlt /></div>
                        <div className='card-number-single-card'>**** **** **** {singleCard.card_no}</div>
                        <div className='expiry-date-single-card'>{singleCard.expiry_dat}</div>
                    
                        <div className='card-type-single-card'>{cardType.toUpperCase()}</div>
                    </div>
                )
            });

        } else {
            tempSavedCardsComponent.push(
                <div className='full-width center bold margin-top3-per' key={count++}>
                    No Cards Saved
                </div>
            );
        }

        //update the list
        this.setState({savedCardsComponents : tempSavedCardsComponent});
    }

    openDeleteCard = (cardId, cardNo) => {
        //this is called to update the card number that is meant to be edited, change the message of the deletion, update the show the deleteCard's Personal modal and also say that this deletion
        //is occuring from the card which would be used to prevent the toggling of [add new card]
        this.setState({cardIdToBeDeleted: cardId});
        this.setState({deleteCardMessage: `Are you sure you want to delete the card ending with ${cardNo}?`});
        this.setState({showDeleteCardData: true});
        this.setState({deletionFromCard: true});
    }

    noToDeleteCard = () => {
        //this is used to hid the deleteCard's Personal modal and also say that this deletion is not occuring from the card
        this.setState({showDeleteCardData: false});
        this.setState({deletionFromCard: false});
    }

    yesToDeleteCard = () => {
        //add loading icon
        addLoadingIconProcedurally('alpha-container-profile-page');

        //headers
        const config = {
            headers: {
                'x-auth-token': localStorage.getItem('token')
            }
        }
        
        //get all products for this vendor
        axios.get(`${apiURL}/api/profile/del_card/${this.state.cardIdToBeDeleted}/${this.props.userData._id}`, config)
        .then(res => {
            removeLoadingIconProcedurally();
            this.setState({showDeleteCardData: false});  

            //open the modal
            this.toggleModal();
            
            //show the error
            this.setState({response: {status: res.data.deleted, msg: res.data.msg}});
            this.setState({showAddNewCardModalData: true});
        });
    }






    //this is to validate the vendor data
    validateDriverUpData = () => {
        this.clearResponseFields();
        var driversLicenceNumber = document.getElementById('drivers-licence-input').value;
        var noErrors = [{driversLicenceNumberError: true}];

        if(driversLicenceNumber.length === 15 && !isNaN(driversLicenceNumber)) {
            noErrors.driversLicenceNumberError = false;
            document.getElementById('drivers-licence-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.driversLicenceNumberError = true;
            document.getElementById('drivers-licence-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //update the data
        this.setState({drivingInformationError : {
            errorInDriversLiscenceNumber: noErrors.driversLicenceNumberError
        }});

        return !noErrors.driversLicenceNumberError;
    }

    updateDriverDetails = () => {
        if(this.validateDriverUpData()) {
            var driversLicenceNumber = document.getElementById('drivers-licence-input').value;
            var userId = this.props.userData._id;

            //headers        
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
        
            //make body
            const body = JSON.stringify({driversLicenceNumber, userId});

            //add loading icon procedurally
            addLoadingIconProcedurally('alpha-container-profile-page');

            axios.post(`${apiURL}/api/profile/update_driver_info`, body, config)
            .then(res => {
                this.setState({responseDriver: {status: res.data.updated, msg: res.data.msg}});

                //remove the loading icon
                removeLoadingIconProcedurally();
            });
        }
    }







    validateVendorData = () => {
        this.clearResponseFields();
        var noErrors = [{storeNameError: true, storeAddressError: true, postalCodeError: true, provinceError: true, 
            cityError: true, companyIconError: true, companyBgImageError: true, textDesciptionError: true}];

        var storeName = document.getElementById('store-name-input').value;
        var postalCode = document.getElementById('postal-code-input').value;
        var province = document.getElementById('province-input').value;
        var city = document.getElementById('city-input').value;
        var companyIcon = document.getElementById('company-icon-input').files[0];
        var companyBgImage = document.getElementById('company-background-image-input').files[0];
        var textDesciption = document.getElementById('text-description-input').value;

        if(storeName && storeName.length > 5) {
            noErrors.storeNameError = false;
            document.getElementById('store-name-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.storeNameError = true;
            document.getElementById('store-name-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(postalCode && validatePostalCode(postalCode)) {
            noErrors.postalCodeError = false;
            document.getElementById('postal-code-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.postalCodeError = true;
            document.getElementById('postal-code-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(province) {
            noErrors.provinceError = false;
            document.getElementById('province-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.provinceError = true;
            document.getElementById('province-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(city) {
            noErrors.cityError = false;
            document.getElementById('city-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.cityError = true;
            document.getElementById('city-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(!companyIcon) {
            noErrors.companyIconError = false;
            document.getElementById('company-icon-input').style.borderColor = `${this.state.goodColor}`;
        }else if(companyIcon && companyIcon.type === 'image/png') {
            noErrors.companyIconError = false;
            document.getElementById('company-icon-input').style.borderColor = `${this.state.goodColor}`;
        } 
        else {
            noErrors.companyIconError = true;
            document.getElementById('company-icon-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //if there is no image its okay, if there is an image and the image is 'image/jpeg' its okay else its not okat
        if(!companyBgImage) {
            noErrors.companyBgImageError = false;
            document.getElementById('company-background-image-input').style.borderColor = `${this.state.goodColor}`;
        }else if(companyBgImage && companyBgImage.type === 'image/jpeg') {
            noErrors.companyBgImageError = false;
            document.getElementById('company-background-image-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.companyBgImageError = true;
            document.getElementById('company-background-image-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(textDesciption && textDesciption.length > 100) {
            noErrors.textDesciptionError = false;
            document.getElementById('text-description-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.textDesciptionError = true;
            document.getElementById('text-description-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //update the data
        this.setState({storeDetailsTypeError : {
            errorInStoreName: noErrors.storeNameError, 
            errorInStoreAddress: !this.props.destinationAddressData.addressIsValid, 
            errorInPostalCode: noErrors.postalCodeError, 
            errorInProvince: noErrors.provinceError,
            errorInCity: noErrors.cityError,
            errorInCompanyIcon: noErrors.companyIconError, 
            errorInCompanyBackgroundImage: noErrors.companyBgImageError,
            errorInTextDescription: noErrors.textDesciptionError}
        });

        return !noErrors.storeNameError && this.props.destinationAddressData.addressIsValid && !noErrors.postalCodeError && !noErrors.provinceError && 
        !noErrors.cityError && !noErrors.companyIconError && !noErrors.companyBgImageError && !noErrors.textDesciptionError;
    }

    updateStoreDetails = () => {
        this.clearResponseFields();

        if(this.validateVendorData()) {
            var storeName = document.getElementById('store-name-input').value;
            var storeNameAndDate = storeName.replace(/\s+/g, '-').toLowerCase() +'-'+ new Date().getTime();
            var postalCode = document.getElementById('postal-code-input').value;
            var province = document.getElementById('province-input').value;
            var city = document.getElementById('city-input').value;
            var companyIcon = document.getElementById('company-icon-input').files[0];
            var companyBgImage = document.getElementById('company-background-image-input').files[0];
            var textDesciption = document.getElementById('text-description-input').value;

            //if we have images its a different type of message than when we dont
            if(companyIcon || companyBgImage) {      
                //headers
                const config = {
                    headers: {
                        'Content-Type': 'application/json',
                        'x-auth-token': localStorage.getItem('token')
                    }
                }

                //build our form data
                let data = new FormData();
                data.append('storeName', storeName);
                data.append('storeAddress', this.props.destinationAddressData.address);
                data.append('postalCode', postalCode);
                data.append('province', province);
                data.append('city', city);
                data.append('textDesciption', textDesciption);
                data.append('userId', this.props.userData._id);

                //add the images if they exists
                if(companyIcon) {
                    data.append('companyImages', companyIcon, 'logo-'+storeNameAndDate+'.png');
                    data.append('companyIconImgName', 'logo-'+storeNameAndDate);
                }
                if(companyBgImage) {
                    data.append('companyImages', companyBgImage, 'bg-img-'+storeNameAndDate+'.jpg');
                    data.append('companyBgImageName', 'bg-img-'+storeNameAndDate);
                }
                
                //add loading icon procedurally
                addLoadingIconProcedurally('alpha-container-profile-page');

                axios.post(`${apiURL}/api/profile/update_vendor_data_with_pic`, data, config)
                .then(res => {
                    //remove loading icon procedurally
                    removeLoadingIconProcedurally();

                    this.setState({responseVendor: {status: res.data.updated, msg: res.data.msg}});
                }).catch(err => {
                //handle error
                });
            } else {
                //headers        
                const config = {
                    headers: {
                        'Content-Type': 'application/json',
                        'x-auth-token': localStorage.getItem('token')
                    }
                }
            
                //make body
                const body = JSON.stringify({storeName, 
                                            storeAddress: this.props.destinationAddressData.address,
                                            postalCode, 
                                            province, 
                                            city, 
                                            textDesciption, 
                                            userId: this.props.userData._id});

                //add loading icon procedurally
                addLoadingIconProcedurally('alpha-container-profile-page');

                axios.post(`${apiURL}/api/profile/update_vendor_data_no_pic`, body, config)
                .then(res => {
                    //remove loading icon procedurally
                    removeLoadingIconProcedurally();

                    this.setState({responseVendor: {status: res.data.updated, msg: res.data.msg}});
                }).catch(err => {
                //handle error
                });
            }
        }
    }


    validateAdmin = () => {
        var adminPassword = document.getElementById('admin-password-input').value;

        if(validatePassword(adminPassword)) {
            //good
            document.getElementById('admin-password-input').style.borderColor = `${this.state.goodColor}`;

            var vendorId = this.props.userData.vendor_id_for_vendor;
            //headers        
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
        
            //make body
            const body = JSON.stringify({adminPassword, vendorId});

            //add loading icon procedurally
            addLoadingIconProcedurally('alpha-container-profile-page');

            axios.post(`${apiURL}/api/profile/validate_admin`, body, config)
            .then(res => {
                if(!res.data.validated) {
                    this.setState({adminLoginResponse: {status: res.data.validated, msg: res.data.msg}});
                } else {
                    this.boilerPlateCodeForComponentUpdateAndMount();

                    this.setState({validatedAsAdmin: true})
                }

                //remove the loading icon
                removeLoadingIconProcedurally();
            });
        } else {
            document.getElementById('admin-password-input').style.borderColor = `${this.state.badColor}`;
        }
    }

    changeAdminPasswordOpenModal = () => {
        this.setState({showChangeAdminPasswordOpenModal: true});
        this.toggleModal();
    }

    validateChangeAdminPasswordData = () => {
        //this is to reset the response
        this.clearResponseFields();
        
        var newPassword = document.getElementById('admin-password-input').value;
        var reTypePassword = document.getElementById('re-admin-password-input').value;
        var noErrors = [{newPasswordError: true, reTypePasswordError: true}];
    
        if(validatePassword(newPassword)) {
            noErrors.newPasswordError = false;
            document.getElementById('admin-password-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.newPasswordError = true;
            document.getElementById('admin-password-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(reTypePassword === newPassword) {
            noErrors.reTypePasswordError = false;
            document.getElementById('re-admin-password-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.reTypePasswordError = true;
            document.getElementById('re-admin-password-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //update the data
        this.setState({changeAdminPasswordDetailsTypeError : {
            errorInNewPassword: noErrors.newPasswordError, 
            errorInRetypePassword: noErrors.reTypePasswordError
        }});

        return !noErrors.newPasswordError && !noErrors.reTypePasswordError;   
    }

    updateAdminPassword = () => {
        if(this.validateChangeAdminPasswordData()) {
            var adminPassword = document.getElementById('admin-password-input').value;
            var vendorId = this.props.userData.vendor_id_for_vendor;

            //headers        
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
        
            //make body
            const body = JSON.stringify({adminPassword, vendorId});

            //add loading icon procedurally
            addLoadingIconProcedurally('alpha-container-profile-page');

            axios.post(`${apiURL}/api/profile/change_admin_password`, body, config)
            .then(res => {
                this.setState({response: {status: res.data.updated, msg: res.data.msg}});

                //remove the loading icon
                removeLoadingIconProcedurally();
            });
        }
    }

    render() {
        return (
            <div id='alpha-container-profile-page'>
                <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/> 
                
                {
                    this.state.showDeleteCardData ? 
                        <div className='modal-background' style={{display:'block'}}>
                            <div id='modal-container'>
                                <div id='edit-modal-title' className='title'>Delete Card Info</div>

                                <div id='delete-addon-group-message' className='center margin-top20'>
                                    {this.state.deleteCardMessage}
                                </div>

                                <div className='center margin-top20'>
                                    <button id='yes-to-delete-addon-button' className='custom-button' onClick={() => this.yesToDeleteCard()}>Yes</button>
                                    <button id='no-to-delete-addon-button' className='custom-button' onClick={() => this.noToDeleteCard()}>No</button>
                                </div>
                            </div>
                        </div>
                    :
                        ''
                }

                {/* if this is a vendor then when the user opens profile tell them to enter their admin password */}
                {
                    this.props.userData.can_access_vendors_page && !this.state.validatedAsAdmin ?
                        <div>
                            <div id='some-space-above'>
                            </div>

                            <div className='modal-background'>
                                <div id='modal-container' className='sign-up-modal-container'>
                                    <div className='sub-section-title center margin-top10'>Validate with Admin Password</div>

                                    <div className='margin-top10'>
                                        <b>Password:</b> 
                                        <input type='password' className='input-entries' id='admin-password-input'/>
                                    </div>

                                    <div className='response-from-database-in-modal center margin-top20' style={this.state.adminLoginResponse.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.adminLoginResponse.msg}</div>
                        
                                    <div className='center'>
                                        <button className='custom-button' id='update-user-details-button' onClick={() => this.validateAdmin()}>
                                            Validate
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    :
                        <div>
                            {/* the modal */}
                            <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                                <div id='modal-container' className='sign-up-modal-container'>
                                    {
                                        // i dont want the clear if this is the type of error that occurs
                                        this.state.showPostRequestDataInModal ? 
                                            <div id='post-sign-up-modal-container' className={this.state.response.status ?  'valid-color' : 'bad-color'}>
                                                {this.state.response.msg}
                                            </div>
                                        : 
                                            <div>
                                                <div className='overflow-auto'><MdClear id='close-modal' onClick={() => this.toggleModal()}/></div>

                                                {
                                                    this.state.showAddNewCardModalData ? 
                                                        <div id='add-new-card-modal-container' className={this.state.response.status ?  'valid-color center' : 'bad-color center'}>
                                                                {
                                                                    this.state.response.status ? 
                                                                    <FaRegCheckCircle className='times-or-check-icons'/>
                                                                :
                                                                    <FaTimesCircle className='times-or-check-icons'/>
                                                                }
                                                            {this.state.response.msg}
                                                        </div>
                                                    :
                                                        ''
                                                }

                                                {
                                                    this.state.showChangePasswordModalInfo ?
                                                        <div id='change-password-modal'>
                                                            <div className='sub-section-title center margin-top10'>Change Password</div>

                                                            <div id='eye-icons-container'>
                                                                {
                                                                    this.state.showPasswordInText ? 
                                                                        <FaRegEyeSlash className='eye-icons'  onClick={() => this.toggleShowPasswordInText()}/>
                                                                    :
                                                                        <FaRegEye className='eye-icons'  onClick={() => this.toggleShowPasswordInText()}/>
                                                                }
                                                            </div>

                                                            <div className='margin-top10'>
                                                                <b>Old Password:</b> 
                                                                <input type={this.state.showPasswordInText ? 'text': 'password'} className='input-entries' id='old-password-input' onChange={this.validateChangePasswordData}/>
                                                                <div className='error-message' style={this.state.changePasswordDetailsTypeError.errorInOldPassword ? {display:'block'} : {display:'none'}}>
                                                                    Invalid Password | password must be more than 6 characters | Must have an upper case and Lowercase | Must have special Symbol ($) and numeric character
                                                                </div>
                                                            </div>

                                                            <div className='margin-top10'>
                                                                <b>Password:</b> 
                                                                <input type={this.state.showPasswordInText ? 'text': 'password'} className='input-entries' id='password-input' onChange={this.validateChangePasswordData}/>
                                                                <div className='error-message' style={this.state.changePasswordDetailsTypeError.errorInNewPassword ? {display:'block'} : {display:'none'}}>
                                                                    Invalid Password | password must be more than 6 characters | Must have an upper case and Lowercase | Must have special Symbol ($) and numeric character
                                                                </div>
                                                            </div>
                                                            
                                                            <div className='margin-top10'>
                                                                <b>Re-type Password:</b> 
                                                                <input type={this.state.showPasswordInText ? 'text': 'password'} className='input-entries' id='re-password-input'  onChange={this.validateChangePasswordData}/>
                                                                <div className='error-message' style={this.state.changePasswordDetailsTypeError.errorInRetypePassword ? {display:'block'} : {display:'none'}}>
                                                                    Re-type password must be the same as new password
                                                                </div>
                                                            </div>

                                                            <div className='response-from-database-in-modal center margin-top20' style={this.state.response.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.response.msg}</div>
                                    
                                                            <div className='center'>
                                                                <button className='custom-button' id='update-user-details-button' onClick={() => this.updatePassword()}>
                                                                    Update
                                                                </button>

                                                                <button className='custom-button' id='change-password-button' onClick={() => this.forgotPasswordModal()}>
                                                                    Forgot Password
                                                                </button>
                                                            </div>
                                                        </div>
                                                    :
                                                        ''
                                                }

                                                {
                                                    this.state.showForgotPassowrdDataInModal ?
                                                        <div id='change-password-modal'>
                                                            <div className='sub-section-title center margin-top10'>Forgot Password</div>

                                                            <div id='eye-icons-container'>
                                                                {
                                                                    this.state.showPasswordInText ? 
                                                                        <FaRegEyeSlash className='eye-icons'  onClick={() => this.toggleShowPasswordInText()}/>
                                                                    :
                                                                        <FaRegEye className='eye-icons'  onClick={() => this.toggleShowPasswordInText()}/>
                                                                }
                                                            </div>

                                                            <div className='margin-top10'>
                                                                <b>Verification Code:</b> 
                                                                <input type='text' className='input-entries' id='verification-code' onChange={this.validateForgotPasswordData}/>
                                                                <div className='error-message' style={this.state.forgotPasswordDetailsTypeError.errorInVerificationCode ? {display:'block'} : {display:'none'}}>
                                                                    Enter the code
                                                                </div>
                                                            </div>

                                                            <div className='margin-top10'>
                                                                <b>Password:</b> 
                                                                <input type={this.state.showPasswordInText ? 'text': 'password'} className='input-entries' id='password-input' onChange={this.validateForgotPasswordData}/>
                                                                <div className='error-message' style={this.state.forgotPasswordDetailsTypeError.errorInNewPassword ? {display:'block'} : {display:'none'}}>
                                                                    Invalid Password | password must be more than 6 characters | Must have an upper case and Lowercase | Must have special Symbol ($) and numeric character
                                                                </div>
                                                            </div>
                                                            
                                                            <div className='margin-top10'>
                                                                <b>Re-type Password:</b> 
                                                                <input type={this.state.showPasswordInText ? 'text': 'password'} className='input-entries' id='re-password-input'  onChange={this.validateForgotPasswordData}/>
                                                                <div className='error-message' style={this.state.forgotPasswordDetailsTypeError.errorInRetypePassword ? {display:'block'} : {display:'none'}}>
                                                                    Re-type password must be the same as new password
                                                                </div>
                                                            </div>

                                                            <div className='response-from-database-in-modal center margin-top20' style={this.state.response.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.response.msg}</div>
                                    
                                                            <div className='center'>
                                                                <button className='custom-button' id='update-user-details-button' onClick={() => this.updatePasswordWithVerification()}>
                                                                    Update
                                                                </button>
                                                            </div>
                                                        </div>
                                                    :
                                                        ''
                                                }


                                                {
                                                    this.state.showChangeEmailInfoInModal ?
                                                        <div id='change-email-modal'>
                                                            <div className='sub-section-title center margin-top10'>Change Email</div>
                                                            <div className='center italic bad-color'>Just enter Email to request verification code and then enter Email, Verification Code and Password to Update Email</div>

                                                            <div id='eye-icons-container'>
                                                                {
                                                                    this.state.showPasswordInText ? 
                                                                        <FaRegEyeSlash className='eye-icons margin-top20'  onClick={() => this.toggleShowPasswordInText()}/>
                                                                    :
                                                                        <FaRegEye className='eye-icons margin-top20'  onClick={() => this.toggleShowPasswordInText()}/>
                                                                }
                                                            </div>

                                                            <div className='margin-top20'>
                                                                <b>New Email:</b> 
                                                                <input type="text" className='input-entries' id='email-input' onChange={this.validateChangeEmailData}/>
                                                                <div className='error-message' style={this.state.changeEmailDetailsTypeError.errorInEmail ? {display:'block'} : {display:'none'}}>
                                                                    Invalid email
                                                                </div>
                                                            </div>

                                                            <div className='margin-top10'>
                                                                <b>New Email's Verification Code:</b> 
                                                                <input type='text' className='input-entries' id='verification-code' onChange={this.validateChangeEmailData}/>
                                                                <div className='error-message' style={this.state.changeEmailDetailsTypeError.errorInVerificationCode ? {display:'block'} : {display:'none'}}>
                                                                    Enter the Verification Code that was sent to your Email
                                                                </div>
                                                            </div>

                                                            <div className='margin-top10'>
                                                                <b>Password:</b> 
                                                                <input type={this.state.showPasswordInText ? 'text': 'password'} className='input-entries' id='password-input' onChange={this.validateChangeEmailData}/>
                                                                <div className='error-message' style={this.state.changeEmailDetailsTypeError.errorInPassword ? {display:'block'} : {display:'none'}}>
                                                                    Invalid Password | password must be more than 6 characters | Must have an upper case and Lowercase | Must have special Symbol ($) and numeric character
                                                                </div>
                                                            </div>

                                                            <div className='response-from-database-in-modal center margin-top20' style={this.state.response.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.response.msg}</div>
                                    
                                                            <div className='center'>
                                                                <button className='custom-button' id='update-user-details-button' onClick={() => this.updateEmail()}>
                                                                    Update
                                                                </button>

                                                                <button className='custom-button' id='change-password-button' onClick={() => this.requestVerificationCode()}>
                                                                    Request Verification Code
                                                                </button>
                                                            </div>
                                                        </div>
                                                    :
                                                        ''
                                                }

                                                {
                                                    this.state.showChangeAdminPasswordOpenModal ?
                                                        <div id='change-password-modal'>
                                                            <div className='sub-section-title center margin-top10'>Change Admin Password</div>

                                                            <div id='eye-icons-container'>
                                                                {
                                                                    this.state.showPasswordInText ? 
                                                                        <FaRegEyeSlash className='eye-icons'  onClick={() => this.toggleShowPasswordInText()}/>
                                                                    :
                                                                        <FaRegEye className='eye-icons'  onClick={() => this.toggleShowPasswordInText()}/>
                                                                }
                                                            </div>

                                                            <div className='margin-top10'>
                                                                <b>Password:</b> 
                                                                <input type={this.state.showPasswordInText ? 'text': 'password'} className='input-entries' id='admin-password-input' onChange={this.validateChangeAdminPasswordData}/>
                                                                <div className='error-message' style={this.state.changeAdminPasswordDetailsTypeError.errorInNewPassword ? {display:'block'} : {display:'none'}}>
                                                                    Invalid Admin Password | password must be more than 6 characters | Must have an upper case and Lowercase | Must have special Symbol ($) and numeric character
                                                                </div>
                                                            </div>
                                                            
                                                            <div className='margin-top10'>
                                                                <b>Re-type Password:</b> 
                                                                <input type={this.state.showPasswordInText ? 'text': 'password'} className='input-entries' id='re-admin-password-input'  onChange={this.validateChangeAdminPasswordData}/>
                                                                <div className='error-message' style={this.state.changeAdminPasswordDetailsTypeError.errorInRetypePassword ? {display:'block'} : {display:'none'}}>
                                                                    Re-type Admin password must be the same as new password
                                                                </div>
                                                            </div>

                                                            <div className='response-from-database-in-modal center margin-top20' style={this.state.response.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.response.msg}</div>
                                    
                                                            <div className='center'>
                                                                <button className='custom-button' id='update-user-details-button' onClick={() => this.updateAdminPassword()}>
                                                                    Update
                                                                </button>
                                                            </div>
                                                        </div>
                                                    :
                                                        ''
                                                }
                                            </div>
                                    }
                                </div>
                            </div>

                            <div id='container-profile-page'>
                                <div className='absolute-company-logo'>
                                    <img src={company_logo} className='image-take-full-space' alt='company_logo'/>
                                </div>

                                <div className='title-for-edit-profile'><b>Edit Profile</b></div>

                                <div id='user-details-section'>
                                    <div className='sub-section-title'>Edit Personal Information</div>

                                    <div className='margin-top10'>
                                        <b>FirstName:</b>
                                        <input type="text" className='input-entries' id='first-name-input' onChange={this.validateUserData}/>
                                        <div className='error-message' style={this.state.userDetailsTypeError.errorInFirstName ? {display:'block'} : {display:'none'}}>
                                            Invalid First Name
                                        </div>
                                    </div>

                                    <div className='margin-top10'>
                                        <b>LastName:</b>
                                        <input type="text" className='input-entries' id='last-name-input' onChange={this.validateUserData}/>
                                        <div className='error-message' style={this.state.userDetailsTypeError.errorInLastName ? {display:'block'} : {display:'none'}}>
                                            Invalid Last Name
                                        </div>
                                    </div>

                                    <div className='margin-top10'>
                                        <b>Username:</b>
                                        <input type="text" className='input-entries' id='username-input' onChange={this.validateUserData}/>
                                        <div className='error-message' style={this.state.userDetailsTypeError.errorInUserName ? {display:'block'} : {display:'none'}}>
                                            Invalid Username | username must be more than 5 letters
                                        </div>
                                    </div>
                                    
                                    <div className='margin-top10'>
                                        <b>Mobile Number:</b> 
                                        <input type="text" id='mobile-number-input' className='input-entries' onChange={this.validateUserData} maxLength='11'/>
                                        <div className='error-message' style={this.state.userDetailsTypeError.errorInPhoneNo ? {display:'block'} : {display:'none'}}>
                                            Invalid Number | Please Enter your phone number
                                        </div>
                                    </div>  

                                    {/* <div className='margin-top10'>
                                        <b>Email:</b> 
                                        <input type="text" className='input-entries' id='email-input' onChange={this.validateUserData}/>
                                        <div className='error-message' style={this.state.userDetailsTypeError.errorInEmail ? {display:'block'} : {display:'none'}}>
                                            Invalid email
                                        </div>
                                    </div> */}

                                    {/* <div className='margin-top10'>
                                        <b>Password:</b> 
                                        <input type="password" className='input-entries' id='password-input' onChange={this.validateUserData}/>
                                        <div className='error-message' style={this.state.userDetailsTypeError.errorInPassword ? {display:'block'} : {display:'none'}}>
                                            Invalid Password | password must be more than 6 characters | Must have an upper case and Lowercase | Must have special Symbol ($) and numeric character
                                        </div>
                                    </div>
                                    
                                    <div className='margin-top10'>
                                        <b>Re-type Password:</b> 
                                        <input type="password" className='input-entries' id='re-password-input'  onChange={this.validateUserData}/>
                                        <div className='error-message' style={this.state.userDetailsTypeError.errorInRetypePassword ? {display:'block'} : {display:'none'}}>
                                            Re-type password must be the same as password
                                        </div>
                                    </div> */}

                                    <div className='response-from-database-in-modal center margin-top20' style={this.state.response.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.response.msg}</div>
                                
                                    <div className='center'>
                                        <button className='custom-button' id='update-user-details-button' onClick={() => this.updateUserDetails()}>
                                            Update
                                        </button>

                                        <button className='custom-button' id='change-password-button' onClick={() => this.changePasswordOpenModal()}>
                                            Change Password
                                        </button>
                                        {
                                            this.props.userData.can_access_vendors_page ?
                                                <button className='custom-button' id='change-password-button' onClick={() => this.changeAdminPasswordOpenModal()}>
                                                    Change Admin Password
                                                </button>
                                            :
                                                ''
                                        }

                                        <button className='custom-button' id='change-password-button' onClick={() => this.changeEmailOpenModal()}>
                                            Change Email
                                        </button>
                                    </div>
                                </div>{/* end of user-details-section */}
        
                                {
                                    this.props.userData ?
                                        this.props.userData.can_access_order_details_page ?
                                            <div id='payment-info-profile-page-section'>
                                                <div className='sub-section-title-payment'>Payment Information</div>
                                                <div className='sub-sub-info'>Maximum of 4 cards</div>
                                                <div id='open-close-add-new-card-container'>
                                                    {
                                                        !this.state.showAddNewCard ? 
                                                            <div className='open-close-add-new-card-icon' onClick={() => this.toggleShowAddNewCard()}><MdAddCircleOutline /></div>
                                                        :
                                                            <div className='open-close-add-new-card-icon' onClick={() => this.toggleShowAddNewCard()}><MdClear /></div>
                                                    }
                                                </div>

                                                <div id='saved-cards-container'>
                                                    <b className='width-100-percent'>Saved Cards</b>

                                                    <div id='saved-cards'>
                                                        {this.state.savedCardsComponents}
                                                    </div>
                                                </div>

                                                <div id='new-cards-container' style={this.state.showAddNewCard ? {display:'block'} : {display:'none'}}>
                                                    <b className='width-100-percent'>Add New Card</b>

                                                    <div id='new-cards'> 
                                                        <b className='width-100-percent'>Card Type:</b>
                                                        <div className='radio-buttons-card-type-profile'><input type="radio" onChange={this.handleRadioButtonCardType} name="card-type" id="visa-debit-profile"/> Visa Debit  </div>
                                                        <div className='radio-buttons-card-type-profile'><input type="radio" onChange={this.handleRadioButtonCardType} name="card-type" id="credit-card-profile"/> Credit Card</div>
                                                        <div className='radio-buttons-card-type-profile'><input type="radio" onChange={this.handleRadioButtonCardType} name="card-type" id="master-card-profile"/> Master Card</div>

                                                        <b className='width-100-percent'>Card Number:</b> <input type="text" id='card-number-payment-details' minLength='16' maxLength='16' className='payment-details-input-profile' onChange={this.handleCardNumberChange}/>
                                                        <b className='width-100-percent'>Name on Card:</b> <input type="text" id='name-on-card-details' className='payment-details-input-profile' onChange={this.handleCardNameOnCardChange}/>
                                                        <b className='width-100-percent'>Security Number:</b> <input type="text" id='security-code-details' minLength='3' maxLength='3' className='payment-details-input-profile' onChange={this.handleSecurityCode}/>
                                                        <b className='width-100-percent'>Expiry Date:</b> <input type="date" id='expiry-date-details' className='payment-details-input-profile' onChange={this.handleExpiryDateChange}/>

                                                        <div className='left'>
                                                            <button className='custom-button' id='update-user-details-button' onClick={() => this.validateAddNewPaymentMethod()}>
                                                                Add this Card
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        :
                                            ''
                                    :
                                        ''
                                }

                                {
                                    this.props.userData ?
                                        this.props.userData.can_access_drivers_page ?
                                            <div id='drivers-details-section'>
                                                <div className='sub-section-title'>Edit Driving Information</div>

                                                <div className='margin-top10'>
                                                    <b>Drivers Licence Number:</b>
                                                    <input type="text" id='drivers-licence-input' className='input-entries' onChange={this.validateDriverUpData} maxLength='15'/>
                                                    <div className='error-message' style={this.state.drivingInformationError.errorInDriversLiscenceNumber ? {display:'block'} : {display:'none'}}>
                                                        Please enter drivers liscence
                                                    </div>
                                                </div>

                                                <div className='response-from-database-in-modal center margin-top20' style={this.state.responseDriver.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.responseDriver.msg}</div>
                    
                                                <div className='center'>
                                                    <button className='custom-button' id='update-user-details-button' onClick={() => this.updateDriverDetails()}>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        :
                                            ''
                                    :
                                        ''
                                }






                                {
                                    this.props.userData ? 
                                        this.props.userData.can_access_vendors_page ?
                                            <div id='store-details-section'>
                                                <div className='sub-section-title'>Edit Store Information</div>

                                                <div className='margin-top10'>
                                                    <b>Store Name:</b> 
                                                    <input type="text" id='store-name-input' className='input-entries' onChange={this.validateVendorData} />
                                                    <div className='error-message' style={this.state.storeDetailsTypeError.errorInStoreName ? {display:'block'} : {display:'none'}}>
                                                        Please enter a store name | Must be more than 5 letters
                                                    </div>
                                                </div>

                                                <div className='margin-top10'>
                                                    <b>Store Address:</b> 
                                                    <GoogleSearchLocator />
                                                </div>

                                                <div className='margin-top10'>
                                                    <b>Postal Code:</b> 
                                                    <input type="text" id='postal-code-input' className='input-entries' onChange={this.validateVendorData} />
                                                    <div className='error-message' style={this.state.storeDetailsTypeError.errorInPostalCode ? {display:'block'} : {display:'none'}}>
                                                        Please enter a Postal Code [X2X-2X2 | X2X 2X2 | X2X2X2]
                                                    </div>
                                                </div>

                                                <div className='margin-top10'>
                                                    <b>Province:</b> 
                                                    <select name="province" id='province-input' className='input-entries' onChange={this.validateVendorData}>
                                                        <option value="Ontario">Ontario</option>
                                                    </select>
                                                    <div className='error-message' style={this.state.storeDetailsTypeError.errorInProvince ? {display:'block'} : {display:'none'}}>
                                                        Please enter province
                                                    </div>
                                                </div>
                                                
                                                <div className='margin-top10'>
                                                    <b>City:</b> 
                                                    <select name="City" id='city-input' className='input-entries' onChange={this.validateVendorData}>
                                                        <option value="London">London</option>
                                                    </select>
                                                    <div className='error-message'  style={this.state.storeDetailsTypeError.errorInCity ? {display:'block'} : {display:'none'}}>
                                                        Please enter city
                                                    </div>
                                                </div>

                                                <div className='margin-top10'>
                                                    <b>Company Icon:</b> 
                                                    <input type="file" id='company-icon-input' className='input-entries' onChange={this.validateVendorData}  accept="image/png" />
                                                    <div className='error-message' style={this.state.storeDetailsTypeError.errorInCompanyIcon ? {display:'block'} : {display:'none'}}>
                                                        Choose an Image Icon | Image can only be png
                                                    </div>
                                                </div>
                                                
                                                <div className='margin-top10'>
                                                    <b>Company Background Image:</b> 
                                                    <input type="file" id='company-background-image-input' className='input-entries' onChange={this.validateVendorData} accept="image/jpeg" />
                                                    <div className='error-message' style={this.state.storeDetailsTypeError.errorInCompanyBackgroundImage ? {display:'block'} : {display:'none'}}>
                                                        Choose background Image | Image can only be jpg
                                                    </div>
                                                </div>

                                                <div className='margin-top10'>
                                                    <b>Text Description:</b> 
                                                    <textarea type="text" id='text-description-input' className='input-entries' onChange={this.validateVendorData} />
                                                    <div className='error-message' style={this.state.storeDetailsTypeError.errorInTextDescription ? {display:'block'} : {display:'none'}}>
                                                        Enter a description for the company and it must be more than 100 letters
                                                    </div>
                                                </div>

                                                <div className='response-from-database-in-modal center margin-top20' style={this.state.responseVendor.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.responseVendor.msg}</div>
                    
                                                <div className='center'>
                                                    <button className='custom-button' id='update-user-details-button' onClick={() => this.updateStoreDetails()}>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        :
                                            ''
                                    :
                                        ''
                                }
                            </div>
                        </div>
                }
                <Footer />
            </div>// end of alpha-container-profile-page
        )
    }
}

const mapStateToProps = state => ({
    userData: state.auth.userData,
    destinationAddressData: state.utility.destinationAddressData
});


export default connect(mapStateToProps, {storeAddressData})(Profile);