//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';
import FormData from 'form-data'
import Footer from '../utility/Footer';
import { Redirect } from 'react-router';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

//custom made component
import PageLoading from '../utility/PageLoading';
import GoogleSearchLocator from '../utility/GoogleSearchLocator';

//icons
import { MdClear } from 'react-icons/md';

//stylings
import '../../stylings/sign-up.css';

//images
import company_logo from '../../images/icons/logo9.png';
import client_logo from '../../images/icons/client-logo.png';
import driver_logo from '../../images/icons/driver-logo.png';
import vendor_logo from '../../images/icons/vendors-logo.png';
import loading_icon from '../../images/icons/loading.gif';


const $ = require("jquery");


class SignUp extends Component {
    constructor(props) {
        super(props);

        this.state = {
            goodColor: '#258e25',
            badColor: '#ff3333',
            defaultColor: '#a7a7a7',
            showPageLoading: false,
            showMoreEntrieForClientType: false, 
            showMoreEntrieForDriverType: false,
            showMoreEntrieForVendorType: false, //change to 
            showSignUpEnd : true, //change to 
            clientType :  '',
            showModal: false,
            showPostSignUpModalInfo: false,
            showPostVerifyModalInfo: false,
            response : {
                status: false,
                msg: ''
            },                                           
            userAccountTypeErrorArray : {
                errorInFirstName: false,
                errorInLastName: false,
                errorInUserName : false,
                errorInEmail : false,
                errorInPassword : false,
                errorInRetypePassword : false,
                errorInPhoneNo : false
            },        
            userAccountVerifyTypeErrorArray : {
                errorInEmail : false,
                errorInVerificationCode: false
            },
            driverAccountTypeErrorArray : {
                errorInFirstName : false,
                errorInLastName : false,
                errorInEmail : false,
                errorInDateofBirth : false,
                errorInAge : false,
                errorInDriversLiscenceNumber : false,
                errorInPhoneNo: false,
                errorInPassword : false,
                errorInRetypePassword : false
            },
            vendorAccountTypeErrorArray : {
                errorInStoreName : false,
                errorInStoreAddress : false,
                errorInPostalCode : false,
                errorInProvince : false,
                errorInCity : false,
                errorInAccountManagerUserName : false,
                errorInAccountManagerFirstName: false,
                errorInAccountManagerLastName: false,
                errorInAccountManagerEmail: false,
                errorInCompanyIcon : false,
                errorInCompanyBackgroundImage : false,
                errorInContactNo : false,
                errorInTextDescription : false,
                errorInPassword : false,
                errorInRetypePassword : false,
                errorInAdminPassword : false,
                errorInAdminRetypePassword : false
            },
            redirectToSignIn: false
        }
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        destinationAddressData : PropTypes.object
    }

    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
        this.setState({showPostSignUpModalInfo: false});
        this.setState({showPostVerifyModalInfo: false});

        //reset the response for calls
        this.setState({response: {status: false, msg: ''}});
        }

        //open the modal
        this.setState({showModal: !this.state.showModal});
    }

    validateAccount = () => {            
        //modify the view
        this.showAccountTypes(); //sets everything to false -> user,vendor, driver
        this.setState({showMoreEntrieForClientType: true}); //show the client display
        this.setState({showSignUpEnd: false});  //modifies the view to display the validation section
    }

    openMoreEnteries = (type) => {
        if(type === 'client') {
            this.setState({showMoreEntrieForClientType : true});
            this.setState({showMoreEntrieForDriverType : false});
            this.setState({showMoreEntrieForVendorType : false});
            this.setState({clientType : 'client'});
        } else if (type === 'driver') {
            this.setState({showMoreEntrieForClientType : false});
            this.setState({showMoreEntrieForDriverType : true});
            this.setState({showMoreEntrieForVendorType : false});
            this.setState({clientType : 'driver'});
        } else if (type === 'vendor') {
            this.setState({showMoreEntrieForClientType : false});
            this.setState({showMoreEntrieForDriverType : false});
            this.setState({showMoreEntrieForVendorType : true});
            this.setState({clientType : 'vendor'});
        }
    }

    showAccountTypes = () => {
        this.setState({showMoreEntrieForClientType : false});
        this.setState({showMoreEntrieForDriverType : false});
        this.setState({showMoreEntrieForVendorType : false});
        this.setState({showSignUpEnd: true});  //modifies the view to display the validation section
    }

    handleRadioButtonSignUp = () => {
        this.setState({showSignUpEnd : true})
    }

    handleRadioButtonCardVerifyAccount = () => {
        this.setState({showSignUpEnd : false})
    }

    validateEmail =  (email) => {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
    }

    validatePassword = (pw) => {
        return /[A-Z]/.test(pw) &&
            /[a-z]/.test(pw) &&
            /[0-9]/.test(pw) &&
            /[^A-Za-z0-9]/.test(pw) &&
            pw.length > 6;
    }

    validateUserSignUpData = () => {
        //show the loading icon
        this.setState({showPageLoading : true });

        var noErrors = [{firstNameError: true, lastNameError: true, userNameError: true, emailError: true, passwordError: true, reTypePasswordError: true, mobileNoError: true}];
        
        //only get these data if we are in the sign up option
        if(this.state.showSignUpEnd) {
            var firstName = document.getElementById('first-name-input').value;
            var lastName = document.getElementById('last-name-input').value;
            var userName = document.getElementById('username-input').value;
            var mobileNo = document.getElementById('mobile-number-input').value;

            //validate firstname
            if(firstName) {
                noErrors.firstNameError = false;
                document.getElementById('first-name-input').style.borderColor = `${this.state.goodColor}`;
            } else {
                noErrors.firstNameError = true;
                document.getElementById('first-name-input').style.borderColor = `${this.state.defaultColor}`;
            }

            //validate lastname
            if(lastName) {
                noErrors.lastNameError = false;
                document.getElementById('last-name-input').style.borderColor = `${this.state.goodColor}`;
            } else {
                noErrors.lastNameError = true;
                document.getElementById('last-name-input').style.borderColor = `${this.state.defaultColor}`;
            }

            //validate username
            if(userName && userName.length > 5) {
                noErrors.userNameError = false;
                document.getElementById('username-input').style.borderColor = `${this.state.goodColor}`;
            } else {
                noErrors.userNameError = true;
                document.getElementById('username-input').style.borderColor = `${this.state.defaultColor}`;
            }

            //validate number
            if(mobileNo && this.validatePhoneNumber(mobileNo)) {
                noErrors.mobileNoError = false;
                document.getElementById('mobile-number-input').style.borderColor = `${this.state.goodColor}`;
            } else {
                noErrors.mobileNoError = true;
                document.getElementById('mobile-number-input').style.borderColor = `${this.state.defaultColor}`;
            }
        }

        var email = document.getElementById('email-input').value;
        //validate email
        if(this.validateEmail(email)) {
            noErrors.emailError = false;
            document.getElementById('email-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.emailError = true;
            document.getElementById('email-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //we want to validate this section if signup has been selected and not verify account
        if(this.state.showSignUpEnd) {
            var password = document.getElementById('password-input').value;
            var reTypePassword = document.getElementById('re-password-input').value;

            //validate password    
            if(this.validatePassword(password)) {
                noErrors.passwordError = false;
                document.getElementById('password-input').style.borderColor = `${this.state.goodColor}`;
            } else {
                noErrors.passwordError = true;
                document.getElementById('password-input').style.borderColor = `${this.state.defaultColor}`;
            }

            //validate retype-password    
            if(reTypePassword === password) {
                noErrors.reTypePasswordError = false;
                document.getElementById('re-password-input').style.borderColor = `${this.state.goodColor}`;
            } else {
                noErrors.reTypePasswordError = true;
                document.getElementById('re-password-input').style.borderColor = `${this.state.defaultColor}`;
            }
        }

        //show the loading icon
        this.setState({showPageLoading : false });

        //update the data
        this.setState({userAccountTypeErrorArray : {
            errorInFirstName: noErrors.firstNameError, 
            errorInLastName: noErrors.lastNameError, 
            errorInUserName: noErrors.userNameError, 
            errorInEmail: noErrors.emailError, 
            errorInPhoneNo: noErrors.mobileNoError,
            errorInPassword: noErrors.passwordError, 
            errorInRetypePassword: noErrors.reTypePasswordError}})

        return !noErrors.firstNameError && !noErrors.lastNameError && !noErrors.userNameError && !noErrors.emailError && !noErrors.mobileNoError && !noErrors.passwordError && !noErrors.reTypePasswordError;
    }

    wait = (ms) => {
        var start = new Date().getTime();
        var end = start;
        while(end < start + ms) {
        end = new Date().getTime();
        }
    }

    //sometimes the loading icon does not work with react that is why i have this procedural approach
    addLoadingIconProcedurally = () => {
        // <div id='alpha-sign-up-container'>
        $( "#alpha-sign-up-container" ).append(`<div id='modal-background-2' style='display:block'> <div id='modal-loading'><div id='loading-icon'>  <img src=${loading_icon} class='image-take-full-space' alt='loading_icon'/> </div> <div class='title title-for-loading'>Loading</div></div> </div>`);
    }

    removeLoadingIconProcedurally = () => {
        $( "#modal-background-2" ).remove();
    }

    //when the sign up button is clicked this runs
    signUpUser = () => {
        //validate user data
        if(this.validateUserSignUpData()) {
            var firstName = document.getElementById('first-name-input').value;
            var lastName = document.getElementById('last-name-input').value;
            var userName = document.getElementById('username-input').value;
            var email = document.getElementById('email-input').value;
            var mobileNo = document.getElementById('mobile-number-input').value;
            var password = document.getElementById('password-input').value;
            var userType = this.state.clientType || 'client';

            //headers
            const config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        
            //make body
            const body = JSON.stringify({firstName, lastName, userName, email, mobileNo, password, userType});

            //add loading icon procedurally
            this.addLoadingIconProcedurally();

            axios.post(`${apiURL}/api/sign_up/add_user`, body, config)
            .then(res => {
                //remove the loading icon
                this.removeLoadingIconProcedurally();

                //if we are successful we clear the passwords if we fil we clear all
                if(res.data.grantedAccess) {
                    document.getElementById('first-name-input').value = '';
                    document.getElementById('last-name-input').value = '';
                    document.getElementById('mobile-number-input').value = '';
                    document.getElementById('password-input').value = '';
                    document.getElementById('re-password-input').value = '';
                }


                this.setState({response: {status: res.data.grantedAccess, msg: res.data.msg}});
                this.setState({showPostSignUpModalInfo: true});

                //open the modal
                this.toggleModal();
            })
            .catch(err => {
            });
        }
        
    }

    //this runs after the user signs up
    successAfterSignUp = () => {
        //if the current selected typoe is vendor and the account has been verified, when they click okay we want to modify view to show verification page
        if(this.state.clientType === 'vendor') {
            if(document.getElementById('acct-mgr-email-input')) {
                //open the modal
                this.toggleModal();

                //modify the view
                this.showAccountTypes(); //sets everything to false -> user,vendor, driver
                this.setState({showMoreEntrieForClientType: true}); //show the client display
                this.setState({showSignUpEnd: false});  //modifies the view to display the validation section
            }
        } else if(this.state.clientType === 'driver') {
            //open the modal
            this.toggleModal();

            //modify the view
            this.showAccountTypes(); //sets everything to false -> user,vendor, driver
            this.setState({showMoreEntrieForClientType: true}); //show the client display
            this.setState({showSignUpEnd: false});  //modifies the view to display the validation section
        }  else if(this.state.clientType === 'client') {
            //open the modal
            this.toggleModal();

            //modify the view
            this.showAccountTypes(); //sets everything to false -> user,vendor, driver
            this.setState({showMoreEntrieForClientType: true}); //show the client display
            this.setState({showSignUpEnd: false});  //modifies the view to display the validation section
        }

    } 

    //validate the verify data
    validateVerifyData = () => {
        var noErrors = [{emailError: true, verificationCodeError: true}];
        
        var email = document.getElementById('email-input').value;
        var verificationCode = document.getElementById('verification-code-input').value;

        //validate email
        if(this.validateEmail(email)) {
            noErrors.emailError = false;
            document.getElementById('email-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.emailError = true;
            document.getElementById('email-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //validate email
        if(verificationCode) {
            noErrors.verificationCodeError = false;
            document.getElementById('verification-code-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.verificationCodeError = true;
            document.getElementById('verification-code-input').style.borderColor = `${this.state.defaultColor}`;
        }

        //update the data
        this.setState({userAccountVerifyTypeErrorArray : {
            errorInEmail: noErrors.emailError, 
            errorInVerificationCode: noErrors.verificationCodeError}})

        return !noErrors.emailError && !noErrors.verificationCodeError;
    }

    //this runs after we successfully verify the page
    successAfterVerification = () => {
        //open the modal
        this.toggleModal();
        this.setState({redirectToSignIn: true});
    }

    //this runs when the user clicks on the Verify account -> when they have entered the verification code
    verifyTheUserAccount = () => {
        //send the validation code to the back end
        if(this.validateVerifyData()) {
            var email = document.getElementById('email-input').value;
            var verificationCode = document.getElementById('verification-code-input').value;

            //headers
            const config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
                
            //make body
            const body = JSON.stringify({email, verificationCode});
            
            //add loading icon procedurally
            this.addLoadingIconProcedurally();

            axios.post(`${apiURL}/api/sign_up/validate_user`, body, config)
            .then(res => {
                //clear the verification code each time
                document.getElementById('verification-code-input').value = '';
                //this.validateVerifyData();
                
                this.setState({response: {status: res.data.accountValidated, msg: res.data.msg}});
                this.setState({showPostVerifyModalInfo: true});

                //remove loading icon
                this.removeLoadingIconProcedurally();
                
                //open the modal
                this.toggleModal();
            })
            .catch(err => {
            });
        }
    }
    
    requestNewCode = () => {
        var email = document.getElementById('email-input').value;
        if(this.validateEmail(email)) {
            document.getElementById('email-input').style.borderColor = `${this.state.goodColor}`;

            //headers
            const config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
                
            //make body
            const body = JSON.stringify({email});
            
            //add loading icon procedurally
            this.addLoadingIconProcedurally();

            axios.post(`${apiURL}/api/sign_up/request_new_code`, body, config)
            .then(res => {
                //clear the email
                document.getElementById('email-input').value = '';
                
                this.setState({response: {status: res.data.successfullySent, msg: res.data.msg}});
                this.setState({showPostVerifyModalInfo: true});

                //remove loading icon
                this.removeLoadingIconProcedurally();
                
                //open the modal
                this.toggleModal();
            })
            .catch(err => {
            });
        } else {
            document.getElementById('email-input').style.borderColor = `${this.state.badColor}`;
        }
    }
    
    
    getAge = (dateString) => {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    validateDriverSignUpData = () => {
        var noErrors = [{firstNameError: true, lastNameError: true, emailError: true, dobError: true, 
            ageError: true, driversLicenceNumberError: true, phoneNoError: true, passwordError: true, reTypePasswordError: true}];

        var firstName = document.getElementById('firstname-input').value;
        var lastName = document.getElementById('lastname-input').value;
        var email = document.getElementById('email-input').value;
        var dob = document.getElementById('dob-input').value;
        var age = document.getElementById('age-input').value;
        var driversLicenceNumber = document.getElementById('drivers-licence-input').value;
        var phoneNo= document.getElementById('drivers-phone-no-input').value; 
        var password = document.getElementById('password-input-driver').value;
        var reTypePassword = document.getElementById('re-password-input-driver').value;

        //validate firstname
        if(firstName) {
            noErrors.firstNameError = false;
            document.getElementById('firstname-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.firstNameError = true;
            document.getElementById('firstname-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(lastName) {
            noErrors.lastNameError = false;
            document.getElementById('lastname-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.lastNameError = true;
            document.getElementById('lastname-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(this.validateEmail(email)) {
            noErrors.emailError = false;
            document.getElementById('email-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.emailError = true;
            document.getElementById('email-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(dob && (this.getAge(dob) >= 18)) {
            noErrors.dobError = false;
            document.getElementById('dob-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.dobError = true;
            document.getElementById('dob-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(age && !isNaN(age) && age > 18) {
            noErrors.ageError = false;
            document.getElementById('age-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.ageError = true;
            document.getElementById('age-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(driversLicenceNumber.length === 15 && !isNaN(driversLicenceNumber)) {
            noErrors.driversLicenceNumberError = false;
            document.getElementById('drivers-licence-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.driversLicenceNumberError = true;
            document.getElementById('drivers-licence-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(phoneNo && this.validatePhoneNumber(phoneNo)) {
            noErrors.phoneNoError = false;
            document.getElementById('drivers-phone-no-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.phoneNoError = true;
            document.getElementById('drivers-phone-no-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(this.validatePassword(password)) {
            noErrors.passwordError = false;
            document.getElementById('password-input-driver').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.passwordError = true;
            document.getElementById('password-input-driver').style.borderColor = `${this.state.defaultColor}`;
        }

        if(reTypePassword === password) {
            noErrors.reTypePasswordError = false;
            document.getElementById('re-password-input-driver').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.reTypePasswordError = true;
            document.getElementById('re-password-input-driver').style.borderColor = `${this.state.defaultColor}`;
        }

        //update the data
        this.setState({driverAccountTypeErrorArray : {
            errorInFirstName: noErrors.firstNameError, 
            errorInLastName: noErrors.lastNameError, 
            errorInEmail: noErrors.emailError, 
            errorInDateofBirth: noErrors.dobError,
            errorInAge: noErrors.ageError, 
            errorInDriversLiscenceNumber: noErrors.driversLicenceNumberError, 
            errorInPhoneNo: noErrors.phoneNoError,
            errorInPassword: noErrors.passwordError, 
            errorInRetypePassword: noErrors.reTypePasswordError}});

        return !noErrors.firstNameError && !noErrors.lastNameError && !noErrors.emailError && !noErrors.dobError && 
        !noErrors.ageError && !noErrors.driversLicenceNumberError && !noErrors.phoneNoError && !noErrors.passwordError && !noErrors.reTypePasswordError;
    }


    validatePhoneNumber = (p) => {
        var phoneRe = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
        var digits = p.replace(/\D/g, "");
        return phoneRe.test(digits);
    }

    validatePostalCode = (postal) => {
        var regex = new RegExp(/^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ][ -]?\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i);
        if (regex.test(postal))
            return true;
        else return false;
    }

    validateVendorData = () => {
        var noErrors = [{storeNameError: true, storeAddressError: true, postalCodeError: true, provinceError: true, 
            cityError: true, accountManagerNameError: true, accountManagerFirstNameError: true, accountManagerLastError: true, accountManagerEmailError: true, 
            companyIconError: true, companyBgImageError: true, 
            contactNoError: true, textDesciptionError: true, passwordError: true, reTypePasswordError: true, adminPasswordError: true, reTypeAdminPasswordError: true}];

        var storeName = document.getElementById('store-name-input').value;
        //var storeAddress = document.getElementById('store-address-input').value;
        var postalCode = document.getElementById('postal-code-input').value;
        var province = document.getElementById('province-input').value;
        var city = document.getElementById('city-input').value;
        var accountManagerName = document.getElementById('acct-mgr-user-name-input').value;
        var accountManagerFirstName = document.getElementById('acct-mgr-first-name-input').value;
        var accountManagerLastName = document.getElementById('acct-mgr-last-name-input').value;
        var accountManagerEmail = document.getElementById('acct-mgr-email-input').value;
        var companyIcon = document.getElementById('company-icon-input').files[0];
        var companyBgImage = document.getElementById('company-background-image-input').files[0];
        var contactNo= document.getElementById('contact-number-input').value; 
        var textDesciption = document.getElementById('text-description-input').value;
        var password = document.getElementById('password-input-vendor').value;
        var reTypePassword = document.getElementById('re-type-password-input-vendor').value;
        var adminPassword = document.getElementById('admin-password-input-vendor').value;
        var reTypeAdminPassword = document.getElementById('admin-re-type-password-input-vendor').value;

        if(storeName && storeName.length > 5) {
            noErrors.storeNameError = false;
            document.getElementById('store-name-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.storeNameError = true;
            document.getElementById('store-name-input').style.borderColor = `${this.state.defaultColor}`;
        }

        // if(storeAddress) {
        //     noErrors.storeAddressError = false;
        //     document.getElementById('store-address-input').style.borderColor = `${this.state.goodColor}`;
        // } else {
        //     noErrors.storeAddressError = true;
        //     document.getElementById('store-address-input').style.borderColor = `${this.state.defaultColor}`;
        // }

        if(postalCode && this.validatePostalCode(postalCode)) {
            noErrors.postalCodeError = false;
            document.getElementById('postal-code-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.postalCodeError = true;
            document.getElementById('postal-code-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(province) {
            noErrors.provinceError = false;
            document.getElementById('province-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.provinceError = true;
            document.getElementById('province-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(city) {
            noErrors.cityError = false;
            document.getElementById('city-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.cityError = true;
            document.getElementById('city-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(accountManagerName && accountManagerName.length > 5) {
            noErrors.accountManagerNameError = false;
            document.getElementById('acct-mgr-user-name-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.accountManagerNameError = true;
            document.getElementById('acct-mgr-user-name-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(accountManagerFirstName) {
            noErrors.accountManagerFirstNameError = false;
            document.getElementById('acct-mgr-first-name-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.accountManagerFirstNameError = true;
            document.getElementById('acct-mgr-first-name-input').style.borderColor = `${this.state.defaultColor}`;
        }


        if(accountManagerLastName) {
            noErrors.accountManagerLastNameError = false;
            document.getElementById('acct-mgr-last-name-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.accountManagerLastNameError = true;
            document.getElementById('acct-mgr-last-name-input').style.borderColor = `${this.state.defaultColor}`;
        }


        if(this.validateEmail(accountManagerEmail)) {
            noErrors.accountManagerEmailError = false;
            document.getElementById('acct-mgr-email-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.accountManagerEmailError = true;
            document.getElementById('acct-mgr-email-input').style.borderColor = `${this.state.defaultColor}`;
        }
        
        if(companyIcon && companyIcon.type === 'image/png') {
            noErrors.companyIconError = false;
            document.getElementById('company-icon-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.companyIconError = true;
            document.getElementById('company-icon-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(companyBgImage && companyBgImage.type === 'image/jpeg') {
            noErrors.companyBgImageError = false;
            document.getElementById('company-background-image-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.companyBgImageError = true;
            document.getElementById('company-background-image-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(contactNo && this.validatePhoneNumber(contactNo)) {
            noErrors.contactNoError = false;
            document.getElementById('contact-number-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.contactNoError = true;
            document.getElementById('contact-number-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(textDesciption && textDesciption.length > 100) {
            noErrors.textDesciptionError = false;
            document.getElementById('text-description-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.textDesciptionError = true;
            document.getElementById('text-description-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(password && this.validatePassword(password)) {
            noErrors.passwordError = false;
            document.getElementById('password-input-vendor').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.passwordError = true;
            document.getElementById('password-input-vendor').style.borderColor = `${this.state.defaultColor}`;
        }

        if(reTypePassword === password) {
            noErrors.reTypePasswordError = false;
            document.getElementById('re-type-password-input-vendor').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.reTypePasswordError = true;
            document.getElementById('re-type-password-input-vendor').style.borderColor = `${this.state.defaultColor}`;
        }

        if(adminPassword && this.validatePassword(adminPassword)) {
            noErrors.adminPasswordError = false;
            document.getElementById('admin-password-input-vendor').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.adminPasswordError = true;
            document.getElementById('admin-password-input-vendor').style.borderColor = `${this.state.defaultColor}`;
        }

        if(reTypeAdminPassword === adminPassword) {
            noErrors.reTypeAdminPasswordError = false;
            document.getElementById('admin-re-type-password-input-vendor').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.reTypeAdminPasswordError = true;
            document.getElementById('admin-re-type-password-input-vendor').style.borderColor = `${this.state.defaultColor}`;
        }
        //update the data
        this.setState({vendorAccountTypeErrorArray : {
            errorInStoreName: noErrors.storeNameError, 
            errorInStoreAddress: !this.props.destinationAddressData.addressIsValid, 
            errorInPostalCode: noErrors.postalCodeError, 
            errorInProvince: noErrors.provinceError,
            errorInCity: noErrors.cityError, 
            errorInAccountManagerUserName: noErrors.accountManagerNameError, 
            errorInAccountManagerFirstName: noErrors.accountManagerFirstNameError, 
            errorInAccountManagerLastName: noErrors.accountManagerLastNameError, 
            errorInAccountManagerEmail: noErrors.accountManagerEmailError, 
            errorInCompanyIcon: noErrors.companyIconError, 
            errorInCompanyBackgroundImage: noErrors.companyBgImageError,
            errorInContactNo: noErrors.contactNoError, 
            errorInTextDescription: noErrors.textDesciptionError, 
            errorInPassword: noErrors.passwordError, 
            errorInRetypePassword: noErrors.reTypePasswordError,
            errorInAdminPassword: noErrors.adminPasswordError, 
            errorInAdminRetypePassword: noErrors.reTypeAdminPasswordError}});

            return !noErrors.storeNameError && this.props.destinationAddressData.addressIsValid && !noErrors.postalCodeError && !noErrors.provinceError && 
            !noErrors.cityError && !noErrors.accountManagerNameError && !noErrors.companyIconError && !noErrors.companyBgImageError&& 
            !noErrors.contactNoError && !noErrors.textDesciptionError && !noErrors.passwordError && !noErrors.reTypePasswordError && 
            !noErrors.accountManagerFirstNameError && !noErrors.accountManagerLastNameError && !noErrors.accountManagerEmailError &&
            !noErrors.adminPasswordError && !noErrors.reTypeAdminPasswordError;
    }

    //this gets ran when the submit button is hit for the vendor data
    submitVendorInfo = () => {
        if(this.validateVendorData()) {
            var storeName = document.getElementById('store-name-input').value;
            var storeNameAndDate = storeName.replace(/\s+/g, '-').toLowerCase() +'-'+ new Date().getTime();
            var postalCode = document.getElementById('postal-code-input').value;
            var province = document.getElementById('province-input').value;
            var city = document.getElementById('city-input').value;
            var accountManagerName = document.getElementById('acct-mgr-user-name-input').value;
            var accountManagerFirstName = document.getElementById('acct-mgr-first-name-input').value;
            var accountManagerLastName = document.getElementById('acct-mgr-last-name-input').value;
            var accountManagerEmail = document.getElementById('acct-mgr-email-input').value;
            var companyIcon = document.getElementById('company-icon-input').files[0];
            var companyBgImage = document.getElementById('company-background-image-input').files[0];
            var contactNo= document.getElementById('contact-number-input').value;
            var textDesciption = document.getElementById('text-description-input').value;
            var password = document.getElementById('password-input-vendor').value;
            var reTypePassword = document.getElementById('re-type-password-input-vendor').value;
            var adminPassword = document.getElementById('admin-password-input-vendor').value;
            var reTypeAdminPassword = document.getElementById('admin-re-type-password-input-vendor').value;

            //build our form data
            let data = new FormData();
            data.append('storeName', storeName);
            data.append('storeAddress', this.props.destinationAddressData.address);
            data.append('postalCode', postalCode);
            data.append('province', province);
            data.append('city', city);
            data.append('accountManagerName', accountManagerName);
            data.append('accountManagerFirstName', accountManagerFirstName);
            data.append('accountManagerLastName', accountManagerLastName);
            data.append('accountManagerEmail', accountManagerEmail);
            data.append('companyImages', companyIcon, 'logo-'+storeNameAndDate+'.png');
            data.append('companyIconImgName', 'logo-'+storeNameAndDate);
            data.append('companyImages', companyBgImage, 'bg-img-'+storeNameAndDate+'.jpg');
            data.append('companyBgImageName', 'bg-img-'+storeNameAndDate);
            data.append('contactNo', contactNo);
            data.append('textDesciption', textDesciption);
            data.append('password', password);
            data.append('reTypePassword', reTypePassword);
            data.append('adminPassword', adminPassword);
            data.append('userType', this.state.clientType || 'vendor');

            //headers
            const config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            }

            //add the loading icon procedurally
            this.addLoadingIconProcedurally();

            axios.post(`${apiURL}/api/sign_up/validate_email`, {accountManagerEmail}, config)
            .then(res => {
            //if we are successful we clear the passwords if we fil we clear all
            if(res.data.accountExists) {
                //remove loading icon procedurally
                this.removeLoadingIconProcedurally();
                
                this.setState({response: {status: !res.data.accountExists, msg: res.data.msg}});
                this.setState({showPostSignUpModalInfo: true});

                //open the modal
                this.toggleModal();
            } else {
                //if the email does not exist them we can send the actual adding the vendor data
                this.setState({response: {status: false, msg: ''}});

                axios.post(`${apiURL}/api/sign_up/add_vendor`, data)
                .then(res => {
                    //remove loading icon procedurally
                    this.removeLoadingIconProcedurally();

                    this.setState({response: {status: res.data.accountHasBeenSetUp, msg: res.data.msg}});
                    this.setState({showPostSignUpModalInfo: true});

                    //open the modal
                    this.toggleModal();

                    //if its a success we want to clear everything other than email
                    if(res.data.accountHasBeenSetUp) {
                        document.getElementById('store-name-input').value = '';
                        document.getElementById('store-address-input').value = '';
                        document.getElementById('postal-code-input').value = '';
                        document.getElementById('province-input').value = '';
                        document.getElementById('city-input').value = '';
                        document.getElementById('acct-mgr-user-name-input').value = '';
                        document.getElementById('acct-mgr-first-name-input').value = '';
                        document.getElementById('acct-mgr-last-name-input').value = '';        
                        document.getElementById('email-input').value = '';
                        document.getElementById('company-icon-input').value = '';
                        document.getElementById('company-background-image-input').value = '';
                        document.getElementById('contact-number-input').value = '';
                        document.getElementById('text-description-input').value = '';
                        document.getElementById('password-input-vendor').value = '';
                        document.getElementById('re-type-password-input-vendor').value = '';
                    }
                }).catch(err => {
                //handle error
                });
            }
            });
        }
        
        //show the loading icon
        this.setState({showPageLoading : false });
    }

    submitDriverInfo = () => {
        if(this.validateDriverSignUpData()) {
            var firstName = document.getElementById('firstname-input').value;
            var lastName =  document.getElementById('lastname-input').value;
            var email =     document.getElementById('email-input').value;
            var dob =       document.getElementById('dob-input').value;
            var age =       document.getElementById('age-input').value;
            var driversLicenseNumber = document.getElementById('drivers-licence-input').value;
            var phoneNo              = document.getElementById('drivers-phone-no-input').value; 
            var password =             document.getElementById('password-input-driver').value;
            var reTypePassword =       document.getElementById('re-password-input-driver').value;

            //headers
            const config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
                
            //make body
            const body = JSON.stringify({firstName, lastName, email, dob, age, driversLicenseNumber, phoneNo, password});

            //add the loading icon procedurally
            this.addLoadingIconProcedurally();

            axios.post(`${apiURL}/api/sign_up/add_driver`, body, config)
            .then(res => {
                //add the loading icon procedurally
                this.removeLoadingIconProcedurally();

                //if we are successful we clear the passwords if we fil we clear all
                if(res.data.grantedAccess) {
                    // document.getElementById('email-input').value;
                    // document.getElementById('password-input-driver').value;
                    document.getElementById('firstname-input').value = '';
                    document.getElementById('lastname-input').value = '';
                    document.getElementById('dob-input').value = '';
                    document.getElementById('age-input').value = '';
                    document.getElementById('drivers-licence-input').value = '';
                    document.getElementById('re-password-input-driver').value = '';
                }

                this.setState({response: {status: res.data.grantedAccess, msg: res.data.msg}});
                this.setState({showPostSignUpModalInfo: true});

                //open the modal
                this.toggleModal();
            })
            .catch(err => {
            });
        }
    }

    render() {
        return this.state.redirectToSignIn ?
            <Redirect to='/signIn'/>
        :
            (
                <div id='alpha-sign-up-container'>
                    <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/>

                    <div>
                        <button className='custom-button' id='sign-up-button' onClick={() => this.validateAccount()}>Verify Account</button>
                    </div>

                    {/* the modal */}
                    <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                        <div id='modal-container' className='sign-up-modal-container'>
                            <MdClear id='close-modal' style={this.state.response.status ? {display:'none'} : {display:'block'}}  onClick={() => this.toggleModal()}/>
                            {
                                this.state.showPostSignUpModalInfo ? 
                                    <div id='post-sign-up-modal-container' className={this.state.response.status ?  'valid-color' : 'bad-color'}>
                                        {this.state.response.msg}

                                        {
                                            this.state.response.status ? 
                                                <div id='ok-button-container'>
                                                    <button className='custom-button ok-button' onClick={() => this.successAfterSignUp()}>Ok</button>
                                                </div>
                                            :
                                                ''
                                        }
                                    </div>
                                : 
                                    this.state.showPostVerifyModalInfo ? 
                                        <div>
                                                <div id='post-sign-up-modal-container' className={this.state.response.status ?  'valid-color' : 'bad-color'}>
                                                    {this.state.response.msg}

                                                    {
                                                        this.state.response.status ? 
                                                            <div id='ok-button-container'>
                                                                <button className='custom-button ok-button' onClick={() => this.successAfterVerification()}>Ok</button>
                                                            </div>
                                                        :
                                                            ''
                                                    }
                                                </div>
                                        </div>
                                    :
                                        '' 
                            }
                        </div>
                    </div>

                    <div className='absolute-company-logo'>
                        <img src={company_logo} className='image-take-full-space' alt='company_logo'/>
                    </div>
                
                    {
                        !this.state.showMoreEntrieForClientType && !this.state.showMoreEntrieForDriverType && !this.state.showMoreEntrieForVendorType ?
                            <div id='type-of-accounts'>
                                <div className='account-type-container' id='account-type-orderer' onClick={() => this.openMoreEnteries('client')}>
                                    <div className='account-type-icon-container'>
                                        <img src={client_logo} className='account-type-icon' alt='client_logo'/>
                                    </div>
                                    <div className='account-type-name'>Client/Orderer</div>
                                    <div className='account-type-discription'>
                                        You can make request for different services ranging from Food, Item deliveries, Pets Food and more.
                                    </div>
                                </div>

                                <div className='account-type-container' id='account-type-driver' onClick={() => this.openMoreEnteries('driver')}>
                                    <div className='account-type-icon-container'>
                                        <img src={driver_logo} className='account-type-icon' alt='driver_logo'/>
                                    </div>
                                    <div className='account-type-name'>Driver</div>
                                    <div className='account-type-discription'>As a driver we need some personal informations for your application, when verified you would bring items to the Clients\Orderer</div>
                                </div>

                                <div className='account-type-container' id='account-type-vendor' onClick={() => this.openMoreEnteries('vendor')}>
                                    <div className='account-type-icon-container'>
                                        <img src={vendor_logo} className='account-type-icon' alt='vendor_logo'/>
                                    </div>
                                    <div className='account-type-name'>Vendor/Seller</div>
                                    <div className='account-type-discription'>As a Vendor/Seller you would respond to orders and the Driver can bring them to you location, you can also request a driver if an order was made away from our platform, although we need you to give the address to the Driver for proper documentation and delivery</div>
                                </div>
                            </div>
                        :
                            ''
                    }

                    {
                        this.state.showMoreEntrieForClientType || this.state.showMoreEntrieForDriverType || this.state.showMoreEntrieForVendorType ?
                            <div id='entries-all-container'>
                                <button className='custom-button' id='go-back-to-account-types' onClick={() => this.showAccountTypes()}>
                                    Switch Account Types
                                </button>

                                {
                                    this.state.showMoreEntrieForClientType ?
                                        <div id='entries-for-orderer'>
                                            <div className='account-type-icon-container all-accounts-sign-up-details-icon-container'>
                                                <img src={client_logo} className='account-type-icon' alt='client_logo'/>
                                            </div>

                                            {
                                                this.state.showSignUpEnd ? 
                                                    <div>
                                                        <div className='margin-top10'>
                                                            <b>FirstName:</b>
                                                            <input type="text" className='input-entries' id='first-name-input' onChange={this.validateUserSignUpData}/>
                                                            <div className='error-message' style={this.state.userAccountTypeErrorArray.errorInFirstName ? {display:'block'} : {display:'none'}}>
                                                                Invalid First Name
                                                            </div>
                                                        </div>

                                                        <div className='margin-top10'>
                                                            <b>LastName:</b>
                                                            <input type="text" className='input-entries' id='last-name-input' onChange={this.validateUserSignUpData}/>
                                                            <div className='error-message' style={this.state.userAccountTypeErrorArray.errorInLastName ? {display:'block'} : {display:'none'}}>
                                                                Invalid Last Name
                                                            </div>
                                                        </div>

                                                        <div className='margin-top10'>
                                                            <b>Username:</b>
                                                            <input type="text" className='input-entries' id='username-input' onChange={this.validateUserSignUpData}/>
                                                            <div className='error-message' style={this.state.userAccountTypeErrorArray.errorInUserName ? {display:'block'} : {display:'none'}}>
                                                                Invalid Username | username must be more than 5 letters
                                                            </div>
                                                        </div>
                                                    
                                                        <div className='margin-top10'>
                                                            <b>Mobile Number:</b> 
                                                            <input type="text" id='mobile-number-input' className='input-entries' onChange={this.validateUserSignUpData} maxLength='11'/>
                                                            <div className='error-message' style={this.state.userAccountTypeErrorArray.errorInPhoneNo ? {display:'block'} : {display:'none'}}>
                                                                Invalid Number | Please Enter your phone number
                                                            </div>
                                                        </div>  
                                                    </div>
                                                :
                                                    ''
                                            }
                                    
                                            <div className='margin-top10'>
                                                <b>Email:</b> 
                                                <input type="text" className='input-entries' id='email-input' onChange={this.validateUserSignUpData}/>
                                                <div className='error-message' style={this.state.userAccountTypeErrorArray.errorInEmail ? {display:'block'} : {display:'none'}}>
                                                    Invalid email
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <div id='radio-buttons'>
                                                    <div id='sign-up-container'><input type="radio" onChange={this.handleRadioButtonSignUp}  checked={this.state.showSignUpEnd === true} name="client-method" id="sign-up"/>
                                                        Sign Up
                                                    </div>
                                                    <div id='verify-account-container'>
                                                        <input type="radio" onChange={this.handleRadioButtonCardVerifyAccount} checked={this.state.showSignUpEnd === false} name="client-method" id="verify-account"/>   Verify Account
                                                    </div>
                                                </div>
                                            </div>

                                            {
                                                this.state.showSignUpEnd ? 
                                                    <div id='sign-up-container-lower'>
                                                        <div className='margin-top10'>
                                                            <b>Password:</b> 
                                                            <input type="password" className='input-entries' id='password-input' onChange={this.validateUserSignUpData}/>
                                                            <div className='error-message' style={this.state.userAccountTypeErrorArray.errorInPassword ? {display:'block'} : {display:'none'}}>
                                                                Invalid Password | password must be more than 6 characters | Must have an upper case and Lowercase | Must have special Symbol ($) and numeric character
                                                            </div>
                                                        </div>
                                                    
                                                        <div className='margin-top10'>
                                                            <b>Re-type Password:</b> 
                                                            <input type="password" className='input-entries' id='re-password-input'  onChange={this.validateUserSignUpData}/>
                                                            <div className='error-message' style={this.state.userAccountTypeErrorArray.errorInRetypePassword ? {display:'block'} : {display:'none'}}>
                                                                Re-type password must be the same as password
                                                            </div>
                                                        </div>

                                                        <button className='custom-button' id='sign-up-button-account-create' onClick={() => this.signUpUser()}>
                                                            Sign Up
                                                        </button>
                                                    </div>
                                                :
                                                    <div id='verify-account-container-lower'>
                                                        <div className='margin-top10'>
                                                            <b>Verification Code (Check Email):</b> 
                                                            <input type="text" className='input-entries' id='verification-code-input' onChange={this.validateVerifyData}/>
                                                            <div className='error-message' style={this.state.userAccountVerifyTypeErrorArray.errorInVerificationCode ? {display:'block'} : {display:'none'}}>
                                                                Type a Code
                                                            </div>
                                                        </div>

                                                        <div className='center'>
                                                            <button className='custom-button' id='verify-account-button-account-create' onClick={() => this.verifyTheUserAccount()}>
                                                                Verify Account
                                                            </button>

                                                            <button className='custom-button' id='new-code-request-verify-account' onClick={() => this.requestNewCode()}>
                                                                Request New Code
                                                            </button>
                                                        </div>
                                                    </div>
                                            }
                                        </div>
                                    :
                                        ''
                                }

                                {
                                    this.state.showMoreEntrieForDriverType ?
                                        <div id='entries-for-driver'>
                                            <div className='account-type-icon-container all-accounts-sign-up-details-icon-container'>
                                                <img src={driver_logo} className='account-type-icon' alt='driver_logo'/>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Firstname:</b> 
                                                <input type="text" id='firstname-input' className='input-entries' onChange={this.validateDriverSignUpData}/>
                                                <div className='error-message' style={this.state.driverAccountTypeErrorArray.errorInFirstName ? {display:'block'} : {display:'none'}}>
                                                    Invalid Firstname
                                                </div>
                                            </div>
                                
                                            <div className='margin-top10'>
                                                <b>Lastname:</b> 
                                                <input type="text" id='lastname-input' className='input-entries' onChange={this.validateDriverSignUpData}/>
                                                <div className='error-message' style={this.state.driverAccountTypeErrorArray.errorInLastName ? {display:'block'} : {display:'none'}}>
                                                    Invalid Lastname
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Email:</b> 
                                                <input type="text" id='email-input' className='input-entries' onChange={this.validateDriverSignUpData}/>
                                                <div className='error-message' style={this.state.driverAccountTypeErrorArray.errorInEmail ? {display:'block'} : {display:'none'}}>
                                                    Invalid Email | Invalid email syntax
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Date of Birth:</b> 
                                                <input type="date" id='dob-input' className='input-entries' onChange={this.validateDriverSignUpData}/>
                                                <div className='error-message' style={this.state.driverAccountTypeErrorArray.errorInDateofBirth ? {display:'block'} : {display:'none'}}>
                                                    Invalid Date Of Birth | You must be 18 and above
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Age:</b> 
                                                <input type="number" id='age-input' className='input-entries' min='16' max='120' onChange={this.validateDriverSignUpData}/>
                                                <div className='error-message' style={this.state.driverAccountTypeErrorArray.errorInAge ? {display:'block'} : {display:'none'}}>
                                                    Invalid Age | You have to be older than 18
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Drivers Licence Number:</b> 
                                                <input type="text" id='drivers-licence-input' className='input-entries' onChange={this.validateDriverSignUpData} maxLength='15'/>
                                                <div className='error-message' style={this.state.driverAccountTypeErrorArray.errorInDriversLiscenceNumber ? {display:'block'} : {display:'none'}}>
                                                    Please enter drivers liscence
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Phone Number:</b> 
                                                <input type="text" id='drivers-phone-no-input' className='input-entries' onChange={this.validateDriverSignUpData} maxLength='11'/>
                                                <div className='error-message' style={this.state.driverAccountTypeErrorArray.errorInPhoneNo ? {display:'block'} : {display:'none'}}>
                                                    Please enter a valid phone number (xxx-xxx-xxxx)
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Password:</b> 
                                                <input type="password" id='password-input-driver' className='input-entries' id='password-input-driver' onChange={this.validateDriverSignUpData}/>
                                                <div className='error-message' style={this.state.driverAccountTypeErrorArray.errorInPassword ? {display:'block'} : {display:'none'}}>
                                                    Invalid Password | password must be more than 6 characters | Must have an upper case and Lowercase | Must have special Symbol ($) and numeric character
                                                </div>
                                            </div>
                                                    
                                            <div className='margin-top10'>
                                                <b>Re-type Password:</b> 
                                                <input type="password" id='re-password-input-driver' className='input-entries' id='re-password-input-driver' onChange={this.validateDriverSignUpData}/>
                                                <div className='error-message' style={this.state.driverAccountTypeErrorArray.errorInRetypePassword ? {display:'block'} : {display:'none'}}>
                                                    Re-type password must be the same as password
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>License Agreement:</b> 
                                                <div id='aggrement-text'>
                                                    A licensing agreement, or license agreement, is a deal between the owner of a patent, brand, or trademark and someone who wants to use the patented or trademarked goods and services. The license grants permission to the licensee and includes stipulations.

                                                    <div id='i-agree-checkbox-container'>
                                                        <input type="checkbox" value="Boat"/> I agree
                                                    </div>
                                                </div>
                                            </div>

                                            <div id='submit-driver-info-container'>
                                                <button className='custom-button' id='submit-driver-info-button' onClick={() => this.submitDriverInfo()}>
                                                    Process Info
                                                </button>
                                            </div>
                                        </div>
                                    :
                                        ''
                                }




                                {
                                    this.state.showMoreEntrieForVendorType ?
                                        <div id='entries-for-vendor'>
                                            <div className='account-type-icon-container all-accounts-sign-up-details-icon-container'>
                                                <img src={vendor_logo} className='account-type-icon' alt='vendor_logo'/>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Store Name:</b> 
                                                <input type="text" id='store-name-input' className='input-entries' onChange={this.validateVendorData} />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInStoreName ? {display:'block'} : {display:'none'}}>
                                                    Please enter a store name | Must be more than 5 letters
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Store Address:</b> 
                                                <GoogleSearchLocator />
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Postal Code:</b> 
                                                <input type="text" id='postal-code-input' className='input-entries' onChange={this.validateVendorData} />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInPostalCode ? {display:'block'} : {display:'none'}}>
                                                    Please enter a Postal Code [X2X-2X2 | X2X 2X2 | X2X2X2]
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Province:</b> 
                                                <select name="province" id='province-input' className='input-entries' onChange={this.validateVendorData}>
                                                    <option value="Ontario">Ontario</option>
                                                </select>
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInProvince ? {display:'block'} : {display:'none'}}>
                                                    Please enter province
                                                </div>
                                            </div>
                                    
                                            <div className='margin-top10'>
                                                <b>City:</b> 
                                                <select name="City" id='city-input' className='input-entries' onChange={this.validateVendorData}>
                                                    <option value="London">London</option>
                                                </select>
                                                <div className='error-message'  style={this.state.vendorAccountTypeErrorArray.errorInCity ? {display:'block'} : {display:'none'}}>
                                                    Please enter city
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Account Manager Username:</b> 
                                                <input type="text" id='acct-mgr-user-name-input' className='input-entries' onChange={this.validateVendorData} />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInAccountManagerUserName ? {display:'block'} : {display:'none'}}>
                                                    Please enter your username | username must be more than 5 letters
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Account Manager First Name:</b> 
                                                <input type="text" id='acct-mgr-first-name-input' className='input-entries' onChange={this.validateVendorData} />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInAccountManagerFirstName ? {display:'block'} : {display:'none'}}>
                                                    Please enter your Firstname 
                                                </div>
                                            </div>

                                            <div className='margin-top10'>
                                                <b>Account Manager Last Name:</b> 
                                                <input type="text" id='acct-mgr-last-name-input' className='input-entries' onChange={this.validateVendorData} />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInAccountManagerLastName ? {display:'block'} : {display:'none'}}>
                                                    Please enter your Lastname
                                                </div>
                                            </div>
                                            

                                            <div className='margin-top10'>
                                                <b>Account Manager Email:</b> 
                                                <input type="text" id='acct-mgr-email-input' className='input-entries' onChange={this.validateVendorData} />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInAccountManagerEmail ? {display:'block'} : {display:'none'}}>
                                                    Please enter a valid email
                                                </div>
                                            </div> 
                                            
                                            <div className='margin-top10'>
                                                <b>Company Icon:</b> 
                                                <input type="file" id='company-icon-input' className='input-entries' onChange={this.validateVendorData}  accept="image/png" />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInCompanyIcon ? {display:'block'} : {display:'none'}}>
                                                    Choose an Image Icon | Image can only be png
                                                </div>
                                            </div>
                                            
                                            <div className='margin-top10'>
                                                <b>Company Background Image:</b> 
                                                <input type="file" id='company-background-image-input' className='input-entries' onChange={this.validateVendorData} accept="image/jpeg" />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInCompanyBackgroundImage ? {display:'block'} : {display:'none'}}>
                                                    Choose background Image | Image can only be jpg
                                                </div>
                                            </div>
                                            
                                            <div className='margin-top10'>
                                                <b>Contact Number:</b> 
                                                <input type="text" id='contact-number-input' className='input-entries' onChange={this.validateVendorData} maxLength='11'/>
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInContactNo ? {display:'block'} : {display:'none'}}>
                                                    Please Enter the companies phone number
                                                </div>
                                            </div>  
                                                
                                            <div className='margin-top10'>
                                                <b>Text Description:</b> 
                                                <textarea type="text" id='text-description-input' className='input-entries' onChange={this.validateVendorData} />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInTextDescription ? {display:'block'} : {display:'none'}}>
                                                    Enter a description for the company and it must be more than 100 letters
                                                </div>
                                            </div>
                                            
                                            <div className='margin-top10'>
                                                <b>Login Password:</b> 
                                                <input type="password" id='password-input-vendor' className='input-entries' onChange={this.validateVendorData} />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInPassword ? {display:'block'} : {display:'none'}}>
                                                    Invalid Password | password must be more than 6 characters | Must have an upper case and Lowercase | Must have special Symbol ($) and numeric character
                                                </div>
                                            </div>
                                            
                                            <div className='margin-top10'>
                                                <b>Login Re-Type Password:</b> 
                                                <input type="password" id='re-type-password-input-vendor' className='input-entries' onChange={this.validateVendorData} />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInRetypePassword ? {display:'block'} : {display:'none'}}>
                                                    Re-Type password
                                                </div>
                                            </div>
                                            
                                            <div className='margin-top10'>
                                                <b>Admin Password:</b> 
                                                <input type="password" id='admin-password-input-vendor' className='input-entries' onChange={this.validateVendorData} />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInAdminPassword ? {display:'block'} : {display:'none'}}>
                                                    Invalid Password | password must be more than 6 characters | Must have an upper case and Lowercase | Must have special Symbol ($) and numeric character
                                                </div>
                                            </div>
                                            
                                            <div className='margin-top10'>
                                                <b>Admin Re-Type Password:</b> 
                                                <input type="password" id='admin-re-type-password-input-vendor' className='input-entries' onChange={this.validateVendorData} />
                                                <div className='error-message' style={this.state.vendorAccountTypeErrorArray.errorInAdminRetypePassword ? {display:'block'} : {display:'none'}}>
                                                    Re-Type admin password
                                                </div>
                                            </div>

                                            <div id='submit-driver-info-container'>
                                                <button className='custom-button' id='submit-vendor-info' onClick={() => this.submitVendorInfo()}>
                                                    Submit Info
                                                </button>
                                            </div>
                                        </div>
                                    :
                                        ''
                                }
                            </div>
                        :
                            ''
                    }

                    <Footer />
                </div>                          
            )   
    }
}

const mapStateToProps = state => ({
    destinationAddressData: state.utility.destinationAddressData
});
  

export default connect(mapStateToProps, {})(SignUp);