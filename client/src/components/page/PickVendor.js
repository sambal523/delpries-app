//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';

//custom made component
import PickProducts from '../page/PickProducts';
import Footer from '../utility/Footer';
import PageLoading from '../utility/PageLoading';

//stylings
import '../../stylings/pick-vendor.css';

//images
import company_logo from '../../images/icons/logo9.png';

//icons
import { MdStar } from 'react-icons/md';

//definitions
const _ = require('lodash');

class PickVendor extends Component {
    constructor(props) {
        super(props);

        this.state = {
            productTypes : [],
            serviceCategories: [],
            serviceCategoryIdChosen: this.props.serviceCategoryIdChosen,
            htmlIdOfChosenServiceCategory : '',
            serviceCategoriesSelectionMenuComponents : [],
            productVendorsForTheServiceCategory: [],
            productVendorsForTheServiceCategoryComponent: [],
            vendorIdChosen: '',
            showPickProduct: true,
            showPageLoading: true,

            loadMore: {
                incrementVal: 1,
                searchCount: 0
            }
        }
    }
    
    handleChange = selectedOptionVal => {
        this.setState({ selectedOption : selectedOptionVal });
    };

    handleServiceCategorySelection = (serviceCategoryId) => {
        this.setState({ serviceCategoryIdChosen : serviceCategoryId });

        //store the selection in local-storage
        if (global.localStorage) {
            localStorage.setItem('service-category-id', serviceCategoryId);
        }

        document.getElementById(`${this.state.htmlIdOfChosenServiceCategory}`).style.backgroundColor="#fdfdfd";
        document.getElementById(`${serviceCategoryId}`).style.backgroundColor="#45a470";

        //update the old id
        this.setState({ htmlIdOfChosenServiceCategory : serviceCategoryId });

        //get the data for this service category
        this.getProductVendorData(serviceCategoryId, false);
    }

    filterProductVendorList = () => {
        //make the product vendor components
        var filterVendorName = document.getElementById('vendor-name-in-filter').value;

        if(filterVendorName) {
            filterVendorName = filterVendorName.toLowerCase();
            var vendors = _.filter(this.state.productVendorsForTheServiceCategory, function(vendor){
                return vendor.product_vendor_name.toLowerCase().indexOf(filterVendorName) > -1;
            });

            this.makeProductVendorComponent(vendors);
        } else {
            this.makeProductVendorComponent(this.state.productVendorsForTheServiceCategory);
        }
    }

    clearFilterProductVendorList = () => {
        document.getElementById('vendor-name-in-filter').value = '';

        this.makeProductVendorComponent(this.state.productVendorsForTheServiceCategory)
    }

    handleVendorSelection = vendorId => {
        this.setState({vendorIdChosen: vendorId});

        //store the selection in local-storage
        if (global.localStorage) {
            localStorage.setItem('vendor-id', vendorId);
        }

        this.setState({showPickProduct: true});
    }

    getProductVendorData = (serviceCategoryId = undefined, calledToLoadMore) => {
        // if(calledToLoadMore) {
        //     this.setState({loadMore : {incrementVal: this.state.loadMore.incrementVal, searchCount: ++this.state.loadMore.searchCount}});
        // }

        //get all product vendors for the type currently selected -- /${this.state.loadMore.incrementVal}/${this.state.loadMore.searchCount}
        axios.get(`${apiURL}/api/pick_items/get_prod_vendors/${serviceCategoryId || this.state.serviceCategoryIdChosen}`)
        .then(res => {
            if(calledToLoadMore) {
                //join the newly loaded vendors with the vendor list we have
                this.makeProductVendorComponent(this.state.productVendorsForTheServiceCategory.concat(res.data.productVendors));
                this.setState({productVendorsForTheServiceCategory: this.state.productVendorsForTheServiceCategory.concat(res.data.productVendors)});
            } else {
                this.setState({productVendorsForTheServiceCategory: res.data.productVendors});
                this.makeProductVendorComponent(res.data.productVendors);
            }

            this.setState({showPageLoading : false });
        });
    }

    makeProductVendorComponent = (productVendorData) => {
        //build the product vendor components
        var count = 0;
        var productVendorsForTheServiceCategoryComponentTemp = [];
        var imgUrl, iconUrl = '';
        this.setState({showPageLoading : true });

        if(productVendorData.length > 0 ) {
            productVendorData.forEach(productVendor => {
                //build the icon images for the more-info-icon
                imgUrl = require('../../images/vendor_images/' + productVendor.product_vendor_image_name+'.jpg');
                iconUrl = require('../../images/vendor_images/' + productVendor.product_vendor_icon_name+'.png');

                productVendorsForTheServiceCategoryComponentTemp.push(
                    <div key={count++} className='product-vendor-each'>            
                        { 
                            !productVendor.is_currently_active ?
                                <div className='currenty-closed-overlay'>Currently Closed</div>
                            :
                                ''
                        }
                        <div className='cursor-pointer' onClick={ () => this.handleVendorSelection(productVendor._id)}>
                            <div className='product-vendor-image-container'>
                                <img className='product-vendor-image' src={imgUrl} alt={productVendor.service_category_image_icon}/>
                            </div>
                            <div className='product-vendor-logo-container'>
                                <img className='product-vendor-logo' src={iconUrl} alt={productVendor.service_category_image_icon}/>
                            </div>
                            <div className='product-vendor-title'>{productVendor.product_vendor_name}</div>
                            <div className='product-vendor-ratings'>{productVendor.product_vendor_ratings} <MdStar className='star-icon' /></div>
                        </div>
                    </div>
                );
            }); //end of the forEach
        } else {
            productVendorsForTheServiceCategoryComponentTemp.push(
                <div id='no-sellers-available' key={'key-no-sellers-available'}>
                    No Store Available for this Product Type
                </div>
            );
        }
        
        this.setState({productVendorsForTheServiceCategoryComponent : productVendorsForTheServiceCategoryComponentTemp});
        this.setState({showPageLoading : false });
    }

    componentDidMount = () => {
        //check if a service category has been chosen
        var vendorId = localStorage.getItem('vendor-id');
        if(vendorId) {
            this.setState({vendorId: vendorId});
            this.setState({showPickProduct: true});
        } else {
            this.setState({vendorId: ''});
            this.setState({showPickProduct: false});
        }

        //get all products types
        axios.get(`${apiURL}/api/home/all_serv_cat`)
        .then(res => {
            this.setState({serviceCategories: res.data})
              
            //build the products selection menu
            var serviceCategoriesSelectionMenuComponentsTemp = [];
            var imgUrl = '';
            var count = 0;

            //style the background of the service category that the user picked
            var styleUsersPickedBackground = {
                backgroundColor: '#45a470'
            };

            res.data.forEach(value => {
                //build the icon images for the more-info-icon
                imgUrl = require('../../images/icons/' + value.service_category_image_icon);
                count++;

                serviceCategoriesSelectionMenuComponentsTemp.push(
                    <div id={value._id} key={count} className='service-categories-container' onClick={ () => this.handleServiceCategorySelection(value._id)} style={value._id === this.state.serviceCategoryIdChosen ? styleUsersPickedBackground : {}}> 
                        {/* get the id of the one that the user choose for styling purposes */}
                        {value._id === this.state.serviceCategoryIdChosen ? this.setState({htmlIdOfChosenServiceCategory:value._id}) : ''}
                        <div className='service-categories-icon-container'>
                            <img className='service-categories-icon-image' src={imgUrl} alt={value.service_category_image_icon}/>
                        </div>
                        <div className='service-categories-title-text'>{value.service_category_name}</div>
                    </div>
                );
            });
            
            //ending the for each
            this.setState({serviceCategoriesSelectionMenuComponents : serviceCategoriesSelectionMenuComponentsTemp});
        });

        //make the product vendor components
        this.getProductVendorData(undefined, false);
    }


    render() {
        return (
            <div id='all-container'>
                {!this.state.showPickProduct ? 
                    <div id='pick-items-component-container'>
                        <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/>

                        <div className='absolute-company-logo'>
                            <img src={company_logo} className='image-take-full-space' alt='company_logo'/>
                        </div>

                        <div id='filter-container'>
                            <div id='select-product-type-container'>
                                <div className='title title-for-pick-items'>Change Store Category</div>
                                <div> {this.state.serviceCategoriesSelectionMenuComponents} </div>
                            </div>

                            <div id='pick-vendor-filter'>
                                <input type='text' id='vendor-name-in-filter' placeholder='Enter Store Name' className='vendor-name-entry' onChange={this.filterProductVendorList}/>
                                <button id='filter-vendor-list-button' className='custom-button' onClick={() => this.filterProductVendorList()}>Filter</button>
                                <button id='clear-filter-vendor-list-button' className='custom-button' onClick={() => this.clearFilterProductVendorList()}>Clear Filter</button>
                            </div>
                        </div>

                        <div id='product-vendors-container'>
                            <div id='title-for-product-vendors' className='title'>Stores</div>
                            <div id='product-vendors-container-stores-list'>
                                {this.state.productVendorsForTheServiceCategoryComponent}

                                {/* <div className='full-width center clear-both'>
                                    <div className='load-more' onClick={() => {this.getProductVendorData(undefined, true)}}>
                                        Load More
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </div>
                :
                    <PickProducts vendorIdChosen={this.state.vendorIdChosen === '' ? '5d870cd1f2f1ad35d8044db9' : this.state.vendorIdChosen}/>
                }

                <div id='product-vendor-footer'>
                    <Footer/>
                </div>
            </div>
        )
    }
}


export default PickVendor;