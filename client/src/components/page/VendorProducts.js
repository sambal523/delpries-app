//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';
import Select from 'react-select';

//redux imports
import { connect } from 'react-redux';
import PropTypes, { array } from 'prop-types';

//custom made component
import PageLoading from '../utility/PageLoading';

//icons
import { MdClear } from 'react-icons/md';
import { MdAddCircleOutline } from 'react-icons/md';
import { FiEdit2, FiTrash2 } from 'react-icons/fi';

//utility functions
import {capitalizeFirstLetter, addLoadingIconProcedurally, removeLoadingIconProcedurally} from '../utility/utilityFunctions';

//stylings
import '../../stylings/vendor-products.css';

//define loadash
const _ = require('lodash');
const $ = require("jquery"); 

class VendorProducts extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            goodColor: '#258e25',
            badColor: '#ff3333',
            blueColor: '#4d79ff',
            defaultColor: '#a7a7a7',
            blackColor: '#000000',
            allProductsData : null,
            filteredProductAmount: 0,
            productsListComponents : [],
            vendorId : this.props.userData.vendor_id_for_vendor,
            showPageLoading : true,
            showModal: false,
            showEditProductDetailsInModal: false,
            serviceCategories : [],
            selectOptions : [],
            selectedValueFilterServiceCategory: {  value: 'no-value',  label: 'Choose a service Category' },
            selectedValueFilterServiceCategoryProductModal : {},
            selectedProductId : '',
            selectedProductName : '',
            selectedProductPrice : '',
            selectedProductDescription : '',
            selectedProductType: '',
            selectedProductNonCountableOutOfStock: false,
            productDataErrorArray : {
                errorInProductName: false,
                errorInServiceCategory: false,
                errorInPrice : false,
                errorInProductDescription : false,
                errorInProductType: false,
                errorInAmountLeft : false
            },
            response : {
                status: false,
                msg: ''
            },
            newAddons: {
                errorInTitle: false
            },
            iWantToAddNotEdit : false,

            showAddNewAddons: false,
            newAddonsListComponents: [],
            newAddonsData: [],
            allAddonsListComponents: [],
            showAddonsContainer: true,

            addonGroupTitle: '',
            addonGroupHowManyToBeChosen: 1,
            addonGroupEditId: '',

            showDeleteAddonGroup: false,
            deleteAddonMessage: '',
            deleteAddonId: ''
        }

        //this is the token
        const token = localStorage.getItem('token');
        
        //headers
        const config = {
            headers: {
                'x-auth-token': token
            }
        }

        //get all products for this vendor
        axios.get(`${apiURL}/api/vendors/get_all_products_for_this_vendor/${this.props.userData.vendor_id_for_vendor}`, config)
        .then(res => {
            //show the loading sign
            this.setState({showPageLoading : true });

            //build the service category components
            this.setState({serviceCategories : res.data.serviceCategories});
            if(res.data.serviceCategories) { 
                var selectOptionsTemp = [];

                selectOptionsTemp.push(
                    { 
                        value: 'no-value', 
                        label: 'Choose a service Category'
                    }
                );

                res.data.serviceCategories.forEach(singleServiceCategory => {
                    selectOptionsTemp.push(
                        { 
                            value: singleServiceCategory._id, 
                            label: singleServiceCategory.service_category_name 
                        }
                    );
                });

                this.setState({selectOptions : selectOptionsTemp})
            }

            //build the orders data
            this.buildProductsToBeDisplayed(res.data.products);
            this.setState({allProductsData : res.data.products});
        })
        .catch(err => {});
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        userData: PropTypes.object
    }

    addOneNewRowOfNewAddon = () => {
        //add the empty fields to the newAddonsListComponents
        var tempNewAddonsListComponents = [];
        var tempNewAddonsData = [];
        var key = Date.now();

        tempNewAddonsData.push({
            key: key+'',
            name: '',
            price: ''
        });

        this.setState({newAddonsData : tempNewAddonsData});

        tempNewAddonsListComponents.push(
            <div key={key} id={key+'whole-container'}>
                <div className='left-names'> <input type="text" id={key+'-name-input'} className='input-entries-addons-names' onChange={(e) => this.handleChangeInAddonName(key+'', e)}/> </div>
                <div className='right-prices'> <input id={key+'-price-input'} type="text" className='input-entries-addons-price' onChange={(e) => this.handleChangeInAddonPrice(key+'', e)}/> </div>
                <div className='right-close'> <MdClear id={key+'-cancel'} className='input-entries-addons-clear' onClick={() => this.handleRemoveAddon(key+'')}/> </div>
            </div>
        );

        this.setState({newAddonsListComponents: tempNewAddonsListComponents});
    }

    componentDidMount = () => {
        this.addOneNewRowOfNewAddon();
    }

    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
            this.setState({showEditProductDetailsInModal: false});
          
            //reset the response for calls
            this.setState({response: {status: false, msg: ''}});
        }

        //open the modal
        this.setState({showModal: !this.state.showModal});
    }

    toggleShowNewAddons = () => {
        this.setState({showAddNewAddons: !this.state.showAddNewAddons})

        //clear the entries
        this.setState({addonGroupTitle: ''})
        this.setState({addonGroupHowManyToBeChosen: 1})
        this.setState({addonGroupEditId: ''});
        this.setState({newAddonsListComponents: []});  
        this.setState({newAddonsData: []});
        this.addOneNewRowOfNewAddon();
    }

    handleChangeForServiceCategorySelection = (value) => {
        this.setState({ selectedValueFilterServiceCategory : value })

        //filter the list
        this.filterProducts(value);
    }

    handleChangeProductNameInModal = (value) => {
        this.setState({ selectedProductName : document.getElementById('product-name-input').value });
        this.validateProductData();
    }

    handleChangeForServiceCategorySelectionProductModal = (value) => {
        this.setState({ selectedValueFilterServiceCategoryProductModal : value });
        this.validateProductData();
    }

    handleChangePriceInModal = (value) => {
        this.setState({ selectedProductPrice : document.getElementById('price-input').value });
        this.validateProductData();
    }

    handleChangeDescriptionInModal = (value) => {
        this.setState({ selectedProductDescription : document.getElementById('product-description-textarea').value });
        this.validateProductData();
    }

    handleChangeInProductType = (value) => {
        this.setState({ selectedProductType : value});
        this.validateProductData();
    }

    handleChangeAmountLeftInModal = (value) => {
        this.setState({ selectedProductAmountLeft : document.getElementById('amount-left-input').value });
        this.validateProductData();
    }

    filterProducts = (serviceCategoryOption = undefined) => {
        this.buildProductsToBeDisplayed(this.state.allProductsData, document.getElementById('product-name-in-filter').value, serviceCategoryOption)
    }

    clearFilter = () => {
        document.getElementById('product-name-in-filter').value = '';
        this.setState({selectedValueFilterServiceCategory: this.state.selectOptions[0]});

        this.buildProductsToBeDisplayed(this.state.allProductsData)
    }

    displayKey = (key) => {
    }

    editAddonGroup = (singleAddonGroup) => {
        //set the values
        this.setState({addonGroupTitle: singleAddonGroup.group_name})
        this.setState({addonGroupHowManyToBeChosen: singleAddonGroup.how_many_must_be_chosen})
        this.setState({addonGroupEditId: singleAddonGroup._id});

        //porpulate the addons items and prices
        if(singleAddonGroup.addons) {
            //add the empty fields to the newAddonsListComponents
            var tempNewAddonsListComponents = [];
            var tempNewAddonsData = [];
            var addons = singleAddonGroup.addons;

            addons.forEach(singleAddon => {     
                var key = singleAddon._id;

                tempNewAddonsData.push({
                    key: key,
                    name: singleAddon.name,
                    price: singleAddon.price
                });

                tempNewAddonsListComponents.push(
                    <div key={key} id={key+'whole-container'}>
                        <div className='left-names'> <input type="text" defaultValue={singleAddon.name} id={key+'-name-input'} className='input-entries-addons-names' onChange={(e) => this.handleChangeInAddonName(key, e)}/> </div>
                        <div className='right-prices'> <input id={key+'-price-input'} defaultValue={singleAddon.price} type="text" className='input-entries-addons-price' onChange={(e) => this.handleChangeInAddonPrice(key, e)}/> </div>
                        <div className='right-close'> <MdClear id={key+'-cancel'} className='input-entries-addons-clear' onClick={() => this.handleRemoveAddon(key)}/> </div>
                    </div>
                );
            });

            this.setState({newAddonsData : tempNewAddonsData});
            this.setState({newAddonsListComponents: tempNewAddonsListComponents});
        }


        //open the data in the slide down group
        this.setState({showAddNewAddons: !this.state.showAddNewAddons})
    }

    openDeleteGroupAddonModal = (id, groupName) => {
        this.setState({deleteAddonId: id});
        this.setState({deleteAddonMessage: `Are you sure you want to delete the ${groupName} Addon Group?`});
        this.setState({showDeleteAddonGroup: true});
    }

    noToDeleteGroupAddon = () => {
        this.setState({showDeleteAddonGroup: false});
    }

    yesToDeleteGroupAddon = () => {
        this.setState({showDeleteAddonGroup: true});

        //headers
        const config = {
            headers: {
                'x-auth-token': localStorage.getItem('token')
            }
        }

        //get all products for this vendor
        axios.get(`${apiURL}/api/vendors/del_addon_group/${this.state.deleteAddonId}/${this.state.selectedProductId}`, config)
        .then(res => {
            this.setState({response: {status: res.data.deleted, msg: res.data.msg}});

            var updatedProduct = res.data.updatedProduct;
            if(res.data.updatedProduct && res.data.deleted) {
                //replace the updated one in our list
                var allProductData = this.state.allProductsData;
                var index = allProductData.findIndex(item => item._id === this.state.selectedProductId);
                allProductData.splice(index, 1, updatedProduct)

                //rebuild the products
                this.filterProducts(this.state.selectedValueFilterServiceCategory);

                //handle the addons
                //--close the add new addons drop
                this.setState({showAddNewAddons: false})

                //update the addons group list
                this.buildAddonGroupList(res.data.updatedProduct.add_on_groups);

                this.setState({showDeleteAddonGroup: false});
            }
        })
    }
    
    buildAddonGroupList = (addonGroups) => {
        var tempAllAddonsListComponents = [];
        var count = 0;

        //build the addons list
        if(addonGroups) {
            addonGroups.forEach(singleAddon => {
                tempAllAddonsListComponents.push(
                    <div className='single-addon-group-container' key={count++}>
                        <div className='icons-single-addon-group-container'>
                            <FiEdit2 className='icons-single-addon-group' onClick={() => this.editAddonGroup(singleAddon)}/>
                            <FiTrash2 className='icons-single-addon-group' onClick={() => this.openDeleteGroupAddonModal(singleAddon._id, capitalizeFirstLetter(singleAddon.group_name))}/>
                        </div>
                        <div><b>Addon Group Name:</b> {capitalizeFirstLetter( singleAddon.group_name)}</div>
                        <div><b>How many items must user choose:</b> {singleAddon.how_many_must_be_chosen}</div>
                        <div><b>Addons Available:</b> {singleAddon.addons.length}</div>
                    </div>
                )
            });
            
            //update the list
            this.setState({allAddonsListComponents : tempAllAddonsListComponents});
        }
    }

    openModalWithEditProduct = (singleProduct) => {
        //show the loading sign
        this.setState({showPageLoading : true });

        //
        this.buildAddonGroupList(singleProduct.add_on_groups);
        this.setState({showAddonsContainer : true});

        //change the contents in the modal
        this.setState({iWantToAddNotEdit : false})

        //get the selected product
        this.setState({selectedProductId : singleProduct._id});
        this.setState({selectedValueFilterServiceCategoryProductModal : {value: singleProduct.service_category_id, label: this.getTheServiceCategory(singleProduct.service_category_id) ? this.getTheServiceCategory(singleProduct.service_category_id).service_category_name : 'unavaliable category'}});
        this.setState({selectedProductName: singleProduct.product_name});    
        this.setState({selectedProductPrice: singleProduct.product_price});    
        this.setState({selectedProductDescription: singleProduct.product_description});
        this.setState({selectedProductType: singleProduct.uncountable_product_type ? 'non-countable' : 'countable'});
        this.setState({selectedProductNonCountableOutOfStock: singleProduct.uncountable_product_type_out_of_stock});
        this.setState({selectedProductAmountLeft: singleProduct.product_number_available});
        this.setState({selectedProductIsActive : singleProduct.is_active});

        //close the loading sign
        this.setState({showPageLoading : false });

        //allow the modal data to show
        this.setState({showEditProductDetailsInModal : true });

        //toggle modal
        this.toggleModal();
    }

    getTheServiceCategory(serviceCategoryId) {
        return _.find(this.state.serviceCategories, {_id: serviceCategoryId});
    }

    buildProductsToBeDisplayed = (products, filterProductName, filterServiceCategoryOption) => {
        //filter the product data first
        if(filterProductName) {
            filterProductName = filterProductName.toLowerCase();
            products = _.filter(products, function(singleProduct){
                return singleProduct.product_name.toLowerCase().indexOf(filterProductName) > -1;
            });
        }

        if(filterServiceCategoryOption && filterServiceCategoryOption.value && filterServiceCategoryOption.value !== 'no-value') {
            products = _.filter(products, function(singleProduct){
                return singleProduct.service_category_id === filterServiceCategoryOption.value
            });
        }

        this.setState({filteredProductAmount: products.length});
        if(products.length > 0) { 
            var count = 0;
            var productsListComponentsTemp = [];

            products.forEach(singleProduct => {
                //get the logo
                productsListComponentsTemp.push(
                    <div key={count++} className='product-single-info-container' style={singleProduct.is_active ? 
                                                                                            singleProduct.uncountable_product_type ?
                                                                                                singleProduct.uncountable_product_type_out_of_stock ?
                                                                                                    {borderBottom:`3px solid ${this.state.blueColor}`, height : '247px' } 
                                                                                                :
                                                                                                    {border: 'none'} 
                                                                                            :
                                                                                                singleProduct.product_number_available === 0 ? 
                                                                                                    {borderBottom:`3px solid ${this.state.blueColor}`, height : '247px' } 
                                                                                                : 
                                                                                                    {border: 'none'} 
                                                                                        : 
                                                                                            {borderBottom:`3px solid ${this.state.badColor }`, height : '247px'}} 
                                                                                  onClick={() => this.openModalWithEditProduct(singleProduct)}>
                        <div className='product-info-single-regular'><b>Name: </b>{singleProduct.product_name}</div>
                        <div className='product-info-single-regular'><b>Price: </b>${singleProduct.product_price}</div>
                        <div className='product-info-single-regular'><b>Service Category: </b>{this.getTheServiceCategory(singleProduct.service_category_id) ? this.getTheServiceCategory(singleProduct.service_category_id).service_category_name : 'unavaliable category'}</div>   
                        {
                            !singleProduct.uncountable_product_type ? 
                                <div className='product-info-single-regular'><b>Amount Left: </b>{singleProduct.product_number_available}</div>
                            :
                                ''
                        }
                        <div className='product-info-single-description'><b>Description: </b>{singleProduct.product_description}</div>
                    </div>
                );
            });

            this.setState({productsListComponents : productsListComponentsTemp});
        } else {
            this.setState({productsListComponents : <div key={'no-product-in-list'} id='no-product-available-in-list'>No Products Available</div>});
        }

        //remove the loading sign
        this.setState({showPageLoading : false });
    }

    validateProductData = () => {
        //reset the response for calls
        this.setState({response: {status: false, msg: ''}});

        var noErrors = [{productNameError: true, productServiceCategoryError: true, priceError: true, descriptionError: true, productTypeError: true,
          amountLeftError: true, addonTitleError: true, addonAmountToChooseError: true, addonsError: true}];

        var productName = document.getElementById('product-name-input').value;
        var productServiceCategoryId = ''; 
        var price = document.getElementById('price-input').value;
        var description = document.getElementById('product-description-textarea').value; 
        var countableProduct = document.getElementById('countable');
        var nonCountableProduct = document.getElementById('non-countable');

        if(productName) {
            noErrors.productNameError = false;
            document.getElementById('product-name-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.productNameError = true;
            document.getElementById('product-name-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(this.state.selectedValueFilterServiceCategoryProductModal.value !== 'no-value') {
            noErrors.productServiceCategoryError = false;
            document.getElementById('service-type-selection-in-product-modal').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.productServiceCategoryError = true;
            document.getElementById('service-type-selection-in-product-modal').style.borderColor = `${this.state.defaultColor}`;
        }

        if(price && !isNaN(price)) {
            noErrors.priceError = false;
            document.getElementById('price-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.priceError = true;
            document.getElementById('price-input').style.borderColor = `${this.state.defaultColor}`;
        }
        
        if(description) {
            noErrors.descriptionError = false;
            document.getElementById('product-description-textarea').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.descriptionError = true;
            document.getElementById('product-description-textarea').style.borderColor = `${this.state.defaultColor}`;
        }

        if(countableProduct.checked || nonCountableProduct.checked) {
            noErrors.productTypeError = false;

            if(countableProduct.checked) {
                if(document.getElementById('amount-left-input')) {
                    var amountLeft = document.getElementById('amount-left-input').value;
            
                    if(amountLeft && !isNaN(amountLeft)) {
                        noErrors.amountLeftError = false;
                        document.getElementById('amount-left-input').style.borderColor = `${this.state.goodColor}`;
                    } else {
                        noErrors.amountLeftError = true;
                        document.getElementById('amount-left-input').style.borderColor = `${this.state.defaultColor}`;
                    }
                }
            } else {
                noErrors.amountLeftError = false;
            }
        } else {
            noErrors.productTypeError = true;
            noErrors.amountLeftError = false;
        }

        //only if we are to show add new addons should the data be validated
        if(this.state.showAddNewAddons) {
            //validate addons
            var title = document.getElementById('new-addons-title').value;
            var amountUserMustChoose = document.getElementById('new-addons-must-choose-amount').value;
            var allAddonsData = this.state.newAddonsData;

            if(!title) {
                document.getElementById('new-addons-title').style.borderColor = `${this.state.badColor}`;
                noErrors.addonTitleError = true;
            } else {
                document.getElementById('new-addons-title').style.borderColor = `${this.state.defaultColor}`;
                noErrors.addonTitleError = false;
            }

            if(!amountUserMustChoose || isNaN(amountUserMustChoose) || parseInt(amountUserMustChoose) > this.state.newAddonsData.length) {
                document.getElementById('new-addons-must-choose-amount').style.borderColor = `${this.state.badColor}`;
                noErrors.addonAmountToChooseError = true;
            } else {
                document.getElementById('new-addons-must-choose-amount').style.borderColor = `${this.state.defaultColor}`;
                noErrors.addonAmountToChooseError = false;
            }

            //key name price
            for(var i=0; i<allAddonsData.length; i++) {
                if(allAddonsData[i].name === '') {
                    document.getElementById(allAddonsData[i].key+'-name-input').style.borderColor = `${this.state.badColor}`; 
                    noErrors.addonsError = true;
                    break;
                } else {
                    noErrors.addonsError = false;
                }

                /*if(allAddonsData[i].price === 0 || allAddonsData[i].price === '0') {  noErrors.addonsError = false;
                          } else if(!(allAddonsData[i].price) || !(isFloat(allAddonsData[i].price) || isInt(allAddonsData[i].price))) {
                  document.getElementById(allAddonsData[i].key+'-price-input').style.borderColor = `${this.state.badColor}`; 
                  noErrors.addonsError = true;
                  break;
                  */
                //we have this because 0 -> the value can be interpreted as false but this is a bypass for it
                if(allAddonsData[i].price === 0 || allAddonsData[i].price === '0') {  
                    noErrors.addonsError = false;
                }else if(!Number(allAddonsData[i].price)) {
                    noErrors.addonsError = true;
                    break;
                } else {
                    noErrors.addonsError = false;
                }
            }
        } else {
            noErrors.addonTitleError = false;
            noErrors.addonAmountToChooseError = false;
            noErrors.addonsError = false;
        }


        //update the data
        this.setState({productDataErrorArray : {
            errorInProductName: noErrors.productNameError, 
            errorInServiceCategory: noErrors.productServiceCategoryError, 
            errorInPrice: noErrors.priceError, 
            errorInProductDescription: noErrors.descriptionError, 
            errorInProductType: noErrors.productTypeError,
            errorInAmountLeft: noErrors.amountLeftError
        }})
      
        return !noErrors.productNameError && !noErrors.productServiceCategoryError && !noErrors.priceError && !noErrors.descriptionError && !noErrors.productTypeError && !noErrors.amountLeftError &&
              !noErrors.addonTitleError && !noErrors.addonAmountToChooseError && !noErrors.addonsError;
    }

    UpdateProductData = () => {
        //show the loading icon
        this.setState({showPageLoading : true });

        if(this.validateProductData()) {
            //
            addLoadingIconProcedurally('vendor-products-alpha-container');

            var productData = {
                productId: this.state.selectedProductId,
                productName: this.state.selectedProductName,
                serviceCategoryId: this.state.selectedValueFilterServiceCategoryProductModal.value,
                price: this.state.selectedProductPrice,
                productDescription: this.state.selectedProductDescription,
                amountLeft: this.state.selectedProductAmountLeft,
                productIsCountable: document.getElementById('countable').checked,
                thereAreAddons: this.state.showAddNewAddons,
                addonsInfo: undefined
            }

            if(this.state.showAddNewAddons) {
                productData.addonsInfo = {
                    title: (document.getElementById('new-addons-title').value).toLowerCase(),
                    amountUserMustChoose: document.getElementById('new-addons-must-choose-amount').value,
                    addonsInfo: this.state.newAddonsData,
                    addonGroupId: this.state.addonGroupEditId
                }
            }

            if(!productData.productIsCountable) {
                productData.amountLeft = 0;
            }

            //headers
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
                
            //make body
            const body = JSON.stringify(productData);

            axios.post(`${apiURL}/api/vendors/update_product`, body, config)
            .then(res => {          
                //remove loading icon
                removeLoadingIconProcedurally();

                //close the page loading
                this.setState({showPageLoading : false });

                this.setState({response: {status: res.data.productUpdated, msg: res.data.msg}});

                var updatedProduct = res.data.updatedProduct;
                if(res.data.updatedProduct) {
                    //replace the updated one in our list
                    var allProductData = this.state.allProductsData;
                    var index = allProductData.findIndex(item => item._id === productData.productId);
                    allProductData.splice(index, 1, updatedProduct)

                    //rebuild the products
                    this.filterProducts(this.state.selectedValueFilterServiceCategory);

                    //handle the addons
                    //--close the add new addons drop
                    this.setState({showAddNewAddons: false})

                    //update the addons group list
                    this.buildAddonGroupList(res.data.updatedProduct.add_on_groups);
                }
            });
        } 

        //remove the loading icon
        this.setState({showPageLoading : false });
    }

    AlternateDeactivateActivateProduct = () => {
        //show the loading icon
        this.setState({showPageLoading : true });

        if(this.validateProductData()) {
            var productId = this.state.selectedProductId;

            //headers
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
            
            //make body
            const body = JSON.stringify({productId});

            axios.post(`${apiURL}/api/vendors/deactivate_product`, body, config)
            .then(res => {          
                //close the page loading
                this.setState({showPageLoading : false });

                this.setState({response: {status: res.data.productDeactivated, msg: res.data.msg}});

                var deactivatedProduct = res.data.deactivatedProduct;
                if(res.data.deactivatedProduct) {
                    //update the selected 
                    this.setState({selectedProductIsActive : deactivatedProduct.is_active});

                    //replace the updated one in our list
                    var allProductData = this.state.allProductsData;
                    var index = allProductData.findIndex(item => item._id === productId);
                    allProductData.splice(index, 1, deactivatedProduct)

                    //rebuild the products
                    this.filterProducts(this.state.selectedValueFilterServiceCategory);
                }
            });
        }

        //remove the loading icon
        this.setState({showPageLoading : false });
    }

    ProductOutOfStock = (productIsOutOfStock) => {
        //show the loading icon
        this.setState({showPageLoading : true });

        if(this.validateProductData()) {
            var productId = this.state.selectedProductId;

            //headers
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
                
            //make body
            const body = JSON.stringify({productId, productIsOutOfStock});

            axios.post(`${apiURL}/api/vendors/product_in_stock_status`, body, config)
            .then(res => {         
                //close the page loading
                this.setState({showPageLoading : false });

                this.setState({response: {status: res.data.productUpdated, msg: res.data.msg}});

                if(res.data.productUpdated) {
                    this.state.selectedProductNonCountableOutOfStock = productIsOutOfStock;
                }

                var updatedProduct = res.data.updatedProduct;
                if(res.data.updatedProduct) {
                    //replace the updated one in our list
                    var allProductData = this.state.allProductsData;
                    var index = allProductData.findIndex(item => item._id === productId);
                    allProductData.splice(index, 1, updatedProduct)

                    //rebuild the products
                    this.filterProducts(this.state.selectedValueFilterServiceCategory);
                }
            });
        }

        //remove the loading icon
        this.setState({showPageLoading : false });
    }

    openAddModal = () => {
        //clear addons and addons group
        this.setState({allAddonsListComponents : []});
        this.setState({showAddNewAddons: false});
        this.setState({showAddonsContainer : false});

        //show the loading icon
        this.setState({showPageLoading : true });

        //change to i want to add edit
        this.setState({iWantToAddNotEdit : true})
        
        //get the selected product
        this.setState({selectedProductId : ''});
        this.setState({selectedValueFilterServiceCategoryProductModal : {  value: 'no-value',  label: 'Choose a service Category' }});
        this.setState({selectedProductName: ''});    
        this.setState({selectedProductPrice: ''});    
        this.setState({selectedProductDescription: ''});
        this.setState({selectedProductType: ''});
        this.setState({selectedProductAmountLeft: ''});


        //close the loading sign
        this.setState({showPageLoading : false });

        //allow the modal data to show
        this.setState({showEditProductDetailsInModal : true });

        //toggle modal
        this.toggleModal();
    }

    CreateProduct = () => {
        //show the loading icon
        this.setState({showPageLoading : true });

        if(this.validateProductData()) {
            var productData = {
                productName: this.state.selectedProductName,
                serviceCategoryId: this.state.selectedValueFilterServiceCategoryProductModal.value,
                price: this.state.selectedProductPrice,
                productDescription: this.state.selectedProductDescription,
                amountLeft: this.state.selectedProductAmountLeft,
                productIsCountable: document.getElementById('countable').checked,
                vendorId: this.state.vendorId
            }

            if(!productData.productIsCountable) {
                productData.amountLeft = 0;
            }

            //headers
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
                
            //make body
            const body = JSON.stringify(productData);
            
            axios.post(`${apiURL}/api/vendors/add_product`, body, config)
            .then(res => {          
                //close the page loading
                this.setState({showPageLoading : false });

                //display the response
                this.setState({response: {status: res.data.productAdded, msg: res.data.msg}});
              
                //get the product added
                var newProduct = res.data.newAdded;
                if(newProduct) {
                    //update the selected 
                    this.setState({selectedProductIsActive : newProduct.is_active});
                    this.setState({selectedProductId : newProduct._id});

                    //change to i want to add edit
                    this.setState({iWantToAddNotEdit : false})
                
                    //replace the updated one in our list
                    var allProductData = this.state.allProductsData;
                    allProductData.push(newProduct);
                    this.setState({allProductsData : allProductData});
                
                    //rebuild the products
                    this.filterProducts(this.state.selectedValueFilterServiceCategory);
                }
            });
        }
      
        //remove the loading icon
        this.setState({showPageLoading : false });
    }

    addNewAddonRow = () => {
        //check and see if the last addon data has been populated
        var lengthOfCurrentAddons = (this.state.newAddonsData.length - 1);
        var newAddons = this.state.newAddonsData;
        var key = Date.now();
        var keyForLastElement = newAddons[lengthOfCurrentAddons].key;

        if(newAddons[lengthOfCurrentAddons].name === '') {
            document.getElementById(keyForLastElement+'-name-input').style.borderColor = `${this.state.badColor}`;   
        } else if(isNaN(newAddons[lengthOfCurrentAddons].price)){
            document.getElementById(keyForLastElement+'-price-input').style.borderColor = `${this.state.badColor}`;   
        } else {
            document.getElementById(keyForLastElement+'-name-input').style.borderColor = `${this.state.defaultColor}`;   
            document.getElementById(keyForLastElement+'-price-input').style.borderColor = `${this.state.defaultColor}`;   
            document.getElementById(keyForLastElement+'-cancel').style.color = `${this.state.blackColor}`;

            //add the empty fields to the newAddonsListComponents
            var tempNewAddonsListComponents = this.state.newAddonsListComponents;
            var tempNewAddonsData = this.state.newAddonsData;

            tempNewAddonsData.push({
                key: key+'',
                name: '',
                price: ''
            });
            this.setState({newAddonsData : tempNewAddonsData});

            tempNewAddonsListComponents.push(
                <div key={key} id={key+'whole-container'}>
                    <div className='left-names'> <input type="text" id={key+'-name-input'} className='input-entries-addons-names' onChange={(e) => this.handleChangeInAddonName(key+'', e)}/> </div>
                    <div className='right-prices'> <input id={key+'-price-input'} type="text" className='input-entries-addons-price' onChange={(e) => this.handleChangeInAddonPrice(key+'', e)}/> </div>
                    <div className='right-close'> <MdClear id={key+'-cancel'} className='input-entries-addons-clear' onClick={() => this.handleRemoveAddon(key+'')}/> </div>
                </div>
            );
            this.setState({newAddonsListComponents: tempNewAddonsListComponents});
        }
    }

    handleChangeInAddonName = (key, value) => {
        //get the addon
        var newAddons = this.state.newAddonsData;

        //find the addon representative data and the index
        var idx = _.findIndex(newAddons, function(addOn) { return addOn.key === key; });

        //update the name
        if(newAddons[idx]) {
            newAddons[idx].name = value.target.value;
        }

        //replace the add on
        this.setState({newAddonsData: newAddons});

        //reset the colors for inputs
        document.getElementById(key+'-cancel').style.color = `${this.state.blackColor}`;
        document.getElementById(key+'-name-input').style.borderColor = `${this.state.defaultColor}`;   
    }

    handleChangeInAddonPrice = (key, value) => {
        value = value.target.value;      

        //get the addon
        var newAddons = this.state.newAddonsData;

        //find the addon representative data and the index
        var idx = _.findIndex(newAddons, function(addOn) { return addOn.key === key; });

        //update the name
        if(newAddons[idx]) {
            newAddons[idx].price = value;
        }

        //replace the add on
        this.setState({newAddonsData: newAddons});

        //test to see if the value is a number
        if(!isNaN(value)) {
            document.getElementById(key+'-price-input').style.borderColor = `${this.state.defaultColor}`;   
            document.getElementById(key+'-cancel').style.color = `${this.state.blackColor}`;
        } else {
            document.getElementById(key+'-price-input').style.borderColor = `${this.state.badColor}`;
        }
    }

    handleRemoveAddon = (key) => {
        //check and see if the last addon data has been populated
        var lengthOfCurrentAddons = this.state.newAddonsData.length;
        var newAddons = this.state.newAddonsData;

        //if we just have 1 we cannot remove it 
        if(lengthOfCurrentAddons > 1) {
            //reset the colors for inputs
            document.getElementById(key+'-cancel').style.color = `${this.state.blackColor}`;

            //remove the div
            $('#'+key+'whole-container').remove(); 

            //remove the divs data
            _.remove(newAddons, function(addOn) {
                return addOn.key === key;
            })

            //replace the add on
            this.setState({newAddonsData: newAddons});
        } else {
            document.getElementById(key+'-cancel').style.color = `${this.state.badColor}`;
        }
    }

    render() {
        return (
            <div id='vendor-products-alpha-container'>
                <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                    <div id='modal-container-vendor-products'>
                        <MdClear id='close-modal' onClick={() => this.toggleModal()}/>
                        <br />
                        <br />
                        {
                            this.state.showEditProductDetailsInModal ? 
                                <div id='edit-product-modal'>
                                <div id='edit-modal-title' className='title'>{this.state.iWantToAddNotEdit ? 'Create A Product' : 'Edit Product'}</div>

                                <div className='margin-top10'>
                                    <b>Product Name:</b> 
                                    <input type="text" className='input-entries' id='product-name-input' defaultValue={this.state.selectedProductName} onChange={this.handleChangeProductNameInModal}/>
                                    <div className='error-message' style={this.state.productDataErrorArray.errorInProductName ? {display:'block'} : {display:'none'}}>
                                        Enter a produt Name
                                    </div>
                                </div>

                                <div id='product-service-category-list-container' className='margin-top10'>
                                    <b>Service Category Of Product:</b> 

                                    <Select 
                                        styles={
                                          {
                                            control: (base, state) => ({
                                              ...base,
                                              '&:hover': this.state.productDataErrorArray.errorInServiceCategory ? '2px solid #a7a7a7' : `2px solid ${this.state.goodColor}`, // border style on hover
                                              border: this.state.productDataErrorArray.errorInServiceCategory ? '2px solid #a7a7a7' : `2px solid ${this.state.goodColor}`, // default border color
                                              boxShadow: 'none', // no box-shadow
                                            }),
                                            singleValue: base => ({
                                              ...base,
                                              color: "#000000"
                                            }),
                                            input: base => ({
                                              ...base,
                                              color: "#000000"
                                            }),
                                            option: base => ({
                                              ...base,
                                              color: "#000000",
                                              fontSize: '16px'
                                            })
                                          }
                                        } 
                                        
                                        id='service-type-selection-in-product-modal' 
                                        value = {this.state.selectedValueFilterServiceCategoryProductModal}
                                        onChange={value => this.handleChangeForServiceCategorySelectionProductModal(value)} 
                                        options={this.state.selectOptions} 
                                        classNamePrefix='react-select-modal'
                                    />
                                    
                                    <div className='error-message' style={this.state.productDataErrorArray.errorInServiceCategory ? {display:'block'} : {display:'none'}}>
                                        Select a Service Category
                                    </div>
                                </div>

                                <div className='margin-top10'>
                                    <b>Price:</b> 
                                    <input type="text" className='input-entries' defaultValue={this.state.selectedProductPrice}  id='price-input' onChange={this.handleChangePriceInModal}/>
                                    <div className='error-message' style={this.state.productDataErrorArray.errorInPrice ? {display:'block'} : {display:'none'}}>
                                        Enter a price 
                                    </div>
                                </div>

                                <div className='margin-top10'>
                                    <b>Product Description:</b> 
                                    <textarea type="text" className='input-entries' defaultValue={this.state.selectedProductDescription} id='product-description-textarea' onChange={this.handleChangeDescriptionInModal}/>
                                    <div className='error-message' style={this.state.productDataErrorArray.errorInProductDescription? {display:'block'} : {display:'none'}}>
                                        Enter product Description
                                    </div>
                                </div>

                                
                                <div className='margin-top10'>
                                    <b>Product Type:</b> 
                                    <div>
                                        <input type="radio" name="product-type" id="countable" value="countable" onChange={() => this.handleChangeInProductType('countable')} checked={this.state.selectedProductType === 'countable' ? true : false}/> Countable<br/>
                                        <input type="radio" name="product-type" id="non-countable" value="non-countable" onChange={() => this.handleChangeInProductType('non-countable')} checked={this.state.selectedProductType === 'non-countable' ? true : false}/> Non-Countable<br/>
                                    </div>
                                    <div className='error-message' style={this.state.productDataErrorArray.errorInProductType ? {display:'block'} : {display:'none'}}>
                                        Please specify the product type, if its countable (products in bags) non-countable (products in servings)
                                    </div>
                                </div>
                                
                                {
                                    this.state.selectedProductType === 'countable' ?
                                        <div className='margin-top10'>
                                            <b>Amount Left:</b> 
                                            <input type="number" className='input-entries' defaultValue={this.state.selectedProductAmountLeft}  id='amount-left-input' onChange={this.handleChangeAmountLeftInModal}/>
                                            <div className='error-message' style={this.state.productDataErrorArray.errorInAmountLeft ? {display:'block'} : {display:'none'}}>
                                                  Enter an Amount 
                                            </div>
                                        </div>
                                    :
                                        ''
                                }

                                <div id='addons-modal-container' style={this.state.showAddonsContainer ? {display:'block'} : {display:'none'}}>
                                    <div id='addons-title'><b>Add-ons</b></div>

                                    {
                                        !this.state.showAddNewAddons ? 
                                            <div id='open-add-new-addons-icon' onClick={() => this.toggleShowNewAddons()}><MdAddCircleOutline /></div>
                                        :
                                            <div id='open-add-new-addons-icon' onClick={() => this.toggleShowNewAddons()}><MdClear /></div>
                                    }

                                    <div id='container-for-all-addons-new-and-group'>
                                        {
                                            this.state.showAddNewAddons ? 
                                                <div id='new-addons-container'>
                                                    <div className='margin-top10 row-of-info'>
                                                        <b className='title-entries-addons'>Title:</b> 
                                                        <input type="text" className='input-entries-addons' id='new-addons-title' defaultValue={this.state.addonGroupTitle}/>
                                                        <div className='error-message-addons' style={this.state.newAddons.errorInTitle ? {display:'block'} : {display:'none'}}>
                                                            Please enter a title
                                                        </div>
                                                    </div>

                                                    <div className='margin-top10 row-of-info'>
                                                        <b className='title-entries-addons'>How many must be chosen:</b> 
                                                        <input type="number" className='input-entries-addons' id='new-addons-must-choose-amount' defaultValue={this.state.addonGroupHowManyToBeChosen}/>
                                                        <div className='error-message-addons' style={this.state.newAddons.errorInTitle ? {display:'block'} : {display:'none'}}>
                                                            Enter an Amount that range with the amount of add-ons
                                                        </div>
                                                    </div>


                                                    <div className='margin-top10'>
                                                        <div id='new-add-ons-specifics'>
                                                            <div className='left-names'> <b>Names</b> </div> 
                                                            <div className='right-close'/>
                                                            <div className='straight-line margin-top-bottom-5' />

                                                            <div id='new-addons-list'> 
                                                                {this.state.newAddonsListComponents}
                                                            </div>

                                                            <div className='margin-top-bottom-5'>
                                                                <MdAddCircleOutline id='add-new-addon-specifics-icon' onClick={() => this.addNewAddonRow()}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            :
                                                ''
                                        }

                                        <div id='all-addons-group'>
                                            {this.state.allAddonsListComponents}
                                        </div>
                                    </div>
                                </div>

                                <div id='buttons-product-modal'>
                                    {
                                        !this.state.iWantToAddNotEdit ? 
                                            <div>
                                                <button id='update-product-button' className='custom-button' onClick={this.UpdateProductData}>Update</button>
                                                <button id='delete-product-button' className='custom-button' onClick={this.AlternateDeactivateActivateProduct}>{this.state.selectedProductIsActive ? 'Deactivate Product' : 'Activate Product'}</button>
                                                {
                                                    this.state.selectedProductType === 'non-countable' ?
                                                        this.state.selectedProductNonCountableOutOfStock ?
                                                            <button id='in-stock-button' className='custom-button' onClick={() => this.ProductOutOfStock(false)}>In Stock</button>
                                                        :
                                                            <button id='out-of-stock-button' className='custom-button' onClick={() => this.ProductOutOfStock(true)}>Out Of Stock</button>
                                                    :
                                                      ''
                                                }
                                            </div>
                                        :
                                            <button id='create-product-button' className='custom-button' onClick={this.CreateProduct}>Add Product</button>
                                    }
                                </div>

                                <div className='response-from-database-in-modal center margin-top20' style={this.state.response.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.response.msg}</div>
                            </div>
                            :
                                ''
                        }
                    </div>

                    {
                        this.state.showDeleteAddonGroup ? 
                            <div className='modal-background' style={{display:'block'}}>
                                <div id='modal-delete-group-addon-container'>
                                    <div id='edit-modal-title' className='title'>Delete Addon Group</div>

                                    <div id='delete-addon-group-message' className='center margin-top20'>
                                        {this.state.deleteAddonMessage}
                                    </div>

                                    <div className='center margin-top20'>
                                        <button id='yes-to-delete-addon-button' className='custom-button' onClick={() => this.yesToDeleteGroupAddon()}>Yes</button>
                                        <button id='no-to-delete-addon-button' className='custom-button' onClick={() => this.noToDeleteGroupAddon()}>No</button>
                                    </div>
                                </div>
                            </div>
                        :
                            ''
                    }
                </div>

                <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/> 
              
                <div id='alpha-container-all-products'>
                    <div className='small-title color-black'>All Products</div>

                    <div id='all-products-filter'>
                        <div className='margin-top10'>
                            <input type='text' id='product-name-in-filter' placeholder='Enter Product Name' className='product-name-entry' onChange={() => this.filterProducts(this.state.selectedValueFilterServiceCategory)}/>
                            <div id='filter-option-list-container'>
                                <Select 
                                  styles={
                                    {
                                      control: (base, state) => ({
                                        ...base,
                                        '&:hover': { borderColor: '#e4e4e4' }, // border style on hover
                                        border: '1px solid lightgray', // default border color
                                        boxShadow: 'none', // no box-shadow
                                      }),
                                      singleValue: base => ({
                                        ...base,
                                        color: "#000000"
                                      }),
                                      input: base => ({
                                        ...base,
                                        color: "#000000"
                                      }),
                                      option: base => ({
                                        ...base,
                                        color: "#000000",
                                        fontSize: '14px'
                                      })
                                    }
                                  } 
                                  
                                  id='service-type-selection-in-filter' 
                                  value = {this.state.selectedValueFilterServiceCategory}
                                  onChange={value => this.handleChangeForServiceCategorySelection(value)} 
                                  options={this.state.selectOptions} 
                                  classNamePrefix='react-select'
                                />
                            </div>
                        </div>

                        <div id='vendors-info'> {this.state.allProductsData ? 'You have ' +this.state.filteredProductAmount+ ' products' : ''} </div>

                        <div id='buttons-for-filter' className='margin-top10'>
                            <button id='filter-button' className='custom-button' onClick={() => this.filterProducts(this.state.selectedValueFilterServiceCategory)}>Filter</button>
                            <button id='clear-filter-button' className='custom-button' onClick={this.clearFilter}>Clear Filters</button>

                            <MdAddCircleOutline id='add-new-product-icon' onClick={this.openAddModal}/>
                        </div>
                    </div>

                    <div id='all-products'>
                        {this.state.productsListComponents}
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    userData: state.auth.userData
});

export default connect(mapStateToProps, {})(VendorProducts);