//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';


//custom made component
import PageLoading from '../utility/PageLoading';

//icons
import { MdClear } from 'react-icons/md';

//stylings
import '../../stylings/driver-all-orders.css';

//define loadash
const _ = require('lodash');

class DriverAllOrders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            driverId: '5dda11dbdfbb9813e903d331',
            allOrdersData: [],
            showPageLoading : true,
            ordersListComponents: [],
            orderMoreInfoComponents: [],
            showOrderDetailsItemsInModal: false,
            goodColor: '#258e25',
            badColor: '#ff3333',
            defaultColor: '#a7a7a7'
        }

        //headers
        const token = localStorage.getItem('token');
        const config = {
            headers: {
                'x-auth-token': token
            }
        }

        //get all orders this driver has gone for
        axios.get(`${apiURL}/api/drivers/get_orders_this_driver_has_made/${this.state.driverId}`, config)
        .then(res => {
            //build the orders data
            this.buildTheOrdersToBeDisplayed(res.data);
            this.setState({allOrdersData : res.data});
        })
        .catch(err => {});
    }
    
    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
            this.setState({showOrderDetailsItemsInModal: false});
        }
      
        //open the modal
        this.setState({showModal: !this.state.showModal});
    }

    buildTheOrdersToBeDisplayed = (orders) => {
        if(orders.length > 0) { 
            var count = 0;
            var orderListComponentsTemp = [];

            orders.forEach(singleOrder => {
                //get the logo
                orderListComponentsTemp.push(
                    <div key={count++} className='vendor-single-order-item' onClick={() => this.openModalWithOrderDetails(singleOrder._id, singleOrder.drivers_total_money, singleOrder.order_date, singleOrder.vendor_name, singleOrder.order_num_for_view)}>
                        <div id='order-id' className={'margin-bottom10'}>
                              <b>#{singleOrder.order_num_for_view}</b>
                        </div>
                        <div className='vendor-date-single-order'><b>Store Name:</b> {singleOrder.vendor_name}</div>
                        <div className='vendor-product-vendor-name-single-order'>{singleOrder.product_vendor_name}</div>
                        <div className='vendor-order-info-single-order'>{singleOrder.order_name}</div>
                        <div className='vendor-price-single-order'><b>Total Income:</b> ${singleOrder.drivers_total_money} </div>
                        <div className='vendor-date-single-order'><b>Date: </b>{new Date(singleOrder.order_date).toDateString()}</div>
                    </div>
                );
            });

            this.setState({ordersListComponents : orderListComponentsTemp});
        } else {
            this.setState({ordersListComponents : <div key={'no-order-in-list'} id='no-items-available-in-list-vendor-order'>No Order has been Completed to match criteria</div>});
        }

        //remove the loading sign
        this.setState({showPageLoading: false});
    }

    openModalWithOrderDetails = (orderId, driversTotalMoney, dateOrdered, storeName, orderNumForView) => {
        //show the loading sign
        this.setState({showPageLoading : true });

        //headers
        const token = localStorage.getItem('token');
        const config = {
            headers: {
                'x-auth-token': token
            }
        }

        axios.get(`${apiURL}/api/drivers/get_more_info_on_an_order/${orderId}`, config)
        .then(OrderDetailsData => {
            var count = 0;
            var orderMoreInfoComponentsTemp = [];
            var cartData = OrderDetailsData.data.orderToProducts;

            orderMoreInfoComponentsTemp.push(
                <div key={count++} id='order-id' className={'margin-bottom10 pre-info-order-details-modal'}>
                    <b>#{orderNumForView}</b>
                </div>
            );

            orderMoreInfoComponentsTemp.push(
                <div key={count++} className='pre-info-order-details-modal'>
                    <h3><b>Store:</b> {storeName}</h3>
                </div>
            );
            
            orderMoreInfoComponentsTemp.push(
                <div key={count++} className='pre-info-order-details-modal'>
                    <b>Income:</b> ${driversTotalMoney}  &nbsp;&nbsp;&nbsp;&nbsp;<b>Order Date:</b> {new Date(dateOrdered).toDateString()}.
                </div>
            );

            orderMoreInfoComponentsTemp.push(
                <br key={count++}/>
            );        
            orderMoreInfoComponentsTemp.push(
                <br key={count++}/>
            );

            orderMoreInfoComponentsTemp.push(
                <div key={count++} id='order-details-list-headers'>
                    <div className='more-info-item-name-in-order-details-headers   bold'>Item name</div>
                    <div className='more-info-item-amount-in-order-details-headers bold'>Amount</div>
                    <div className='more-info-item-price-in-order-details-headers  bold'>Price</div>
                </div>
            );

            if(cartData.length > 0) {
                cartData.forEach(singleItem => {
              
                //build the row of item data
                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='single-item-in-cart-container'>
                      <div className='more-info-item-name-in-order-details-data'>{singleItem.product_name}</div>
                      <div className='more-info-item-amount-in-order-details-data'>{singleItem.amount_ordered}</div>
                      <div className='more-info-item-price-in-order-details-data'>${singleItem.product_price}</div>
                    </div>);
                });
            }      
            
            this.setState({orderMoreInfoComponents : orderMoreInfoComponentsTemp});
        });

        //remove the loading sign
        this.setState({showPageLoading : false });

        //allow the modal data to show
        this.setState({showOrderDetailsItemsInModal: true });

        //toggle modal
        this.toggleModal();
    }

    // parse a date in yyyy-mm-dd format
    parseDate = (input) => {
        var parts = input.match(/(\d+)/g);
        // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
        return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
    }

    filterNewOrders = () => {
        //add the loading sign
        this.setState({showPageLoading : false });

        //get all the orders
        var orders = this.state.allOrdersData;

        //if we have start and end that then we can go from here
        if(document.getElementById('start-date-filter').value && document.getElementById('end-date-filter').value) {
            var startDate = this.parseDate(document.getElementById('start-date-filter').value);
            var endDate = this.parseDate(document.getElementById('end-date-filter').value);

            //if the endDate is lower than the start date then we dont want that else we continue
            if(endDate < startDate) {
                document.getElementById('end-date-filter').style.borderColor = `${this.state.badColor}`;
            } else {
                //change all the bad colors that have been set
                document.getElementById('start-date-filter').style.borderColor = `${this.state.defaultColor}`;
                document.getElementById('end-date-filter').style.borderColor = `${this.state.defaultColor}`;
              
                var parts = '';

                orders = _.filter(orders, function(singleOrder){
                    parts = (singleOrder.order_date).match(/(\d+)/g);
                    var orderDate = new Date(parts[0], parts[1]-1, parts[2]);

                    return orderDate >= startDate && orderDate <= endDate;
                });
            }
        }

        //filter based on product name
        var filterStoreName = document.getElementById('store-name-in-filter').value;
        if(filterStoreName) {
            filterStoreName = filterStoreName.toLowerCase();
            orders = _.filter(orders, function(singleOrder){
                return singleOrder.vendor_name.toLowerCase().indexOf(filterStoreName) > -1;
            });
        }
        
        this.buildTheOrdersToBeDisplayed(orders);
    }

    clearFilterNewOrders = () => {
        document.getElementById('start-date-filter').value = '';
        document.getElementById('end-date-filter').value = '';
        document.getElementById('store-name-in-filter').value = '';

        //change the colors of border
        document.getElementById('end-date-filter').style.borderColor = `${this.state.defaultColor}`;

        var orders = this.state.allOrdersData;

        this.buildTheOrdersToBeDisplayed(orders);
    }

    render() {
        return (
            <div>
                <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                    <div id='modal-container-order-details'>
                        <MdClear id='close-modal' onClick={() => this.toggleModal()}/>
                        <br />
                        <br />
                        {
                            this.state.showOrderDetailsItemsInModal ? 
                                <div id='order-details-more-info'>
                                    <div id='order-details-title' className='title'>Order Details</div>
                                    {this.state.orderMoreInfoComponents}
                                    <br />
                                    <br />
                                </div>
                            :
                                ''
                        }
                    </div>
                </div>
                <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/> 
                <div id='alpha-container-all-orders'>
                    <div className='small-title color-black'>Previous Orders</div>

                    <div id='all-orders-filter'>
                        <div className='margin-top10'>
                            <b>Start Date: </b><br/><input type="date" id='start-date-filter' className='date-entries-filter'/><br/>
                            <b>End Date: </b><br/><input type="date" id='end-date-filter'   className='date-entries-filter'/>

                            <input type='text' id='store-name-in-filter' placeholder='Enter Store Name' className='input-name-entry' onChange={this.filterNewOrders}/>

                            <div id='buttons-for-filter'>
                                <button id='filter-button' className='custom-button' onClick={this.filterNewOrders}>Filter</button>
                                <button id='clear-filter-button' className='custom-button' onClick={this.clearFilterNewOrders}>Clear Filters</button>
                            </div>
                        </div>
                    </div>

                    <div id='all-orders-list'>
                        {this.state.ordersListComponents}
                    </div>
                </div>
            </div>
        )
    }
}
  
export default DriverAllOrders;
  