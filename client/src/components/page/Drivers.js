//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';

//socket
import io from 'socket.io-client';
import {websocketUrl} from '../../config.js';


//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {storeNewOrderRequest, removeNewOrderRequest, storeOngoingOrdersAmount} from '../../redux/actions/driverActions';
import {storeSocket} from '../../redux/actions/authActions';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally, playBeep} from '../utility/utilityFunctions';

//custom components
import Footer from '../utility/Footer';
import DriverAllOrders from './DriverAllOrders';
import DriverNewOrders from './DriverNewOrders';
import DriverOngoingOrders from './DriverOngoingOrders';
import Switch from "react-switch";

//images
import company_logo from '../../images/icons/logo9.png';
import cart_icon from '../../images/icons/cart-icon.png';
import new_message_icon from '../../images/icons/chat.png';
import car_icon from '../../images/icons/car.png';

//stylings
import '../../stylings/drivers.css';

class Drivers extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            driversName : this.props.userData.first_name,
            vendorIsOpened: false,
            showAllOrders: false,
            showNewOrders: true,
            showOngoingOrders: false,
            hoverOverColor: '#45a470',
            defaultNoHoverColor: '#f7f7f7',
            iHaveGottenSocketData: false,
            iHaveGottenUserData: false,
            currentlyAvailable: false
        }
    }
  
    //these are my redux proptypes from the global state
    static propTypes = {
        isAuthenticated: PropTypes.bool,
        socketData: PropTypes.object,
        newOrderRemoved: PropTypes.bool,
        storeNewOrderRequest: PropTypes.func.isRequired,
        removeNewOrderRequest: PropTypes.func.isRequired,
        allNewOrderRequestData: PropTypes.array,
        orderRequestAmount: PropTypes.number,
        amountOfOngoingOrder: PropTypes.number,
        userData: PropTypes.object,
        storeOngoingOrdersAmount: PropTypes.func.isRequired,
        storeSocket: PropTypes.func.isRequired
    }

    handleChangeCurrentlyAvailable = () => {
        //send data to backend of drivers current status
        const config = {
            headers: {
                'x-auth-token': localStorage.getItem('token')
            }
        }
        
        if(!this.state.currentlyAvailable) { //its false currently -- to be set to true
            //connect to the socket and create user object
            if(this.props.isAuthenticated && !this.props.socketData) {
                if(this.props.userData.can_access_drivers_page) {
                    axios.get(`${apiURL}/api/drivers/current_availability/${this.props.userData._id}/true`, config)
                    .then(res => {
                        if(res.data.successful) {
                            if(!this.state.iHaveGottenSocketData) {
                                //connect to the socket
                                const socket = io.connect(websocketUrl, {'forceNew': true});
                          
                                //load the users data to the socket
                                socket.emit('createUserObject', this.props.userData);
                          
                                //store the socket data
                                this.props.storeSocket(socket);
                                
                                this.setState({iHaveGottenSocketData: true});
                            }
                        }
                    })
                    .catch(err => {
                    });
                }
            }
        } else {
            axios.get(`${apiURL}/api/drivers/current_availability/${this.props.userData._id}/false`, config)
            .then(res => {
                if(res.data.successful) {
                    if(this.state.iHaveGottenSocketData) {
                        //disconnect user
                        this.props.socketData.disconnect();
                    }
                }
            })
            .catch(err => {
            });
        }

        this.setState({ currentlyAvailable : !this.state.currentlyAvailable });
    }
    
    boilerPlateForDidMountAndUpdate = () => {
        if(!this.state.iHaveGottenSocketData && this.props.socketData) {
            this.setState({iHaveGottenSocketData: true});
            
            //listeners to mount
            this.props.socketData.on('thereIsARequestOnDelivery', this.thereIsARequestOnDelivery);
        }
        
        if(!this.state.iHaveGottenUserData && this.props.userData) {
            this.setState({iHaveGottenUserData: true});

            //get the amount of ongoing orders
            const config = {
                headers: {
                    'x-auth-token': localStorage.getItem('token')
                }
            }
            
            //ToDo: get all orders that have their status to be pending   
            axios.get(`${apiURL}/api/drivers/get_orders_driver_accepted_to_make/${this.props.userData._id}`, config)
            .then(res => {
                //store the socket data
                this.props.storeOngoingOrdersAmount(res.data.orders.length);

                this.setState({currentlyAvailable: res.data.driver.driverCurrentAvailabilityStatus})

                if(res.data.driver.driverCurrentAvailabilityStatus) {
                    const socket = io.connect(websocketUrl, {'forceNew': true});
                    socket.emit('createUserObject', this.props.userData);
                    socket.on('thereIsARequestOnDelivery', this.thereIsARequestOnDelivery);
                  
                    this.props.storeSocket(socket);
                    this.setState({iHaveGottenSocketData: true});
                }
            })
            .catch(err => {});
        }
    }

    componentDidMount = () => {
        this.boilerPlateForDidMountAndUpdate();
    }

    componentDidUpdate = () => {
        this.boilerPlateForDidMountAndUpdate();
    }

    thereIsARequestOnDelivery = (orderRequestData) => {
        //play the beep
        playBeep();

        //store the socket data
        this.props.storeNewOrderRequest(orderRequestData);

        if(this.state.showNewOrders) {
            //call child function
            this.child.changeHasHappenedInNewOrderRequestData();
        }

        //remove the order after 20 seconds
        setTimeout(function () { 
                this.removeTheNewOrderRequest(orderRequestData);
            }.bind(this),
            20 * 1000
        ); //20 seconds wait
    }

    removeTheNewOrderRequest = (orderRequestData) => {
        //remove the new order data
        this.props.removeNewOrderRequest(orderRequestData.orderId);
        
        if(this.state.showNewOrders && this.props.newOrderRemoved) {
            //rebuild the new order list
            this.child.changeHasHappenedInNewOrderRequestData();

            //send response that the driver will not take it
            this.props.socketData.emit('driverCannotTakeOrder', Object.assign({}, orderRequestData, {orderRequestAmount: this.props.allNewOrderRequestData.length}));
        }
    }

    whichBottomASectionShouldIShow = (sectionName) => {
        if(sectionName === 'allOrders') {
            this.setState({showAllOrders : true});
            this.setState({showNewOrders : false});
            this.setState({showOngoingOrders : false});
        } else if(sectionName === 'newOrders') {
            this.setState({showAllOrders : false});
            this.setState({showNewOrders : true});
            this.setState({showOngoingOrders : false});
        } else if(sectionName === 'ongoingOrders') {
            this.setState({showAllOrders : false});
            this.setState({showNewOrders : false});
            this.setState({showOngoingOrders : true});
        }
    }
  
    render() {
        return (
            <div id='drivers-alpha-container'>
                <div className='absolute-company-logo'>
                    <img src={company_logo} className='image-take-full-space driver-company-icon' alt='company_logo'/>
                </div>

                <div id='middle-section-of-page-alpha-driver'>
                    <div id='drivers-title' className='title color-black center'>{this.props.userData.first_name}'s Page</div>
    
                    <div id='driver-status' className='center margin-top10'>
                        {
                            this.state.currentlyAvailable ? 
                                <div className='valid-color'>Currently Active</div>
                            :
                                <div className='bad-color'>Currently Unavailable</div>
                        }
                        <Switch 
                          onChange={this.handleChangeCurrentlyAvailable} 
                          checked={this.state.currentlyAvailable}    
                          height={20}
                          width={48}
                          uncheckedIcon={false}
                          checkedIcon={false}
                          handleDiameter={30}    
                          onColor="#258e25" //when good bar color
                          onHandleColor="#F7C544" //when good ball color
                          boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                          activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                          />
                    </div>
    
                    <div id='list-of-all-option-icons-driver'>
                        <div id='list-of-all-option-icons-content-container-driver'>
                            <div className='single-icon-container-driver' style={this.state.showAllOrders ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('allOrders')}>
                                <div className='single-image-icon-container'>
                                    <img src={cart_icon} className='single-image-icon' alt='cart-icon' />
                                </div>
                                <div className='title-name-single-icon'>All Orders</div>
                            </div>
    
                            <div className='single-icon-container-driver'  style={this.state.showNewOrders ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('newOrders')}>
                                <div className='one-new-order'>{this.props.orderRequestAmount === 0 ? '' : this.props.orderRequestAmount}</div>
                                <div className='single-image-icon-container'>
                                    <img src={new_message_icon} className='single-image-icon' alt='new-message-icon' />
                                </div>
                                <div className='title-name-single-icon'>New Order</div>
                            </div>
    
                            <div className='single-icon-container-driver'  style={this.state.showOngoingOrders ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('ongoingOrders')}>
                                <div className='one-new-order'>{this.props.amountOfOngoingOrder === 0 ? '' : this.props.amountOfOngoingOrder}</div>
                                <div className='single-image-icon-container'>
                                    <img src={car_icon} className='single-image-icon' alt='car-icon' />
                                </div>
                                <div className='title-name-single-icon'>Ongoing Orders</div>
                            </div>
                        </div>
                    </div>
    
                    {
                        this.state.showAllOrders ?
                            <DriverAllOrders />
                        :
                            this.state.showNewOrders ?
                                <DriverNewOrders childRef={ref => (this.child = ref)}/>
                            :
                                this.state.showOngoingOrders ?
                                    <DriverOngoingOrders />
                                :
                                    ''
                    }
                </div>
    
                <div id='drivers-footer'>
                    <Footer />
                </div>

                { 
                    !this.props.userData ?
                        addLoadingIconProcedurally('drivers-alpha-container')
                    :
                        removeLoadingIconProcedurally()
                }
            </div>
        )
    }
  }
  
  
const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    socketData: state.auth.socketData,
    newOrderRemoved: state.driver.newOrderRemoved,
    allNewOrderRequestData: state.driver.allNewOrderRequestData,
    orderRequestAmount: state.driver.orderRequestAmount,
    amountOfOngoingOrder: state.driver.amountOfOngoingOrder,
    userData: state.auth.userData
});
  
export default connect(mapStateToProps, {storeNewOrderRequest, removeNewOrderRequest, storeOngoingOrdersAmount, storeSocket})(Drivers);
  