//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';

//redux imports
import { connect } from 'react-redux';
import PropTypes, { array } from 'prop-types';
import {storeAmountOfOrder, removeOneOrder, addOneOrder, addOneOngoingOrder} from '../../redux/actions/vendorActions';

//stylings
import '../../stylings/vendor-new-order.css';


//definitions
const _ = require('lodash');


class VendorNewOrder extends Component {
    constructor(props) {
        super(props);

        this.state = {
            iHaveGottenSocketData: false,
            allPendingOrdersComponent: [],
            vendorId : this.props.userData.vendor_id_for_vendor,
            allOrdersData: null
        }

        //this is the token
        const token = localStorage.getItem('token');
        
        //headers
        const config = {
            headers: {
                'x-auth-token': token
            }
        }

        //ToDo: get all orders that have their status to be pending   
        axios.get(`${apiURL}/api/vendors/get_pending_orders_made_to_vendor/${this.props.userData.vendor_id_for_vendor}`, config)
        .then(res => {
            //build the orders data
            this.setState({allOrdersData : res.data});
            this.buildOrder(res.data, true);

            //store the socket data
            this.props.storeAmountOfOrder(res.data.length);
        })
        .catch(err => {});
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        isAuthenticated: PropTypes.bool,
        socketData: PropTypes.object,
        utilityData: PropTypes.object,
        userSpecifics: PropTypes.object,
        storeAmountOfOrder: PropTypes.func.isRequired,
        removeOneOrder: PropTypes.func.isRequired,
        addOneOrder: PropTypes.func.isRequired,
        addOneOngoingOrder: PropTypes.func.isRequired,
        userData: PropTypes.object
    }

    componentDidUpdate = () => {
        if(!this.state.iHaveGottenSocketData && this.props.socketData) {
            this.setState({iHaveGottenSocketData: true});
            
            //listeners to mount - this callback function is different from the one in the vendors component (this one is to build the order data)
            this.props.socketData.on('thereIsANewOrder', this.thereIsANewOrder);
        }
    }

    checkIfPropertyMatchesAnyInArray = (arrayName, propertyName, value) => {
        return _.findIndex(arrayName, {[propertyName]: value}) !=-1 ? true : false;
    }

    buildOrder = (orders, clearOrdersInState = false) => {
        var tempAllNewOrdersComponents = [];
        var count = Date.now();
        var addonsComponents = [];

        //if we dont want to clear that means the data is most likely coming from the socket so we want to run this check to see if we already have the data, if we do then we dont add it again, else we do
        if(!clearOrdersInState) {
            orders.forEach(order => {
                if(this.checkIfPropertyMatchesAnyInArray(this.state.allOrdersData, '_id', order._id)) {
                    //remove this order from the list of orders to be added
                    _.remove(orders, function(orderIn) {
                        return orderIn._id === order._id;
                    });
                }
            })
        }

        if(clearOrdersInState && orders.length === 0) {
            tempAllNewOrdersComponents.push(<div key={count++} id='no-new-order-available'>
                No New Order Available
            </div>);
        } else {
            if(this.state.allPendingOrdersComponent.length === 0) {
                //clear what was there
                this.setState({allPendingOrdersComponent: []});
            }

            //get the current orders built
            var tempSingleOrderComponents = [];
            var total = 0.0;
            var cartItems = [];

            orders.forEach(order => {
                tempSingleOrderComponents = [];
                total = 0.0;
                cartItems = JSON.parse(order.cartItems);

                tempSingleOrderComponents.push(<div key={count++}>
                    <div className='new-order-header-item-name bold'>Item name</div>
                    <div className='new-order-header-item-amount bold'>Amount</div>
                    <div className='new-order-header-item-price bold'>Price</div>
                </div>);
            
                tempSingleOrderComponents.push(
                    <div key={count++} className='straight-line'/>
                );

                cartItems.forEach(singleItem => {
                    //add the total
                    total += ((singleItem.product_price + singleItem.addons_total) * singleItem.amount_ordered);
                    addonsComponents = [];

                    //build the addons if any
                    if(singleItem.addons_description && singleItem.addons_description.length > 0) {
                        var addons = singleItem.addons_description.split(',');
                        for(var i=0; i<addons.length; i++) {
                            if(addons[i] != '') {
                                addonsComponents.push(
                                  <div key={singleItem._id+i} className='single-product-addons-data-in-cart'>{addons[i]}</div>
                                );
                            }
                        }
                    } else if(singleItem.addons_description === undefined && singleItem.addons_groups_selected.length > 0) {
                        _.map(singleItem.addons_groups_selected, oAdd =>  {
                            var i = 0;

                            if(oAdd) {
                                _.map(oAdd.addons_selected, oAddSingle =>  {
                                    addonsComponents.push(
                                      <div key={oAddSingle.id+i} className='single-product-addons-data-in-cart'>{oAddSingle.name}</div> 
                                    );
                                    i++;
                                });
                            }
                        });
                    }

                    tempSingleOrderComponents.push(
                        <div key={count++} className='margin-bottom10 overflow-auto'>              
                            <div className='new-order-value-item-name'>
                                {singleItem.product_name}

                                <div className='product-addons-data-in-cart'>
                                    {addonsComponents}
                                </div>

                                {
                                    singleItem.special_instructions ? 
                                        <div className='special-instructions-in-cart'>
                                            "{singleItem.special_instructions}"
                                        </div>
                                    :
                                        ''
                                }
                            </div>
                            {/* <div className='new-order-header-item-name'>{singleItem.product_name}</div> */}
                            <div className='new-order-header-item-amount'>{singleItem.amount_ordered}</div>
                            <div className='new-order-header-item-price'>${(singleItem.product_price + singleItem.addons_total).toFixed(2)}</div>
                        </div>
                    );
                });

                //and some spaces before and then the cummulative details
                tempSingleOrderComponents.push(
                    <br key={count++}/>
                );

                //add the total row and the vendors total income and everthing here
                tempSingleOrderComponents.push(
                    <div key={count++} className='single-item-in-cart-container'>
                        <div className='lower-item-name-new-order'>Price</div>
                        <div className='lower-item-value-new-order'>${(total + (total * this.props.utilityData.tax)).toFixed(2)}</div>
                    </div>
                );
                tempSingleOrderComponents.push(
                    <div key={count++} className='single-item-in-cart-container'>
                        <div className='lower-item-name-new-order'>Total Income</div>
                        <div className='lower-item-value-new-order'>${order.vendors_total_money.toFixed(2)}</div>
                    </div>
                );

                tempSingleOrderComponents.push(<div key={count++}>     
                    <button className='yes-we-have-the-item-button custom-button' key={count++} onClick={() => this.handleYesWeHaveTheItem(order._id, (5 * 60 * 1000))}>Ready in 5mins</button>
                    <button className='yes-we-have-the-item-button custom-button' key={count++} onClick={() => this.handleYesWeHaveTheItem(order._id, (10 * 60 * 1000))}>Ready in 10mins</button>
                    <button className='yes-we-have-the-item-button custom-button' key={count++} onClick={() => this.handleYesWeHaveTheItem(order._id, (15 * 60 * 1000))}>Ready in 15mins</button>

                    <button className='no-we-dont-have-the-item-button custom-button' key={count++} onClick={() => this.handleCancelOrder(order._id)}>Cancel Order</button>
                    <button className='not-all-items-can-be-handled-buttom custom-button' key={count++} onClick={() => this.handleCantHandleAllItems(order._id)}>Some Items</button>
                </div>);

                //add that single order
                tempAllNewOrdersComponents.push(<div className='single-new-order' key={count++}>{tempSingleOrderComponents}</div>);
            });

            //if clearOrdersInState is false then we want all the previous orders stored in the state to be retained so we need to add them to our list,
            //this case is mainly for when we get an order from a socket so we dont want to erase all the previous orders we have
            if(!clearOrdersInState) {
                //this would hold the new order data as well as the old one if necessary and this is for filtering purposes
                var tempAllOrdersData = [];

                //the new order comes as an array so we want to iterate and i am not just assingning orders[0] because we might want to send more than one
                //in the future and this code would be able to handle it
                _.map(orders, function(order) {
                    tempAllOrdersData.push(order);
                });

                //this check is here because if this.state.allOrdersData.length is 0 the "we just have No New Orders Available" and we dont want to add that in again
                if(this.state.allOrdersData) {
                    if(this.state.allOrdersData.length !== 0) {
                        tempAllNewOrdersComponents.push(this.state.allPendingOrdersComponent);

                        //we get an array likewise here so we want to iterate through it
                        _.map(this.state.allOrdersData, function(order) {
                            tempAllOrdersData.push(order);
                        });
                    }
                }

                this.setState({allOrdersData : tempAllOrdersData});
            }
        }

        //add the items
        this.setState({allPendingOrdersComponent: <div>{tempAllNewOrdersComponents}</div>}); 
    }

    handleYesWeHaveTheItem = (orderId, time) => {
        //headers
        const token = localStorage.getItem('token');
        const config = {
            headers: {
                'x-auth-token': token
            }
        }
          
        axios.get(`${apiURL}/api/vendors/vendor_yes_we_have_order/${orderId}/${time}`, config)
        .then(res => {
            if(res.data.SuccessInUpdate) {
                //remove the item from the list of orders
                this.removeOrderFromOrderList(orderId);
          
                //send a response to the client
                this.notifyClientOfAvailability(true, res.data.order);

                //find driver 
                setTimeout(
                    function () { 
                        //find a driver
                        this.findDriverForOrder(res.data.order)
                    }.bind(this),

                    time
                );//change to time
            }
        })
        .catch(err => {
        });
    }

    handleCantHandleAllItems = (orderId) => {
        //contact delpries admin of the issue
    }

    handleCancelOrder = (orderId) => {
        //headers
        const token = localStorage.getItem('token');
        const config = {
            headers: {
                'x-auth-token': token
            }
        }

        axios.get(`${apiURL}/api/vendors/cancel_order/${orderId}`, config)
        .then(res => {
            if(res.data.SuccessInUpdate) {
                //remove the item from the list of orders
                this.removeOrderFromOrderList(orderId);
          
                //send a response to the client
                this.notifyClientOfAvailability(false, res.data.order);
            }
        });
    }

    notifyClientOfAvailability = (orderIsAvailable, orderData) => {
        if(this.props.isAuthenticated) {
            var orderAvailability = {
                orderId: orderData._id,
                clientId: orderData.user_id,
                clientLocation: orderData.address,
                vendorId: orderData.product_vendor_id,
                vendorName: this.props.userSpecifics.vendorName,
                vendorLocation: this.props.userSpecifics.vendorAddress,
                orderIsAvailable: orderIsAvailable
            };   
        
            //listeners to mount
            this.props.socketData.emit('notifyClientOfAvailability', orderAvailability);
        }
        
        //add one ongoing order to the redux state
        if(orderIsAvailable) {
            this.props.addOneOngoingOrder();  
        }
    }

    findDriverForOrder = (orderData) => {
        if(this.props.isAuthenticated) {
            //headers
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'x-auth-token': token
                }
            }
            
            //check to see if the order_needs_driver is true if so then dont send for a driver because one has already been sent for else send for one
            axios.get(`${apiURL}/api/vendors/does_order_need_driver/${orderData._id}/${this.state.vendorId}`, config)
            .then(res => {
                if(res.data) {
                    if(!res.data.error && res.data.send_for_driver) {
                        var orderAvailability = {
                            orderId: orderData._id,
                            clientId: orderData.user_id,
                            clientLocation: orderData.address,
                            vendorId: orderData.product_vendor_id,
                            vendorName: this.props.userSpecifics.vendorName,
                            vendorLocation: this.props.userSpecifics.vendorAddress
                        };   
                        
                        //listeners to mount
                        this.props.socketData.emit('findADriverForOrder', orderAvailability);
                    }
                }
            });
        } 
    }

    removeOrderFromOrderList = (orderId) => {
        var currOrders = this.state.allOrdersData;

        //if the current order is not an object we want to parse it
        if(!(typeof currOrders === 'object')) {
            currOrders = JSON.parse(currOrders);
        }
        
        //we want to remove the order
        _.remove(currOrders, function(order) {
            return order._id === orderId;
        });
        
        //update our order data
        this.setState({allOrdersData : currOrders});

        //rebuild the order list
        this.buildOrder(currOrders, true);

        //remove an order to the redux state
        //this.props.remove();

        //add an order to the redux state
        this.props.removeOneOrder();
    }

    thereIsANewOrder = (newOrderAndClientData) => {
        //call the build order function so that our order can be built and added to the display for this vendor
        this.buildOrder(newOrderAndClientData);

        //add an order to the redux state
        //this.props.addOneOrder();
    }


    /** Filter Options */
    filterNewOrders = () => {
        var filterOrderNumber = (document.getElementById('order-number-in-filter').value).toLowerCase();
        var filterProductName = (document.getElementById('product-name-in-filter').value).toLowerCase();
        var orders = this.state.allOrdersData;

        if(filterOrderNumber) {
            orders = _.filter(orders, function(order) {
                return order.order_num_for_view.toLowerCase().indexOf(filterOrderNumber) > -1;
            });
        }

        if(filterProductName) {
            var filteredOrders = [];

            //only use the orders with products that have the text entered in productName as their name
            _.map(orders, function(order) {
                var cartItems = order.cartItems;

                //if the current cartItems is not an object we want to parse it
                if(!(typeof cartItems === 'object')) {
                    cartItems = JSON.parse(cartItems);
                }

                //search though the cart items for any item that has its product name to be what we are searching for, if we find any we just add it to the orders
                //to be returned and then continue form the current iteration
                for(var i=0; i< cartItems.length; i++) {
                    if(cartItems[i].product_name.toLowerCase().indexOf(filterProductName) > -1) {
                        filteredOrders.push(order);
                        break;
                    }
                }
            });
            
            //update the orders to be returned to be the filtered orders
            orders = filteredOrders;
        }

        this.buildOrder(orders, true)
    }

    clearFilter = () => {
        //clear the fields and get the orders
        document.getElementById('order-number-in-filter').value = "";
        document.getElementById('product-name-in-filter').value = "";
        var orders = this.state.allOrdersData;

        //send the orders to be built and pass true so it can clear all previous data in the orders pane
        this.buildOrder(orders, true);
    }

    render() {
        return (
            <div id='new-order-alpha-container'>
                <div id='new-order-title' className='small-title color-black'>All New Orders <div className='mini-important-information'>Please attend to them immediately</div></div>
                <div id='new-orders-filter-container'>
                    <div className='margin-top10'>
                        <input type='text' id='order-number-in-filter' placeholder='Enter Order Number' className='product-name-entry' onChange={this.filterNewOrders}/>
                        <input type='text' id='product-name-in-filter' placeholder='Enter Product Name' className='product-name-entry' onChange={this.filterNewOrders}/>
                    </div>


                    <div id='buttons-for-filter' className='margin-top10'>
                        <button id='filter-button' className='custom-button' onClick={this.filterNewOrders}>Filter</button>
                        <button id='clear-filter-button' className='custom-button' onClick={this.clearFilter}>Clear Filters</button>
                    </div>
                </div>

                <div id='all-pending-orders'>
                    {this.state.allPendingOrdersComponent}
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    socketData: state.auth.socketData,
    utilityData: state.utility.utilityData,
    userSpecifics: state.auth.userSpecifics,
    userData: state.auth.userData
});
  
export default connect(mapStateToProps, {storeAmountOfOrder, removeOneOrder, addOneOrder, addOneOngoingOrder})(VendorNewOrder);