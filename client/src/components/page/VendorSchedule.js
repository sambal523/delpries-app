//system defined components
import React, {Component} from 'react';
import Calendar from 'react-calendar';
import axios from 'axios';
import {apiURL} from '../../config.js';
import moment from 'moment'

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally, validateScheduleTimeBasedOnDelpries} from '../utility/utilityFunctions';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

//icons
import { MdClear } from 'react-icons/md';

//stylings
import '../../stylings/vendor-schedule.css';

class VendorSchedule extends Component {
    constructor(props) {
        super(props);

        this.state = {
            goodColor: '#258e25',
            badColor: '#ff3333',
            defaultColor: '#a7a7a7',
            ihaveGottenReduxUserData: false,
            response : {
                status: false,
                msg: ''
            },
            specialtyResponse : {
                status: false,
                msg: ''
            },
            showModal: false,
            showPostRequestDataInModal: false, //this is to toggle the display from the response of a query to the database in a modal 
            showSpecialDaysInfoInModal: false,
            minTime: '9:00',
            maxTime: '22.00',

            todaysDate: new Date(),
            currentSelectedDateDetails: {
              date: '',
              openingTime: '',
              closingTime: '',
              notes: '',
              dateOriginalFormat: ''
            }
        }
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        userData: PropTypes.object
    }

    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
            this.setState({showPostRequestDataInModal: false});
            this.setState({showSpecialDaysInfoInModal: false});
            
            //reset the response for calls
            this.setState({response: {status: false, msg: ''}});
        }

        //open the modal
        this.setState({showModal: !this.state.showModal});
    }
    
    onChangeDate = (date) => {
        date = moment(date, 'YYYY-MM-DD').format('L');
        var weekDay = moment(date).weekday();
        
        if(weekDay === 0) {
            weekDay = 'sunday'
        } else if (weekDay === 1) {
            weekDay = 'monday'
        } else if (weekDay === 2) {
            weekDay = 'tuesday'
        } else if (weekDay === 3) {
            weekDay = 'wednesday'
        } else if (weekDay === 4) {
            weekDay = 'thursday'
        } else if (weekDay === 5) {
            weekDay = 'friday'
        } else if (weekDay === 6) {
            weekDay = 'saturday'
        }
        
        this.getAndOpenSelectedDateDate(date, weekDay);
    }

    getAndOpenSelectedDateDate = (date, weekDay) => {
        this.clearResponseFields();

        //headers
        const config = {
        headers: {
              'Content-Type': 'application/json',
              'x-auth-token': localStorage.getItem('token')
            }
        }

        //add loading icon
        addLoadingIconProcedurally('alpha-container-vendor-schedule');

        const body  = JSON.stringify({
            vendorId: this.props.userData.vendor_id_for_vendor,
            date,
            weekDay
        });

        axios.post(`${apiURL}/api/vendors/get_special_date_info`, body, config)
        .then(res => {
            //if there was no error and data was found then we do this
            if(res.data.foundData) {
                this.setState({currentSelectedDateDetails: {date: moment(date).format('LL'),
                                                            openingTime: res.data.data.start_time,
                                                            closingTime: res.data.data.end_time,
                                                            notes: res.data.foundSpecialty ? res.data.data.notes : '',
                                                            dateOriginalFormat: date
                                                          }});
                              
                this.setState({showSpecialDaysInfoInModal: true});   
                this.toggleModal();                                  
            } else {
                this.setState({specialtyResponse: {status:  false, msg: res.data.msg}});
            }

            removeLoadingIconProcedurally();
        });
    }

    validateSpecialScheduleData = () => {
        this.clearResponseFields();
        var specialOpeningTime = document.getElementById('opening-special-time').value;
        var specialClosingTime = document.getElementById('closing-special-time').value;
        var note = document.getElementById('special-notes').value;
        var errorInNotes = false;

        var openAndClosingTimeValidBool = validateScheduleTimeBasedOnDelpries(specialOpeningTime, specialClosingTime);

        if(note.length === 0) {
            errorInNotes = true;
            document.getElementById('special-notes').style.borderColor = `${this.state.badColor}`;
        } else {
            errorInNotes = false;
            document.getElementById('special-notes').style.borderColor = `${this.state.goodColor}`;
        }
        if(!openAndClosingTimeValidBool[0]) {
            document.getElementById('opening-special-time').style.borderColor = `${this.state.badColor}`;
        } else {
            document.getElementById('opening-special-time').style.borderColor = `${this.state.goodColor}`;
        }

        if(!openAndClosingTimeValidBool[1]) {
            document.getElementById('closing-special-time').style.borderColor = `${this.state.badColor}`;
        } else {
            document.getElementById('closing-special-time').style.borderColor = `${this.state.goodColor}`;
        }
        

        return openAndClosingTimeValidBool[0] && openAndClosingTimeValidBool[1] && !errorInNotes;
    }

    updateSpecialData = () => {
        this.clearResponseFields();
        if(this.validateSpecialScheduleData()) {
            var specialOpeningTime = document.getElementById('opening-special-time').value;
            var specialClosingTime = document.getElementById('closing-special-time').value;
            var note = document.getElementById('special-notes').value;
            var date = this.state.currentSelectedDateDetails.dateOriginalFormat;

            //headers
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': localStorage.getItem('token')
                }
            }
        
            //add loading icon
            addLoadingIconProcedurally('alpha-container-vendor-schedule');
        
            const body  = JSON.stringify({
                vendorId: this.props.userData.vendor_id_for_vendor,
                specialOpeningTime,
                specialClosingTime,
                note,
                date
            });
        
            axios.post(`${apiURL}/api/vendors/set_special_date_info`, body, config)
            .then(res => {
                this.setState({specialtyResponse: {status:res.data.setData, msg: res.data.msg}});

                removeLoadingIconProcedurally();
            });
        }
    }
  
    clearModalData = () => {
        this.setState({showPostRequestDataInModal: false});
        this.setState({showSpecialDaysInfoInModal: false});
    }

    clearResponseFields = () => {
        //reset the response for calls
        this.setState({response: {status: false, msg: ''}});
        this.setState({specialtyResponse: {status: false, msg: ''}});
    }

    componentDidMount = () => {
        this.boilerPlateCodeForComponentUpdateAndMount();
    }

    componentDidUpdate = () => {
        this.boilerPlateCodeForComponentUpdateAndMount();
    }
    
    boilerPlateCodeForComponentUpdateAndMount = () => {
        //get the current schedule data
        if(!this.state.ihaveGottenReduxUserData && this.props.userData) {
            //add loading icon
            addLoadingIconProcedurally('alpha-container-vendor-schedule');

            //headers
            const config = {
            headers: {
                    'x-auth-token': localStorage.getItem('token')
                }
            }

            axios.get(`${apiURL}/api/vendors/get_schedule_data/${this.props.userData.vendor_id_for_vendor}`, config)
            .then(res => {
                if(res.data.gottenData) {
                    var schedules = res.data.schedule;
                    document.getElementById('monday-start-time').value = schedules.monday_start_time;
                    document.getElementById('monday-end-time').value = schedules.monday_end_time;
                    document.getElementById('tuesday-start-time').value = schedules.tuesday_start_time;
                    document.getElementById('tuesday-end-time').value = schedules.tuesday_end_time;
                    document.getElementById('wednesday-start-time').value = schedules.wednesday_start_time;
                    document.getElementById('wednesday-end-time').value = schedules.wednesday_end_time;
                    document.getElementById('thursday-start-time').value = schedules.thursday_start_time;
                    document.getElementById('thursday-end-time').value = schedules.thursday_end_time;
                    document.getElementById('friday-start-time').value = schedules.friday_start_time;
                    document.getElementById('friday-end-time').value = schedules.friday_end_time;
                    document.getElementById('saturday-start-time').value = schedules.saturday_start_time;
                    document.getElementById('saturday-end-time').value = schedules.saturday_end_time;
                    document.getElementById('sunday-start-time').value = schedules.sunday_start_time;
                    document.getElementById('sunday-end-time').value = schedules.sunday_end_time;
                } else {
                    //display the error in a modal
                    this.setState({response: {status: res.data.gottenData, msg: res.data.msg}});
                    this.setState({showPostRequestDataInModal: true});

                    //open the modal
                    this.toggleModal();
                }

                removeLoadingIconProcedurally();
            });

            //this is to prevent this code section from running multiple times
            this.setState({ihaveGottenReduxUserData : true});
        }
    }

    validateDefaultScheduleData = () => {
        var daysOfWeek = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
        var errorFound = false;
        var openAndClosingTimeValidBool = []; //the first value would hold if the open time is valid and the second would hold if the closing time is valid

        for(var i=0; i<daysOfWeek.length; i++) {
            openAndClosingTimeValidBool = validateScheduleTimeBasedOnDelpries(document.getElementById(daysOfWeek[i]+'-start-time').value, document.getElementById(daysOfWeek[i]+'-end-time').value);

            if(!openAndClosingTimeValidBool[0]) {
                document.getElementById(daysOfWeek[i]+'-start-time').style.borderColor = `${this.state.badColor}`;
                errorFound = true;
            } else {
                document.getElementById(daysOfWeek[i]+'-start-time').style.borderColor = `${this.state.goodColor}`;
                if(!errorFound) //if there is no error found already then you can change else dont change
                    errorFound = false;
            }

            if(!openAndClosingTimeValidBool[1]) {
                document.getElementById(daysOfWeek[i]+'-end-time').style.borderColor = `${this.state.badColor}`;
                errorFound = true;
            } else {
                document.getElementById(daysOfWeek[i]+'-end-time').style.borderColor = `${this.state.goodColor}`;
                if(!errorFound)
                    errorFound = false;
            }
        }

        return errorFound;
    }

    updateScheduleData = () => {
        this.clearResponseFields();

        if(!this.validateDefaultScheduleData()) {
            const body  = JSON.stringify({
                monday_start_time: document.getElementById('monday-start-time').value,
                monday_end_time: document.getElementById('monday-end-time').value,    
                tuesday_start_time: document.getElementById('tuesday-start-time').value,  
                tuesday_end_time: document.getElementById('tuesday-end-time').value,     
                wednesday_start_time: document.getElementById('wednesday-start-time').value, 
                wednesday_end_time: document.getElementById('wednesday-end-time').value,   
                thursday_start_time: document.getElementById('thursday-start-time').value,  
                thursday_end_time: document.getElementById('thursday-end-time').value,    
                friday_start_time: document.getElementById('friday-start-time').value,    
                friday_end_time: document.getElementById('friday-end-time').value,      
                saturday_start_time: document.getElementById('saturday-start-time').value,  
                saturday_end_time: document.getElementById('saturday-end-time').value,    
                sunday_start_time: document.getElementById('sunday-start-time').value,    
                sunday_end_time: document.getElementById('sunday-end-time').value,
                vendor_id:  `${this.props.userData.vendor_id_for_vendor}`
            });

            //headers        
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': localStorage.getItem('token')
                }
            }

            //add loading icon procedurally
            addLoadingIconProcedurally('alpha-container-vendor-schedule');

            axios.post(`${apiURL}/api/vendors/update_schedule_data`, body, config)
            .then(res => {
                this.setState({response: {status: res.data.updated, msg: res.data.msg}});

                //remove the loading icon
                removeLoadingIconProcedurally();
            });
        }
    }

    render() {
        return (
            <div id='alpha-container-vendor-schedule'>
                {/* the modal */}
                <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                    <div id='modal-container' className='vendor-schedule-modal-container'>
                        {!this.state.showPostRequestDataInModal ?  <div className='overflow-auto'><MdClear id='close-modal' onClick={() => this.toggleModal()}/></div> : ''}

                        {
                            //i dont want the clear if this is the type of error that occurs
                            this.state.showPostRequestDataInModal ? 
                                <div id='post-request-modal-container' className={this.state.response.status ?  'valid-color center' : 'bad-color center'}>
                                    {this.state.response.msg}
                                </div>
                            :
                                ''
                        }

                        {
                            this.state.showSpecialDaysInfoInModal ?
                                <div>
                                    {/* 
                                            this.setState({currentSelectedDateDetails: {date: moment(date, 'MMMM Do YYYY'),
                                                                openingTime: res.data.data.start_time,
                                                                closingTime: res.data.data.end_time,
                                                                notes: res.data.foundSpecialty ? res.data.data.notes : ''
                                                              }});
                                    */}
                                    <div className='center-sub-title'>Update the Schedule for {this.state.currentSelectedDateDetails.date}</div>
                                    <div className=''>
                                        <div className='italic bad-color'>* Please Note that your schedule has to fall between Delpries hours to ensure drivers are available to serve you, our hours are 9am - 10pm.</div>
                                    </div>

                                    <div id='special-date-time-entries' className='margin-top20'>
                                        <div className='margin-top10'>
                                            <b>Opening Time:</b> 
                                            <input type='time' id='opening-special-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} defaultValue={this.state.currentSelectedDateDetails.openingTime} onChange={this.validateSpecialScheduleData}/>
                                        </div>
                                        <div className='margin-top10'>
                                            <b>Closing Time:</b> 
                                            <input type='time' id='closing-special-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} defaultValue={this.state.currentSelectedDateDetails.closingTime} onChange={this.validateSpecialScheduleData}/>
                                        </div>
                                        <div className='margin-top10'>
                                            <b>Notes:</b> 
                                            <textarea type="text" id='special-notes' className='input-entries' defaultValue={this.state.currentSelectedDateDetails.notes} onChange={this.validateSpecialScheduleData}/>
                                        </div>
                                    </div>           

                                    <div className='response-from-database-in-modal center margin-top20' style={this.state.specialtyResponse.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.specialtyResponse.msg}</div>
                              
                                    <div className='center margin-top10'>
                                        <button id='update-special-button' className='custom-button' onClick={() => this.updateSpecialData()}>Update</button>
                                    </div>
                                </div>
                            :
                                ''
                        }
                    </div>
                </div>

                <div className='small-title'>Shifts</div>

                <div className=''>
                    <div className='italic bad-color'>* Please Note that your schedule has to fall between Delpries hours to ensure drivers are available to serve you, our hours are 9am - 10pm.</div>
                    <div className='italic bad-color'>* The Default shift is 11am - 6pm, modify it to suit your Opening and Closing hours.</div>
                </div>

                <div id='default-schedule-container'>
                    <div className='center-sub-title'>Default schedule</div>

                    <div className='days-default-schedule-title'><b>Days</b></div>
                    <div className='start-time-default-schedule-title'><b>Opening-Time</b></div>
                    <div className='end-time-default-schedule-title'><b>Closing-Time</b></div>

                    <div className='straight-line' />

                    <div className='days-default-schedule-title'>Monday</div>
                    <div className='start-time-default-schedule-title'><input type='time' id='monday-start-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>
                    <div className='end-time-default-schedule-title'><input type='time' id='monday-end-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>


                    <div className='days-default-schedule-title'>Tuesday</div>
                    <div className='start-time-default-schedule-title'><input type='time' id='tuesday-start-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>
                    <div className='end-time-default-schedule-title'><input type='time' id='tuesday-end-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>

                    <div className='days-default-schedule-title'>Wednesday</div>
                    <div className='start-time-default-schedule-title'><input type='time' id='wednesday-start-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>
                    <div className='end-time-default-schedule-title'><input type='time' id='wednesday-end-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>

                    <div className='days-default-schedule-title'>Thursday</div>
                    <div className='start-time-default-schedule-title'><input type='time' id='thursday-start-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>
                    <div className='end-time-default-schedule-title'><input type='time' id='thursday-end-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>

                    <div className='days-default-schedule-title'>Friday</div>
                    <div className='start-time-default-schedule-title'><input type='time' id='friday-start-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>
                    <div className='end-time-default-schedule-title'><input type='time' id='friday-end-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>

                    <div className='days-default-schedule-title'>Saturday</div>
                    <div className='start-time-default-schedule-title'><input type='time' id='saturday-start-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>
                    <div className='end-time-default-schedule-title'><input type='time' id='saturday-end-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>

                    <div className='days-default-schedule-title'>Sunday</div>
                    <div className='start-time-default-schedule-title'><input type='time' id='sunday-start-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>
                    <div className='end-time-default-schedule-title'><input type='time' id='sunday-end-time' className='date-schedule-entry' min={this.state.minTime} max={this.state.maxTime} onChange={this.validateDefaultScheduleData}/></div>
                    
                    <div className='response-from-database-in-modal center margin-top20' style={this.state.response.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.response.msg}</div>
                        
                    <div className='center'>
                        <button id='update-schedule-button' className='custom-button' onClick={() => this.updateScheduleData()}>Update</button>
                    </div>
                </div>


                <div id='specific-schedule-container'>
                    <div className='center-sub-title'>Special Days</div>

                    <div className=''>
                        <div className='italic bad-color center'>Click on a day to modify the shift hours for that specific day</div>
                    </div>
                    <Calendar
                        id='calendar'
                        onChange={this.onChangeDate}
                        value={this.state.todaysDate}
                        minDate={this.state.todaysDate}
                      />
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    userData: state.auth.userData
});

export default connect(mapStateToProps, {})(VendorSchedule);