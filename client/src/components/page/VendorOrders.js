//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';

//redux imports
import { connect } from 'react-redux';
import PropTypes, { array } from 'prop-types';

//custom made component
import PageLoading from '../utility/PageLoading';

//icons
import { MdClear } from 'react-icons/md';

//stylings
import '../../stylings/vendor-orders.css';

//define loadash
const _ = require('lodash');


class VendorOrders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            allOrdersData : null,
            ordersListComponents : [],
            vendorId : this.props.userData.vendor_id_for_vendor,
            goodColor: '#258e25',
            badColor: '#ff3333',
            defaultColor: '#a7a7a7',
            showPageLoading : true,
            showModal: false,
            showOrderDetailsItemsInModal: false,
            orderMoreInfoComponents: []
        }

        //this is the token
        const token = localStorage.getItem('token');

        //headers
        const config = {
            headers: {
                'x-auth-token': token
            }
        }

        //get all orders for this vendor
        axios.get(`${apiURL}/api/vendors/get_orders_made_to_vendor/${this.props.userData.vendor_id_for_vendor}`, config)
        .then(res => {
            //build the orders data
            this.buildTheOrdersToBeDisplayed(res.data);
            this.setState({allOrdersData : res.data});
        })
        .catch(err => {});
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        userData: PropTypes.object,
        utilityData: PropTypes.object
    }

    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
            this.setState({showOrderDetailsItemsInModal: false});
        }

        //open the modal
        this.setState({showModal: !this.state.showModal});
    }

    openModalWithOrderDetails = (orderId, totalPrice, dateOrdered, totalIncome, orderNumForView) => {
        //show the loading sign
        this.setState({showPageLoading : true });

        //this is the token
        const token = localStorage.getItem('token');

        //headers
        const config = {
            headers: {
                'x-auth-token': token
            }
        }

        axios.get(`${apiURL}/api/vendors/get_more_info_on_an_order/${orderId}`, config)
        .then(OrderDetailsData => {
            var count = 0;
            var orderMoreInfoComponentsTemp = [];
            var cartData = OrderDetailsData.data.orderToProducts;
            var total = 0.0;
            var addonsComponents = [];

            orderMoreInfoComponentsTemp.push(
                <div id='order-id' key={count++} className={'margin-bottom10 pre-info-order-details-modal'}>
                    <b>#{orderNumForView}</b>
                </div>
            );

            orderMoreInfoComponentsTemp.push(
                <div key={count++} className='pre-info-order-details-modal'>
                    <b>Total Income Made:</b> ${totalIncome}  &nbsp;&nbsp;&nbsp;&nbsp;<b>Order Date:</b> {new Date(dateOrdered).toDateString()}.
                </div>
            );

            if(cartData.length > 0) {
                orderMoreInfoComponentsTemp.push(
                    <br key={count++}/>
                );        
                orderMoreInfoComponentsTemp.push(
                    <br key={count++}/>
                );
                orderMoreInfoComponentsTemp.push(
                    <div key={count++} id='order-details-list-headers'>
                        <div className='more-info-item-name-in-order-details-headers   bold'>Item name</div>
                        <div className='more-info-item-amount-in-order-details-headers bold'>Amount</div>
                        <div className='more-info-item-price-in-order-details-headers  bold'>Price</div>
                    </div>
                );

                cartData.forEach(singleItem => {
                    //add the total
                    total += ((singleItem.product_price + singleItem.addons_total) * singleItem.amount_ordered);
                    addonsComponents = [];

                    //build the addons if any
                    if(singleItem.addons_description.length > 0) {
                        var addons = singleItem.addons_description.split(',');
                        for(var i=0; i<addons.length; i++) {
                            if(addons[i] != '') {
                                addonsComponents.push(
                                    <div key={singleItem._id+i} className='single-product-addons-data-in-cart'>{addons[i]}</div>
                                );
                            }
                        }
                    }
            
                    //build the row of item data
                    orderMoreInfoComponentsTemp.push(
                        <div key={count++} className='single-item-in-cart-container margin-bottom10 overflow-auto'>              
                            <div className='item-name-in-order-details-data'>
                                {singleItem.product_name}

                                <div className='product-addons-data-in-cart'>
                                    {addonsComponents}
                                </div>

                                {
                                    singleItem.special_instructions ? 
                                        <div className='special-instructions-in-cart'>
                                          "{singleItem.special_instructions}"
                                        </div>
                                    :
                                        ''
                                }
                            </div>
                            <div className='more-info-item-amount-in-order-details-data'>{singleItem.amount_ordered}</div>
                            <div className='more-info-item-price-in-order-details-data'>${(singleItem.product_price + singleItem.addons_total).toFixed(2)}</div>
                        </div>
                    );
                });
          
                //and some spaces before and then the straight line
                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='straight-line'/>
                );
          
                //add the total data and design to the modal
                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='single-item-in-cart-container'>
                        <div className='lower-item-name-checkout'>Price</div>
                        <div className='lower-item-value-checkout'>${(total + (total * this.props.utilityData.tax)).toFixed(2)}</div>
                    </div>
                );
          
                //add the tax data and design to the modeal
                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='single-item-in-cart-container'>
                        <div className='lower-item-name-checkout'>Total Income</div>
                        <div className='lower-item-value-checkout'>${totalIncome.toFixed(2)}</div>
                    </div>
                );
            }      
            
            this.setState({orderMoreInfoComponents : orderMoreInfoComponentsTemp});
        });

        //remove the loading sign
        this.setState({showPageLoading : false });

        //allow the modal data to show
        this.setState({showOrderDetailsItemsInModal: true });

        //toggle modal
        this.toggleModal();
    }

    buildTheOrdersToBeDisplayed = (orders) => {
        if(orders.length > 0) { 
            var count = 0;
            var orderListComponentsTemp = [];

            orders.forEach(singleOrder => {
                //get the logo
                orderListComponentsTemp.push(
                    <div key={count++} className='vendor-single-order-item' onClick={() => this.openModalWithOrderDetails(singleOrder._id, singleOrder.order_price_total_with_tax, singleOrder.order_date, singleOrder.vendors_total_money, singleOrder.order_num_for_view)}>
                        <div id='order-id' className={'margin-bottom10'}>
                            <b>#{singleOrder.order_num_for_view}</b>
                        </div>
                        <div className='vendor-product-vendor-name-single-order'>{singleOrder.product_vendor_name}</div>
                        <div className='vendor-order-info-single-order'>{singleOrder.order_name}</div>
                        <div className='vendor-price-single-order'>Total Income: ${singleOrder.vendors_total_money} </div>
                        <div className='vendor-date-single-order'> Date: {new Date(singleOrder.order_date).toDateString()}</div>
                    </div>
                );
            });

            this.setState({ordersListComponents : orderListComponentsTemp});
        } else {
            this.setState({ordersListComponents : <div key={'no-order-in-list'} id='no-items-available-in-list-vendor-order'>No Order has been made</div>});
        }

        //remove the loading sign
        this.setState({showPageLoading : false });
    }

    // parse a date in yyyy-mm-dd format
    parseDate = (input) => {
        var parts = input.match(/(\d+)/g);
        // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
        return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
    }

    filterOrders = () => {
        //add the loading sign
        this.setState({showPageLoading : false });

        //if we dont have a start date then change the border color
        if(!document.getElementById('start-date-filter').value) 
            document.getElementById('start-date-filter').style.borderColor = `${this.state.badColor}`;
        
        //if we dont have an end date we also change the border color
        if(!document.getElementById('end-date-filter').value) 
            document.getElementById('end-date-filter').style.borderColor = `${this.state.badColor}`;

        //if we have start and end that then we can go from here
        if(document.getElementById('start-date-filter').value && document.getElementById('end-date-filter').value) {
            var startDate = this.parseDate(document.getElementById('start-date-filter').value);
            var endDate = this.parseDate(document.getElementById('end-date-filter').value);

            //if the endDate is lower than the start date then we dont want that else we continue
            if(endDate < startDate) {
                document.getElementById('end-date-filter').style.borderColor = `${this.state.badColor}`;
            } else {
                //change all the bad colors that have been set
                document.getElementById('start-date-filter').style.borderColor = `${this.state.defaultColor}`;
                document.getElementById('end-date-filter').style.borderColor = `${this.state.defaultColor}`;
          
                var orders = this.state.allOrdersData;
                var parts = '';

                orders = _.filter(orders, function(singleOrder){
                    parts = (singleOrder.order_date).match(/(\d+)/g);
                    var orderDate = new Date(parts[0], parts[1]-1, parts[2]);

                    return orderDate >= startDate && orderDate <= endDate;
                });
          
                this.buildTheOrdersToBeDisplayed(orders);
            }
        }
    }

    clearFilter = () => {
        document.getElementById('start-date-filter').value = '';
        document.getElementById('end-date-filter').value = '';

        var orders = this.state.allOrdersData;
        this.buildTheOrdersToBeDisplayed(orders);
    }

    render() {
        return (
            <div>
                <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                    <div id='modal-container-order-details'>
                        <MdClear id='close-modal' onClick={() => this.toggleModal()}/>
                        <br />
                        <br />
                        {
                            this.state.showOrderDetailsItemsInModal ? 
                                <div id='order-details-more-info'>
                                    <div id='order-details-title' className='title'>Order Details</div>
                                    {this.state.orderMoreInfoComponents}
                                </div>
                            :
                                ''
                        }
                    </div>
                </div>

                <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/> 

                <div id='alpha-container-all-orders'>
                    <div className='small-title color-black title-all-order'>All Orders</div>

                    <div id='all-orders-filter'>
                        <div className='margin-top10'>
                            <b>Start Date: </b><br/><input type="date" id='start-date-filter' className='date-entries-filter'/><br/>
                            <b>End Date: </b><br/><input type="date" id='end-date-filter'   className='date-entries-filter'/>

                            <div id='buttons-for-filter'>
                                <button id='filter-button' className='custom-button' onClick={this.filterOrders}>Filter</button>
                                <button id='clear-filter-button' className='custom-button' onClick={this.clearFilter}>Clear Filters</button>
                            </div>
                        </div>
                    </div>
                    
                    <div id='vendors-info'> {this.state.allOrdersData ? 'You have made $' +(_.sumBy(this.state.allOrdersData, orderData => {return orderData.vendors_total_money})).toFixed(2)+ ' in total' : ''} </div>
                        
                    <div id='all-orders-list'>
                        {this.state.ordersListComponents}
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
  userData: state.auth.userData,
  utilityData: state.utility.utilityData
});

export default connect(mapStateToProps, {})(VendorOrders);