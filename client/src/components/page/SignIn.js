//system defined components
import React, {Component} from 'react';
import Footer from '../utility/Footer';
import { Redirect } from 'react-router';
import axios from 'axios';
import {apiURL} from '../../config.js';

//redux importations
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { login } from '../../redux/actions/authActions';

//images
import company_logo from '../../images/icons/logo9.png';

//icons
import { MdClear } from 'react-icons/md';
import { FaRegEyeSlash } from 'react-icons/fa';
import { FaRegEye } from 'react-icons/fa';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally} from '../utility/utilityFunctions';

//stylings
import '../../stylings/sign-in.css';


class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
        goodColor: '#258e25',
        badColor: '#ff3333',
        defaultColor: '#a7a7a7',
        showPasswordInText: false,

        errorInEmail : false,
        errorInPassword : false,
        showPageLoading: false,

        response : {
            status: false,
            msg: ''
        },
        showModal: false,
        showForgotPassowrdDataInModal: false,

        forgotPasswordDetailsTypeError: {
            errorInEmail: false,
            errorInVerificationCode: false,
            errorInNewPassword: false,
            errorInRetypePassword: false
        },
    }
  }

  static propTypes = {
    isAuthenticated: PropTypes.bool,
    msg: PropTypes.string,
    userData: PropTypes.object
  }


  toggleModal = () => {
    //if the current value is true that means we want to close the modal so remove all show modals
    if(this.state.showModal) {
        this.setState({showForgotPassowrdDataInModal: false});
        
        //reset the response for calls
        this.setState({response: {status: false, msg: ''}});
    }

    //open the modal
    this.setState({showModal: !this.state.showModal});
  }

  toggleShowPasswordInText = () => {
    this.setState({showPasswordInText: !this.state.showPasswordInText});
  }

  validateEmail =  (email) => {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
  }

  clearResponseFields = () => {
    //reset the response for calls
    this.setState({response: {status: false, msg: ''}});
  }

  validatePassword = (pw) => {
    return /[A-Z]/.test(pw) &&
           /[a-z]/.test(pw) &&
           /[0-9]/.test(pw) &&
           /[^A-Za-z0-9]/.test(pw) &&
           pw.length > 6;
  }

  validateUserSignInData = () => { 
    //change the request status
    //this.setState({response: {status: false, msg: ''}});

    var noErrors = [{emailError: true, passwordError: true}];
   
    var email = document.getElementById('email-input').value;
    var password = document.getElementById('password-input').value;

    //validate email
    if(this.validateEmail(email)) {
        noErrors.emailError = false;
        document.getElementById('email-input').style.borderColor = `${this.state.goodColor}`;
    } else {
        noErrors.emailError = true;
        document.getElementById('email-input').style.borderColor = `${this.state.defaultColor}`;
    }

    //validate password    
    if(this.validatePassword(password)) {
        noErrors.passwordError = false;
        document.getElementById('password-input').style.borderColor = `${this.state.goodColor}`;
    } else {
        noErrors.passwordError = true;
        document.getElementById('password-input').style.borderColor = `${this.state.defaultColor}`;
    }

    //update the data  
    this.setState({errorInEmail: noErrors.emailError});
    this.setState({errorInPassword: noErrors.passwordError});

    //return the value
    return !noErrors.emailError && !noErrors.passwordError;
  }

  
  signInUser = () => {
    //show the page loading
    this.setState({showPageLoading : true });

    if(this.validateUserSignInData()) {
        var email = document.getElementById('email-input').value;
        var password = document.getElementById('password-input').value;

        this.props.login({email, password});
    }
  }


  forgotPasswordOpenModal = () => {
    this.setState({showForgotPassowrdDataInModal: true});
    this.toggleModal();
  }

  validateForgotPasswordData = () => {
    //this is to reset the response
    this.clearResponseFields();
    
    var email = document.getElementById('email-input').value;
    var verificationCode = document.getElementById('verification-code').value;
    var newPassword = document.getElementById('password-input').value;
    var reTypePassword = document.getElementById('re-password-input').value;

    var noErrors = [{emailError: true, verificationCodeError: true, newPasswordError: true, reTypePasswordError: true}];

    if(this.validateEmail(email)) {
        noErrors.emailError = false;
        document.getElementById('email-input').style.borderColor = `${this.state.goodColor}`;
    } else {
        noErrors.emailError = true;
        document.getElementById('email-input').style.borderColor = `${this.state.defaultColor}`;
    }

    if(verificationCode.length > 0) {
        noErrors.verificationCodeError = false;
        document.getElementById('verification-code').style.borderColor = `${this.state.goodColor}`;
    } else {
        noErrors.verificationCodeError = true;
        document.getElementById('verification-code').style.borderColor = `${this.state.defaultColor}`;
    }

    if(this.validatePassword(newPassword)) {
        noErrors.newPasswordError = false;
        document.getElementById('password-input').style.borderColor = `${this.state.goodColor}`;
    } else {
        noErrors.newPasswordError = true;
        document.getElementById('password-input').style.borderColor = `${this.state.defaultColor}`;
    }

    if(reTypePassword === newPassword) {
        noErrors.reTypePasswordError = false;
        document.getElementById('re-password-input').style.borderColor = `${this.state.goodColor}`;
    } else {
        noErrors.reTypePasswordError = true;
        document.getElementById('re-password-input').style.borderColor = `${this.state.defaultColor}`;
    }

    //update the data
    this.setState({forgotPasswordDetailsTypeError : {
        errorInEmail: noErrors.emailError,
        errorInVerificationCode: noErrors.verificationCodeError, 
        errorInNewPassword: noErrors.newPasswordError, 
        errorInRetypePassword: noErrors.reTypePasswordError
    }});

    return !noErrors.emailError || !noErrors.verificationCodeError && !noErrors.newPasswordError && !noErrors.reTypePasswordError;   
  }
  
  updatePasswordWithVerification = () => {
    if(this.validateForgotPasswordData()) {
        var verificationCode = document.getElementById('verification-code').value;
        var newPassword = document.getElementById('password-input').value;
        var email = document.getElementById('email-input').value;

        //headers        
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
    
        //make body
        const body = JSON.stringify({verificationCode, newPassword, email});

        //add loading icon procedurally
        addLoadingIconProcedurally('alpha-sign-up-container');

        axios.post(`${apiURL}/api/sign_in/update_password_with_verification_code_email`, body, config)
        .then(res => {
            this.setState({response: {status: res.data.updated, msg: res.data.msg}});

            //remove the loading icon
            removeLoadingIconProcedurally();
        });
    }
  }

  requestVerificationCode = () => {
    var email = document.getElementById('email-input').value;

    if(this.validateEmail(email)) {
        this.clearResponseFields();
        document.getElementById('email-input').style.borderColor = `${this.state.goodColor}`;

        //headers
        const config = {
            headers: {
                'Content-Type': 'application/json',
            }
        }

        //make body
        const body = JSON.stringify({email});

        //add loading icon procedurally
        addLoadingIconProcedurally('alpha-sign-up-container');

        axios.post(`${apiURL}/api/sign_in/send_user_verification_code_to_email`, body, config)
        .then(res => {
            this.setState({response: {status: res.data.sentCode, msg: res.data.msg}});

            //remove the loading icon
            removeLoadingIconProcedurally();
        });
    } else {
        this.setState({response: {status: false, msg: 'Please enter a valid Email'}});
    }
  }

  render() {
    return this.props.userData ?
        this.props.userData.can_access_order_details_page ?
            localStorage.getItem('cart-data') !== null ?
            <Redirect to='/'/>
            :
            <Redirect to='/orderDetails'/>
        :
            this.props.userData.can_access_vendors_page ?
                <Redirect to='/vendors'/>
            :
                this.props.userData.can_access_drivers_page ?
                    <Redirect to='/drivers'/>
            :
                ''
      :
        (
            <div id='alpha-sign-up-container'>
                <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                    <div id='modal-container' className='sign-up-modal-container'>
                        <div className='overflow-auto'><MdClear id='close-modal' onClick={() => this.toggleModal()}/></div>

                        {
                                this.state.showForgotPassowrdDataInModal ?
                                    <div id='change-password-modal'>
                                        <div className='sub-section-title center margin-top10'>Forgot Password</div>
                                        <div className='center italic bad-color'>Just enter email to request verification code and then enter email, verification code and new password to Update Password</div>


                                        <div id='eye-icons-container'>
                                            {
                                                this.state.showPasswordInText ? 
                                                <FaRegEyeSlash className='eye-icons margin-top20'  onClick={() => this.toggleShowPasswordInText()}/>
                                            :
                                                <FaRegEye className='eye-icons margin-top20'  onClick={() => this.toggleShowPasswordInText()}/>
                                            }
                                        </div>

                                        <div className='margin-top20'>
                                            <b>Email:</b> 
                                            <input type="text" className='input-entries' id='email-input' onChange={this.validateForgotPasswordData}/>
                                            <div className='error-message' style={this.state.forgotPasswordDetailsTypeError.errorInEmail ? {display:'block'} : {display:'none'}}>
                                                Invalid email
                                            </div>
                                        </div>

                                        <div className='margin-top10'>
                                            <b>Verification Code:</b> 
                                            <input type='text' className='input-entries' id='verification-code' onChange={this.validateForgotPasswordData}/>
                                            <div className='error-message' style={this.state.forgotPasswordDetailsTypeError.errorInVerificationCode ? {display:'block'} : {display:'none'}}>
                                                Enter the code
                                            </div>
                                        </div>

                                        <div className='margin-top10'>
                                            <b>New Password:</b> 
                                            <input type={this.state.showPasswordInText ? 'text': 'password'} className='input-entries' id='password-input' onChange={this.validateForgotPasswordData}/>
                                            <div className='error-message' style={this.state.forgotPasswordDetailsTypeError.errorInNewPassword ? {display:'block'} : {display:'none'}}>
                                                Invalid Password | password must be more than 6 characters | Must have an upper case and Lowercase | Must have special Symbol ($) and numeric character
                                            </div>
                                        </div>
                                        
                                        <div className='margin-top10'>
                                            <b>Re-type Password:</b> 
                                            <input type={this.state.showPasswordInText ? 'text': 'password'} className='input-entries' id='re-password-input'  onChange={this.validateForgotPasswordData}/>
                                            <div className='error-message' style={this.state.forgotPasswordDetailsTypeError.errorInRetypePassword ? {display:'block'} : {display:'none'}}>
                                                Re-type password must be the same as new password
                                            </div>
                                        </div>

                                        <div className='response-from-database-in-modal center margin-top20' style={this.state.response.status ? {color : this.state.goodColor} : {color: this.state.badColor}}>{this.state.response.msg}</div>
                
                                        <div className='center'>
                                            <button className='custom-button' id='change-password-button' onClick={() => this.requestVerificationCode()}>
                                                Request Verification Code
                                            </button>

                                            <button className='custom-button' id='update-user-details-button' onClick={() => this.updatePasswordWithVerification()}>
                                                Update Password
                                            </button>
                                        </div>
                                    </div>
                                :
                                    ''
                            }
                    </div>
                </div>

                <div id='alpha-sign-in-container'>
                    <div className='absolute-company-logo'>
                        <img src={company_logo} className='image-take-full-space' alt='company_logo'/>
                    </div>
                
                    <div id='sign-in-container'>
                        <div className='title center color-black'>Login</div>
                
                        <div className='margin-top10'>
                            <b>Email:</b> 
                            <input type="text" className='input-entries' id='email-input' onChange={this.validateUserSignInData}/>
                            <div className='error-message' style={this.state.errorInEmail ? {display:'block'} : {display:'none'}}>
                                Invalid email
                            </div>
                        </div>
                
                        <div className='margin-top10'>
                            <b>Password:</b> 
                            <input type="password" className='input-entries' id='password-input' onChange={this.validateUserSignInData}/>
                            <div className='error-message' style={this.state.errorInPassword ? {display:'block'} : {display:'none'}}>
                                Enter password | Enter Valid password
                            </div>
                        </div>
                
                        <div className='center margin-top10' style={this.props.isAuthenticated ? {color: this.state.goodColor} : {color: this.state.badColor}}>
                            {this.props.msg}
                        </div>
                
                        <div className='button-container'>
                            <button className='custom-button' id='sign-in-button-sign-in-page' onClick={() => this.signInUser()}>
                                Login
                            </button>

                            <button className='custom-button' id='forgot-password-button-sign-in-page' onClick={() => this.forgotPasswordOpenModal()}>
                                Forgot Password
                            </button>
                        </div>
                    </div>
                
                    <Footer />
                </div>
            </div>
        )
  }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    msg: state.auth.msg,
    userData: state.auth.userData
});


export default connect(mapStateToProps, { login })(SignIn);