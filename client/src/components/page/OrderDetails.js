//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';
import Footer from '../utility/Footer';

//custom made component
import PageLoading from '../utility/PageLoading';
import Checkout from '../page/Checkout';


//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

//images
import company_logo from '../../images/icons/logo9.png';

//icons
import { MdClear } from 'react-icons/md';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally} from '../utility/utilityFunctions';

//stylings
import '../../stylings/order-details.css';

class OrderDetails extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            ordersListComponents: [],
            orderMoreInfoComponents: [],
            userId: null,
            showPageLoading: false,
            showModal: false,
            showOrderDetailsItemsInModal: false,
            goodColor: '#258e25',
            badColor: '#ff3333',
            yellowColor: '#F7C544',
            iHaveUsedUserDataToGetOrders: false,
            iHaveUsedSocketDataToMountListeners : false,
            selectedOrderId: null,
            ordersHasBeenMade: false,
            vendorHasApproved: false,
            driverHasPickedUp: false,
            itemHasBeenDelivered: false,
            driverHasStartedOrder: false,
            showCheckoutPage: false,
            cartData: null
        };
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        isAuthenticated: PropTypes.bool,
        socketData: PropTypes.object,
        utilityData : PropTypes.object,
        userData: PropTypes.object,
    }

    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
            this.setState({showOrderDetailsItemsInModal: false});
        }

        //open the modal
        this.setState({showModal: !this.state.showModal});
    }

    openModalWithOrderDetails = (orderId, vendorName, productVendorId) => {
        //show the loading sign
        this.setState({showPageLoading : true });
        addLoadingIconProcedurally('order-details-alpha-container');

        //this is the token
        const token = localStorage.getItem('token');

        //headers
        const config = {
            headers: {
                'x-auth-token': token
            }
        }
        
        axios.get(`${apiURL}/api/order_details/get_more_info_on_an_order/${orderId}`, config)
        .then(orderDetailsData => {
            removeLoadingIconProcedurally();

            var count = 0;
            var orderMoreInfoComponentsTemp = [];
            var orderData = orderDetailsData.data.order;
            var driverData = orderDetailsData.data.driver;
            var cartData = orderDetailsData.data.orderToProducts;
            var total = 0.0;
            var tax = 0.0;
            var deliveryFee = this.props.utilityData.delivery_fee_for_client;
            var tip = orderData.tip;
            var addonsComponents = [];

            //update the data
            this.setState({ordersHasBeenMade: orderData.just_ordered}); 
            this.setState({vendorHasApproved: orderData.vendor_approved});
            this.setState({driverHasStartedOrder: orderData.driver_has_started_order});
            this.setState({driverHasPickedUp: orderData.driver_has_picked_up});
            this.setState({itemHasBeenDelivered: orderData.delivered});

            this.setState({selectedOrderId : orderId});

            // <div className='order-info-single-order-id'>#{singleOrder._id}</div>
            // orderMoreInfoComponentsTemp.push(
            //   <div key={count++} className='pre-info-order-details-modal'>
            //    #{orderId}
            //   </div>
            // );

            orderMoreInfoComponentsTemp.push(
                <div key={count++} className='pre-info-order-details-modal'>
                    <h3><b>Store:</b> {vendorName}</h3>
                </div>
            );

            orderMoreInfoComponentsTemp.push(
                <div id='order-id' key={count++} className={'margin-bottom10 pre-info-order-details-modal'}>
                    #{orderData.order_num_for_view}
                </div>
            );
            
            orderMoreInfoComponentsTemp.push(
                <div key={count++} className='pre-info-order-details-modal'>
                    <b>Store Location:</b> {orderDetailsData.data.vendorLocation}
                </div>
            );
            
            orderMoreInfoComponentsTemp.push(
                <div key={count++} className='pre-info-order-details-modal'>
                    <b>Total Price:</b> ${orderData.order_price_total_with_tax}  &nbsp;&nbsp;&nbsp;&nbsp;<b>Order Date:</b> {new Date(orderData.order_date).toDateString()}.
                </div>
            );

            orderMoreInfoComponentsTemp.push(
                <div key={count++} className='pre-info-order-details-modal'>
                    <b>Destination:</b> {orderData.address}
                </div>
            );

            orderMoreInfoComponentsTemp.push(
                <br key={count++}/>
            );        
            orderMoreInfoComponentsTemp.push(
                <br key={count++}/>
            );
            orderMoreInfoComponentsTemp.push(
                <div key={count++} id='order-details-list-headers'>
                    <div className='item-name-in-order-details-headers   bold'>Item name</div>
                    <div className='item-amount-in-order-details-headers bold'>Amount</div>
                    <div className='item-price-in-order-details-headers  bold'>Price</div>
                </div>
            );
            
            if(cartData.length > 0) {
                cartData.forEach(singleItem => {
                    addonsComponents = [];
                    
                    //add the total
                    total += ((singleItem.product_price + singleItem.addons_total) * singleItem.amount_ordered);
            
                    //build the addons if any
                    if(singleItem.addons_description.length > 0) {
                        var addons = singleItem.addons_description.split(',');
                        for(var i=0; i<addons.length; i++) {
                            if(addons[i] !== '') {
                                addonsComponents.push(
                                    <div key={singleItem._id+i} className='single-product-addons-data-in-cart'>{addons[i]}</div>
                                );
                            }
                        }
                    }

            
                    //build the row of item data
                    orderMoreInfoComponentsTemp.push(
                        <div key={count++} className='single-item-in-cart-container margin-bottom10'>            
                            <div className='item-name-in-order-details-data'>
                                {singleItem.product_name}

                                <div className='product-addons-data-in-cart'>
                                    {addonsComponents}
                                </div>

                                {
                                    singleItem.special_instructions ? 
                                        <div className='special-instructions-in-cart'>
                                            "{singleItem.special_instructions}"
                                        </div>
                                    :
                                        ''
                                }
                            </div>
                            <div className='item-amount-in-order-details-data'>{singleItem.amount_ordered}</div>
                            <div className='item-price-in-order-details-data'>${(singleItem.product_price + singleItem.addons_total).toFixed(2)}</div>
                        </div>
                    );
                });

                //add the total row and tax and everthing here
                tax = total * this.props.utilityData.tax; // this is for ontario and the app is only starting with ontario - London
          

                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='straight-line'/>
                );
          
                //add the total data and design to the modal
                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='single-item-in-cart-container'>
                        <div className='lower-item-name-order-details'>Price</div>
                        <div className='lower-item-value-order-details'>${total.toFixed(2)}</div>
                    </div>
                );
          
                //add the tax data and design to the modeal
                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='single-item-in-cart-container'>
                        <div className='lower-item-name-order-details'>Tax</div>
                        <div className='lower-item-value-order-details'>${tax.toFixed(2)}</div>
                    </div>
                );
          
                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='single-item-in-cart-container'>
                        <div className='lower-item-name-order-details'>Tip</div>
                        <div className='lower-item-value-order-details'>${tip.toFixed(2)}</div>
                    </div>
                );

                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='single-item-in-cart-container'>
                        <div className='lower-item-name-order-details'>Delivery Fee</div>
                        <div className='lower-item-value-order-details'>${deliveryFee.toFixed(2)}</div>
                    </div>
                );
          
                //add the summation data and design to the modal
                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='single-item-in-cart-container'>
                        <div className='lower-item-name-order-details'>Total</div>
                        <div className='lower-item-value-order-details'>${(total+tax+deliveryFee+tip).toFixed(2)}</div>
                    </div>
                );
            }

            if(!orderData.cancelled) {
                orderMoreInfoComponentsTemp.push(
                    <br key={count++}/>
                );        
                orderMoreInfoComponentsTemp.push(
                    <br key={count++}/>
                );            

                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='pre-info-order-details-modal'>
                        <h3>Order Status</h3>
                    </div>
                );
            
                //if driver has not started the order and driver has been assigned then we need to display that info
                if(!this.state.driverHasStartedOrder && driverData) {
                    orderMoreInfoComponentsTemp.push(
                        <div key={count++} className='center valid-color italic'>
                            {driverData.first_name} {driverData.last_name} has been assigned your order
                        </div>
                    );   
                }

                //if driver has not picked up the order but has started the order then they are going to the store
                if(this.state.driverHasStartedOrder && !this.state.driverHasPickedUp) {
                    orderMoreInfoComponentsTemp.push(
                        <div key={count++} className='center valid-color italic'>
                            {driverData.first_name} {driverData.last_name} is going to the store to get your order
                        </div>
                    );   
                }

                //if a driver has picked up the order, display the driver details
                if(this.state.driverHasPickedUp && !this.state.itemHasBeenDelivered) {
                    orderMoreInfoComponentsTemp.push(
                        <div key={count++} className='center valid-color italic'>
                            {driverData.first_name} {driverData.last_name} is coming to you with your order
                        </div>
                    );   
                }

                //add the order agin button
                orderMoreInfoComponentsTemp.push(
                    <div key={count++} id='order-status'>
                        <svg id='order-status-svg-container'>
                            <circle  cx='8%' cy='50%' r='6%' fill={this.state.ordersHasBeenMade ? this.state.goodColor : this.state.yellowColor}/>
                            <line x1="8%" x2="32%" y1="50%" y2="50%" style={{stroke: this.state.ordersHasBeenMade ? this.state.goodColor : this.state.yellowColor, strokeWidth:'0.7%'}} />
                            <text x="0%" y="95%" fill={this.state.ordersHasBeenMade ? this.state.goodColor : this.state.yellowColor} className='order-status-text'>Order's Placed</text>


                            <circle cx='35%' cy='50%' r='6%' fill={this.state.vendorHasApproved ? this.state.goodColor : this.state.yellowColor}/>
                            <line x1="35%" x2="60%" y1="50%" y2="50%" style={{stroke: this.state.vendorHasApproved ? this.state.goodColor : this.state.yellowColor, strokeWidth:'0.7%'}} />
                            <text x="24%" y="95%" fill={this.state.vendorHasApproved ? this.state.goodColor : this.state.yellowColor} className='order-status-text'>Store has Verified</text>


                            <circle cx='62%' cy='50%' r='6%' fill={this.state.driverHasPickedUp ? this.state.goodColor : this.state.yellowColor}/>
                            <line x1="62%" x2="87%" y1="50%" y2="50%" style={{stroke: this.state.driverHasPickedUp ? this.state.goodColor : this.state.yellowColor, strokeWidth:'0.7%'}} />
                            <text x="52%" y="95%" fill={this.state.driverHasPickedUp ? this.state.goodColor : this.state.yellowColor} className='order-status-text'>Driver's PickedUp</text>

                            <circle cx='90%' cy='50%' r='6%' fill={this.state.itemHasBeenDelivered ? this.state.goodColor : this.state.yellowColor}/>
                            <text x="84%" y="95%" fill={this.state.itemHasBeenDelivered ? this.state.goodColor : this.state.yellowColor} className='order-status-text'>Delivered</text>
                        </svg> 
                    </div>
                );    
            } else {
                orderMoreInfoComponentsTemp.push(
                    <br key={count++}/>
                ); 
                orderMoreInfoComponentsTemp.push(
                    <br key={count++}/>
                );       

                orderMoreInfoComponentsTemp.push(
                    <div key={count++} className='pre-info-order-details-modal'>
                        <h3 className='bad-color'>We are Sorry to Inform you but {vendorName} has cancelled your Order</h3>
                        <h4 className='bad-color'>No charges would be made</h4>
                    </div>
                );
            }

            orderMoreInfoComponentsTemp.push(
                <br key={count++}/>
            );   

            //add the order agin button
            orderMoreInfoComponentsTemp.push(
                <div key={count++} id='order-again-button-div'>
                    <button className='custom-button order-again-button' onClick={() => this.orderAgain()}>Order Again</button>
                </div>
            );
            
            //set the cart data
            cartData[0].product_vendor_id = productVendorId
            this.setState({cartData : cartData});

            this.setState({orderMoreInfoComponents : orderMoreInfoComponentsTemp});
        });

        //remove the loading sign
        this.setState({showPageLoading : false });

        //allow the modal data to show
        this.setState({showOrderDetailsItemsInModal: true });

        //toggle modal if modal is not opened already
        if(!this.state.showModal) {
            this.toggleModal();
        }
    }

    orderAgain = () => {
        //open the checkout page
        this.setState({showCheckoutPage : true});

        //store the source of cartData
        // if (global.localStorage) {
        //   localStorage.setItem('cart-data-from-order-details', true);
        // }
    }

    buildOrdersList = (orderDetailsData) => {
        //show the loading sign
        this.setState({showPageLoading : true });

        //get the orders and make the components
        var count = 0;
        var iconUrl = '';
        var orderListComponentsTemp = [];

        if(orderDetailsData.data.length > 0) {
            orderDetailsData.data.forEach(singleOrder => {
                //get the logo
                iconUrl = require('../../images/vendor_images/' + singleOrder.product_vendor_icon_name+'.png');

                orderListComponentsTemp.push(
                    <div key={count++} className='single-order-item' onClick={() => this.openModalWithOrderDetails(singleOrder._id, singleOrder.product_vendor_name, singleOrder.product_vendor_id)}>
                        <div id='order-id' className={'margin-bottom20'}>
                            #{singleOrder.order_num_for_view}
                        </div>
                        <div className='product-vendor-icon-single-order'>
                            <img className='product-vendor-icon-single-order-img' src={iconUrl} alt={singleOrder.product_vendor_name}/>
                        </div>
                        <div className='product-vendor-name-single-order'>{singleOrder.product_vendor_name}</div>
                        <div className='order-info-single-order'>{singleOrder.order_name}</div>
                        <div className='price-single-order'>Total Price: ${singleOrder.order_price_total_with_tax} </div>
                        <div className='date-single-order'> Date: {new Date(singleOrder.order_date).toDateString()}</div>
                        {/* <div className='order-info-single-order-id'>#{singleOrder._id}</div> */}
                    </div>
                );
            });
        } else {
            orderListComponentsTemp.push(
                <div key={'no-order-in-list'} id='no-items-available-in-list'>
                    No Order has been made
                </div>
            );
        }

        //remove the loading sign
        this.setState({showPageLoading : false });

        this.setState({ordersListComponents : orderListComponentsTemp});
    }

    componentDidMount = () => {
        if(!this.state.iHaveUsedUserDataToGetOrders && this.props.userData) {
            this.getOrderDetailsData();
        }
    }

    componentDidUpdate = () => {
        if(!this.state.iHaveUsedUserDataToGetOrders && this.props.userData) {
            this.getOrderDetailsData();
        }

        if(!this.state.iHaveUsedSocketDataToMountListeners && this.props.socketData) {
            //define our listeners
            this.props.socketData.on('vendorHasRespondedToOrderAvailability', this.vendorHasRespondedToOrderAvailability);

            this.setState({iHaveUsedSocketDataToMountListeners : true});
        }
    }
    
    getOrderDetailsData = () => {
        //show the loading sign
        addLoadingIconProcedurally('order-details-alpha-container');

        //get the orders and make the components
        this.setState({userId : this.props.userData.id});

        //this is the token
        const token = localStorage.getItem('token');
        
        //headers
        const config = {
            headers: {
                'x-auth-token': token
            }
        }

        axios.get(`${apiURL}/api/order_details/get_all_order_data_for_user/${this.props.userData._id}`, config)
        .then(orderDetailsData => {
            this.buildOrdersList(orderDetailsData);
        });

        //remove the loading sign
        removeLoadingIconProcedurally();

        this.setState({iHaveUsedUserDataToGetOrders: true});
    }


    vendorHasRespondedToOrderAvailability = (orderAvailabilityData) => {
        if(orderAvailabilityData.orderId === this.state.selectedOrderId) {
            this.openModalWithOrderDetails(orderAvailabilityData.orderId);
        } else {
        }
    }

    render() {
        return (
            <div id='order-details-alpha-container'>
                { 
                    this.state.showCheckoutPage ? 
                        <Checkout cartItems={this.state.cartData} fromOrderDetails={true}/>
                    :
                        <div>
                            <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                                <div id='modal-container-order-details'>
                                    <MdClear id='close-modal' onClick={() => this.toggleModal()}/>
                                    <br />
                                    <br />
                                    {
                                        this.state.showOrderDetailsItemsInModal ? 
                                            <div id='order-details-more-info'>
                                                <div id='order-details-title' className='title'>Order Details</div>
                                                {this.state.orderMoreInfoComponents}
                                            </div>
                                        :
                                            ''
                                    }
                                </div>
                            </div>

                            <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/> 

                            <div className='absolute-company-logo order-details-company-logo'>
                                <img src={company_logo} className='image-take-full-space' alt='company_logo'/>
                            </div>
                            
                            <div id='order-main-container'>
                                <div id='orders-title' className='title'>Orders</div>

                                <div id='orders-list-main-container'>
                                    {this.state.ordersListComponents}
                                </div>
                            </div>
                        </div>
                }

                <Footer />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    utilityData: state.utility.utilityData,
    isAuthenticated: state.auth.isAuthenticated,
    socketData: state.auth.socketData,
    userData: state.auth.userData
});


export default connect(mapStateToProps, {})(OrderDetails);