//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';

//custom made component
import PageLoading from '../utility/PageLoading';
import Checkout from '../page/Checkout';
import Footer from '../utility/Footer';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

//images
import company_logo from '../../images/icons/logo9.png';

//icons
import { MdKeyboardBackspace } from 'react-icons/md'; //go back  
import { MdShoppingCart } from 'react-icons/md'; 
import { MdInfo } from 'react-icons/md'; 
import { MdStar } from 'react-icons/md';
import { MdClear } from 'react-icons/md';

//utility functions
import {capitalizeFirstLetter} from '../utility/utilityFunctions';

//stylings
import '../../stylings/pick-products.css';

//libraries
const _ = require('lodash');
const $ = require("jquery");

class PickProducts extends Component {
    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            vendorIdChosen : this.props.vendorIdChosen,
            vendorInfo : null,
            vendorMoreInfo : null,
            vendorInfoComponentsTop: null,
            productData : null,
            productListComponent: null,
            productCurrentlySelected : null,
            productCurrentlySelectedAddonsTotal: 0,
            cart: [],
            showPageLoading: true,
            showVerifyItemDataInModal: false,
            showCartItemDataInModal: false,
            showMoreInfoDataInModal: false,
            itemsInCartComponents : [],
            showCheckoutPage: false,
            moreInfoModalComponents: [],
            addonGroupsComponent: [],
            selectedAddonGroups: [],
            allMoreThanOneToSelectAddons: [],
            currentlySelectedProductAmount: 1,
            allAddonGroupsForCurrentlySelectedProduct: [],
            currentSelectedProductSpecialInstruction: '',
            //urrentSelectedProductAmount: 1,
            iWantToUpdateProductInCart: false
        }
    }
    
    //these are my redux proptypes from the global state
    static propTypes = {
        utilityData : PropTypes.object
    }

    handleGoBack = () => {
        //remove the vendor-id and refresh the window
        localStorage.removeItem('vendor-id');

        //refresh the page
        window.location.reload(false);
    }

    changeTheSelectionMadeOfAddon = (singleAddonGroup, addonSelected) => {
        var selectedAddonGroups = this.state.selectedAddonGroups;
        var allMoreThanOneToSelectAddons = this.state.allMoreThanOneToSelectAddons;
        var productCurrentlySelectedAddonsTotal = this.state.productCurrentlySelectedAddonsTotal;

        //Find the group to know if it already exists -- if it does not exist that means that this is the first addon from that group
        var indexAddonGroup = selectedAddonGroups.findIndex(group => group.group_id === singleAddonGroup._id);
        if(indexAddonGroup === -1) {
            selectedAddonGroups.push({
                group_id: singleAddonGroup._id,
                group_name: singleAddonGroup.group_name,
                how_many_must_be_chosen: singleAddonGroup.how_many_must_be_chosen,
                addons_selected: [
                    {
                        id: addonSelected._id,
                        name: addonSelected.name,
                        price: addonSelected.price
                    }
                ]
            })

            //add the price of the newly added addon
            productCurrentlySelectedAddonsTotal += addonSelected.price;
        } else {
            /*
                if i was found check to see if the how_many_must_be_chosen is 1 if its 1 replace the existing addon with the incoming addon
                                                                                its not 1 then check if the data coming in already exists, if it does then remove the
                                                                                addon that currently exists and if it does not exists then add it to the list
            */
            if(selectedAddonGroups[indexAddonGroup].how_many_must_be_chosen === 1) {
                //remove the price of the previously added addon
                productCurrentlySelectedAddonsTotal -= selectedAddonGroups[indexAddonGroup].addons_selected[0].price;

                selectedAddonGroups[indexAddonGroup].addons_selected = [{
                    id: addonSelected._id,
                    name: addonSelected.name,
                    price: addonSelected.price
                }]

                productCurrentlySelectedAddonsTotal += addonSelected.price;
            } else {
                //if the data coming does not exist then add it, else remove it
                var indexAddon = selectedAddonGroups[indexAddonGroup].addons_selected.findIndex(addon => addon.id === addonSelected._id);
                if(indexAddon === -1) {
                    selectedAddonGroups[indexAddonGroup].addons_selected.push({
                        id: addonSelected._id,
                        name: addonSelected.name,
                        price: addonSelected.price
                    });

                    productCurrentlySelectedAddonsTotal += addonSelected.price;
                } else {
                    _.remove(selectedAddonGroups[indexAddonGroup].addons_selected, function(addon) {
                        if(addon.id === addonSelected._id) {
                            productCurrentlySelectedAddonsTotal -= addon.price;
                        }

                        return addon.id === addonSelected._id;
                    });
                }

                //if we have selected the how_many_must_be_chosen then deactivate the rest checkboxes
                //in the list of allMoreThanOneToSelectAddons get the index of the addonGroup in iteration
                var allMoreThanOneToSelectAddonGroupIdx = allMoreThanOneToSelectAddons.findIndex(addonGroup => addonGroup.group_id === singleAddonGroup._id);
                if(selectedAddonGroups[indexAddonGroup].addons_selected.length === selectedAddonGroups[indexAddonGroup].how_many_must_be_chosen) {
                    allMoreThanOneToSelectAddons[allMoreThanOneToSelectAddonGroupIdx].addons.forEach(singleAddon => {
                        //check to see if the currently selected singleAddon has been selected
                        var indexAddon = selectedAddonGroups[indexAddonGroup].addons_selected.findIndex(addon => addon.id === singleAddon.id);
                        if(indexAddon === -1) {
                            document.getElementById(singleAddon.id+'-div').style.color = 'grey';
                            document.getElementById(singleAddon.id+'-checkbox').disabled = true;
                        }
                    });
                } else {
                    allMoreThanOneToSelectAddons[allMoreThanOneToSelectAddonGroupIdx].addons.forEach(singleAddon => {
                        document.getElementById(singleAddon.id+'-div').style.color = '#000000';
                        document.getElementById(singleAddon.id+'-checkbox').disabled = false;
                    });
                }
            }
        }

        //update the selectedAddonsGroups
        this.setState({selectedAddonGroups : selectedAddonGroups});
        this.setState({productCurrentlySelectedAddonsTotal: productCurrentlySelectedAddonsTotal});
        this.validateAddons(selectedAddonGroups);
    }

    /* 
      this function is used to build the addons list based on groups and its also used to specify if the addons being built have been checked or not, this is for scenarios where we are building the addons groups for the edit product from cart
    */
    buildAddonsGroupSelectionComponent = (prodData) => {
        var addonGroups = prodData.add_on_groups;
        var addonsGroupsSelected = prodData.addons_groups_selected;
        var addonsComponent = [];
        var addonGroupsComponentTemp = []; 
        var allMoreThanOneToSelectAddons = [];

        this.setState({allAddonGroupsForCurrentlySelectedProduct: addonGroups});

        if(addonsGroupsSelected) {
            this.setState({selectedAddonGroups : prodData.addons_groups_selected});
        }

        addonGroups.forEach(singleAddonGroup => {
            //clear the addons component to prepare for a new iteration
            addonsComponent = [];

            //find if this group being iterated has a corresponding occurence in the list of selectedGroups
            var selectedAddonsForCurrentGroup = undefined;
            if(addonsGroupsSelected) {
                selectedAddonsForCurrentGroup = addonsGroupsSelected.find(addonGroup => addonGroup.group_id === singleAddonGroup._id);
            }

            singleAddonGroup.addons.forEach(singleAddon => {
                //if the group has a corresponding occurence check if the addon being iterated does also and if it does make the item checked or selected
                var currentAddonFoundFromSelected = false;
                if(selectedAddonsForCurrentGroup) {
                    currentAddonFoundFromSelected = (selectedAddonsForCurrentGroup.addons_selected.findIndex(addon => addon.id === singleAddon._id) === -1) ? false : true;
                }

                if(singleAddonGroup.how_many_must_be_chosen === 1) {
                    addonsComponent.push(
                        <div className='radio-buttons-addons' key={singleAddon._id}>
                            <input type="radio" name={singleAddonGroup._id} onChange={() => this.changeTheSelectionMadeOfAddon(singleAddonGroup, singleAddon)} defaultChecked={currentAddonFoundFromSelected}/>{singleAddon.name}
                            {singleAddon.price !== 0 ? <span className='addons-price'>  + ${singleAddon.price}</span> : ''}
                        </div>
                    )
                } else {
                    //check to see if there is an entry for this group already if so add the new addon else create a new entry for the group before adding the new addon
                    var indexAddonGroup = allMoreThanOneToSelectAddons.findIndex(group => group.group_id === singleAddonGroup._id);
                    if(indexAddonGroup === -1) {
                        allMoreThanOneToSelectAddons.push({
                            group_id: singleAddonGroup._id,
                            addons: [{
                                id : singleAddon._id
                            }]
                        })
                    } else {
                        allMoreThanOneToSelectAddons[indexAddonGroup].addons.push({
                            id : singleAddon._id
                        })
                    }

                    //if we have a selection and check to see if the how_many_must_be_chosen is same as the amount that has been chosen
                    if(addonsGroupsSelected && selectedAddonsForCurrentGroup && (selectedAddonsForCurrentGroup.addons_selected.length === selectedAddonsForCurrentGroup.how_many_must_be_chosen)) {
                        var currentAddonHasBeenChosen = false;
                        if(selectedAddonsForCurrentGroup.addons_selected.findIndex(addon => addon.id === singleAddon._id) !== -1) {
                            currentAddonHasBeenChosen = true;
                        }

                        addonsComponent.push(
                            <div className='checkbox-addons' id={`${singleAddon._id}-div`} key={singleAddon._id} style={currentAddonHasBeenChosen ? {} : {color:'grey'}}>
                                <input type="checkbox" id={`${singleAddon._id}-checkbox`} name={singleAddonGroup._id} onChange={() => this.changeTheSelectionMadeOfAddon(singleAddonGroup, singleAddon)} defaultChecked={currentAddonFoundFromSelected} disabled={!currentAddonHasBeenChosen}/>
                                {singleAddon.name}
                                {singleAddon.price !== 0 ? 
                                <span className='addons-price'>  + ${singleAddon.price}</span> : ''}
                            </div>
                        )
                    } else {
                        addonsComponent.push(
                            <div className='checkbox-addons' id={`${singleAddon._id}-div`} key={singleAddon._id}>
                                <input type="checkbox" id={`${singleAddon._id}-checkbox`} name={singleAddonGroup._id} onChange={() => this.changeTheSelectionMadeOfAddon(singleAddonGroup, singleAddon)} defaultChecked={currentAddonFoundFromSelected}/>{singleAddon.name}
                                {singleAddon.price !== 0 ? <span className='addons-price'>  + ${singleAddon.price}</span> : ''}
                            </div>
                        )
                    }
                }
            });

            addonGroupsComponentTemp.push(
                <div key={singleAddonGroup._id} id={`${singleAddonGroup._id}-group-container`} className='single-addon-group-select-container'>
                    <div className='addon-group-title'>{capitalizeFirstLetter(singleAddonGroup.group_name)}</div>

                    {
                        singleAddonGroup.how_many_must_be_chosen === 1 ? 
                            <div className='addon-group-info' id={`${singleAddonGroup._id}-group-info`}>Please choose {singleAddonGroup.how_many_must_be_chosen} item</div>
                        :
                            <div className='addon-group-info' id={`${singleAddonGroup._id}-group-info`}>Please choose {singleAddonGroup.how_many_must_be_chosen} items</div>
                    }

                    {addonsComponent}
                </div>
            );
        });

        this.setState({addonGroupsComponent : addonGroupsComponentTemp});
        this.setState({allMoreThanOneToSelectAddons: allMoreThanOneToSelectAddons});
    }

    addThisItemToCartOpenCartVerification = (prodData) => {
        //build the addonGroupsData
        if(prodData.add_on_groups.length > 0) {
            this.buildAddonsGroupSelectionComponent(prodData);
        }

        //change the data
        this.setState({productCurrentlySelected: prodData});

        //set the value of the modal that should be shown to true
        this.setState({showVerifyItemDataInModal : true});

        //reset the productCurrentlySelectedAddonsTotal
        this.setState({productCurrentlySelectedAddonsTotal: 0})


        /*

        this.setState({currentSelectedProductAmount: prodData.amount_ordered});
        this.setState({currentSelectedProductSpecialInstruction: prodData.special_instructions});
        this.setState({productCurrentlySelectedAddonsTotal: prodData.addons_total});
        */

        //toggle modal
        if(!this.state.showModal) {
            this.toggleModal();
        }
    }
    // defaultValue='1' onChange={() => updateAmountSelected()}/>
    // :currentlySelectedProductAmount
    //   <input id='input-amount-of-items-i-want' type="number" min="1" 
    //   max={this.state.productCurrentlySelected.product_number_available} 
    //   defaultValue='1'/>

    updateAmountSelected = () => {
        this.setState({currentlySelectedProductAmount: document.getElementById('input-amount-of-items-i-want').value});
    }

    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
            this.setState({showCartItemDataInModal: false});
            this.setState({showVerifyItemDataInModal: false});
            this.setState({showMoreInfoDataInModal : false});
        }

        if(this.state.showVerifyItemDataInModal) {
            this.setState({selectedAddonGroups: []});
            this.setState({currentSelectedProductSpecialInstruction: ''});
            this.setState({iWantToUpdateProductInCart: false});
            this.setState({currentlySelectedProductAmount: 1});
            this.setState({productCurrentlySelectedAddonsTotal: 0});
            this.setState({allAddonGroupsForCurrentlySelectedProduct: []});

            /*
              this.setState({currentSelectedProductAmount: prodData.amount_ordered});
              this.setState({currentSelectedProductSpecialInstruction: prodData.special_instructions});
              this.setState({productCurrentlySelectedAddonsTotal: prodData.addons_total});
            */

            /*
              ${((this.state.productCurrentlySelectedAddonsTotal 
                + this.state.productCurrentlySelected.product_price) * 
                this.state.currentlySelectedProductAmount).toFixed(2)}
            */
        }

        //open the modal
        this.setState({showModal: !this.state.showModal});

        //
        if(this.state.showVerifyItemDataInModal) {
            document.getElementById('form-verify-user-item').reset(); 
        }
    }

    shakes = () => {
        for (var x = 1; x <= 3; x++) {
            $(document.getElementById('shopping-cart-icon')).css({
                "position" : "relative"
            });

            $(document.getElementById('shopping-cart-icon')).animate({
                    left : -25
                }, 10).animate({
                    left : 0
                }, 50).animate({
                    left : 25
                }, 10).animate({
                    left : 0
                }, 50
            );
        }
    }

    validateAddons = (selectedAddonsGroupsArg) => {
        var selectedAddonGroups = selectedAddonsGroupsArg || this.state.selectedAddonGroups;
        var allAddonGroupsForCurrentlySelectedProduct = this.state.allAddonGroupsForCurrentlySelectedProduct;
        var foundAnError = false;

        allAddonGroupsForCurrentlySelectedProduct.forEach(allAddonGroupsForSingleAddonGroup => {
            //add the new items amount and re add it to the cart
            var addonsForCurrentGroup = _.find(selectedAddonGroups, {'group_id' : allAddonGroupsForSingleAddonGroup._id});

            if(!addonsForCurrentGroup) {
                document.getElementById(`${allAddonGroupsForSingleAddonGroup._id}-group-container`).style.border = '1px solid #ff3333';
                document.getElementById(`${allAddonGroupsForSingleAddonGroup._id}-group-info`).style.color = '#ff3333';
                foundAnError = true;
            } else {
                if(addonsForCurrentGroup.addons_selected.length !== allAddonGroupsForSingleAddonGroup.how_many_must_be_chosen) {
                    document.getElementById(`${allAddonGroupsForSingleAddonGroup._id}-group-container`).style.border = '1px solid #ff3333';
                    document.getElementById(`${allAddonGroupsForSingleAddonGroup._id}-group-info`).style.color = '#ff3333';
                    foundAnError = true;
                } else {
                    document.getElementById(`${allAddonGroupsForSingleAddonGroup._id}-group-container`).style.border = 'none';
                    document.getElementById(`${allAddonGroupsForSingleAddonGroup._id}-group-info`).style.color = '#000000';
                }
            }
        });

        if(allAddonGroupsForCurrentlySelectedProduct.length === 0) {
            return true;
        }

        if(foundAnError)
            return false
        else 
            return true;
    }

    yesIWantThisItemHandler = (ev) => {
        ev.preventDefault();

        //verify that an ammount has been entered
        var amountIWant = parseInt(document.getElementById("input-amount-of-items-i-want").value);
        if(isNaN(amountIWant)) {
            document.getElementById("input-amount-of-items-i-want").style.border = '1px solid #ff4d4d';
        } else {
            document.getElementById("input-amount-of-items-i-want").style.border = '';

            if(this.validateAddons()) {      
                //add the item to the cart
                var tempCart = [...this.state.cart];
          
                //new orderItem
                var newOrder = Object.assign({}, 
                                            this.state.productCurrentlySelected, 
                                            {'amount_ordered': amountIWant, 
                                            'order_item_id_for_frontend': tempCart.length,
                                            'addons_total': this.state.productCurrentlySelectedAddonsTotal,
                                            'addons_groups_selected': this.state.selectedAddonGroups,
                                            'special_instructions': this.state.currentSelectedProductSpecialInstruction
                                            });
                
                tempCart.push(newOrder);
          
                //change the cart to the upated value
                this.setState({cart: tempCart});
          
                //close the modal
                this.toggleModal();
          
                //make the cart vibrate
                document.getElementById("shopping-cart-icon").style.color = '#45a470';
                document.getElementById('shopping-cart-icon').style.animation = 'shake 0.6s infinite';
          
                //remove the vibration
                setTimeout(function(){
                    document.getElementById('shopping-cart-icon').style.animation = 'none';
                    document.getElementById("shopping-cart-icon").style.color = '#F7C544';
                }, 600);
            }
        }
    }

    yesIWantToUpdateThisItemInCart = (ev) => {
        ev.preventDefault();

        //verify that an ammount has been entered
        var amountIWant = parseInt(document.getElementById("input-amount-of-items-i-want").value);
        if(isNaN(amountIWant)) {
            document.getElementById("input-amount-of-items-i-want").style.border = '1px solid #ff4d4d';
        } else {
            document.getElementById("input-amount-of-items-i-want").style.border = '';

            if(this.validateAddons()) {   
                //add the item to the cart
                var tempCart = [...this.state.cart];
                var productCurrentlySelected = this.state.productCurrentlySelected;
                //this.state.productCurrentlySelectedAddonsTotal + this.state.productCurrentlySelected.product_price) * this.state.currentlySelectedProductAmount;
                productCurrentlySelected.amount_ordered = amountIWant;
                productCurrentlySelected.addons_total = this.state.productCurrentlySelectedAddonsTotal;
                productCurrentlySelected.addons_groups_selected = this.state.selectedAddonGroups;
                productCurrentlySelected.special_instructions = this.state.currentSelectedProductSpecialInstruction;

                //find the current product in the cart, so we can replace it with productCurrentlySelected
                var cartItemIndex = _.findIndex(tempCart, {'order_item_id_for_frontend' : this.state.productCurrentlySelected.order_item_id_for_frontend});
                if(cartItemIndex !== -1) {
                    //replace it with what we currently have
                    tempCart.splice(cartItemIndex, 1, productCurrentlySelected);        
                    
                    //change the cart to the upated value
                    this.setState({cart: tempCart});
              
                    //close the modal
                    this.toggleModal();
              
                    //make the cart vibrate
                    document.getElementById("shopping-cart-icon").style.color = '#45a470';
                    document.getElementById('shopping-cart-icon').style.animation = 'shake 0.8s infinite';
              
                    //remove the vibration
                    setTimeout(function(){
                        document.getElementById('shopping-cart-icon').style.animation = 'none';
                        document.getElementById("shopping-cart-icon").style.color = '#F7C544';
                    }, 600);
                }
            }
        }
    }

    displayCartDataInModal = () => {
        //do this so that it is the cart data in the modal that shows
        this.setState({showCartItemDataInModal : true});

        //build the cart component
        this.buildTheItemsInTheCart();

        //open the modal
        this.toggleModal();
    }

    editThisProductDataFromCart = (prodData) => {
        this.setState({showCartItemDataInModal: false});

        this.addThisItemToCartOpenCartVerification(prodData);
        this.setState({currentlySelectedProductAmount: prodData.amount_ordered});
        this.setState({currentSelectedProductSpecialInstruction: prodData.special_instructions});
        this.setState({productCurrentlySelectedAddonsTotal: prodData.addons_total});
        this.setState({iWantToUpdateProductInCart: true});

        this.setState({showVerifyItemDataInModal: true});
    }

    buildTheItemsInTheCart = (cartItems) => {
        var count = 0;
        var itemsInCartComponentsTemp = [];
        var cartData = cartItems || this.state.cart;
        var total = 0.0;
        var tax = 0.0;
        var addonsComponents = [];

        if(cartData.length > 0) {
            cartData.forEach(singleItem => {
                addonsComponents = [];

                //add the total
                total += ((singleItem.product_price + singleItem.addons_total) * singleItem.amount_ordered);
                
                //build the addons if any
                singleItem.addons_groups_selected.forEach(singleAddonGroup => {
                    singleAddonGroup.addons_selected.forEach(singleAddonSelected => {
                        addonsComponents.push(
                            <div key={singleAddonSelected.id} className='single-product-addons-data-in-cart'>{singleAddonSelected.name} +${singleAddonSelected.price}</div>
                        );
                    })
                })

                //build the row of item data
                itemsInCartComponentsTemp.push(
                    <div key={count++} className='single-item-in-cart-container product-data-in-cart-row' onClick={() => this.editThisProductDataFromCart(singleItem)}>
                        <div className='item-name-in-modal'>
                            {singleItem.product_name}

                            <div className='product-addons-data-in-cart'>
                                {addonsComponents}
                            </div>

                            {
                                singleItem.special_instructions ? 
                                    <div className='special-instructions-in-cart'>
                                        "{singleItem.special_instructions}"
                                    </div>
                                :
                                    ''
                            }
                        </div>
                        <div className='item-amount-in-modal'>{singleItem.amount_ordered}</div>
                        <div className='item-price-in-modal'>${(singleItem.product_price + singleItem.addons_total).toFixed(2)}</div>
                        <div className='item-remove-in-modal-content' onClick={(ev) => this.removeThisItemFromTheCart(ev, singleItem)}>x</div>
                    </div>
                );
            });

            //add the total row and tax and everthing here
            tax = total * this.props.utilityData.tax; // this is for ontario and the app is only starting with ontario - London

            //and some spaces before and then the straight line
            itemsInCartComponentsTemp.push(
                <br key={count++}/>
            );
            itemsInCartComponentsTemp.push(
                <br key={count++}/>
            );
            itemsInCartComponentsTemp.push(
                <div key={count++} className='straight-line'/>
            );

            //add the total data and design to the modal
            itemsInCartComponentsTemp.push(
                <div key={count++} className='single-item-in-cart-container'>
                    <div className='lower-item-name'>Price</div>
                    <div className='lower-item-value'>${total.toFixed(2)}</div>
                </div>
            );

            //add the tax data and design to the modeal
            itemsInCartComponentsTemp.push(
                <div key={count++} className='single-item-in-cart-container'>
                    <div className='lower-item-name'>Tax</div>
                    <div className='lower-item-value'>${tax.toFixed(2)}</div>
                </div>
            );

            //add the summation data and design to the modal
            itemsInCartComponentsTemp.push(
                <div key={count++} className='single-item-in-cart-container'>
                    <div className='lower-item-name'>Total</div>
                    <div className='lower-item-value'>${(total+tax).toFixed(2)}</div>
                </div>
            );
        } else {
            itemsInCartComponentsTemp.push(
                <div key={'no-items-available-in-cart-text'} id='no-items-available-in-cart-text'>
                    No Items In Cart
                </div>
            );
        }

        this.setState({itemsInCartComponents: itemsInCartComponentsTemp});
    }


    removeThisItemFromTheCart = (e, orderItem) => {
        if (!e) e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();

        var currCart = [...this.state.cart];
        
        _.remove(currCart, function(orderItemDirectlyFromCart) {
            return orderItemDirectlyFromCart.order_item_id_for_frontend === orderItem.order_item_id_for_frontend;
        });

        //update our cart
        this.setState({cart : currCart});

        //rebuild the cart
        this.buildTheItemsInTheCart(currCart);
    }

    proceedToPayment = () => {
        if(this.state.cart.length === 0) {
            document.getElementById('no-items-available-in-cart-text').style.color = '#ff4d4d';
        } else {
            //open the checkout page
            this.setState({showCheckoutPage : true});

            //store the show checkout page value
            if (global.localStorage) {
                localStorage.setItem('show-checkout-page', true);
                //localStorage.setItem('cart-data-from-order-details', false);
            }
        }
    }

    openMoreInfoModal = () => {
        //show the more info data
        this.setState({showMoreInfoDataInModal : true});

        //open the modal
        this.toggleModal();
    }

    clearCart = () => {
        this.setState({cart : []});
        this.buildTheItemsInTheCart([]);
    }

    componentDidMount = () => {
        //we need to know if we have mounted to prevent memory leaks and then if mounted set the state of every object required  
        this._isMounted = true;

        //check if the checkout page is to be shown has been chosen
        var showCheckoutPage = localStorage.getItem('show-checkout-page');
        if(this._isMounted) {
            if(showCheckoutPage) {
                this.setState({showCheckoutPage: showCheckoutPage});
            } else {
                this.setState({showCheckoutPage: false});
            }
        }

        //get the vendor data
        axios.get(`${apiURL}/api/pick_products/get_product_vendor_info_and_more_info/${this.props.vendorIdChosen}`)
        .then(vendorData => {
            //build the more info modal components
            this.state.moreInfoModalComponents.push(
                <div id='show-more-info-alpha-container' key='show-more-info-alpha-container-key'>
                    <div id='top-background-image'>
                        <div id='logo-of-company'>
                        </div>
                    </div>

                    <div id='more-info-modal-title' className='bold'>
                        {vendorData.data.productVendor.product_vendor_name}
                    </div>

                    <div id='more-info-modal-location'>
                        <b>Location: </b>
                        {vendorData.data.productVendorMoreInfo.product_vendor_address}, {vendorData.data.productVendorMoreInfo.product_vendor_postal_code}
                    </div>

                    <div id='more-info-modal-description'>
                        <b>Description: </b>
                        {vendorData.data.productVendorMoreInfo.product_vendor_text_description}
                    </div>

                    <div id='more-info-modal-ratings'>
                        <b>Ratings: </b>
                        {vendorData.data.productVendor.product_vendor_ratings} <MdStar className='star-icon-more-info-modal' />
                    </div>
                </div>
            );
            

            if(this._isMounted) {
                this.setState({vendorInfo : vendorData.data.productVendor})
                this.setState({vendorMoreInfo : vendorData.data.productVendorMoreInfo})
            }

            //build the vendor info components
            var vendorInfoComponentsTopTemp = null;
            var vendorLogogUrl = require('../../images/vendor_images/' + vendorData.data.productVendor.product_vendor_icon_name+'.png');
            var backgroundImage = require('../../images/vendor_images/' + vendorData.data.productVendor.product_vendor_image_name+'.jpg');

            var styleUsersPickedBackground = {
                backgroundImage: `url(${backgroundImage})`
            };

            vendorInfoComponentsTopTemp = <div> 
                <div id='companies-logo-container' style={styleUsersPickedBackground}>
                    <img id='vendor-logo' src={vendorLogogUrl} alt={vendorData.data.productVendor.product_vendor_name}/>
                    <div className='title title-for-product-vendor'> 
                        {vendorData.data.productVendor.product_vendor_name} 
                        <MdInfo id='more-info-icon' onClick={() => this.openMoreInfoModal()}/> 

                        <div className='vendor-ratings'>{vendorData.data.productVendor.product_vendor_ratings} <MdStar className='star-icon' /></div>
                    </div>
                </div>
            </div>;

            if(this._isMounted) {
                this.setState({vendorInfoComponentsTop : vendorInfoComponentsTopTemp});
            }
        });

        //get the product data
        axios.get(`${apiURL}/api/pick_products/get_all_prod_for_this_vendor_for/${this.props.vendorIdChosen}`)
        .then(res => {
            var productData = res.data;

            //save the product data
            if(this._isMounted) {
                this.setState({productData: productData});
            }

            //build the product list
            var productListComponentTemp = [];
            var count = 0;
            productData.forEach(prodData => {
                productListComponentTemp.push(
                    <div key={count++} className='product-list-container-single'>
                        { 
                            prodData.uncountable_product_type ?
                                prodData.uncountable_product_type_out_of_stock ?
                                    <div className='out-of-stock-overlay'>Out Of Stock</div>
                                :
                                    ''
                            :
                                prodData.product_number_available === 0 ?
                                    <div className='out-of-stock-overlay'>Out Of Stock</div>
                                :
                                    ''
                        }
                        <div className='product-name'>{prodData.product_name}</div>
                        <div className='product-price'>${prodData.product_price}</div>
                        <div className='product-description'>{prodData.product_description}</div>
                        <button className='custom-button add-to-cart-button' onClick={() => this.addThisItemToCartOpenCartVerification(prodData)}>Add to Cart</button>
                    </div>
                );
            });//end of the for each

            if(this._isMounted) {
                this.setState({productListComponent : productListComponentTemp});
                this.setState({showPageLoading : false });
            }
        });
    }

    componentWillUnmount = () => {    
        this._isMounted = false;
    }

    updateSpecialInstructions = (ev) => {
        this.setState({currentSelectedProductSpecialInstruction: ev.target.value});
    }

    render() {
        return (
            <div>
                { !this.state.showCheckoutPage ? 
                    <div id='pick-product-alpha-container'>
                        <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                            <div id='modal-container'>
                                <MdClear id='close-modal' onClick={() => this.toggleModal()}/>
                                <br />
                                <br />
                                <br />
                                
                                {
                                    this.state.showVerifyItemDataInModal ? 
                                        <form id='form-verify-user-item'>
                                            <div id='modal-product-name'>{this.state.productCurrentlySelected.product_name || ''}</div>
                                            <div id='modal-product-name-price'>${this.state.productCurrentlySelected.product_price || ''}</div>
                                            <div id='modal-product-description'>{this.state.productCurrentlySelected.product_description || ''}</div>
                                            <div id='modal-product-amount'>
                                                Amount: 
                                                {
                                                    this.state.productCurrentlySelected.uncountable_product_type ? 
                                                        <input id='input-amount-of-items-i-want' type="number" min="1" 
                                                        defaultValue={this.state.currentlySelectedProductAmount} onChange={() => this.updateAmountSelected()}/>
                                                    :
                                                        <input id='input-amount-of-items-i-want' type="number" min="1" 
                                                        max={this.state.productCurrentlySelected.product_number_available} 
                                                        defaultValue={this.state.currentlySelectedProductAmount} onChange={() => this.updateAmountSelected()}/>
                                                }
                                            </div>

                                            <div id='modal-product-special-instruction'>
                                                Special Instructions: 
                                                <textarea type="text" id='special-instructions' placeholder='...' className='input-entries' defaultValue={this.state.currentSelectedProductSpecialInstruction} onChange={(e) => this.updateSpecialInstructions(e)} />
                                            </div>
                                            
                                            <div id='addons-group-container' style={this.state.productCurrentlySelected.add_on_groups.length > 0 ? {display:'block'} : {display:'none'}}>
                                                {this.state.addonGroupsComponent}
                                            </div>

                                            <div id='total-selected-product' className='center margin-bottom10 micro-font margin-top20'>
                                                <b>Total:</b>${((this.state.productCurrentlySelectedAddonsTotal + this.state.productCurrentlySelected.product_price) * this.state.currentlySelectedProductAmount).toFixed(2)}
                                            </div>

                                            <div id='modal-product-button-verify' className='center'>
                                                {
                                                    !this.state.iWantToUpdateProductInCart ? 
                                                        <button id='verify-order-button' className='custom-button' onClick={(ev) => this.yesIWantThisItemHandler(ev)}>Yes I want!</button>
                                                    :
                                                        <button id='verify-order-button' className='custom-button' onClick={(ev) => this.yesIWantToUpdateThisItemInCart(ev)}>Update</button>
                                                }
                                            </div>
                                        </form>
                                :
                                    this.state.showCartItemDataInModal ?
                                        <div id='items-in-cart-alpha-container'>
                                            <div className='item-name-in-modal bold'>Item name</div>
                                            <div className='item-amount-in-modal bold'>Amount</div>
                                            <div className='item-price-in-modal bold'>Price</div>
                                            <div className='item-remove-in-modal bold'>Remove</div>
                                            <div id='items-in-the-cart-container'>
                                                {this.state.itemsInCartComponents}
                                            </div>
                                            <div id='proceed-to-payment-button-container'>
                                                <button className='custom-button proceed-to-payment-button margin-right-10' onClick={() => this.proceedToPayment()}>
                                                    Proceed to Checkout
                                                </button>
                                                <button className='custom-button proceed-to-payment-button' onClick={() => this.clearCart()}>
                                                    Clear
                                                </button>
                                            </div>
                                        </div>
                                    :
                                        this.state.showMoreInfoDataInModal ?
                                            this.state.moreInfoModalComponents
                                        :
                                            ''
                                }
                            </div>
                        </div>

                        <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/>
                        
                        <div className='absolute-company-logo pick-products-company-logo'>
                            <img src={company_logo} className='image-take-full-space' alt='company_logo'/>
                        </div>

                        <div id='cart-goback-icons-container'>
                            <MdKeyboardBackspace className='cart-goback-icons icon-1' onClick={ () => this.handleGoBack()}/>  
                            <MdShoppingCart className='cart-goback-icons' id='shopping-cart-icon' onClick={()=> this.displayCartDataInModal()}/>
                        </div>

                        <div id='top-section-vendor-data-container'>
                            {this.state.vendorInfoComponentsTop}
                        </div>

                        <div id='product-list-section-container'>
                        <div id='pick-your-items-title' className='title'>Pick your Items</div>
                            {this.state.productListComponent}
                        </div>
                    </div>
                :
                    <Checkout cartItems={this.state.cart} fromOrderDetails={false}/>
                }

                <Footer />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    utilityData: state.utility.utilityData
});


export default connect(mapStateToProps, {})(PickProducts);