//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';
import Footer from '../utility/Footer';

//stylings
import '../../stylings/home.css';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

//images
import company_logo from '../../images/icons/logo9.png';
import name_icon from '../../images/icons/name-icon.png';
import ceo_icon from '../../images/icons/bro-moyo1.png';
import we_deliver_your_order_to_you_img from '../../images/our_images/we_deliver_orders_to_door.jpg';
//import group_of_service_chain_we_offer from '../../images/our_images/services_we_offer_in_future.jpg';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally} from '../utility/utilityFunctions';

//custom made component
import PickVendor from '../page/PickVendor';
import PageLoading from '../utility/PageLoading';

import { MdInfo } from "react-icons/md";

//libraries
const $ = require("jquery");

class HomePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            allServicesOffered : null,
            allServicesOfferedComponents: null,
            moreInfoComponents: null,
            showPickItems: false,  //false
            serviceCategoryId: '', //''
            showPageLoading: true,
            showSignUpPage : false
        };
    }



    //these are my redux proptypes from the global state
    static propTypes = {
        userData: PropTypes.object,
    }

    componentDidMount = () => {
        //check if a service category has been chosen
        var serviceCategoryId = localStorage.getItem('service-category-id');

        //show the loading icon
        this.setState({showPageLoading : true });

        if(serviceCategoryId) {
            this.setState({serviceCategoryId: serviceCategoryId});
            this.setState({showPickItems: true});
        } else {
            this.setState({serviceCategoryId: ''});
            this.setState({showPickItems: false});
        }
        

        //get all the service categoryfaxios
        axios.get(`${apiURL}/api/home/all_serv_cat`)
        .then(res => {
            addLoadingIconProcedurally('all-container');
            this.setState({allServicesOffered: res.data})
            
            //make all the service categories & the more info pane
            var allServicesOfferedComponentsTemp = [];
            var moreInfoServicesOfferedTemp = [];
            var count = 0;
            var imgUrl = '';

            res.data.forEach(value => {
                removeLoadingIconProcedurally();
                
                imgUrl = require('../../images/icons/' + value.service_category_image_name);
                
                allServicesOfferedComponentsTemp.push(
                    <div key={count++} className={'service'}>
                        <div className='top-section-service' onClick={ () => this.showPickItems(value._id)}>
                            <div className='top-section-image-container'>
                                <img className='top-section-image' src={imgUrl} alt={value.service_category_image_name}/>
                            </div>
                        </div>
                        <div className='more-info-service-button' onClick={ () => this.scrollToSection(value.service_category_name.toLowerCase().replace(/ /g, '_')) }>{value.service_category_name} <MdInfo className='more-info-icon-in-lower-section'/></div>
                    </div>
                );
                
                //make all more info components
                let stylesLeftServiceMoreInfo, stylesRightServiceMoreInfo, stylesRightMoreInfoText, stylesLeftMoreInfoText; 

                stylesLeftServiceMoreInfo = {
                    borderRight: `6px solid ${value.service_category_nice_color}`
                }
                stylesRightServiceMoreInfo = {
                    borderLeft: `6px solid ${value.service_category_nice_color}`
                }
                stylesRightMoreInfoText = {
                    float : 'right'
                }
                stylesLeftMoreInfoText = {
                    float : 'left'
                }
                
                //build the icon images for the more-info-icon
                imgUrl = require('../../images/icons/' + value.service_category_image_icon);
                moreInfoServicesOfferedTemp.push(
                    <div key={count} className={'service-more-info'} style={count % 2 === 0 ? stylesLeftServiceMoreInfo : stylesRightServiceMoreInfo} id={value.service_category_name.toLowerCase().replace(/ /g, '_')}>
                        <div className='more-info-container' style={count % 2 === 0 ? stylesLeftMoreInfoText : stylesRightMoreInfoText}>
                            <div className='more-info-icon'>
                                <img className='more-info-icon-image' src={imgUrl} alt={value.service_category_image_icon}/>
                            </div>
                            <div className='more-info-title'>{`${value.service_category_name} delivery`}</div>
                            <div className='more-info-text'>
                                {value.service_category_more_info}
                            </div>
                            <button className='custom-button order-now-more-info-service-button' onClick={ () => this.showPickItems(value._id)}>Order Now</button>
                        </div>
                    </div>
                );
            });

            this.setState({allServicesOfferedComponents: allServicesOfferedComponentsTemp});
            this.setState({moreInfoComponents: moreInfoServicesOfferedTemp});
        })
        .catch(err => {});//end of request


        //remove the loading icon
        this.setState({showPageLoading : false });
    }

    //this scrolls the user to the div they want
    scrollToSection = (anchor_id) => {
        var tag = $("#" + anchor_id + "");
        $('html,body').animate({ scrollTop: tag.offset().top }, 'slow');
    }

    //this shows the show pick items
    showPickItems = (serviceCategoryIdVal) => {
        this.setState({serviceCategoryId: serviceCategoryIdVal});

        //store the selection in local-storage
        if (global.localStorage) {
            localStorage.setItem('service-category-id', serviceCategoryIdVal);
        }

        this.setState({showPickItems: true});
    }

    showSignUp = () => {
        this.setState({showSignUpPage : true});
    }

    //make text lowecase and replace space with underscore
    render() {
        return (
            <div id='all-container'>
                <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/>

                {
                    !this.state.showPickItems ? 
                        <div>
                            {
                                !this.props.userData ?
                                    <div>
                                        <button className='custom-button' id='sign-in-button'><a href='\SignIn'>SIGN IN</a></button>
                                        <button className='custom-button' id='sign-up-button'><a href='\SignUp'>SIGN UP</a></button>
                                    </div>
                                :
                                    ''
                            }

                            <div id='head-container'>
                                <div className='company-logo'>
                                    <img src={company_logo} className='image-take-full-space' alt='company_logo'/>
                                </div>
                          
                                <div className='name-icon'>
                                    <img src={name_icon} className='image-take-full-space' alt='company_logo'/>
                                </div>

                                {/* <div id='special-note'>
                                    We got you all your deliveries covered
                                </div> */}
                                <div id='services-we-offer'>
                                    {this.state.allServicesOfferedComponents}
                                </div>

                                {/* <div id='address-lookup'>
                                    <GoogleSearchLocator />
                                </div> */}
                            </div>

                            <div className='snippet-info'>
                                <div className='snippet-info-left'>
                                    <div className='snippet-left-or-right-image-container'>
                                        <img className='left-or-right-image' src={we_deliver_your_order_to_you_img} alt={'we_deliver_your_order_to_you_img'}/>
                                    </div>
                                </div>
                                <div className='snippet-info-right'>
                                    <div className='snippet-info-left-container'>
                                        <div className='snippet-info-left-massive-text'>GET YOUR ORDERS DELIVERED TO YOUR DOOR!</div>
                                    </div>
                                </div>
                            </div>
                          
                            <div id='servies-we-offer-more-info'>
                                {this.state.moreInfoComponents}
                            </div>
                            
                            <div id='about-us-container'>
                                <div id='title-of-about-us' className='title color-black'>Who are we?</div>
                                <div id='description-on-who-we-are'>
                                    The smell of fresh baked goods can make shoppers hungry and get them to buy more. Retail specialists place the meat and dairy sections at the back of the store to force shoppers to travel through fully stocked aisles to get there. Like humans, dogs, cats, and other animals need specific nutrients to stay healthy. Being aware of your pet's individual needs are can help you make better decisions about what to feed them. Knowing how to identify exaggerated marketing claims can make selecting nutritious and affordable food easier Like humans, dogs, cats, and other animals need specific nutrients to stay healthy. Being aware of your pet's individual needs are can help you make better decisions about what to feed them. Knowing how to identify exaggerated marketing claims can make selecting nutritious and affordable food easier
                                    The smell of fresh baked goods can make shoppers hungry and get them to buy more. Retail specialists place the meat and dairy sections at the back of the store to force shoppers to travel through fully stocked aisles to get there. Like humans, dogs, cats, and other animals need specific nutrients to stay healthy. Being aware of your pet's individual needs are can help you make better decisions about what to feed them. Knowing how to identify exaggerated marketing claims can make selecting nutritious and affordable food easier Like humans, dogs, cats, and other animals need specific nutrients to stay healthy. Being aware of your pet's individual needs are can help you make better decisions about what to feed them. Knowing how to identify exaggerated marketing claims can make selecting nutritious and affordable food easier
                                </div>
                                <div id='ceo-signature'>
                                    <div id='ceo-icon-logo'>
                                        <img className='image-take-full-space' src={ceo_icon} alt='ceo-cartoon'/>
                                    </div>
                                    <div id='ceo-name'>Moyo Akomolafe</div>
                                </div>
                            </div>
                            
                            <Footer />
                        </div>
                    :
                        <PickVendor serviceCategoryIdChosen={this.state.serviceCategoryId === '' ? '5d859e138bee5a28bc89f052' : this.state.serviceCategoryId}/>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userData: state.auth.userData
});


export default connect(mapStateToProps, { })(HomePage);