//system defined components
import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { HashLink as Link } from 'react-router-hash-link';
import axios from 'axios';
import {apiURL} from '../../config.js';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {logout, storeSocket} from '../../redux/actions/authActions';
import {storeUtilityData} from '../../redux/actions/utilityActions';

//custom made component
import HomePage from './Home';
import SignUp from './SignUp';
import OrderDetails from './OrderDetails';
import Vendors from './Vendors';
import Drivers from './Drivers';
import PageNotFound from '../utility/PageNotFound';
import SignIn from './SignIn';
import Profile from './Profile';
import AdminHome from './Admin/AdminHome';

//socket
import io from 'socket.io-client';
import {websocketUrl} from '../../config.js';

//stylings
import '../../stylings/default-for-all.css';

//icons
import { MdClear, MdDehaze } from 'react-icons/md';
import { MdKeyboardArrowUp } from 'react-icons/md';
import Home from './Home';

//libraries
const $ = require("jquery");

class Navigator extends Component {
    constructor(props) {
        super(props);

        this.state = {
            openMenu : false,
            showGoBackUp : false,
            userType: localStorage.getItem('user-type'),
            iHaveGottenUserData: false
        }
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        isAuthenticated: PropTypes.bool,
        logout: PropTypes.func.isRequired,
        userData: PropTypes.object,
        socketData: PropTypes.object,
        storeSocket: PropTypes.func.isRequired,
        storeUtilityData : PropTypes.func.isRequired
    }

    componentDidMount = () => {
        window.addEventListener('scroll', this.handleScroll);
        //window.addEventListener('beforeunload', this.handleCloseTab);

        /*
        Get the utilities data for the application payment location specifics  -- this would get the regular one
        */
        if(this.props.userData && !this.state.iHaveGottenUserData) {
            axios.get(`${apiURL}/api/home/get_utility/London/Ontario/${this.props.userData._id}`)
            .then(res => {
                this.props.storeUtilityData(res.data);
            });

            this.setState({iHaveGottenUserData: true});
        }
        else {
            axios.get(`${apiURL}/api/home/get_utility/London/Ontario/undefined`)
            .then(res => {
                this.props.storeUtilityData(res.data);
            });
        }
    }

    componentDidUpdate = () => {
        if(this.props.userData && !this.props.socketData) {
            //only do this for clients -> vendors and drivers will connect to the socket when they specify that they are available
            if(this.props.userData.can_access_order_details_page) {
                //connect to the socket
                const socket = io.connect(websocketUrl, {'forceNew': true});     
                //load the users data to the socket
                socket.emit('createUserObject', this.props.userData);

                //store the socket data
                this.props.storeSocket(socket);
            }
        }

        /*
        Get the utilities data for the application payment location specifics  -- this would get the one specific to the user
        */
        if(this.props.userData && !this.state.iHaveGottenUserData) {
            axios.get(`${apiURL}/api/home/get_utility/London/Ontario/${this.props.userData._id}`)
            .then(res => {
                this.props.storeUtilityData(res.data);
            });

            this.setState({iHaveGottenUserData: true});
        }
    }

    componentWillUnmount = () => {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = () => {

        var scroll = $(window).scrollTop();
        if(scroll > 600) 
            this.setState({showGoBackUp: true});
        else
          this.setState({showGoBackUp: false});
    }

    toggleMenuBar = (ev) => {
        this.setState({openMenu: !this.state.openMenu});

        ev.cancelBubble = true;
        if (ev.stopPropagation) ev.stopPropagation();
    }

    leaveModalOpen = (ev) => {
        //if (!ev) var ev = window.event;
        ev.cancelBubble = true;
        if (ev.stopPropagation) ev.stopPropagation();
    }

    closeMenuBar = (e) => {
        if(this.state.openMenu) {
            this.setState({openMenu: !this.state.openMenu});
        }
    }

    closeMenuBarAndClearCart = (e) => {
        if(this.state.openMenu) {
            this.setState({openMenu: !this.state.openMenu});
        }

        localStorage.removeItem('cart-data');
    }

    //this scrolls the user to the div they want
    scrollToSection = (anchor_id) => {
        var tag = $("#" + anchor_id + "");
        $('html,body').animate({ scrollTop: tag.offset().top }, 'slow');
    }

    performPresequisitesForHomePage = () => {
        //close the modal
        this.setState({openMenu: !this.state.openMenu});

        //notify the user that their order data would be lost and then remove the id of the service category they choose so that the home page can load
        localStorage.removeItem('service-category-id');
        localStorage.removeItem('vendor-id');    
        localStorage.removeItem('show-checkout-page');
        localStorage.removeItem('cart-data');

        //refresh the page
        setTimeout(
            function()  {
                window.location.reload(false);
            },
            500
        );
    }

    //this function runs when the user wants to logout
    logout = () => {
        this.props.logout();
        this.closeMenuBar();
    }

    render() {
        return (
            <div onClick={(ev) => this.closeMenuBar(ev)}>
                <Router>
                    <div id='regular-navigation'>
                        <div id='accordian' onClick={(ev) => this.toggleMenuBar(ev)} style={this.state.openMenu ? {display:'none'} : {display:'block'}}><MdDehaze /></div>
                        <div id='menu' style={this.state.openMenu ? {display:'block'} : {display:'none'}} onClick={(ev) => this.leaveModalOpen(ev)}>
                            <MdClear id='closeIcon' onClick={(ev) => this.toggleMenuBar(ev)} />

                            <div id='items-in-menu'>
                                <ul>
                                    {/* handle what not logged in and then logged in users see can link to */}
                                    {/* {
                                        !this.props.userData ?
                                            <li>
                                                <Link to="/" className="link" onClick={this.performPresequisitesForHomePage}>Home</Link>
                                            </li>
                                        :
                                            this.props.userData && this.props.userData.can_access_order_details_page ?
                                                <li>
                                                    <Link to="/" className="link" onClick={this.performPresequisitesForHomePage}>Home</Link>
                                                </li>
                                            :
                                                ''
                                    } */}

                                    {
                                        this.props.userData && this.props.userData.can_access_order_details_page ?
                                            <li>
                                                <Link to="/" className="link" onClick={this.performPresequisitesForHomePage}>Home</Link>
                                            </li>
                                        :
                                            ''
                                    }

                                    {/* handle what logged users can link to */}
                                    {
                                        this.props.userData ?
                                            <div>
                                                {
                                                    this.props.userData.can_access_order_details_page ?
                                                        <div>
                                                            <li>
                                                                <Link to="/orderDetails" className="link" onClick={this.closeMenuBarAndClearCart}>Order Details</Link>
                                                            </li>
                                                            <li>
                                                                <Link to="/#about-us-container" className="link" onClick={this.performPresequisitesForHomePage}>About Us</Link>
                                                            </li>
                                                        </div>
                                                    :
                                                        ''
                                                }


                                                {/* handle what vendors can link to */}
                                                {
                                                    this.props.userData.can_access_vendors_page ?
                                                        <li>
                                                            <Link to="/vendors" className="link" onClick={this.closeMenuBar}>Vendors Home Page</Link>
                                                        </li>
                                                    :
                                                        ''
                                                }

                                                {/* handle what drivers can link to */}
                                                {
                                                    this.props.userData.can_access_drivers_page ?
                                                        <li>
                                                            <Link to="/drivers" className="link" onClick={this.closeMenuBar}>Drivers Home Page</Link>
                                                        </li>
                                                    :
                                                        ''
                                                }
                                            </div>
                                        :
                                            ''
                                    }

                                    {
                                        //you can only see sign in/sign up if you are not logged in
                                        !this.props.userData ?
                                            <li>
                                                <Link to="/signUp" className="link" onClick={this.closeMenuBar}>Sign Up</Link> / <Link to="/signIn" className="link" onClick={this.closeMenuBar}>Sign In</Link>
                                            </li>
                                        :
                                            ''
                                    }

                                    {
                                        //you cant see the logout link if you are not logged in
                                        this.props.userData ?
                                            <div>
                                                <li>
                                                    <Link to="/profile" className="link" onClick={this.closeMenuBar}>Profile</Link>
                                                </li>
                                                <li>
                                                    <Link to="/signIn" className="link" onClick={this.logout}>Logout</Link>
                                                </li>
                                            </div>
                                        :
                                            ''
                                    }
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div id='top-for-scroll'/>
                  
                    <Switch>
                        {/* handle what not logged in and the logged in users see */}
                        {/* {
                            !this.props.userData ?
                                <Route path="/" exact component={HomePage} />
                            :
                                this.props.userData && this.props.userData.can_access_order_details_page ?
                                    <Route path="/" exact component={HomePage} />
                                :
                                    ''
                        } */}
                        {
                            !this.props.userData ?
                                <Route path="/" exact component={SignIn} />
                            :
                                this.props.userData && this.props.userData.can_access_order_details_page ?
                                    <Route path="/" exact component={HomePage} />
                                :
                                    ''
                        }

                        {/* handle what logged in users|vendors|drivers can link to */}
                        {
                            this.props.userData ?
                                this.props.userData.can_access_order_details_page ?
                                    <Route path="/orderDetails" component={OrderDetails} />
                                :
                                    this.props.userData.can_access_vendors_page ?
                                        <Route path="/vendors" component={Vendors} />
                                    :
                                        this.props.userData.can_access_drivers_page ?
                                            <Route path="/drivers" component={Drivers} />
                                        :
                                            ''
                            :
                                ''
                        }

                        {/* {
                            //you can only link to sign in/sign up if you are not logged in
                            !this.props.userData ?
                                <Route path="/signUp" component={SignUp} />
                            :
                                ''
                        } */}

                        {
                            //you can access the profile if you are logged in
                            this.props.userData ?
                                <Route path="/profile" component={Profile} />
                            :
                                ''
                        }

                        <Route path="/signIn" component={SignIn} />
                        <Route path="*" component={PageNotFound} />
                    </Switch>
                </Router>

                <div id='scroll-back-up' style={this.state.showGoBackUp ? {display:'block'} : {display:'none'}} onClick={() => this.scrollToSection('top-for-scroll')}><MdKeyboardArrowUp /></div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    socketData: state.auth.socketData,
    userData: state.auth.userData
});


export default connect(mapStateToProps, { logout, storeSocket, storeUtilityData })(Navigator);

