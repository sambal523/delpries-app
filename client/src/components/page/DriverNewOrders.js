//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';

//redux imports
import { connect } from 'react-redux';
import PropTypes, { array } from 'prop-types';
import {storeNewOrderRequest, removeNewOrderRequest, addOneOngoingOrder} from '../../redux/actions/driverActions';

//icons
import { MdTimelapse } from 'react-icons/md';  

//stylings
import '../../stylings/driver-new-orders.css';

//define loadash
const _ = require('lodash');

class DriverNewOrders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            iHaveGottenNewOrderData: false,
            allOrderRequestData: [],
            driversOrderRequestComponent: []
        }
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        isAuthenticated: PropTypes.bool,
        socketData: PropTypes.object,
        allNewOrderRequestData: PropTypes.array,
        newOrderRemoved: PropTypes.bool,
        removeNewOrderRequest: PropTypes.func.isRequired,
        addOneOngoingOrder: PropTypes.func.isRequired
    }

    componentWillUnmount() {
        const { childRef } = this.props;
        childRef(undefined);
    }

    componentDidMount = () => {  
        const { childRef } = this.props;
        childRef(this);

        if(!this.state.iHaveGottenNewOrderData && this.props.allNewOrderRequestData.length !== 0) {
            this.setState({iHaveGottenNewOrderData: true});
            
            this.buildOrderRequest();
        }
    }

    componentDidUpdate = () => {
        if(!this.state.iHaveGottenNewOrderData && this.props.allNewOrderRequestData.length !== 0) {
            this.setState({iHaveGottenNewOrderData: true});

            //show the request
            //this.buildOrderRequest(this.props.allNewOrderRequestData);
            this.buildOrderRequest();
        }
    }

    changeHasHappenedInNewOrderRequestData = () => {
        this.buildOrderRequest();
    }

    buildOrderRequest = () => {
        var allOrderRequestData = this.props.allNewOrderRequestData;
        var count = 0;
        var timeStamp = Date.now();
        var incomingDriversOrderRequestComponent = [];

        allOrderRequestData.forEach(orderRequestData => {
            incomingDriversOrderRequestComponent.push(<div key={timeStamp++} className='single-order-request'> 
                <div className='timer-container'>
                    <MdTimelapse /> 20s
                </div>

                <div className='order-request-top-section'>
                    <div className=''><b>Store Name</b>:{orderRequestData.vendorName}</div>
                    <div className=''><b>Store Address</b>:{orderRequestData.vendorLocation}</div>
                </div>

                <div className='order-request-mid-section'>
                    {/* <div className=''><b>Client's Name</b>:{orderRequestData.vendorName}</div> */}
                    <div className=''><b>Client's Address</b>:{orderRequestData.clientLocation}</div>
                </div>

                <div className='order-request-lower-section'>     
                    <button className='yes-i-would-take-it-button custom-button' key={count++} onClick={() => this.handleYesIWouldTakeIt(orderRequestData.orderId, orderRequestData.driverId)}>I will take It!</button>
                    <button className='cant-take-it-button custom-button' key={count++} onClick={() => this.handleCantTakeIt(orderRequestData.orderId, orderRequestData.clientId, orderRequestData.clientLocation, orderRequestData.vendorId, orderRequestData.vendorName, orderRequestData.vendorLocation, orderRequestData.driverId)}>Cant take It!</button>
                </div>
            </div>);
        });

        //var currentDriversOrderRequestComponent = this.state.driversOrderRequestComponent;
        //currentDriversOrderRequestComponent.push(incomingDriversOrderRequestComponent);

        this.setState({driversOrderRequestComponent : incomingDriversOrderRequestComponent});
    }

    handleYesIWouldTakeIt = (orderId, driverId) => {
        //send response to backEnd -> in the backend change the isOnAnOrderStatus
        //headers
        const token = localStorage.getItem('token');
        const config = {
            headers: {
                'x-auth-token': token
            }
        }
          
        axios.get(`${apiURL}/api/drivers/driver_will_take_order/${orderId}/${driverId}`, config)
        .then(res => {
            if(res.data.SuccessInUpdate) {
                //send response to socket -> in the backend change the pending status and then isOnAnOrderStatus
                if(this.props.isAuthenticated) {
                    var orderData = {
                        orderId, driverId
                    };   
                
                    //listeners to mount
                    this.props.socketData.emit('driverCanTakeOrder', Object.assign({}, orderData, {orderRequestAmount: this.props.allNewOrderRequestData.length}));

                    //remove the order from redux state and then rebuild
                    this.props.removeNewOrderRequest(orderId);

                    //add on ongoing order
                    this.props.addOneOngoingOrder();  
                    
                    this.buildOrderRequest();
                } 
            }
        })
        .catch(err => {
        });
    }

    handleCantTakeIt = (orderId, clientId, clientLocation, vendorId, vendorName, vendorLocation, driverId) => {
        //remove the order from redux state and then rebuild
        this.props.removeNewOrderRequest(orderId);
        this.buildOrderRequest();

        //send response saying the driver cannot take this order
        if(this.props.isAuthenticated) {
            var orderData = {
                orderId, clientId,  clientLocation, vendorId, vendorName, vendorLocation, driverId, orderRequestAmount: this.props.allNewOrderRequestData.length
            };   
        
            //listeners to mount
            this.props.socketData.emit('driverCannotTakeOrder', orderData);
        } 
    }

    driverDidntRespond = (orderId, clientId, clientLocation, vendorId, vendorName, vendorLocation, driverId) => {
        this.props.removeNewOrderRequest(orderId);
    }

    //recieve the order request and if accept send a response, if not accepted and 20 seconds pass remove the order and send i dont want to go response so it can be sent to another driver
    render() {
        //this.callToChange();
        return (
            <div id='order-request-container'>
                {
                    this.props.allNewOrderRequestData.length === 0 ?
                        <div id='no-new-order-available'>
                          No New Order Available
                        </div>
                    :
                        <div id='new-order-request-container'>
                          {this.state.driversOrderRequestComponent}
                        </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    socketData: state.auth.socketData,
    allNewOrderRequestData: state.driver.allNewOrderRequestData,
    newOrderRemoved: state.driver.newOrderRemoved
});

export default connect(mapStateToProps, {removeNewOrderRequest, addOneOngoingOrder})(DriverNewOrders);
  