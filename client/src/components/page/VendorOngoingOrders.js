//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';

//redux imports
import { connect } from 'react-redux';
import PropTypes, { array } from 'prop-types';
import {storeAmountOfOngoingOrder} from '../../redux/actions/vendorActions';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally} from '../utility/utilityFunctions';

//custom made component
import PageLoading from '../utility/PageLoading';

//icons
import { MdClear } from 'react-icons/md';

//stylings
import '../../stylings/vendor-ongoing-orders.css';

//definitions
const _ = require('lodash');


class VendorOngoingOrders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            goodColor: '#258e25',
            badColor: '#ff3333',
            defaultColor: '#a7a7a7',
            showPageLoading: false,
            showModal: false, 
            responseMsg: '',
            responseMsgStatus: false,

            showResponseMessageInModal: false,

            allOngoingOrders : [],
            allOngoingOrdersComponents: [],
            iHaveGottenUserData: false,
            vendorId : this.props.userData.vendor_id_for_vendor
        }
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        userData: PropTypes.object,
        storeAmountOfOngoingOrder: PropTypes.func.isRequired,
        userSpecifics: PropTypes.object,
        socketData: PropTypes.object
    }

    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
            this.setState({showResponseMessageInModal: false});
        }

        //open the modal
        this.setState({showModal: !this.state.showModal});
    }

    getAndBuildOngoingOrderData = () => {
        this.setState({showPageLoading : true });
        this.setState({iHaveGottenUserData: true});

        //get the amount of ongoing orders
        const config = {
            headers: {
                'x-auth-token': localStorage.getItem('token')
            }
        }

        //GET THE ongoing orders: orders that have been approved and have not been given to the driver
        axios.get(`${apiURL}/api/vendors/get_ongoing_orders/${this.props.userData.vendor_id_for_vendor}`, config)
        .then(res => {
            if(res.data) {
                if(res.data.status) {
                    this.setState({allOngoingOrders: res.data.orders});
                    this.buildOngoingOrders(res.data.orders);

                    //store the amount data
                    this.props.storeAmountOfOngoingOrder(res.data.orders.length);
                } else {
                    //display the error message
                    this.setState({responseMsg : res.data.msg});
                    this.setState({responseMsgStatus : res.data.orderCreated});
                    this.setState({showResponseMessageInModal : true});

                    this.toggleModal();

                    this.setState({showPageLoading : false });
                }
            }
        })
        .catch(err => {
        });


        this.setState({showPageLoading : false });
    }

    boilerPlateForDidMountAndUpdate = () => {
        if(!this.state.iHaveGottenUserData && this.props.userData) {
            this.getAndBuildOngoingOrderData();
        }
    }

    componentWillUnmount() {
        const { childRef } = this.props;
        childRef(undefined);
    }

    componentDidMount = () => {
        const { childRef } = this.props;
        childRef(this);

        this.boilerPlateForDidMountAndUpdate();
    }

    componentDidUpdate = () => {
        this.boilerPlateForDidMountAndUpdate();
    }


    buildOngoingOrders = (orders) => {                                                        
        addLoadingIconProcedurally('alpha-container-ongoing-orders');

        if(orders.length > 0) { 
            var count = 0;
            var allOngoingOrdersComponentsTemp = [];
            var cartItems = [];
            var singleOrderTemp = [];
            var addonsComponents = [];

            orders.forEach(singleOrder => {
                cartItems = JSON.parse(singleOrder.cartItems);
                
                singleOrderTemp.push(
                    <div key={count++}>
                        <div id='order-id' className={'margin-bottom10'}>
                            <b>#{singleOrder.order_num_for_view}</b>
                        </div>
                        <div className='vendor-product-vendor-name-single-order'>{singleOrder.product_vendor_name}</div>
                        <div className='vendor-client-name-single-order'>Client Name: {singleOrder.user_name} </div>
                        <div className='vendor-ongoing-order-info-single-order'>{singleOrder.order_name}</div>
                        <div className='vendor-price-single-order'>Total Income: ${singleOrder.vendors_total_money}</div>
                        <div className='vendor-date-single-order'> Date: {new Date(singleOrder.order_date).toDateString()}</div>
                    </div>
                );


                //push the cart items if any
                singleOrderTemp.push(
                    <div className='margin-top20 center' key={count++}>
                        <b>Cart Items</b>
                    </div>
                )

                if(cartItems.length > 0) {
                    var cartItemComponents = [];
                    //center cart-items-list-single
                    cartItemComponents.push(
                        <div key={count++}>
                            <div className='ongoing-order-header-item-name bold left'>Item name</div>
                            <div className='ongoing-order-header-item-amount bold'>Amount</div>
                        </div>
                    )

                    cartItemComponents.push(
                        <div key={count++} className='straight-line'/>
                    )
                    
                    cartItems.forEach(singleItem => {
                        if(singleItem.addons_description && singleItem.addons_description.length > 0) {
                            var addons = singleItem.addons_description.split(',');
                            for(var i=0; i<addons.length; i++) {
                                if(addons[i] != '') {
                                    addonsComponents.push(
                                        <div key={singleItem._id+i} className='single-product-addons-data-in-cart left'>{addons[i]}</div>
                                    );
                                }
                            }
                        }

                        cartItemComponents.push(
                            <div key={count++} className='margin-bottom10 overflow-auto'>
                                <div className='item-name-in-order-details-data left'>
                                    {singleItem.product_name}

                                    <div className='product-addons-data-in-cart'>
                                        {addonsComponents}
                                    </div>

                                    {
                                        singleItem.special_instructions ? 
                                            <div className='special-instructions-in-cart'>
                                              "{singleItem.special_instructions}"
                                            </div>
                                        :
                                            ''
                                    }
                                </div>
                                {/* <div className='ongoing-order-header-item-name'>{singleItem.product_name}</div> */}
                                <div className='ongoing-order-header-item-amount'>{singleItem.amount_ordered}</div>
                            </div>
                        );

                        addonsComponents = [];
                    });

                    singleOrderTemp.push(
                        <div key={count++} className='center cart-items-list-single'>
                            {cartItemComponents}
                        </div>
                    )

                    cartItemComponents = [];
                } else {
                    singleOrderTemp.push(
                        <div className='center cart-items-list-single' key={count++}>
                            No Cart Items 
                        </div>
                    )
                }

                singleOrderTemp.push(
                    !singleOrder.order_needs_driver ?
                        <div key={count++}className='center'><button id='order-is-ready-btn' className='custom-button' onClick={() => this.OrderIsReady(singleOrder._id, singleOrder.user_id, singleOrder.address)}>Order Is Ready</button></div>
                    :
                        ''
                );

                //push the buttons
                /*
                singleOrderTemp.push(
                  !singleOrder.order_is_ready_from_vendor ?
                    <div key={count++}className='center'><button id='order-is-ready-btn' className='custom-button' onClick={() => this.OrderIsReady(singleOrder._id)}>Order Is Ready</button></div>
                  :
                    !singleOrder.vendor_has_given_driver_order ?
                      <div  key={count++} className='center'><button id='given-driver-order-btn' className='custom-button' onClick={() => this.GivenToDriver(singleOrder._id)}>Given To Driver</button></div>
                    :
                      ''
                );
                */

                //push the single order into the main orders list
                allOngoingOrdersComponentsTemp.push(
                    <div key={count++} className='vendor-single-ongoing-order-item'>
                      {singleOrderTemp}
                    </div>
                );

                //clear the tray
                singleOrderTemp = [];
            });

            this.setState({allOngoingOrdersComponents : allOngoingOrdersComponentsTemp});
            removeLoadingIconProcedurally();
        } else {
            this.setState({allOngoingOrdersComponents : <div key={'no-order-in-list'} id='no-items-available-in-list-vendor-order'>No Ongoing Order</div>});
            removeLoadingIconProcedurally();
        }

        //remove the loading sign
        this.setState({showPageLoading : false });
    }
    
    OrderIsReady = (orderId, userId, address) => {
        //headers
        const config = {
            headers: {
                'x-auth-token': localStorage.getItem('token')
            }
        }

        //check to see if the order_needs_driver is true if so then dont send for a driver because one has already been sent for else send for one
        axios.get(`${apiURL}/api/vendors/does_order_need_driver/${orderId}/${this.state.vendorId}`, config)
        .then(res => {
            if(res.data) {
                if(!res.data.error && res.data.send_for_driver) {
                    var orderAvailability = {
                        orderId: orderId,
                        clientId: userId,
                        clientLocation: address,
                        vendorId: this.state.vendorId,
                        vendorName: this.props.userSpecifics.vendorName,
                        vendorLocation: this.props.userSpecifics.vendorAddress
                    };   
                    
                    //listeners to mount
                    this.props.socketData.emit('findADriverForOrder', orderAvailability);
                }
            }

            //update the order data and rebuild the list
            var currOngoingOrders = this.state.allOngoingOrders;

            for(var i=0; i < currOngoingOrders.length; i++) {
                if(currOngoingOrders[i]._id === orderId) {
                    currOngoingOrders[i].order_needs_driver = true;
                    break;
                }
            }

            //update the ongoing orders data
            this.setState({allOngoingOrders: currOngoingOrders});

            //rebuild the list
            this.buildOngoingOrders(currOngoingOrders);
        });
    }

    /*

        axios.get(`/api/vendors/order_is_ready_from_vendor/${this.props.userData.vendor_id_for_vendor}/${orderId}`, config)
      .then(res => {
        if(res.data.updated) {
          //send to socket to find a driver
          var orderInfo = {

          };
          
          this.notifyClientOfAvailability(true, orderId);
        } 
      });

    GivenToDriver = () => {
      //send response to back end
    }
    */


    /** Filter Options */
    filterOngoingOrders = () => { 
        var filterOrderNumber = (document.getElementById('order-number-in-filter').value).toLowerCase();
        var filterClientName = (document.getElementById('client-name-in-filter').value).toLowerCase();
        
        var orders = this.state.allOngoingOrders;

        if(filterOrderNumber) {
            orders = _.filter(orders, function(order) {
                return order.order_num_for_view.toLowerCase().indexOf(filterOrderNumber) > -1;
            });
        }

        if(filterClientName) {
            orders = _.filter(orders, function(order) {
                return order.user_name.toLowerCase().indexOf(filterClientName) > -1;
            });
        }

        this.buildOngoingOrders(orders)
    }

    clearFilter = () => {
        //clear the fields and get the orders
        document.getElementById('order-number-in-filter').value = "";
        var orders = this.state.allOngoingOrders;

        //send the orders to be built and pass true so it can clear all previous data in the orders pane
        this.buildOngoingOrders(orders)
    }

    render() {
        return (
            <div id='alpha-container-ongoing-orders'>
                <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/>

                <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                    <div id='modal-container-vendor'>
                        <MdClear id='close-modal' onClick={() => this.toggleModal()}/>
                        <br />
                        <br />

                        {
                            this.state.showResponseMessageInModal ?
                                <div className='center'>
                                    <h3 style={this.state.responseMsgStatus ? {color: this.state.goodColor} : {color: this.state.badColor}}>{this.state.responseMsg}</h3>
                                </div>
                            :
                                ''
                        }
                    </div>
                </div>
                
                <div id='ongoing-order-alpha-container'>
                    <div id='new-orders-filter-container'>
                        <div className='margin-top10'>
                            <input type='text' id='order-number-in-filter' placeholder='Enter Order Number' className='product-name-entry' onChange={this.filterOngoingOrders}/>
                            <input type='text' id='client-name-in-filter' placeholder='Enter Client Name' className='product-name-entry' onChange={this.filterOngoingOrders}/>
                        </div>

                        <div id='buttons-for-filter' className='margin-top10'>
                            <button id='filter-button' className='custom-button' onClick={this.filterOngoingOrders}>Filter</button>
                            <button id='clear-filter-button' className='custom-button' onClick={this.clearFilter}>Clear Filters</button>
                        </div>
                    </div>

                    <div id='all-ongoing-orders'>
                        {this.state.allOngoingOrdersComponents}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userData: state.auth.userData,
    userSpecifics: state.auth.userSpecifics,
    socketData: state.auth.socketData
});

export default connect(mapStateToProps, {storeAmountOfOngoingOrder})(VendorOngoingOrders);