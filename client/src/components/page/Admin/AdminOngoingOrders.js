//system defined components
import React, {Component} from 'react';
import moment from 'moment'
import axios from 'axios';
import {apiURL} from '../../../config.js';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

//utility components
import GoogleSearchLocator from '../../utility/GoogleSearchLocator';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally} from '../../utility/utilityFunctions';

import { MdRefresh, MdCheckBox, MdCheckBoxOutlineBlank, MdModeEdit} from 'react-icons/md'; 

//stylings
import '../../../stylings/Admin/admin-ongoing-orders.css';

//icons
import { MdClear } from 'react-icons/md';

//definitions
const _ = require('lodash');

class AdminOngoingOrders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            allOngoingOrders: null,
            allOngoingOrdersComponents: [],
            openOrderAsideMoreInfo: false,
            goodColor: '#258e25',
            badColor: '#ff3333',
            blueColor: '#4d79ff',
            defaultColor: '#a7a7a7',
            blackColor: '#000000',
            moreInfoOnOrderContainer : undefined,
            editAsideOrderInfo: false
        }

        //get the orders
        axios.get(`${apiURL}/api/admin/get_all_ongoing_orders`)
        .then(res => {
            //show loading icon procedurally
            addLoadingIconProcedurally('alpha-container-admin-ongoing-order');

            this.setState({allOngoingOrders : res.data});
            this.buildOngoingOrders(res.data);

            //remove procedural loading icon
            removeLoadingIconProcedurally();
        })
        .catch(err => {});
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        destinationAddressData : PropTypes.object
    }

    /* This function accepts the ongoing orders and builds them */
    buildOngoingOrders = (orders) => {
        if(orders.length === 0) {
            this.setState({allOngoingOrdersComponents : <div key={'no-product-in-list'} id='no-product-available-in-list' className='bold small-big-font'>No Ongoing Orders</div>});
        } else {
            var count = 0;
            var tempAllOngoingOrdersComponents = [];
            var orderStatus = "";

            orders.forEach(order => {
                if(order.vendor_approved === false) {
                    orderStatus = 'Awaiting stores approval ';
                }else {
                    orderStatus = 'Store has approved ';

                    //
                    if(order.order_needs_driver === false) {
                        orderStatus += '-> Store has not prepared order yet ';
                    }else {
                        orderStatus += '-> Order has been prepared, in need of a driver ';

                        //
                        if(order.driver_id) {
                            orderStatus += '-> Driver found';

                            //
                            if(order.driver_has_started_order === false) {
                                orderStatus += '-> Driver has not started order ';
                            }else {
                                orderStatus += '-> Driver has started order ';

                                //
                                if(order.driver_has_picked_up === false) {
                                    orderStatus += '-> Driver has not picked up order ';
                                }else {
                                    orderStatus += '-> Driver has picked up order';
                                }
                            }
                        }else {
                            orderStatus += '-> No driver found';
                        }
                    }
                }

                tempAllOngoingOrdersComponents.push(
                    <div key={count++} className='admin-single-ongoing-order-container' onClick={(ev) => this.openMoreInfoForOrder(ev, order._id, order.store_name, false)}>     
                        <div className='refresh-sign' onClick={(ev) => this.updateOrderInfoFromOrdersDashboard(ev, order._id, order.order_num_for_view)}><MdRefresh/></div>    
                        <div className='single-ongoing-order-data-row'>Order Id:  # <span className='micro-mini-font'>{order.order_num_for_view}</span></div>
                        <div className='single-ongoing-order-data-row'>Store Name: <span className='micro-mini-font'>{order.store_name}</span></div>
                        <div className='single-ongoing-order-data-row order-status'>Order Status: <span className='micro-mini-font italic'>{orderStatus}</span></div>

                        <div className='single-ongoing-order-data-row margin-top10'>Driver Id: <span className='micro-mini-font'>{order.driver_id || <div className='mini-important-information'> found no driver</div>}</span></div>
                        <div className='single-ongoing-order-data-row'>Driver Name: <span className='micro-mini-font'>{order.drivers_name || <div className='mini-important-information'> found no driver</div>}</span></div>
                        <div className='single-ongoing-order-data-row'>Driver's Current Location: <span className='micro-mini-font'>{order.order_num_for_view || <div className='mini-important-information'> found no driver</div>}</span></div>

                        <div>
                            { 
                                order.driver_id || order.order_needs_driver === false? 
                                    ''
                                :
                                    <button className='custom-button ongoing-order-single-buttons' onClick={(ev) => this.findDriverForOrder(ev, order._id)}>Find a Driver</button>
                            }
                            <button className='custom-button ongoing-order-single-buttons' onClick={(ev) => this.openMoreInfoForOrder(ev, order._id, order.store_name)}>More Info</button>
                        </div>

                        <div id={order.order_num_for_view+'_msg'} className='refreshed-response-msg-container'/>
                    </div>
                );
            });

            this.setState({allOngoingOrdersComponents: tempAllOngoingOrdersComponents});
        }
    }

    filterOrders = (clearRadioButton = false) => {
        var orders = this.state.allOngoingOrders;
        
        //filter search boxes
        var orderNum = document.getElementById('order-number-in-filter').value.toLowerCase();
        if(orderNum) {
            orders = _.filter(orders, function(order){
                return order.order_num_for_view.toLowerCase().indexOf(orderNum) > -1;
            });
        }
        var storeName = document.getElementById('admin-store-name-in-filter').value.toLowerCase();
        if(storeName) {
            orders = _.filter(orders, function(order){
                return order.store_name.toLowerCase().indexOf(storeName) > -1;
            });
        }
        var driverId = document.getElementById('driver-id-in-filter').value.toLowerCase();
        if(driverId) {
            orders = _.filter(orders, function(order){
                return order.driver_id.toLowerCase().indexOf(driverId) > -1;
            });
        }
        var driverName = document.getElementById('driver-name-in-filter').value.toLowerCase();
        if(driverName) {
            orders = _.filter(orders, function(order){
                return order.drivers_name.toLowerCase().indexOf(driverName) > -1;
            });
        }

        //filter radio buttons
        if(!clearRadioButton) {
            if(document.getElementById('store-has-verified').checked) {
                orders = _.filter(orders, function(order){
                    return order.vendor_approved === true;
                });
            } else if(document.getElementById('store-has-not-verified').checked) {
                orders = _.filter(orders, function(order){
                    return order.vendor_approved === false;
                });
            }

            if(document.getElementById('order-has-driver').checked) {
                orders = _.filter(orders, function(order){
                    return order.driver_id !== '';
                });
            } else if(document.getElementById('order-has-no-driver').checked) {
                orders = _.filter(orders, function(order){
                    return order.driver_id === '';
                });
            }

            if(document.getElementById('driver-has-picked-order').checked) {
                orders = _.filter(orders, function(order){
                    return order.driver_has_picked_up === true;
                });
            } else if(document.getElementById('driver-has-not-picked-order').checked)  {
                orders = _.filter(orders, function(order){
                    return order.driver_has_picked_up === false;
                });
            }
        } else {
            document.getElementById('store-has-verified').checked = false;
            document.getElementById('store-has-not-verified').checked = false;
            document.getElementById('order-has-driver').checked = false;
            document.getElementById('order-has-no-driver').checked = false;
            document.getElementById('driver-has-picked-order').checked = false;
            document.getElementById('driver-has-not-picked-order').checked = false;
        }
    
        this.buildOngoingOrders(orders);
    }

    clearFilters = () => {
        document.getElementById('order-number-in-filter').value = '';
        document.getElementById('admin-store-name-in-filter').value = '';
        document.getElementById('driver-id-in-filter').value = '';
        document.getElementById('driver-name-in-filter').value = '';

        document.getElementById('store-has-verified').checked = false;
        document.getElementById('store-has-not-verified').checked = false;
        document.getElementById('order-has-driver').checked = false;
        document.getElementById('order-has-no-driver').checked = false;
        document.getElementById('driver-has-picked-order').checked = false;
        document.getElementById('driver-has-not-picked-order').checked = false;

        this.buildOngoingOrders(this.state.allOngoingOrders)
    }

    updateOrderInfoFromOrdersDashboard = (ev, orderId, orderNum) => {
        //get order info
        axios.get(`${apiURL}/api/admin/get_order_info/${orderId}`)
        .then(res => {

            //show loading icon procedurally
            addLoadingIconProcedurally('alpha-container-admin-ongoing-order');

            //build the new order Component
            if(res.data.refreshed) {
                //handle order because an error happened internally
                document.getElementById(orderNum+'_msg').style.color = `${this.state.goodColor}`;
                document.getElementById(orderNum+'_msg').innerHTML = 'Updated';
                setTimeout(() => {  document.getElementById(orderNum+'_msg').innerHTML = '' }, 3000);

                //replace the updated one in our list
                var ongoingOrders = this.state.allOngoingOrders;
                var index = ongoingOrders.findIndex(item => item._id === orderId);

                //remove and dont replace the order when it has been delivered or cancelled 
                if(res.data.cancelled || res.data.delivered) {
                    ongoingOrders.splice(index, 1);
                } else {
                    ongoingOrders.splice(index, 1, res.data);
                }

                //update the ongoing orders list
                this.setState({allOngoingOrders: ongoingOrders});

                //rebuild the order list
                this.filterOrders();
            } else {
                //handle order because an error happened internally
                document.getElementById(orderNum+'_msg').style.color = `${this.state.badColor}`;
                document.getElementById(orderNum+'_msg').innerHTML = res.data.msg;
                setTimeout(() => {  document.getElementById(orderNum+'_msg').innerHTML = '' }, 3000);
            }
            
            //remove procedural loading icon
            removeLoadingIconProcedurally();
        })
        .catch(err => {});

        ev.stopPropagation()
    }

    openMoreInfoForOrder = (ev, orderId, storeName, asideAlreadyOpen) => {
        //change the editor of the order info to false becuase we want to see the new refreshed data and not edit it 
        this.setState({editAsideOrderInfo : false});

        //get order info
        axios.get(`${apiURL}/api/admin/get_order_more_info/${orderId}`)
        .then(res => {
            //show loading icon procedurally
            addLoadingIconProcedurally('alpha-container-admin-ongoing-order');

            //build the info
            this.buildMoreInfoContainerAside(res.data, storeName, asideAlreadyOpen);

            //remove procedural loading icon
            removeLoadingIconProcedurally();
        })
        .catch(err => {});

        //if the aside is not already opened then open it
        if(!asideAlreadyOpen) 
            this.setState({openOrderAsideMoreInfo : true});

        ev.stopPropagation();
    }

    toggleEditOrderInfo = (orderData, storeName, asideAlreadyOpen) => {
        this.setState({editAsideOrderInfo : !this.state.editAsideOrderInfo});
        this.buildMoreInfoContainerAside(orderData, storeName, asideAlreadyOpen);
    }

    buildMoreInfoContainerAside = (orderData, storeName, asideAlreadyOpen) => {
        var moreInfoOnOrderContainerTemp = undefined;
        if(orderData.refreshed) {
            //remove the update message after 3 sec
            if(asideAlreadyOpen) {
                setTimeout(() => {  document.getElementById('refreshed-result-aside-order-more-info').innerHTML = '' }, 500);
            }

            moreInfoOnOrderContainerTemp = <div>
                <div id='close-aside-order-more-info' onClick={this.closeMoreInfoForOrder}><MdClear/> </div>
                <div className='order-num-top'> Order Num # {orderData.orderInfo.order_num_for_view}</div>
                <div id='refresh-aside-order-more-info' onClick={(ev) => this.openMoreInfoForOrder(ev, orderData.orderInfo._id, storeName, true)}><MdRefresh/></div>    
                { asideAlreadyOpen ? <div id='refreshed-result-aside-order-more-info' className='valid-color'>{orderData.msg}</div> : ''}
                
                <div className='sectional-order-more-info-container'>
                    <div className='sectional-order-more-info-container-title'>Order Information</div>
                    <div className='sectional-order-more-info-container-icon' onClick={() => this.toggleEditOrderInfo(orderData, storeName, asideAlreadyOpen)}><MdModeEdit /></div>
    
                    <div>
                        <div className='sub-sectional-info-title'>Destination Address: </div> 
                        {
                            this.state.editAsideOrderInfo ?
                                <div id='destination-address-input-container'> <GoogleSearchLocator /> </div>
                                // <input type="text" id='destination-address-input' onChange={this.handleDestinationAddressChange}/>
                            :
                                <div className='sub-sectional-info-data'>{orderData.orderInfo.address}</div>
                        }
                    </div>
    
                    <div>
                        <div className='sub-sectional-info-title'>Total price with tax: </div> 
                        {
                            this.state.editAsideOrderInfo ?
                                <div className='sub-sectional-info-data'>$<input type="text" id='total-price-input' onChange={this.handleTotalPriceChange}/></div>
                            :
                                <div className='sub-sectional-info-data'>${orderData.orderInfo.order_price_total_with_tax}</div>
                        }
                    </div>
    
                    <div className='straight-line-aside-more-info margin-top10 margin-bottom10'/>
    
                    <div id='order-history'>
                        <div>
                            <div className='sub-sectional-info-title'>Order placed at: </div> 
                            <div className='sub-sectional-info-data'><div className='order-status-icon'><MdCheckBox className='good-order-status'/> {moment(orderData.orderInfo.order_date).format('MMMM Do YYYY, h:mm:ss a')}</div></div>
                        </div>
                        <div>
                            <div className='sub-sectional-info-title'>Vendor approved at:</div> 
                            <div className='sub-sectional-info-data'>{orderData.orderInfo.vendor_approved_at ? <div className='order-status-icon'><MdCheckBox className='good-order-status'/> {moment(orderData.orderInfo.vendor_approved_at).format('MMMM Do YYYY, h:mm:ss a')}</div> : <MdCheckBoxOutlineBlank className='bad-order-status'/>}</div>
                        </div>
                        <div>
                            <div className='sub-sectional-info-title'>Driver accepted Order at:</div> 
                            <div className='sub-sectional-info-data'>{orderData.orderInfo.driver_accepted_at ? <div className='order-status-icon'><MdCheckBox className='good-order-status'/> {moment(orderData.orderInfo.driver_accepted_at).format('MMMM Do YYYY, h:mm:ss a')}</div> : <MdCheckBoxOutlineBlank className='bad-order-status'/>}</div>
                        </div>
                        <div>
                            <div className='sub-sectional-info-title'>Driver started order at:</div> 
                            <div className='sub-sectional-info-data'>{orderData.orderInfo.driver_has_started_order_at ? <div className='order-status-icon'><MdCheckBox className='good-order-status'/> {moment(orderData.orderInfo.driver_has_started_order_at).format('MMMM Do YYYY, h:mm:ss a')}</div> : <MdCheckBoxOutlineBlank className='bad-order-status'/>}</div>
                        </div>
                        <div>
                            <div className='sub-sectional-info-title'>Driver picked up order at:</div> 
                            <div className='sub-sectional-info-data'>{orderData.orderInfo.driver_has_picked_up_at ? <div className='order-status-icon'><MdCheckBox className='good-order-status'/> {moment(orderData.orderInfo.driver_has_picked_up_at).format('MMMM Do YYYY, h:mm:ss a')}</div> : <MdCheckBoxOutlineBlank className='bad-order-status'/>}</div>
                        </div>
                        <div>
                            <div className='sub-sectional-info-title'>Driver delivered order at:</div> 
                            <div className='sub-sectional-info-data'>{orderData.orderInfo.delivered_at ? <div className='order-status-icon'><MdCheckBox className='good-order-status'/> {moment(orderData.orderInfo.delivered_at).format('MMMM Do YYYY, h:mm:ss a')}</div> : <MdCheckBoxOutlineBlank className='bad-order-status'/>}</div>
                        </div>
                    </div>

                    <div className='straight-line-aside-more-info margin-top10 margin-bottom10'/>


                    {
                        this.state.editAsideOrderInfo ?
                            <button id='update-order-info-button-aside' className='custom-button' onClick={this.updateOrderInfo}>Update</button>
                        :
                            ''
                    }
                </div>
    
                <div className='sectional-order-more-info-container'>
                    <div className='sectional-order-more-info-container-title'>Users Information</div>
    
                    <div>
                        <div className='sub-sectional-info-title'>User Name: </div> 
                        <div className='sub-sectional-info-data'>{orderData.usersInfo.first_name + ', '+ orderData.usersInfo.last_name}</div>
                    </div>
                    <div>
                        <div className='sub-sectional-info-title'>Email: </div> 
                        <div className='sub-sectional-info-data'>{orderData.usersInfo.user_email}</div>
                    </div>
                    <div>
                        <div className='sub-sectional-info-title'>Phone No: </div> 
                        <div className='sub-sectional-info-data'>{orderData.usersInfo.mobile_no}</div>
                    </div>
                </div>
    
                <div className='sectional-order-more-info-container'>
                    <div className='sectional-order-more-info-container-title'>Vendors Information</div>
    
                    <div>
                        <div className='sub-sectional-info-title'>Store Name: </div> 
                        <div className='sub-sectional-info-data'>{storeName}</div>
                    </div>
                    <div>
                        <div className='sub-sectional-info-title'>Address: </div> 
                        <div className='sub-sectional-info-data'>{orderData.storeInfo.product_vendor_address}</div>
                    </div>
                    <div>
                        <div className='sub-sectional-info-title'>Phone No: </div> 
                        <div className='sub-sectional-info-data'>{orderData.storeInfo.product_vendor_phone_number}</div>
                    </div>
                </div>
                
                <div className='sectional-order-more-info-container'>
                    <div className='sectional-order-more-info-container-title'>Drivers Information</div>
    
                    {
                        orderData.driversInfo ?
                            <div>
                                <div>
                                    <div className='sub-sectional-info-title'>User Name: </div> 
                                    <div className='sub-sectional-info-data'>{orderData.driversInfo.first_name + ', '+ orderData.driversInfo.last_name}</div>
                                </div>
                                <div>
                                    <div className='sub-sectional-info-title'>Email: </div> 
                                    <div className='sub-sectional-info-data'>{orderData.driversInfo.user_email}</div>
                                </div>
                                <div>
                                    <div className='sub-sectional-info-title'>Phone No: </div> 
                                    <div className='sub-sectional-info-data'>{orderData.driversInfo.mobile_no}</div>
                                </div>
                            </div>
                        :
                            <div>
                                No Driver Found
                            </div>
                    }
                </div>
            </div>;
        } else {
            moreInfoOnOrderContainerTemp = <div id='no-new-order-available'>
                {orderData.msg}
            </div>;
        }

        this.setState({moreInfoOnOrderContainer : moreInfoOnOrderContainerTemp});
    }

    refreshAsideMoreInfoOnStores = (orderId, storeName) => {
    }

    closeMoreInfoForOrder = () => {
        this.setState({openOrderAsideMoreInfo: false});
        this.setState({editAsideOrderInfo : false});
    }

    findDriverForOrder = (ev, orderId) => {
        ev.stopPropagation();
    }

    render() {
        return (
            <div id='alpha-container-admin-ongoing-order'>
                <div className='modal-background admin-ongoing-background' style={this.state.openOrderAsideMoreInfo ? {display:'block'} : {display:'none'}} onClick={this.closeMoreInfoForOrder}></div>
                <aside id='more-info-on-order' style={this.state.openOrderAsideMoreInfo ? {width:'40%'} : {width:'0'}}>
                    {this.state.moreInfoOnOrderContainer}
                </aside>

                <div className='small-big-font bold'>Ongoing Orders</div>

                <div id='admin-ongoing-order-filters-container'>
                    <div className='micro-font bold'>Filters</div>

                    <input type='text' id='order-number-in-filter' placeholder='Enter Order Number' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='admin-store-name-in-filter' placeholder='Enter Store Name' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='driver-id-in-filter' placeholder='Enter Driver Id' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='driver-name-in-filter' placeholder='Enter Driver Name' className='product-name-entry' onChange={() => this.filterOrders()}/>

                    <div className='micro-font bold margin-top10'>Order Status</div>
                    <div className='admin-single-radio-button-filter-container'>
                        <input type="radio" name="store-verified" id="store-has-verified" value="store-has-verified" onChange={() => this.filterOrders()}/> Store has Verified<br/>
                        <input type="radio" name="store-verified" id="store-has-not-verified" value="store-has-not-verified" onChange={() => this.filterOrders()}/> Store has not verified<br/>
                    </div>
                    <div className='admin-single-radio-button-filter-container'>
                        <input type="radio" name="order-has-driver" id="order-has-driver" value="order-has-driver" onChange={() => this.filterOrders()}/> Order has a driver<br/>
                        <input type="radio" name="order-has-driver" id="order-has-no-driver" value="order-has-no-driver" onChange={() => this.filterOrders()}/> Order has no driver<br/>
                    </div>
                    <div className='admin-single-radio-button-filter-container'>
                        <input type="radio" name="driver-picked-order" id="driver-has-picked-order" value="driver-has-picked-order" onChange={() => this.filterOrders()}/> Driver has picked up order<br/>
                        <input type="radio" name="driver-picked-order" id="driver-has-not-picked-order" value="driver-has-not-picked-order" onChange={() => this.filterOrders()}/> Driver has not picked up order<br/>
                    </div>

                    <div className='center'>
                        <button id='clear-filter-button' className='custom-button' onClick={() => this.filterOrders(true)}>Clear Radio Buttons</button>
                        <button id='clear-filter-button' className='custom-button' onClick={this.clearFilters}>Clear Filters</button>
                    </div>
                </div>

                <div id='admin-ongoing-orders-container'>
                    <div className='micro-font bold'>Orders  {this.state.allOngoingOrders ? <div className='total-num-of-data-top'>Total: {this.state.allOngoingOrders.length}</div> : ''}</div>

                    <div>
                        {this.state.allOngoingOrdersComponents}
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    destinationAddressData: state.utility.destinationAddressData
});

export default connect(mapStateToProps, { })(AdminOngoingOrders);