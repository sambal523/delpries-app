//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../../config.js';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally} from '../../utility/utilityFunctions';

//redux imports
import { connect } from 'react-redux';

//stylings
import '../../../stylings/Admin/admin-business.css';


class AdminBusiness extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            goodColor: '#258e25',
            badColor: '#ff3333',
            blueColor: '#4d79ff',
            defaultColor: '#a7a7a7',
            blackColor: '#000000',

            getBusinessLogisticsDataResponse: {
                count: 0,
                msg: '',
                gotten: false, //if an error happens on the call it remains as false
                total: 0,
                called: false //this is changes to true when the call is made
            }
        }
    }

    // parse a date in yyyy-mm-dd format
    parseDate = (input) => {
        var parts = input.match(/(\d+)/g);
        // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
        return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
    }

    searchForDeta = () => {
        //validate time frame data
        if(!document.getElementById('business-start-date-filter').value) 
        document.getElementById('business-start-date-filter').style.borderColor = `${this.state.badColor}`;
    
        //if we dont have an end date we also change the border color
        if(!document.getElementById('business-end-date-filter').value) 
            document.getElementById('business-end-date-filter').style.borderColor = `${this.state.badColor}`;

        //if we have start and end that then we can go from here
        if(document.getElementById('business-start-date-filter').value && document.getElementById('business-end-date-filter').value) {
            var startDate = document.getElementById('business-start-date-filter').value;
            var endDate = document.getElementById('business-end-date-filter').value;

            //if the endDate is lower than the start date then we dont want that else we continue
            if(endDate < startDate) {
                document.getElementById('business-end-date-filter').style.borderColor = `${this.state.badColor}`;
            } else {
                //change all the bad colors that have been set
                document.getElementById('business-start-date-filter').style.borderColor = `${this.state.defaultColor}`;
                document.getElementById('business-end-date-filter').style.borderColor = `${this.state.defaultColor}`;

                //get the orders    
                axios.get(`${apiURL}/api/admin/get_bus_data/${startDate}/${endDate}`)
                .then(res => {
                    //show loading icon procedurally
                    addLoadingIconProcedurally('alpha-container-admin-business');

                    if(res.data.gotten) {
                        this.setState({getBusinessLogisticsDataResponse: {count: res.data.count, msg: '', gotten: true, total: res.data.total, called: true}});
                    } else {
                        this.setState({getBusinessLogisticsDataResponse: {count: 0, msg: res.data.msg, gotten: false, total: 0, called: true}});
                    }

                    //remove procedural loading icon
                    removeLoadingIconProcedurally();
                })
                .catch(err => {});
            }
        }
    }

    render() {
        return (
            <div id='alpha-container-admin-business'>
                <div className='small-big-font bold'>Business and Logistics</div>

                <div id='filter-and-data-container'>
                    <div id='c'>
                        <div className='micro-font bold'>Filters</div>

                        <div className='admin-business-single-search-sectional'>
                            <div className='micro-font bold margin-top1-per'>Time Frame</div>
                            <div className='micro-font margin-top1-per date-title-all-orders-filter'>Start Date:</div><input type="date" id='business-start-date-filter' className='date-entries-filter-all-order'/><br/>
                            <div className='micro-font margin-top1-per date-title-all-orders-filter'>End Date:</div><input type="date" id='business-end-date-filter'   className='date-entries-filter-all-order'/>
                        </div>

                        <div className='filter-button-container'>
                            <button id='clear-filter-button' className='custom-button' onClick={this.searchForDeta}>Search</button>
                        </div>
                    </div>

                    <div id='admin-all-business-and-logistics-container'>
                        <div className='micro-font bold'>Results </div>


                        {
                            !this.state.getBusinessLogisticsDataResponse.called ? 
                                <div>
                                    <div key={'no-product-in-list'} id='no-product-available-in-list' className='bold small-big-font'>No Data Available - Please Choose Time Frame</div>
                                </div>
                            :
                                !this.state.getBusinessLogisticsDataResponse.gotten ? 
                                    <div>
                                        <div key={'no-product-in-list'} id='no-product-available-in-list' className='bold small-big-font bad-color'>{this.state.getBusinessLogisticsDataResponse.msg}</div>
                                    </div>
                                :
                                    <div className='single-result-data'>
                                        <div>
                                            <div className='business-logistics-title'>Total Earnings:</div>
                                            <div className='business-logistics-data'>${this.state.getBusinessLogisticsDataResponse.total}</div>
                                        </div>


                                        <div>
                                            <div className='business-logistics-title'>Total Orders:</div>
                                            <div className='business-logistics-data'>{this.state.getBusinessLogisticsDataResponse.count}</div>
                                        </div>
                                    </div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
});

export default connect(mapStateToProps, {})(AdminBusiness);