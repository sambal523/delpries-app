//system defined components
import React, {Component} from 'react';

//redux imports
import { connect } from 'react-redux';

//custom components
import AdminOngoingOrders from './AdminOngoingOrders';
import AdminAllOrders from './AdminAllOrders';
import AdminUsers from './AdminUsers';
import AdminBusiness from './AdminBusiness';

//images
import company_logo from '../../../images/icons/logo9.png';
import new_message_icon from '../../../images/icons/chat.png';
import cart_icon from '../../../images/icons/cart-icon.png';
import client_logo from '../../../images/icons/client-logo.png';
import calendar_icon from '../../../images/icons/calendar-icon.png';

//stylings
import '../../../stylings/Admin/admin-home.css';

//icons
import { MdClear, MdDehaze } from 'react-icons/md';

class AdminHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            openMenu : false,

            hoverOverColor: '#f4f4f4',
            defaultNoHoverColor: '#F7C544',
            showOngoingOrders: false,
            showAllOrders: false,
            showUsers: false,
            showBusiness: true
        }
    }

    toggleMenuBar = (ev) => {
        this.setState({openMenu: !this.state.openMenu});

        ev.cancelBubble = true;
        if (ev.stopPropagation) ev.stopPropagation();
    }

    whichBottomASectionShouldIShow = (sectionName) => {
        if(sectionName === 'showOngoingOrders') {
            this.setState({showOngoingOrders : true});
            this.setState({showAllOrders : false});
            this.setState({showUsers : false});
            this.setState({showBusiness : false});
        } else if(sectionName === 'showAllOrders') {
            this.setState({showOngoingOrders : false});
            this.setState({showAllOrders : true});
            this.setState({showUsers : false});
            this.setState({showBusiness : false});
        } else if(sectionName === 'showUsers') {
            this.setState({showOngoingOrders : false});
            this.setState({showAllOrders : false});
            this.setState({showUsers : true});
            this.setState({showBusiness : false});
        }else if(sectionName === 'showBusiness') {
            this.setState({showOngoingOrders : false});
            this.setState({showAllOrders : false});
            this.setState({showUsers : false});
            this.setState({showBusiness : true});
        }
    }

    render() {
        return (
            <div id='alpha-admin-container'>
                <div className='modal-background' style={this.state.openMenu ? {display:'block'} : {display:'none'}}></div>

                <div className='center'>
                    <div id='admin-company-logo'>
                        <img src={company_logo} className='image-take-full-space driver-company-icon' alt='company_logo'/>
                    </div>
                    <div className='admin-title'>Admin Console</div>
                </div>

                <div id='full-admin-aside-navigation' style={this.state.openMenu ? {width:'300px'} : {width:'0'}}>
                    <aside id='admin-aside'>
                        <div id='open-close-button'>
                            {
                                this.state.openMenu ?
                                    <div className='admin-open-close-icon' onClick={(ev) => this.toggleMenuBar(ev)}><MdClear/> </div>
                                :
                                    <div className='admin-open-close-icon' onClick={(ev) => this.toggleMenuBar(ev)}><MdDehaze /></div>
                            }
                        </div>

                        <div id='aside-icons-navigation-container'>
                            <div className='single-aside-icon-container' style={this.state.showOngoingOrders ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('showOngoingOrders')}>
                                <div className='aside-single-icon-container'>
                                    <img src={cart_icon} className='single-image-icon' alt='new-message-icon' />
                                </div>

                                <div className='aside-text'>
                                    Ongoing Orders
                                </div>
                            </div>

                            <div className='single-aside-icon-container'  style={this.state.showAllOrders ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('showAllOrders')}>
                                <div className='aside-single-icon-container'>
                                    <img src={new_message_icon} className='single-image-icon' alt='new-message-icon' />
                                </div>

                                <div className='aside-text'>
                                    All Orders
                                </div>
                            </div>

                            <div className='single-aside-icon-container' style={this.state.showUsers ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('showUsers')}>
                                <div className='aside-single-icon-container'>
                                    <img src={client_logo} className='single-image-icon' alt='new-message-icon' />
                                </div>

                                <div className='aside-text'>
                                    Users
                                </div>
                            </div>

                            <div className='single-aside-icon-container last-single-aside-icon-container' style={this.state.showBusiness ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('showBusiness')}>
                                <div className='aside-single-icon-container'>
                                    <img src={calendar_icon} className='single-image-icon' alt='new-message-icon' />
                                </div>

                                <div className='aside-text'>
                                    Business
                                </div>
                            </div>
                        </div>
                    </aside>

                    <div id='aside-navigation' style={this.state.openMenu ? {display:'block'} : {display: 'none'}}>
                        <a href="#">Home</a>
                        <a href="#">Profile</a>
                        <a href="#">Logout</a>
                    </div>
                </div>

                <div id='current-showing-component'>
                    {
                        this.state.showOngoingOrders ?
                            <AdminOngoingOrders />
                        :
                            this.state.showAllOrders ?
                                <AdminAllOrders />
                            :
                                this.state.showUsers ?
                                    <div>
                                        <AdminUsers />
                                    </div>
                                :
                                    this.state.showBusiness ?
                                        <div>
                                            <AdminBusiness />
                                        </div>
                                    :
                                ''
                    }
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
});


export default connect(mapStateToProps, { })(AdminHome);