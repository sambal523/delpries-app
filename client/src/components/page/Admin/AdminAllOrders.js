//system defined components
import React, {Component} from 'react';
import moment from 'moment'
import axios from 'axios';
import {apiURL} from '../../../config.js';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally} from '../../utility/utilityFunctions';

//redux imports
import { connect } from 'react-redux';

//stylings
import '../../../stylings/Admin/admin-all-orders.css';

//definitions
const _ = require('lodash');

class AdminAllOrders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            allOrders: null,
            allOrdersComponents: [],
            goodColor: '#258e25',
            badColor: '#ff3333',
            blueColor: '#4d79ff',
            defaultColor: '#a7a7a7',
            blackColor: '#000000',
        }

        //get the orders
        axios.get(`${apiURL}/api/admin/get_add_orders`)
        .then(res => {
            //show loading icon procedurally
            addLoadingIconProcedurally('alpha-container-admin-all-orders');

            this.setState({allOrders : res.data});
            this.buildAllOrders(res.data);

            //remove procedural loading icon
            removeLoadingIconProcedurally();
        })
        .catch(err => {});
    }

    buildAllOrders = (orders) => {
        if(orders.length === 0) {
            this.setState({allOrdersComponents : <div key={'no-product-in-list'} id='no-product-available-in-list' className='bold small-big-font'>No Orders</div>});
        } else {
            var count = 0;
            var countItemsOrdered = 0;
            var tempAllOrdersComponents = [];
            var itemsOrderedComponents;

            orders.forEach(order => {
                itemsOrderedComponents = [];
                countItemsOrdered = 0;
                if(!order.orderInfo.vendor_requested_order) {
                    order.productsOrdered.forEach(item => {
                        itemsOrderedComponents.push(
                            <div key={countItemsOrdered++}>
                                <div className='order-info-item-name'>{item.product_name}</div>
                                <div className='order-info-item-price-and-amount'>${item.product_price} x {item.amount_ordered}</div>
                                <div className='order-info-special-instructions'>{item.special_instructions} +"{item.addons_description}"</div>
                            </div>
                        );
                    });
                }

                tempAllOrdersComponents.push(
                    <div className='single-all-order-container' key={count++}>
                        <div className='left-all-order-container'>
                            <div className='single-ongoing-order-data-row'>Users Id: <span className='micro-mini-font'>{order.usersInfo._id}</span></div>
                            <div className='single-ongoing-order-data-row'>Name: <span className='micro-mini-font'>{order.usersInfo.first_name}, {order.usersInfo.last_name}</span></div>
                            <div className='single-ongoing-order-data-row'>Mobile No: <span className='micro-mini-font'>{order.usersInfo.mobile_no}</span></div>
                            <div className='single-ongoing-order-data-row'>Email: <span className='micro-mini-font'>{order.usersInfo.user_email}</span></div>
                            <div className='single-ongoing-order-data-row'>Order Destination: <span className='micro-mini-font'>{order.orderInfo.address}</span></div>

                            <div className='single-ongoing-order-data-row margin-top10'>Drivers Id: <span className='micro-mini-font'>{order.driversInfo._id}</span></div>
                            <div className='single-ongoing-order-data-row'>Name: <span className='micro-mini-font'>{order.driversInfo.first_name}, {order.driversInfo.last_name}</span></div>
                            <div className='single-ongoing-order-data-row'>Mobile No: <span className='micro-mini-font'>{order.driversInfo.mobile_no}</span></div>
                            <div className='single-ongoing-order-data-row'>Email: <span className='micro-mini-font'>{order.driversInfo.user_email}</span></div>

                            <div className='single-ongoing-order-data-row margin-top10'>Store Name: <span className='micro-mini-font'>{order.storeInfo.product_vendor_name}</span></div>
                            <div className='single-ongoing-order-data-row'>Store made this order: <span className='micro-mini-font'>{order.orderInfo.vendor_requested_order ? 'Yes' : 'No'}</span></div>
                            <div className='single-ongoing-order-data-row'>Items Ordered:
                                <div className='items-ordered'>
                                    { order.orderInfo.vendor_requested_order ? '' : itemsOrderedComponents}
                                </div>
                            </div>
                        </div>

                        <div className='side-line'/>

                        <div className='right-all-order-container'>
                            <div className='single-ongoing-order-data-row'>Order Num: <span className='micro-mini-font'>{order.orderInfo.order_num_for_view}</span></div>
                            <div className='single-ongoing-order-data-row'>Order Created at: <span className='micro-mini-font'>{moment(order.orderInfo.order_date).format('MMMM Do YYYY, h:mm:ss a')}</span></div>
                            <div className='single-ongoing-order-data-row'>Order Delivered at: <span className='micro-mini-font'>{moment(order.orderInfo.delivered_at).format('MMMM Do YYYY, h:mm:ss a')}</span></div>

                            <div className='single-ongoing-order-data-row margin-top10'>Total amount paid +tx: <span className='micro-mini-font'>${order.orderInfo.order_price_total_with_tax}</span></div>
                            <div className='single-ongoing-order-data-row'>Vendor Made: <span className='micro-mini-font'>${order.orderInfo.vendors_total_money}</span></div>
                            <div className='single-ongoing-order-data-row'>Driver Made: <span className='micro-mini-font'>${order.orderInfo.drivers_total_money}</span></div>


                            <div className='single-ongoing-order-data-row margin-top10'>Cancelled ?: <span className='micro-mini-font'>{order.orderInfo.cancelled ? 'Yes': 'No'}</span></div>
                            <div className='single-ongoing-order-data-row'>Reason ?: <span className='micro-mini-font'></span></div>
                        </div>
                    </div>
                );
            });

            this.setState({allOrdersComponents: tempAllOrdersComponents});
        }
    }

    // parse a date in yyyy-mm-dd format
    parseDate = (input) => {
        var parts = input.match(/(\d+)/g);
        // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
        return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
    }

    filterOrders = (clearRadioButton = false, filterButtonClicked = false) => {
        var orders = this.state.allOrders;
        
        if(filterButtonClicked) {
            if(!document.getElementById('all-orders-start-date-filter').value) 
            document.getElementById('all-orders-start-date-filter').style.borderColor = `${this.state.badColor}`;
        
            //if we dont have an end date we also change the border color
            if(!document.getElementById('all-orders-end-date-filter').value) 
                document.getElementById('all-orders-end-date-filter').style.borderColor = `${this.state.badColor}`;

            //if we have start and end that then we can go from here
            if(document.getElementById('all-orders-start-date-filter').value && document.getElementById('all-orders-end-date-filter').value) {
                var startDate = this.parseDate(document.getElementById('all-orders-start-date-filter').value);
                var endDate = this.parseDate(document.getElementById('all-orders-end-date-filter').value);

                //if the endDate is lower than the start date then we dont want that else we continue
                if(endDate < startDate) {
                    document.getElementById('all-orders-end-date-filter').style.borderColor = `${this.state.badColor}`;
                } else {
                    //change all the bad colors that have been set
                    document.getElementById('all-orders-start-date-filter').style.borderColor = `${this.state.defaultColor}`;
                    document.getElementById('all-orders-end-date-filter').style.borderColor = `${this.state.defaultColor}`;
                
                    var parts = '';

                    orders = _.filter(orders, function(singleOrder){
                        parts = (singleOrder.orderInfo.order_date).match(/(\d+)/g);
                        var orderDate = new Date(parts[0], parts[1]-1, parts[2]);

                        return orderDate >= startDate && orderDate <= endDate;
                    });
                }
            }
        }

        //filter search boxes
        var orderNum = document.getElementById('order-number-in-filter').value.toLowerCase();
        if(orderNum) {
            orders = _.filter(orders, function(order){
                return order.orderInfo.order_num_for_view.toLowerCase().indexOf(orderNum) > -1;
            });
        }
        var storeName = document.getElementById('admin-store-name-in-filter').value.toLowerCase();
        if(storeName) {
            orders = _.filter(orders, function(order){
                return order.storeInfo.product_vendor_name.toLowerCase().indexOf(storeName) > -1;
            });
        }
        var storeId = document.getElementById('admin-store-id-in-filter').value.toLowerCase();
        if(storeId) {
            orders = _.filter(orders, function(order){
                return order.storeInfo._id.toLowerCase().indexOf(storeId) > -1;
            });
        }
        var driverId = document.getElementById('driver-id-in-filter').value.toLowerCase();
        if(driverId) {
            orders = _.filter(orders, function(order){
                return order.driversInfo._id.toLowerCase().indexOf(driverId) > -1;
            });
        }
        var driverName = document.getElementById('driver-name-in-filter').value.toLowerCase();
        if(driverName) {
            orders = _.filter(orders, function(order){ 
                return (order.driversInfo.first_name).toLowerCase().indexOf(driverName) > -1 || (order.driversInfo.last_name).toLowerCase().indexOf(driverName) > -1;
            });
        }
        var userName = document.getElementById('user-name-in-filter').value.toLowerCase();
        if(userName) {
            orders = _.filter(orders, function(order){
                return (order.usersInfo.first_name).toLowerCase().indexOf(userName) > -1 || (order.usersInfo.last_name).toLowerCase().indexOf(userName) > -1;
            });
        }
        var userPhoneNo = document.getElementById('user-phone-no-in-filter').value.toLowerCase();
        if(userPhoneNo) {
            orders = _.filter(orders, function(order){
                return order.usersInfo.mobile_no.toLowerCase().indexOf(userPhoneNo) > -1;
            });
        }
        var userId = document.getElementById('user-id-in-filter').value.toLowerCase();
        if(userId) {
            orders = _.filter(orders, function(order){
                return order.usersInfo._id.toLowerCase().indexOf(userId) > -1;
            });
        }

        //filter radio buttons
        if(!clearRadioButton) {
            if(document.getElementById('cancelled-orders').checked) {
                orders = _.filter(orders, function(order){
                    return order.orderInfo.cancelled === true;
                });
            }

            if(document.getElementById('vendor-generated-order').checked) {
                orders = _.filter(orders, function(order){
                    return order.orderInfo.vendor_requested_order === true;
                });
            } else if(document.getElementById('user-generated-order').checked) {
                orders = _.filter(orders, function(order){
                    return order.orderInfo.vendor_requested_order === false;
                });
            }
        } else {
            document.getElementById('cancelled-orders').checked = false;
            document.getElementById('vendor-generated-order').checked = false;
            document.getElementById('user-generated-order').checked = false;
        }
    
        this.buildAllOrders(orders);
    }

    clearFilters = () => {
        document.getElementById('order-number-in-filter').value = '';
        document.getElementById('admin-store-name-in-filter').value = '';
        document.getElementById('admin-store-id-in-filter').value = '';
        document.getElementById('driver-id-in-filter').value = '';
        document.getElementById('driver-name-in-filter').value = '';
        document.getElementById('user-name-in-filter').value = '';
        document.getElementById('user-phone-no-in-filter').value = '';
        document.getElementById('user-id-in-filter').value = '';

        document.getElementById('cancelled-orders').checked = false;
        document.getElementById('vendor-generated-order').checked = false;
        document.getElementById('user-generated-order').checked = false;

        this.buildAllOrders(this.state.allOrders)
    }

    render() {
        return (
            <div id='alpha-container-admin-all-orders'>
                <div className='small-big-font bold'>All Orders (Delivered | Cancelled)</div>

                <div id='admin-all-order-filters-container'>
                    <div className='micro-font bold'>Filters</div>

                    <input type='text' id='order-number-in-filter' placeholder='Enter Order Number' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='admin-store-name-in-filter' placeholder='Enter Store Name' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='admin-store-id-in-filter' placeholder='Enter Store Id' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='driver-id-in-filter' placeholder='Enter Driver Id' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='driver-name-in-filter' placeholder='Enter Driver Name' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='user-name-in-filter' placeholder='Enter User Name' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='user-phone-no-in-filter' placeholder='Enter User Phone No' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='user-id-in-filter' placeholder='Enter User Id' className='product-name-entry' onChange={() => this.filterOrders()}/>

                    <div className='admin-all-order-single-radio-button-filter-container'>
                        <div className='micro-font bold margin-top1-per'>Order Date</div>
                        <div className='micro-font margin-top1-per date-title-all-orders-filter'>Start Date:</div><input type="date" id='all-orders-start-date-filter' className='date-entries-filter-all-order'/><br/>
                        <div className='micro-font margin-top1-per date-title-all-orders-filter'>End Date:</div><input type="date" id='all-orders-end-date-filter'   className='date-entries-filter-all-order'/>
                    </div>
                    <div className='admin-all-order-single-radio-button-filter-container'>
                        <div className='micro-font bold margin-top1-per'>Order Status</div>
                        <input type="checkbox" name="order-status" id="cancelled-orders" value="cancelled-orders" onChange={() => this.filterOrders()}/> Cancelled Orders<br/>
                    </div>
                    <div className='admin-all-order-single-radio-button-filter-container'>
                        <div className='micro-font bold margin-top1-per'>Order Creator</div>
                        <input type="radio" name="vendor-generated-orders" id="vendor-generated-order" value="vendor-generated-order" onChange={() => this.filterOrders()}/> Vendor Created Order<br/>
                        <input type="radio" name="vendor-generated-orders" id="user-generated-order" value="user-generated-order" onChange={() => this.filterOrders()}/> User Created Order<br/>
                    </div>

                    <div className='filter-button-container'>
                        <button id='clear-filter-button' className='custom-button' onClick={() => this.filterOrders(true)}>Clear Radio Buttons</button>
                        <button id='clear-filter-button' className='custom-button' onClick={this.clearFilters}>Clear Filters</button>
                        <button id='clear-filter-button' className='custom-button' onClick={() => this.filterOrders(true, true)}>Filter</button>
                    </div>
                </div>

                <div id='admin-all-orders-container'>
                    <div className='micro-font bold'>Orders {this.state.allOrders ? <div className='total-num-of-data-top'>Total: {this.state.allOrders.length}</div> : ''}</div>

                    <div>
                        {this.state.allOrdersComponents}
                    </div>
                </div>
            </div>
        )
    }
}



const mapStateToProps = state => ({
});

export default connect(mapStateToProps, { })(AdminAllOrders);