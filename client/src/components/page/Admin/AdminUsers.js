//system defined components
import React, {Component} from 'react';
import moment from 'moment'
import axios from 'axios';
import {apiURL} from '../../../config.js';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally, capitalizeFirstLetter} from '../../utility/utilityFunctions';

//redux imports
import { connect } from 'react-redux';

//stylings
import '../../../stylings/Admin/admin-users.css';

//definitions
const _ = require('lodash');

class AdminUsers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            allUsersComponents : [],
            allUsersData: [],

            goodColor: '#258e25',
            badColor: '#ff3333',
            blueColor: '#4d79ff',
            defaultColor: '#a7a7a7',
            blackColor: '#000000',

            showModal: false,
            deactivateOrActivateUserId: '',
            deactivateOrActivateUserMessage: '',
            showDeactivateOrActivateUser: false,
            deactivateAccount: false
        };

        //get the orders
        axios.get(`${apiURL}/api/admin/get_all_users`)
        .then(res => {
            //show loading icon procedurally
            addLoadingIconProcedurally('alpha-container-admin-users');

            this.setState({allUsersData : res.data});
            this.buildUsersComponents(res.data);

            //remove procedural loading icon
            removeLoadingIconProcedurally();
        })
        .catch(err => {});
    }

    buildUsersComponents = (users) => {
        if(users.length === 0) {
            this.setState({allUsersComponents : <div key={'no-product-in-list'} id='no-product-available-in-list' className='bold small-big-font'>No Users Available</div>});
        } else {
            var count = 0;
            var tempAllUsersComponents = [];
            var userType = '';

            users.forEach(user => {
                if(user.can_access_order_details_page) 
                    userType = 'Client';
                else if(user.can_access_vendors_page)
                    userType = 'Vendor';
                else if(user.can_access_drivers_page) 
                    userType = 'Driver';
                
                tempAllUsersComponents.push(
                    <div className='single-user-container' key={count++} style={user.deactivated ? {borderBottom:`3px solid ${this.state.badColor}`, height : '152px' } : {}}>
                        <div className='single-ongoing-order-data-row'>Users Id: <span className='micro-mini-font'>{user._id}</span></div>
                        <div className='single-ongoing-order-data-row'>UserName (F&L): <span className='micro-mini-font'>{user.first_name}, {user.last_name}</span></div>
                        <div className='single-ongoing-order-data-row'>Phone No: <span className='micro-mini-font'>{user.mobile_no}</span></div>
                        <div className='single-ongoing-order-data-row'>Email: <span className='micro-mini-font'>{user.user_email}</span></div>
                        <div className='single-ongoing-order-data-row'>Date Joined: <span className='micro-mini-font'>{moment(user.date_joined).format('MMMM Do YYYY, h:mm:ss a')}</span></div>
                        <div className='single-ongoing-order-data-row'>User Type: <span className='micro-mini-font'>{userType}</span></div>

                        <div className='center margin-top10'>
                            {
                                user.deactivated ?
                                    <button id='activate-account' className='custom-button' onClick={() => this.deactivateOrActivateAccountOpenModal(user._id, capitalizeFirstLetter(user.first_name+', '+user.last_name), false)}>Activate Account</button>
                                :
                                    <button id='deactivate-account' className='custom-button' onClick={() => this.deactivateOrActivateAccountOpenModal(user._id, capitalizeFirstLetter(user.first_name+', '+user.last_name), true)}>Deactivate Account</button>
                            }
                        </div>
                    </div>
                );
            });

            this.setState({allUsersComponents : tempAllUsersComponents});
        }
    }

    deactivateOrActivateAccountOpenModal = (userId, userName, deactivateAccount) => {
        this.setState({deactivateOrActivateUserId: userId});
        this.setState({deactivateOrActivateUserMessage : `Are you sure you want to ${deactivateAccount ? 'Deactivate' : 'Activate'} ${userName} - Id ${userId}`});
        this.setState({showDeactivateOrActivateUser: true});
        this.setState({deactivateAccount : deactivateAccount});
        this.setState({showModal : true});
    }

    noToDeactivateOrActivateUser = () => {
        this.setState({showDeactivateOrActivateUser: false});
        this.setState({showModal : false});
    }

    yesToDeactivateOrActivateUser = () => {
        this.setState({showDeactivateOrActivateUser: false});
        this.setState({showModal : false});
    }

    // parse a date in yyyy-mm-dd format
    parseDate = (input) => {
        var parts = input.match(/(\d+)/g);
        // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
        return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
    }

    filterOrders = (clearRadioButton = false, filterButtonClicked = false) => {
        var users = this.state.allUsersData;
        
        if(filterButtonClicked) {
            if(!document.getElementById('all-users-start-date-filter').value) 
            document.getElementById('all-users-start-date-filter').style.borderColor = `${this.state.badColor}`;
        
            //if we dont have an end date we also change the border color
            if(!document.getElementById('all-users-end-date-filter').value) 
                document.getElementById('all-users-end-date-filter').style.borderColor = `${this.state.badColor}`;

            //if we have start and end that then we can go from here
            if(document.getElementById('all-users-start-date-filter').value && document.getElementById('all-users-end-date-filter').value) {
                var startDate = this.parseDate(document.getElementById('all-users-start-date-filter').value);
                var endDate = this.parseDate(document.getElementById('all-users-end-date-filter').value);

                //if the endDate is lower than the start date then we dont want that else we continue
                if(endDate < startDate) {
                    document.getElementById('all-users-end-date-filter').style.borderColor = `${this.state.badColor}`;
                } else {
                    //change all the bad colors that have been set
                    document.getElementById('all-users-start-date-filter').style.borderColor = `${this.state.defaultColor}`;
                    document.getElementById('all-users-end-date-filter').style.borderColor = `${this.state.defaultColor}`;
                
                    var parts = '';

                    users = _.filter(users, function(user){
                        parts = (user.date_joined).match(/(\d+)/g);
                        var orderDate = new Date(parts[0], parts[1]-1, parts[2]);

                        return orderDate >= startDate && orderDate <= endDate;
                    });
                }
            }
        }

        //filter search boxes
        var userName = document.getElementById('user-name-in-filter').value.toLowerCase();
        if(userName) {
            users = _.filter(users, function(user){
                return (user.first_name).toLowerCase().indexOf(userName) > -1 || (user.last_name).toLowerCase().indexOf(userName) > -1;
            });
        }
        var email = document.getElementById('user-email-in-filter').value.toLowerCase();
        if(email) {
            users = _.filter(users, function(user){
                return (user.user_email).toLowerCase().indexOf(email) > -1;
            });
        }
        var phoneNo = document.getElementById('user-phone-no-in-filter').value.toLowerCase();
        if(phoneNo) {
            users = _.filter(users, function(user){
                return (user.mobile_no).toLowerCase().indexOf(phoneNo) > -1;
            });
        }
        var userId = document.getElementById('user-id-in-filter').value.toLowerCase();
        if(userId) {
            users = _.filter(users, function(user){
                return (user._id).toLowerCase().indexOf(userId) > -1;
            });
        }
        
        //filter radio buttons
        if(!clearRadioButton) {
            if(document.getElementById('client-user-type').checked) {
                users = _.filter(users, function(user){
                    return user.can_access_order_details_page === true;
                });
            } else if(document.getElementById('driver-user-type').checked) {
                users = _.filter(users, function(user){
                    return user.can_access_drivers_page === true;
                });
            } else if(document.getElementById('vendor-user-type').checked) {
                users = _.filter(users, function(user){
                    return user.can_access_vendors_page === true;
                });
            }

            if(document.getElementById('deactivated-user').checked) {
                users = _.filter(users, function(user){
                    return user.deactivated === true;
                });
            } else if(document.getElementById('activated-user').checked) {
                users = _.filter(users, function(user){
                    return user.deactivated === false;
                });
            }
        } else {
            document.getElementById('client-user-type').checked = false;
            document.getElementById('driver-user-type').checked = false;
            document.getElementById('vendor-user-type').checked = false;
            document.getElementById('deactivated-user').checked = false;
            document.getElementById('activated-user').checked = false;
        }
    
        this.buildUsersComponents(users);
    }

    clearFilters = () => {
        document.getElementById('user-name-in-filter').value = '';
        document.getElementById('user-email-in-filter').value = '';
        document.getElementById('user-phone-no-in-filter').value = '';
        document.getElementById('user-id-in-filter').value = '';

        document.getElementById('all-users-start-date-filter').style.borderColor = `${this.state.defaultColor}`;
        document.getElementById('all-users-end-date-filter').style.borderColor = `${this.state.defaultColor}`;

        document.getElementById('client-user-type').checked = false;
        document.getElementById('driver-user-type').checked = false;
        document.getElementById('vendor-user-type').checked = false;
        document.getElementById('deactivated-user').checked = false;
        document.getElementById('activated-user').checked = false;

        this.buildAllOrders(this.state.allUsersData)
    }

    render() {
        return (
            <div id='alpha-container-admin-users'>
                <div className='modal-background admin-user-modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                    {
                        this.state.showDeactivateOrActivateUser ? 
                            <div className='modal-background' style={{display:'block'}}>
                                <div id='modal-delete-group-addon-container'>
                                    <div id='edit-modal-title' className='title'>{this.state.deactivateAccount ? 'Deactivate Account' : 'Activate Account'}</div>

                                    <div id='delete-addon-group-message' className='center margin-top20'>
                                        {this.state.deactivateOrActivateUserMessage}
                                    </div>

                                    <div className='center margin-top20'>
                                        <button id='yes-to-delete-addon-button' className='custom-button' onClick={() => this.yesToDeactivateOrActivateUser()}>Yes</button>
                                        <button id='no-to-delete-addon-button' className='custom-button' onClick={() => this.noToDeactivateOrActivateUser()}>No</button>
                                    </div>
                                </div>
                            </div>
                        :
                            ''
                    }
                </div>

                <div className='small-big-font bold'>All Users</div>

                <div id='admin-all-users-filters-container'>
                    <div className='micro-font bold'>Filters</div>


                    <input type='text' id='user-name-in-filter' placeholder='Enter User Name' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='user-email-in-filter' placeholder='Enter User Email' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='user-phone-no-in-filter' placeholder='Enter User Phone No' className='product-name-entry' onChange={() => this.filterOrders()}/>
                    <input type='text' id='user-id-in-filter' placeholder='Enter User Id' className='product-name-entry' onChange={() => this.filterOrders()}/>

                    <div className='admin-all-users-single-radio-button-filter-container'>
                        <div className='micro-font bold margin-top1-per'>Date Joined</div>
                        <div className='micro-font margin-top1-per date-title-all-orders-filter'>Start Date:</div><input type="date" id='all-users-start-date-filter' className='date-entries-filter-all-order'/><br/>
                        <div className='micro-font margin-top1-per date-title-all-orders-filter'>End Date:</div><input type="date" id='all-users-end-date-filter'   className='date-entries-filter-all-order'/>
                    </div>
                    
                    <div className='admin-all-users-single-radio-button-filter-container'>
                        <div className='micro-font bold margin-top1-per'>User Type</div>
                        <input type="radio" name="vendor-generated-orders" id="client-user-type" value="client-user-type" onChange={() => this.filterOrders()}/> Client<br/>
                        <input type="radio" name="vendor-generated-orders" id="driver-user-type" value="driver-user-type" onChange={() => this.filterOrders()}/> Driver<br/>
                        <input type="radio" name="vendor-generated-orders" id="vendor-user-type" value="vendor-user-type" onChange={() => this.filterOrders()}/> Vendor<br/>
                    </div>

                    <div className='admin-all-users-single-radio-button-filter-container'>
                        <div className='micro-font bold margin-top1-per'>User Status</div>
                        <input type="radio" name="vendor-generated-orders" id="deactivated-user" value="deactivated-user" onChange={() => this.filterOrders()}/> Deactivated<br/>
                        <input type="radio" name="vendor-generated-orders" id="activated-user"   value="activated-user" onChange={() => this.filterOrders()}/> Active<br/>
                    </div>

                    <div className='filter-button-container'>
                        <button id='clear-filter-button' className='custom-button' onClick={() => this.filterOrders(true)}>Clear Radio Buttons</button>
                        <button id='clear-filter-button' className='custom-button' onClick={this.clearFilters}>Clear Filters</button>
                        <button id='clear-filter-button' className='custom-button' onClick={() => this.filterOrders(true, true)}>Filter</button>
                    </div>

                    <div id='admin-all-users-container'>
                        <div className='micro-font bold'>Users {this.state.allUsersData ? <div className='total-num-of-data-top'>Total: {this.state.allUsersData.length}</div> : ''}</div>
                        <div>
                            {this.state.allUsersComponents}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
});

export default connect(mapStateToProps, {})(AdminUsers);