//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import {apiURL} from '../../config.js';
import moment from 'moment'

//socket
import io from 'socket.io-client';
import {websocketUrl} from '../../config.js';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {storeSocket} from '../../redux/actions/authActions';
import {storeAmountOfOrder, storeAmountOfOngoingOrder, addOneOrder, addOneOngoingOrder} from '../../redux/actions/vendorActions';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally, playBeep} from '../utility/utilityFunctions';

//custom components
import Footer from '../utility/Footer';
import VendorOrders from './VendorOrders';
import VendorProducts from './VendorProducts';
import VendorNewOrder from './VendorNewOrder';
import VendorOngoingOrders from './VendorOngoingOrders';
import VendorSchedule from './VendorSchedule';
import GoogleSearchLocator from '../utility/GoogleSearchLocator';
import Switch from "react-switch";

//custom made component
import PageLoading from '../utility/PageLoading';

//images
import company_logo from '../../images/icons/logo9.png';
import cart_icon from '../../images/icons/cart-icon.png';
import new_message_icon from '../../images/icons/chat.png';
import product_icon from '../../images/icons/large-scale1.png';
import cart_icon2 from '../../images/icons/calendar-icon.png'; //this would be used for calendar
import car_icon from '../../images/icons/car.png';

//icons
import { FaCar } from "react-icons/fa";
import { MdClear } from 'react-icons/md';
import { FiSend } from "react-icons/fi"; 
import { FaMoneyBillWave } from "react-icons/fa";
import { MdCreditCard } from 'react-icons/md';  

//stylings
import '../../stylings/vendor.css';

class Vendors extends Component {
    constructor(props) {
        super(props);

        this.state = {
            vendorsName : this.props.userSpecifics.vendorName,
            showAllOrders: false,
            showNewOrders: true,
            showAllProducts: false,
            showOngoingOrders: false,
            showSchedule: false,
            hoverOverColor: '#45a470',
            defaultNoHoverColor: '#f7f7f7',
            goodColor: '#258e25',
            badColor: '#ff3333',
            defaultColor: '#a7a7a7',
            showPageLoading: false,
            showModal: false, 
            showINeedDriverModalData: false, 
            vendorsIncome: 0.0,
            
            iHaveGottenSocketData: false,
            iHaveGottenUserData: false,
            currentlyAvailable: false,
            
            orderEnteriesError : {
                errorInCustomerName: false,
                errorInClientsAddress: false,
                errorInCustomerPhoneNumber: false,
                errorInTotalAmountWithTaxAndDelivery : false,
                errorInHowToPayVendor: false,
                errorInHowClientPaysUs: false
            },
            orderEnteriesHowToPayVendor: {
                directTransfer: false,
                cash: false
            },
            orderEnteriesHowToGetPaidFromClient: {
                cashFromClient: false,
                card: false
            },

            responseMsg: '',
            responseMsgStatus: false,
            showResponseMessageInModal: false
        }
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        isAuthenticated: PropTypes.bool,
        userSpecifics: PropTypes.object,
        amountOfNewOrders: PropTypes.number,
        amountOfOngoingOrders: PropTypes.number,
        userData: PropTypes.object,
        destinationAddressData : PropTypes.object,
        socketData: PropTypes.object,
        utilityData : PropTypes.object,

        storeSocket: PropTypes.func.isRequired,
        storeAmountOfOrder: PropTypes.func.isRequired,
        storeAmountOfOngoingOrder: PropTypes.func.isRequired,
        addOneOrder: PropTypes.func.isRequired,
        addOneOngoingOrder: PropTypes.func.isRequired
    }

    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
            this.setState({showINeedDriverModalData: false});
            this.setState({showResponseMessageInModal: false});
        }

        //open the modal
        this.setState({showModal: !this.state.showModal});
    }

    validatePhoneNumber = (p) => {
        var phoneRe = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
        var digits = p.replace(/\D/g, "");
        return phoneRe.test(digits);
    }

    handleChangeCurrentlyAvailable = () => {
        //send data to backend of drivers current status
        const config = {
        headers: {
                'x-auth-token': localStorage.getItem('token')
            }
        }

        if(!this.state.currentlyAvailable) { //its false currently -- to be set to true
            //connect to the socket and create user object
            if(this.props.isAuthenticated && !this.props.socketData) {
                if(this.props.userData.can_access_vendors_page) {
                    addLoadingIconProcedurally('vendors-alpha-container');

                    const body  = {
                        userId: this.props.userData._id,
                        availabilityStatus: true,
                        date: moment(),
                        vendorId: this.props.userData.vendor_id_for_vendor
                    };

                    axios.post(`${apiURL}/api/vendors/current_availability`, body, config)
                    .then(res => {
                        removeLoadingIconProcedurally();
                        if(res.data.successful) {
                            //to toggle the switch
                            this.setState({ currentlyAvailable : !this.state.currentlyAvailable });
                            
                            if(!this.state.iHaveGottenSocketData) {
                                //connect to the socket
                                const socket = io.connect(websocketUrl, {'forceNew': true});
                        
                                //load the users data to the socket
                                socket.emit('createUserObject', this.props.userData);
                                
                                this.setState({iHaveGottenSocketData: true});
                                          
                                //add listener to increase list of new orders  --- this callback function is different from the one in the vendorNewOrder component (this add one order to teh redux new order number)
                                socket.on('thereIsANewOrder', this.thereIsANewOrder);    
                        
                                //store the socket data
                                this.props.storeSocket(socket);
                            }
                        } else {
                            this.setState({responseMsg : res.data.msg});
                            this.setState({responseMsgStatus : res.data.successful});
                            this.toggleModal();
                            this.setState({showResponseMessageInModal : true});
                        }
                    })
                    .catch(err => {
                    });
                }
            }
        } else {
            //axios.get(`/api/vendors/current_availability/${this.props.userData._id}/false/${moment().format("YYYY-MM-DD")}`, config)
            addLoadingIconProcedurally('vendors-alpha-container');

            const body  = {
                userId: this.props.userData._id,
                availabilityStatus: false,
                date: moment(),
                vendorId: this.props.userData.vendor_id_for_vendor
            };
            axios.post(`${apiURL}/api/vendors/current_availability`, body, config)
            .then(res => {
                removeLoadingIconProcedurally();
                if(res.data.successful) {
                    //to toggle the switch
                    this.setState({ currentlyAvailable : !this.state.currentlyAvailable });

                    if(this.state.iHaveGottenSocketData) {
                        //disconnect user
                        this.props.socketData.disconnect();
                    }
                } else {
                    this.setState({responseMsg : res.data.msg});
                    this.setState({responseMsgStatus : res.data.successful});
                    this.toggleModal();
                    this.setState({showResponseMessageInModal : true});
                }
            })
            .catch(err => {
            });
        }
    }


    thereIsANewOrder = () => {
        //play the sound
        playBeep();

        //add an order to the redux state 
        this.props.addOneOrder();
    }

    boilerPlateForDidMountAndUpdate = () => {
        if(!this.state.iHaveGottenUserData && this.props.userData) {
            this.setState({iHaveGottenUserData: true});

            //get the amount of ongoing orders
            const config = {
                headers: {
                    'x-auth-token': localStorage.getItem('token')
                }
            }

            //get the current status of the vendor
            axios.get(`${apiURL}/api/vendors/get_current_availability/${this.props.userData._id}`, config)
            .then(res => {
                if(res.data.gotten) {
                    if(res.data.status) {
                        const socket = io.connect(websocketUrl, {'forceNew': true});
                        socket.emit('createUserObject', this.props.userData);
                        
                        this.setState({iHaveGottenSocketData: true});
                        this.setState({currentlyAvailable: res.data.status});

                        //add listener to increase list of new orders
                        socket.on('thereIsANewOrder', this.thereIsANewOrder); 

                        //store the socket data
                        this.props.storeSocket(socket);
                    }
                }
            })
            .catch(err => {
            });

            //get amount of ongoing orders: orders that have not been picked up by the driver     
            axios.get(`${apiURL}/api/vendors/get_amount_of_ongoing_orders/${this.props.userData.vendor_id_for_vendor}`, config)
            .then(res => {
                //store the socket data
                this.props.storeAmountOfOngoingOrder(res.data.amount);
            })
            .catch(err => {});


            //get the amount of new orders
            axios.get(`${apiURL}/api/vendors/get_pending_orders_made_to_vendor/${this.props.userData.vendor_id_for_vendor}`, config)
            .then(res => {
                //store the socket data
                this.props.storeAmountOfOrder(res.data.length);
            })
            .catch(err => {});
        }
    }

    componentDidMount = () => {
        this.boilerPlateForDidMountAndUpdate();
    }

    componentDidUpdate = () => {
        this.boilerPlateForDidMountAndUpdate();
    }
    
    validateOrderEnteries = (whatCalledMe) => {
        //get all the data
        var noErrors = [{customerNameError: true, customerPhoneNumberError: true, totalAmountWithTaxAndDeliveryError: true}];

        var customerName = document.getElementById('customer-name-input').value;
        var customerPhoneNumber = document.getElementById('customer-number-input').value;
        var totalPrice = document.getElementById('price-input').value;
        var howToPayVendorHasBeenSelected = this.state.orderEnteriesHowToPayVendor.directTransfer || this.state.orderEnteriesHowToPayVendor.cash;
        var howClientPaysUsHasBeenSelected = this.state.orderEnteriesHowToGetPaidFromClient.cashFromClient || this.state.orderEnteriesHowToGetPaidFromClient.card;


        if(whatCalledMe && whatCalledMe === 'howToPayVendorMethodSelected') {
            howToPayVendorHasBeenSelected = true;
        } else if(whatCalledMe && whatCalledMe === 'howToGetPaidFromClientMethodSelected') {
            howClientPaysUsHasBeenSelected = true;
        }

        if(customerName && customerName.length > 1) {
            noErrors.customerNameError = false;
            document.getElementById('customer-name-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.customerNameError = true;
            document.getElementById('customer-name-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(customerPhoneNumber && this.validatePhoneNumber(customerPhoneNumber)) {
            noErrors.customerPhoneNumberError = false;
            document.getElementById('customer-number-input').style.borderColor = `${this.state.goodColor}`;
        } else {
            noErrors.customerPhoneNumberError = true;
            document.getElementById('customer-number-input').style.borderColor = `${this.state.defaultColor}`;
        }

        if(totalPrice && !isNaN(totalPrice) && totalPrice > (this.props.utilityData.i_need_driver_delivery_fee)) {
            noErrors.totalAmountWithTaxAndDeliveryError = false;
            document.getElementById('price-input').style.borderColor = `${this.state.goodColor}`;

            //update the total income value
            var totalPriceParsed = parseFloat(totalPrice);
            var howMuchToCharge = parseFloat(this.props.utilityData.i_need_driver_delivery_fee);
            this.setState({vendorsIncome : (totalPriceParsed - howMuchToCharge)});
        } else {
            noErrors.totalAmountWithTaxAndDeliveryError = true;
            document.getElementById('price-input').style.borderColor = `${this.state.defaultColor}`;

            this.setState({vendorsIncome: 0});
        }


        //update the data
        this.setState({orderEnteriesError : {
            errorInCustomerName: noErrors.customerNameError, 
            errorInTotalAmountWithTaxAndDelivery: noErrors.totalAmountWithTaxAndDeliveryError, 
            errorInCustomerPhoneNumber: noErrors.customerPhoneNumberError, 
            errorInClientsAddress: !this.props.destinationAddressData.addressIsValid, 
            errorInHowToPayVendor: !howToPayVendorHasBeenSelected, 
            errorInHowClientPaysUs: !howClientPaysUsHasBeenSelected}});

        return !noErrors.customerNameError && this.props.destinationAddressData.addressIsValid && !noErrors.customerPhoneNumberError && !noErrors.totalAmountWithTaxAndDeliveryError
        && howToPayVendorHasBeenSelected && howClientPaysUsHasBeenSelected;
    }

    openINeedADriverModal = () => {   
        //allow the modal data to show
        this.setState({showINeedDriverModalData: true });

        //toggle modal if modal is not opened already
        this.toggleModal();
    }

    howToPayVendorMethodSelected = (paymentMethod) => {
        if(paymentMethod === 'directTransfer') {
            this.setState({orderEnteriesHowToPayVendor : {directTransfer: true, cash: false}})
        } else if(paymentMethod === 'cash') {
            this.setState({orderEnteriesHowToPayVendor : {directTransfer: false, cash: true}})
        }

        this.validateOrderEnteries('howToPayVendorMethodSelected');
    }

    howToGetPaidFromClientMethodSelected = (paymentMethod) => {
        if(paymentMethod === 'cashFromClient') {
            this.setState({orderEnteriesHowToGetPaidFromClient : {cashFromClient: true, card: false}})
        } else if(paymentMethod === 'card') {
            this.setState({orderEnteriesHowToGetPaidFromClient : {cashFromClient: false, card: true}})
        }

        this.validateOrderEnteries('howToGetPaidFromClientMethodSelected');
    }

    sendMeDriver = () => {
        //validate the data and then send to the back end
        if(this.validateOrderEnteries()) {
            //show the loading icon
            this.setState({showPageLoading : true });

            //send to back end
            var customerName = document.getElementById('customer-name-input').value;
            var customerAddress = this.props.destinationAddressData.address;
            var customerPhoneNumber = document.getElementById('customer-number-input').value;
            var totalPriceWithTax = document.getElementById('price-input').value;
            var totalIncome = this.state.vendorsIncome;
            var paymentMethodToVendor = '';
            var paymentMethodFromClient = '';

            if(this.state.orderEnteriesHowToPayVendor.directTransfer) {
                paymentMethodToVendor = 'directTransfer';
            } else if(this.state.orderEnteriesHowToPayVendor.cash) {
                paymentMethodToVendor = 'cash';
            }

            if(this.state.orderEnteriesHowToGetPaidFromClient.cashFromClient) {
                paymentMethodFromClient = 'cash';
            } else if(this.state.orderEnteriesHowToGetPaidFromClient.card) {
                paymentMethodFromClient = 'card';
            }

            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': localStorage.getItem('token')
                }
            }

            //make body
            const body = JSON.stringify({userId:this.props.userData._id, 
                                        productVendorId: this.props.userData.vendor_id_for_vendor, 
                                        customerName, 
                                        customerAddress, 
                                        customerPhoneNumber, 
                                        totalPriceWithTax, 
                                        totalIncome, 
                                        paymentMethodToVendor, 
                                        paymentMethodFromClient,
                                        city: this.props.utilityData.city,
                                        province: this.props.utilityData.province});

            axios.post(`${apiURL}/api/vendors/i_need_a_driver`, body, config)
            .then(res => {
                if(res.data) {
                    this.setState({responseMsg : res.data.msg});
                    this.setState({responseMsgStatus : res.data.orderCreated});
                    this.toggleModal();
                    this.setState({showResponseMessageInModal : true});
                    this.toggleModal();

                    this.setState({showPageLoading : false });

                    if(res.data.orderCreated) {
                        //add one ongoing order to the redux state
                        this.props.addOneOngoingOrder();  

                        //rebuild the ongoing order if we are on the ongoing order component
                        if(this.state.showOngoingOrders) {
                            this.child.getAndBuildOngoingOrderData();
                        }

                        //look for a driver
                        this.findDriverForOrder(res.data.order);
                    }
                }
            })
            .catch(err => {
            });

            this.setState({showPageLoading : false });
        }
    }

    findDriverForOrder = (orderData) => {
        if(this.props.isAuthenticated) {
            var orderAvailability = {
                orderId: orderData._id,
                clientId: orderData.user_id,
                clientLocation: orderData.address,
                vendorId: orderData.product_vendor_id,
                vendorName: this.props.userSpecifics.vendorName,
                vendorLocation: this.props.userSpecifics.vendorAddress
            };   
        
            //listeners to mount
            this.props.socketData.emit('findADriverForOrder', orderAvailability);
        } 
    }

    whichBottomASectionShouldIShow = (sectionName) => {
        if(sectionName === 'allOrders') {
            this.setState({showAllOrders : true});
            this.setState({showNewOrders : false});
            this.setState({showAllProducts : false});
            this.setState({showOngoingOrders : false});
            this.setState({showSchedule : false});
        } else if(sectionName === 'newOrders') {
            this.setState({showAllOrders : false});
            this.setState({showNewOrders : true});
            this.setState({showAllProducts : false});
            this.setState({showOngoingOrders : false});
            this.setState({showSchedule : false});
        } else if(sectionName === 'allProducts') {
            this.setState({showAllOrders : false});
            this.setState({showNewOrders : false});
            this.setState({showAllProducts : true});
            this.setState({showOngoingOrders : false});
            this.setState({showSchedule : false});
        }else if(sectionName === 'showOngoingOrders') {
            this.setState({showAllOrders : false});
            this.setState({showNewOrders : false});
            this.setState({showAllProducts : false});
            this.setState({showOngoingOrders : true});
            this.setState({showSchedule : false});
        } else if(sectionName === 'showSchedule') {
            this.setState({showAllOrders : false});
            this.setState({showNewOrders : false});
            this.setState({showAllProducts : false});
            this.setState({showOngoingOrders : false});
            this.setState({showSchedule : true});
        }
    }

    render() {
        return (
            <div id='vendors-alpha-container'>
                <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/>

                <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                    <div id='modal-container-vendor'>
                        <MdClear id='close-modal' onClick={() => this.toggleModal()}/>
                        <br />
                        <br />
                        {
                            this.state.showINeedDriverModalData ? 
                                <div id='order-details-entries'>
                                    <div id='order-details-title' className='title request-driver-title'>Enter Order Details</div>   

                                    <div className='margin-top10 input-entry-vendor-modal-container'>
                                        <b>Customer Name:</b>
                                        <input type="text" className='input-entries' id='customer-name-input' onChange={this.validateOrderEnteries}/>
                                        <div className='error-message error-message-vendor-modal' style={this.state.orderEnteriesError.errorInCustomerName ? {display:'block'} : {display:'none'}}>
                                            Invalid Name | Please Enter Name (more than 1 letter)
                                        </div>
                                    </div>

                                    <div className='margin-top10 input-entry-vendor-modal-container'>
                                        <b>Customers Address:</b> 
                                        <GoogleSearchLocator />
                                    </div>

                                    <div className='margin-top10 input-entry-vendor-modal-container'>
                                        <b>Cutomer Phone Number:</b> 
                                        <input type="text" id='customer-number-input' className='input-entries' onChange={this.validateOrderEnteries} maxLength='10'/>
                                        <div className='error-message error-message-vendor-modal' style={this.state.orderEnteriesError.errorInCustomerPhoneNumber ? {display:'block'} : {display:'none'}}>
                                            Invalid Number | Please Enter phone number
                                        </div>
                                    </div> 

                                    <div className='margin-top10 input-entry-vendor-modal-container'>
                                        <b>Total price with Tax and Delivery Fee:</b> 
                                        <input type="text" className='input-entries'  id='price-input' onChange={this.validateOrderEnteries}/>
                                        <div className='error-message error-message-vendor-modal' style={this.state.orderEnteriesError.errorInTotalAmountWithTaxAndDelivery ? {display:'block'} : {display:'none'}}>
                                            Enter a valid price | Delivery Fee is ${this.props.utilityData.i_need_driver_delivery_fee}, so Price must be more than 
                                        </div>
                                    </div>


                                    <div className='margin-top10 input-entry-vendor-modal-container'>
                                        <b>Your Income:</b> 
                                        <div className='input-entries'  id='vendor-income'>{this.state.vendorsIncome}</div>
                                    </div>

                                    <div className='margin-top10 input-entry-vendor-modal-container container-for-payment-method-row'>
                                        <div style={this.state.orderEnteriesError.errorInHowToPayVendor ? {color : this.state.badColor} : {color: `#000000`}}>
                                            <b>How do you want to get paid</b>
                                        </div>
                                        <div className='container-for-payment-method-icons-row'>
                                            <div className='icon-payment-whole-div'>
                                                <div className='icon-payment-div' onClick={() => this.howToPayVendorMethodSelected('directTransfer')} style={this.state.orderEnteriesHowToPayVendor.directTransfer ? {color: '#F7C544', backgroundColor:'#45a470', border: '2px solid #45a470'} : {}}>
                                                    <FiSend className='icon-payment-actual-icon'/>
                                                </div>
                                                <div className='center payment-options-text'>Direct transfer</div>
                                            </div>

                                            <div className='icon-payment-whole-div'>
                                                <div className='icon-payment-div' onClick={() => this.howToPayVendorMethodSelected('cash')} style={this.state.orderEnteriesHowToPayVendor.cash ? {color: '#F7C544', backgroundColor:'#45a470', border: '2px solid #45a470'} : {}}>
                                                    <FaMoneyBillWave className='icon-payment-actual-icon'/>
                                                </div>
                                                <div className='center payment-options-text'>Cash</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='margin-top10 input-entry-vendor-modal-container container-for-payment-method-row'>
                                        <div style={this.state.orderEnteriesError.errorInHowClientPaysUs ? {color : this.state.badColor} : {color: `#000000`}}>
                                            <b>How would the driver collect money from client</b> 
                                        </div>
                                        <div className='container-for-payment-method-icons-row'>
                                            <div className='icon-payment-whole-div'>
                                                <div className='icon-payment-div' onClick={() => this.howToGetPaidFromClientMethodSelected('cashFromClient')} style={this.state.orderEnteriesHowToGetPaidFromClient.cashFromClient ? {color: '#F7C544', backgroundColor:'#45a470', border: '2px solid #45a470'} : {}}>
                                                    <FaMoneyBillWave className='icon-payment-actual-icon'/>
                                                </div>
                                                <div className='center payment-options-text'>Cash from Client</div>
                                            </div>

                                            <div className='icon-payment-whole-div'>
                                                <div className='icon-payment-div' onClick={() => this.howToGetPaidFromClientMethodSelected('card')} style={this.state.orderEnteriesHowToGetPaidFromClient.card ? {color: '#F7C544', backgroundColor:'#45a470', border: '2px solid #45a470'} : {}}>
                                                    <MdCreditCard className='icon-payment-actual-icon'/>
                                                </div>
                                                <div className='center payment-options-text'>Debit/Credit Card</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='center'><button id='send-me-driver-btn' className='custom-button' onClick={() => this.sendMeDriver()}>Send Driver</button></div>
                                </div>
                            :
                                this.state.showResponseMessageInModal ?
                                    <div className='center'>
                                        <h3 style={this.state.responseMsgStatus ? {color: this.state.goodColor} : {color: this.state.badColor}}>{this.state.responseMsg}</h3>
                                    </div>
                                :
                                    ''
                        }
                    </div>
                </div>

                <div className='absolute-company-logo'>
                    <img src={company_logo} className='image-take-full-space vendor-company-icon' alt='company_logo'/>
                </div>

                <FaCar id='car-icon-vendor' onClick={() => this.openINeedADriverModal()}/>

                <div id='middle-section-of-page-alpha'>
                    <div id='vendors-title' className='title color-black center'>{this.props.userSpecifics.vendorName}</div>

                    <div id='driver-status' className='center margin-top10'>
                        {
                            this.state.currentlyAvailable ? 
                                <div className='valid-color'>Currently Active</div>
                            :
                                <div className='bad-color'>Currently Unavailable</div>
                        }

                        <Switch 
                            onChange={this.handleChangeCurrentlyAvailable} 
                            checked={this.state.currentlyAvailable}    
                            height={20}
                            width={48}
                            uncheckedIcon={false}
                            checkedIcon={false}
                            handleDiameter={30}    
                            onColor="#258e25" //when good bar color
                            onHandleColor="#F7C544" //when good ball color
                            boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                            activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                        />
                    </div>

                    <div id='list-of-all-option-icons'>
                        <div id='list-of-all-option-icons-content-container'>
                            <div className='single-icon-container' style={this.state.showAllOrders ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('allOrders')}>
                                <div className='single-image-icon-container'>
                                    <img src={cart_icon} className='single-image-icon' alt='cart-icon' />
                                </div>
                                <div className='title-name-single-icon'>All Orders</div>
                            </div>

                            <div className='single-icon-container'  style={this.state.showNewOrders ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('newOrders')}>
                                <div className='one-new-order'>{this.props.amountOfNewOrders === 0 ? '' : this.props.amountOfNewOrders}</div>
                                <div className='single-image-icon-container'>
                                    <img src={new_message_icon} className='single-image-icon' alt='new-message-icon' />
                                </div>
                                <div className='title-name-single-icon'>New Order</div>
                            </div>

                            <div className='single-icon-container'  style={this.state.showOngoingOrders ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('showOngoingOrders')}>
                                <div className='one-new-order'>{this.props.amountOfOngoingOrders === 0 ? '' : this.props.amountOfOngoingOrders}</div>
                                <div className='single-image-icon-container'>
                                    <img src={car_icon} className='single-image-icon' alt='product-icon' />
                                </div>
                                <div className='title-name-single-icon'>Ongoing Orders</div>
                            </div>

                            <div className='single-icon-container'  style={this.state.showAllProducts ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('allProducts')}>
                                <div className='single-image-icon-container'>
                                    <img src={product_icon} className='single-image-icon' alt='product-icon' />
                                </div>
                                <div className='title-name-single-icon'>Products</div>
                            </div>

                            <div className='single-icon-container' style={this.state.showSchedule ? {backgroundColor: this.state.hoverOverColor} : {backgroundColor: this.state.defaultNoHoverColor}} onClick={() => this.whichBottomASectionShouldIShow('showSchedule')}>
                                <div className='single-image-icon-container'>
                                    <img src={cart_icon2} className='single-image-icon' alt='cart-icon' />
                                </div>
                                <div className='title-name-single-icon'>Schedule</div>
                            </div>
                        </div>
                    </div>
                    {
                        this.state.showAllOrders ?
                            <VendorOrders />
                        :
                            this.state.showNewOrders ?
                                <VendorNewOrder />
                            :
                                this.state.showAllProducts ?
                                    <VendorProducts />
                                :
                                    this.state.showOngoingOrders ?
                                        <VendorOngoingOrders childRef={ref => (this.child = ref)}/>
                                    :
                                        this.state.showSchedule ?
                                            <VendorSchedule />
                                        :
                        ''
                    }
                </div>

                <div id='vendors-footer'>
                    <Footer />
                </div>

                {
                    !this.props.userData ?
                        addLoadingIconProcedurally('vendors-alpha-container')
                    :
                        removeLoadingIconProcedurally()
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    userSpecifics: state.auth.userSpecifics,
    amountOfNewOrders: state.vendor.amountOfNewOrders,
    amountOfOngoingOrders: state.vendor.amountOfOngoingOrders,
    userData: state.auth.userData,
    destinationAddressData: state.utility.destinationAddressData,
    socketData: state.auth.socketData,
    utilityData: state.utility.utilityData,
});
  
export default connect(mapStateToProps, {storeSocket, storeAmountOfOrder, storeAmountOfOngoingOrder, addOneOrder, addOneOngoingOrder})(Vendors);
