//system defined components
import React, {Component} from 'react';

//redux imports
import { Provider } from 'react-redux'  //this is a react component and its a glue for react and redux
import store from '../redux/store';
import {authenticateUserWithToken} from '../redux/actions/authActions';

//personal components
import Navigator from './page/Navigator';

class App extends Component {
    componentWillUnmount = () => {
        window.removeEventListener('scroll', this.handleScroll);
    }

    componentDidMount = () => {
        store.dispatch(authenticateUserWithToken());
    }

    render() {
        return (
            <Provider store={store}>
                <Navigator />
            </Provider>
        )
    }
}

export default App;

