//system defined components
import React, {Component} from 'react';

import loading_icon from '../../images/icons/loading.gif';

//stylings
import '../../stylings/page-loading.css';

class PageLoading extends Component {
    constructor(props) {
        super(props);

        this.state = {
            style : {}
        }
    }

    componentDidMount = () => {
        this.setState({style: this.props.styling});
    }

    componentDidUpdate = () => {
    }

    componentWillReceiveProps = () => {
        this.setState({style: this.props.styling});
    }


    render() {
        return (
            <div className='modal-background' style={this.state.style}>
                <div id='modal-loading'>
                <div id='loading-icon'>
                    <img src={loading_icon} className='image-take-full-space' alt='loading_icon'/>
                </div>

                <div className='title title-for-loading'>Loading</div>
                </div>
            </div>
        )
    }
}


export default PageLoading;