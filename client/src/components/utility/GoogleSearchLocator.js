//system defined components
import React, {Component} from 'react';

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {storeAddressData} from '../../redux/actions/utilityActions';

//stylings
import '../../stylings/google-search-locator.css';


class GoogleSearchLocator extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            addressIsValid: true
        };
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        storeAddressData : PropTypes.func.isRequired,
        destinationAddressData : PropTypes.object
    }
    
    handleChange = address => {
        this.props.storeAddressData({address, addressIsValid: false});
    };
    
    handleSelect = address => {
        if((address.toLowerCase().indexOf('london, on') > -1)) {
            this.props.storeAddressData({address, addressIsValid: true});
        } else {
            this.props.storeAddressData({address, addressIsValid: false});
        }
    };

    componentDidMount = () => {
        //when you mount clear the previous address
        this.props.storeAddressData({address:'', addressIsValid: false});
    }

    render() {
        var options = {
            types: ['address'],
            componentRestrictions: {country:'ca'}
        };
    
        return (
            <div>
                <PlacesAutocomplete
                  onChange={this.handleChange}
                  value={this.props.destinationAddressData.address}
                  onSelect={this.handleSelect}
                  onError={this.handleError}
                  searchOptions={options}
                >
                    {({ getInputProps, suggestions, getSuggestionItemProps }) => {
                        return (
                            <div className="Demo__search-bar-container">
                                <div className="Demo__search-input-container">
                                    <input
                                        {...getInputProps({
                                          placeholder: 'Enter your Address ...',
                                          className: this.props.destinationAddressData.addressIsValid ? 'Demo__search-input-good' : 'Demo__search-input-bad',
                                        })}
                                    />
                                </div>
                                {suggestions.length > 0 && (
                                    <div className="Demo__autocomplete-container">
                                        {suggestions.map(suggestion => {
                                            const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                      
                                            return (
                                                /* eslint-disable react/jsx-key */
                                                <div
                                                  {...getSuggestionItemProps(suggestion, { className })}
                                                >
                                                    <span>{suggestion.description}</span>
                                                </div>
                                            );
                                        })}
                                    </div>
                                )}
                            </div>
                        );
                    }}
                </PlacesAutocomplete>
                
                <div className='center error-message'>
                    {!this.props.destinationAddressData.addressIsValid ? 'Please enter an Address in London' : ''}
                </div>
            </div>
        );
    };
}

const mapStateToProps = state => ({
    destinationAddressData: state.utility.destinationAddressData
});

export default connect(mapStateToProps, { storeAddressData })(GoogleSearchLocator);