//images
import loading_icon from '../../images/icons/loading.gif';
import moment from 'moment'
import {Howl, Howler} from 'howler';

//sound
import beepPath from '../../sound/beeps.mp3';

//libraries
const $ = require("jquery"); 


/**
 * 
 * This file is to hold utility functions that would be used around the code, it was created three quater way into the project so its not used a lot
 * 
 * Date of Creation: Jan 12, 2020.
 */

//sometimes the loading icon does not work with react that is why i have this procedural approach
export const addLoadingIconProcedurally = (componentName) => {
    $( `#${componentName}` ).append(`<div id='modal-background-2' style='display:block; z-index:8'> <div id='modal-loading'><div id='loading-icon'>  <img src=${loading_icon} class='image-take-full-space' alt='loading_icon'/> </div> <div class='title title-for-loading'>Loading</div></div> </div>`);
}

export const removeLoadingIconProcedurally = () => {
    $( "#modal-background-2" ).remove();
}

export const validatePhoneNumber = (p) => {
    var phoneRe = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
    var digits = p.replace(/\D/g, "");
    return phoneRe.test(digits);
}

export const validatePostalCode = (postal) => {
    var regex = new RegExp(/^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ][ -]?\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i);
    if (regex.test(postal))
        return true;
    else 
      return false;
}

export const validateEmail =  (email) => {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
}

export const validatePassword = (pw) => {
    return /[A-Z]/.test(pw) &&
          /[a-z]/.test(pw) &&
          /[0-9]/.test(pw) &&
          /[^A-Za-z0-9]/.test(pw) &&
        pw.length > 6;
}

export const validateScheduleTimeBasedOnDelpries = (openTime, closingTime) => {
    var openingTimeLimit = moment('9:00','HH:mm');
    var closingTimeLimit = moment('22:00','HH:mm');
    openTime = moment(openTime,'HH:mm');
    closingTime = moment(closingTime,'HH:mm');

    var openTimeValid = false;
    var closeTimeValid = false;
    
    //if the opening time is same of after the limit and the closing time is same or before the limit then its valid
    if(openTime.isSameOrAfter(openingTimeLimit)) {
        openTimeValid = true;
    } else {
        openTimeValid = false;
    }

    if(closingTime.isSameOrBefore(closingTimeLimit)) {
        closeTimeValid = true;
    } else {
        closeTimeValid = false;
    }

    if(openTime.isSameOrAfter(closingTime)) {
        openTimeValid = false;
    }

    return [openTimeValid, closeTimeValid]
}

export const capitalizeFirstLetter = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

export const playBeep = () => {
    const beep = new Howl({
      src: beepPath
    })

    beep.play();
}