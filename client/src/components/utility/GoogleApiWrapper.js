import React, { Component } from 'react';

import GoogleMapReact from 'google-map-react';
import {
    geocodeByAddress,
    getLatLng,
} from 'react-places-autocomplete';

//icons
import { FaMapMarkerAlt } from "react-icons/fa";

//stylings
import '../../stylings/google-api-wrapper.css';


const Marker = props => {
    return <FaMapMarkerAlt  style={{color: 'red', fontSize: '25px'}}/>
}

class GoogleApiWrapper extends Component {  
    constructor(props) {
        super(props);

        geocodeByAddress(this.props.address)
        .then(results => getLatLng(results[0]))
        .then(latLng => {
            this.setState({center: {lat: latLng.lat, lng: latLng.lng}});
        })
        .catch(error => {});

        this.state = { 
            center: {
                lat: undefined,
                lng: undefined
            },
            zoom: 16
        };
    }
    
    render() {
        if(this.state.center.lat && this.state.center.lng) {
            return (
                // Important! Always set the container height explicitly
                <div id='actual-map'>
                    <GoogleMapReact
                        bootstrapURLKeys={{ key: ''}}
                        defaultCenter={this.state.center}
                        defaultZoom={this.state.zoom}
                    >
                        <Marker
                            lat={this.state.center.lat}
                            lng={this.state.center.lng}
                        />
                    </GoogleMapReact>
                </div>
            );
        }
        else {
            return (
                <div></div>
            )
        }
    }
}
 
export default GoogleApiWrapper;