//system defined components
import React, {Component} from 'react';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

//stylings
import '../../stylings/page-not-found.css';

//images
import company_logo from '../../images/icons/logo9.png';


//icons
import { FaSadTear } from 'react-icons/fa';


class PageNotFound extends Component {
    //these are my redux proptypes from the global state
    static propTypes = {
        userData: PropTypes.object,
    }

    render() {
        return (
            <div id='alpha-container-page-not-found'>
                {
                    !this.props.userData ?
                        <div>
                            <button className='custom-button' id='sign-in-button'><a href='\SignIn'>SIGN IN</a></button>
                            <button className='custom-button' id='sign-up-button'><a href='\SignUp'>SIGN UP</a></button>
                        </div>
                    :
                        ''
                }

                <div className='absolute-company-logo'>
                    <img src={company_logo} className='image-take-full-space' alt='company_logo'/>
                </div>

                <div className='title color-black center margin-below-company-icon'>Page Not Found!</div>
                <div id='page-not-found-icon-container'> <FaSadTear /> </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    userData: state.auth.userData
});


export default connect(mapStateToProps, {})(PageNotFound);