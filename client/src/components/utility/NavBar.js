//system defined components
import React, {Component} from 'react';
import { FiUser } from 'react-icons/fi';

//stylings
import '../../stylings/nav-bar.css';


class Navbar extends Component {
    render() {
        return (
            <div id='whole-nav-bar'>
                <div id='navbar-left-wing'>
                    <div id='not-logged-in'><FiUser id='fi-user-for-not-logged-in'/> Not Logged In</div>
                    <div id='logged-in'></div>
                </div>
                <div id='navbar-right-wing'>
                    <button id='sign-in-button' className='custom-button'>Sign Up</button>
                    <button id='login-button' className='custom-button'>Log In</button>
                </div>
            </div>
        )
    }
}


export default Navbar;