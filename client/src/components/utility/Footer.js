//system defined components
import React, {Component} from 'react';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {logout} from '../../redux/actions/authActions';

//custom made component
import '../../stylings/footer.css';

//images
import company_logo from '../../images/icons/logo9.png';


class Footer extends Component {
    //these are my redux proptypes from the global state
    static propTypes = {
        logout: PropTypes.func.isRequired,
        userData: PropTypes.object
    }

    //this function runs when the user wants to logout
    logout = () => {
        this.props.logout();
    }

    performPresequisitesForHomePage = () => {

        //notify the user that their order data would be lost and then remove the id of the service category they choose so that the home page can load
        localStorage.removeItem('service-category-id');
        localStorage.removeItem('vendor-id');    
        localStorage.removeItem('show-checkout-page');
        localStorage.removeItem('cart-data');
    }

    render() {
        return (
            <div id='footer-container'>
                <ul>
                    {/* if you are not signed in or if you are signed in and you are a client you can see to access the home page */}
                    {
                        !this.props.userData || (this.props.userData && this.props.userData.can_access_order_details_page) ?
                            <li><a href='\' className='footer-links-anchor' onClick={this.performPresequisitesForHomePage}>Home</a></li>
                        :
                            ''
                    }

                    {/* if you are signed in and you are a client then you can see to access the order details page */}
                    {
                        this.props.userData && this.props.userData.can_access_order_details_page ?
                            <li><a href='\orderDetails' className='footer-links-anchor'>Order Details</a></li>
                        :
                            ''
                    } 

                    {/* if you are signed in and you are a vendor then you can see to access the vendors page */}
                    {
                        this.props.userData && this.props.userData.can_access_vendors_page ?
                            <li><a href='\vendors' className='footer-links-anchor'>Vendors Page</a></li>
                        :
                            ''
                    } 

                    {
                        this.props.userData ?
                            <li><a href='\profile' className='footer-links-anchor'>My Profile</a></li>
                        :
                            ''
                    } 

                    {/* if you are signed in and you are a driver then you can see to access the drivers page            */}
                    {
                        this.props.userData && this.props.userData.can_access_drivers_page ?
                            <li><a href='\drivers' className='footer-links-anchor'>Drivers Page</a></li>
                        :
                            ''
                    } 

                    {/* if you are not signed in then you can see to access the sign in/signup page            */}
                    {
                        !this.props.userData ?
                            <li><a href='\SignIn' className='footer-links-anchor'>Sign In</a> / <a href='\SignUp' className='footer-links-anchor'>Sign Up</a></li>
                        :
                            ''
                    } 

                    {/* if you are not signed in or if you are signed in as a client then you can see to access the about-us section          */}
                    {
                        !this.props.userData || this.props.userData.can_access_order_details_page ?
                            <li><a href='\#about-us-container' className='footer-links-anchor'>About Us</a></li>
                        :
                            ''
                    }

                    <li>Support: +1 (519) 234-9987</li>
                    
                    {/* if you are signed in then you can see the logout page */}
                    {
                        this.props.userData ?
                            <li><a href='\signIn' className='footer-links-anchor'  onClick={this.logout}>Logout</a></li>
                        :
                            ''
                    }
                </ul>

                <div id='company-logo-container'>
                    <img className='image-take-full-space' src={company_logo} alt='company-logo-img' />
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    userData: state.auth.userData
});


export default connect(mapStateToProps, {logout})(Footer);