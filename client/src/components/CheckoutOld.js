//system defined components
import React, {Component} from 'react';
import axios from 'axios';
import { Redirect } from 'react-router';

//custom made component
import PageLoading from '../utility/PageLoading';
import GoogleSearchLocator from '../utility/GoogleSearchLocator';

//icons
import { MdKeyboardBackspace } from 'react-icons/md'; //go back  
import { MdCreditCard } from 'react-icons/md';   
import { MdClear } from 'react-icons/md';
import { FaTrashAlt} from 'react-icons/fa';

//redux imports
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

//utility functions
import {addLoadingIconProcedurally, removeLoadingIconProcedurally} from '../utility/utilityFunctions';

//socket
import io from 'socket.io-client';

//images
import company_logo from '../../images/icons/logo9.png';
import client_logo from '../../images/icons/client-logo.png';

//stylings
import '../../stylings/checkout.css';

//libraries
const _ = require('lodash');

class Checkout extends Component {
    constructor(props) {
        super(props);

        this.state = {
            vendorId: localStorage.getItem('vendor-id'),
            showModal: false,
            cartItems : [],
            itemsInCartComponents : [],
            showPaymentInfoInModal : false, //used to show the payment processing options
            showPageLoading: false,
            goodColor: '#258e25',
            badColor: '#ff3333',
            defaultColor: '#a7a7a7',
            showOrderDetailsPage : false,
            iHaveUsedUtilityData: false,
            tip: null,
            deliveryFee: null,
            waitingPaymentProcessingResult: false,
            showPleaseLoginInfoInModal: false, 
            fromOrderDetails: null,
            savedCardsComponents: [],
            selectedCardId: '',
            responseAddNewOrder : {
                status: false,
                msg: ''
            },
            showPostAddNewOrderModalInfo: false
        }
    }

    //these are my redux proptypes from the global state
    static propTypes = {
        isAuthenticated: PropTypes.bool,
        socketData: PropTypes.object,
        userData: PropTypes.object,
        utilityData : PropTypes.object,
        destinationAddressData : PropTypes.object
    }

    toggleModal = () => {
        //if the current value is true that means we want to close the modal so remove all show modals
        if(this.state.showModal) {
            this.setState({showPaymentInfoInModal: false});
            this.setState({showPostAddNewOrderModalInfo: false});
            this.setState({showPleaseLoginInfoInModal: false});
        }

        //open the modal
        this.setState({showModal: !this.state.showModal});
    }

    handleGoBack = () => {
        //remove the vendor-id and refresh the window
        localStorage.removeItem('show-checkout-page');
        localStorage.removeItem('cart-data');

        //refresh the page
        window.location.reload(false);
    }

    removeThisItemFromTheCart = (orderItem) => {
        var currCart = this.state.cartItems;

        //if the current cart is not an object we want to parse it
        if(!(typeof currCart === 'object')) {
            currCart = JSON.parse(currCart);
        }
        
        _.remove(currCart, function(orderItemDirectlyFromCart) {
            return orderItemDirectlyFromCart.order_item_id_for_frontend === orderItem.order_item_id_for_frontend;
        });

        //update our cart
        this.setState({cartItems : currCart});

        //update the localstorage cart 
        localStorage.setItem('cart-data', JSON.stringify(currCart));

        //rebuild the cart
        this.buildTheItemsInTheCart(currCart);
    }

    proceedToPayment = () => {
        //this is where the proceedings to payment occurs, no processing occurs if the user has not logged in
        if(this.props.userData) {    
            if(this.props.destinationAddressData && this.props.destinationAddressData.addressIsValid) {
                if(this.state.cartItems.length > 0 ) {
                    this.setState({selectedCardId: ''});
                    this.getAndBuildPaymentOptions();
              
                    //open the modal
                    this.toggleModal();
                }
            }
        }else {
            this.setState({showPleaseLoginInfoInModal: true})

            //open the modal
            this.toggleModal();
        }
    }
    
    getAndBuildPaymentOptions = () => {
        //show loading icon
        addLoadingIconProcedurally('checkout-alpha-container');

        //headers
        const token = localStorage.getItem('token');
        const config = {
            headers: {
                'x-auth-token' : token
            }
        }
      
        //get payment info
        axios.get(`/api/checkout/get_cards_info/${this.props.userData._id}`, config)
        .then(res => {
            if(res.data.gotten) {
                if(this.props.userData.can_access_order_details_page) {
                    //build payment details
                    var savedCards = res.data.cardsInfo;
                    var tempSavedCardsComponent = [];
                    var count = 0;
                
                    if(savedCards.length > 0) {
                        savedCards.forEach(singleCard => {
                            var cardType = singleCard.card_type;
                    
                            tempSavedCardsComponent.push(
                                <div className='single-saved-card-container-checkout' key={count++}>
                                    <input type="radio" onChange={() => this.selectCardOption(singleCard._id)} name="payment-option" className='payment-option-radio-btn'/>
                                    <div className='card-number-single-card'>**** **** **** {singleCard.card_no}</div>
                                    <div className='expiry-date-single-card'>{singleCard.expiry_dat}</div>
                                    
                                    <div className='card-type-single-card'>{cardType.toUpperCase()}</div>
                                </div>
                            )
                        });
                    } else {
                        tempSavedCardsComponent.push(
                            <div className='full-width center bold margin-top6-per' key={count++}>
                              No Cards Saved. Go to Profile Page to Add Card
                            </div>
                        );
                    }
                
                    //update the list
                    this.setState({savedCardsComponents : tempSavedCardsComponent});
                }
            } else {
                //put error message where card info is meant to be
            }

            //remove loading icon
            removeLoadingIconProcedurally();
        });

        //open the modal
        this.setState({showPaymentInfoInModal : true});
    }

    selectCardOption = (cardId) => {
        this.setState({selectedCardId: cardId});

        document.getElementById('choose-payment-method-title').style.color = `#000000`;
    }

    paymentWentThrough = () => {
        if(this.state.fromOrderDetails) {
            window.location.reload(false);
        }

        this.setState({showOrderDetailsPage: true}); 
    }

    requestFromVendorForProductAvailability = (orderId, vendorsTotalMoney, orderNumForView) => {
        //send request to vendor to ask if the items are available
        if(this.props.isAuthenticated) {
            var clientAndOrderInfo = {
                orderId: orderId,
                cartItems: this.state.cartItems, 
                vendorId: this.state.vendorId, 
                userData: this.props.userData,
                vendorsTotalMoney: vendorsTotalMoney,
                orderNumForView
            };   
        
            //listeners to mount
            this.props.socketData.emit('verifyFromVendorOrderAvailability', clientAndOrderInfo);
        } else {
        }
    }

    payForItems = () => { 
        if(this.state.selectedCardId !== '') {
            //show the loading icon
            addLoadingIconProcedurally('checkout-alpha-container');

            //this is the token
            const token = localStorage.getItem('token');

            //headers
            const config = {
              headers: {
                'Content-Type': 'application/json',
                'x-auth-token': token
              }
            }
            
            var cartItems = this.state.cartItems;
            if(!(typeof cartItems === 'object')) {
              cartItems = JSON.parse(cartItems);
            }
            
            const body = JSON.stringify(({
              cartItems : cartItems, 
              tip: this.state.tip, 
              city: this.props.utilityData.city,
              province: this.props.utilityData.province,
              userId: this.props.userData._id,
              address: this.props.destinationAddressData.address,
              userName: this.props.userData.first_name+', '+this.props.userData.last_name,
              fromOrderDetails: this.state.fromOrderDetails,
              paymentId: this.state.selectedCardId
            }));
            
            axios.post('/api/checkout/add_an_order', body, config)
            .then(res => {
                this.setState({responseAddNewOrder: {status: res.data.orderAdded, msg: res.data.msg}});

                if(res.data.orderAdded) {
                    //send request to the vendor that there is a new order
                    this.requestFromVendorForProductAvailability(res.data.orderId, res.data.vendors_total_money, res.data.orderNumForView);

                    //show order details page
                    localStorage.removeItem('cart-data'); //remove the cart data that we stored
                }

                //remove the current data in the modal
                this.setState({showPaymentInfoInModal : false});

                //show the data for post add new order in modal
                this.setState({showPostAddNewOrderModalInfo : true});

                removeLoadingIconProcedurally();
            })
            .catch(err => {});
        } else {
            document.getElementById('choose-payment-method-title').style.color = `${this.state.badColor}`;
        }
    }

    buildTheItemsInTheCart = (cartItems, fromOrderDetails) => {
        var count = 0;
        var itemsInCartComponentsTemp = [];
        var cartData = cartItems || this.state.cartItems;
        var total = 0.0;
        var tax = 0.0;
        var deliveryFee = this.props.utilityData.delivery_fee_for_client;
        var tip = null;
        var addonsComponents = [];

        if(document.getElementById('tip-input')) {
            if(document.getElementById('tip-input').value === '') {
                tip = 0;
            } else {
                tip = parseFloat(document.getElementById('tip-input').value);
            }
        } else {
            tip = 2.99;
        }

        if(cartData.length > 0) {
            cartData.forEach(singleItem => {
                addonsComponents = [];
                
                //add the total
                total += ((singleItem.product_price + singleItem.addons_total) * singleItem.amount_ordered);

                if(!fromOrderDetails) {
                    //build the addons if any
                    singleItem.addons_groups_selected.forEach(singleAddonGroup => {
                        singleAddonGroup.addons_selected.forEach(singleAddonSelected => {
                            addonsComponents.push(
                                <div key={singleAddonSelected.id} className='single-product-addons-data-in-cart'>{singleAddonSelected.name} +${singleAddonSelected.price}</div>
                            );
                        })
                    })
                } else {
                    //this section would run if this checkout page data came from the orderDetails page
                    var addons = singleItem.addons_description.split(',');
                    for(var i=0; i<addons.length; i++) {
                        if(addons[i] != '') {
                            addonsComponents.push(
                                <div key={singleItem._id+i} className='single-product-addons-data-in-cart'>{addons[i]}</div>
                            );
                        }
                    }
                }

                //build the row of item data
                itemsInCartComponentsTemp.push(
                    <div key={count++} className='single-item-in-cart-container'>
                        <div className='item-name-in-page-cart'>
                            {singleItem.product_name}

                            <div className='product-addons-data-in-cart'>
                              {addonsComponents}
                            </div>

                            {
                                singleItem.special_instructions ? 
                                    <div className='special-instructions-in-cart'>
                                      "{singleItem.special_instructions}"
                                    </div>
                                :
                                    ''
                            }
                        </div>
                        <div className='item-amount-in-page-cart'>{singleItem.amount_ordered}</div>
                        <div className='item-price-in-page-cart'>${(singleItem.product_price + singleItem.addons_total).toFixed(2)}</div>
                        <div className='item-remove-in-page-cart-content' onClick={() => this.removeThisItemFromTheCart(singleItem)}>x</div>
                    </div>
                );
            });

            //add the total row and tax and everthing here
            tax = total * this.props.utilityData.tax; // this is for ontario and the app is only starting with ontario - London

            //and some spaces before and then the straight line
            itemsInCartComponentsTemp.push(
                <br key={count++}/>
            );
            itemsInCartComponentsTemp.push(
                <br key={count++}/>
            );
            itemsInCartComponentsTemp.push(
                <div key={count++} className='straight-line'/>
            );

            //add the total data and design to the modal
            itemsInCartComponentsTemp.push(
                <div key={count++} className='single-item-in-cart-container'>
                    <div className='lower-item-name-checkout'>Price</div>
                    <div className='lower-item-value-checkout'>${total.toFixed(2)}</div>
                </div>
            );

            //add the tax data and design to the modeal
            itemsInCartComponentsTemp.push(
                <div key={count++} className='single-item-in-cart-container'>
                    <div className='lower-item-name-checkout'>Tax</div>
                    <div className='lower-item-value-checkout'>${tax.toFixed(2)}</div>
                </div>
            );

            itemsInCartComponentsTemp.push(
                <div key={count++} className='single-item-in-cart-container'>
                    <div className='lower-item-name-checkout'>Tip</div>
                    <div className='lower-item-value-checkout'>$<input type="text" id='tip-input' defaultValue={tip} onChange={this.handleChangeInTip}/></div>
                </div>
            );

            this.setState({tip: tip});

            itemsInCartComponentsTemp.push(
                <div key={count++} className='single-item-in-cart-container'>
                    <div className='lower-item-name-checkout'>Delivery Fee</div>
                    <div className='lower-item-value-checkout'>${deliveryFee.toFixed(2)}</div>
                </div>
            );

            this.setState({deliveryFee: deliveryFee});

            //add the summation data and design to the modal
            itemsInCartComponentsTemp.push(
                <div key={count++} className='single-item-in-cart-container'>
                    <div className='lower-item-name-checkout'>Total</div>
                    <div className='lower-item-value-checkout'>${(total+tax+deliveryFee+tip).toFixed(2)}</div>
                </div>
            );
        } else {
            itemsInCartComponentsTemp.push(
                <div key={'no-items-available-in-cart-text'} id='no-items-available-in-cart-text' className='error-message'>
                  No Items In Cart
                </div>
            );
        }

        this.setState({itemsInCartComponents: itemsInCartComponentsTemp});
    }

    handleChangeInTip = () => {
        var tip = document.getElementById('tip-input').value;

        if(!tip) {
            document.getElementById('tip-input').style.borderColor = `${this.state.defaultColor}`;

            //rebuild the cart
            this.buildTheItemsInTheCart(this.props.cartItems.length > 0 ? this.props.cartItems : JSON.parse(localStorage.getItem('cart-data')));
        } else if(tip && !isNaN(tip)) {
            document.getElementById('tip-input').style.borderColor = `${this.state.goodColor}`;

            //rebuild the cart
            this.buildTheItemsInTheCart(this.props.cartItems.length > 0 ? this.props.cartItems : JSON.parse(localStorage.getItem('cart-data')));
        } else {
            document.getElementById('tip-input').style.borderColor = `${this.state.badColor}`;
        }
    }

    componentDidMount = () => {
        this.setState({cartItems: this.props.cartItems});
        this.setState({fromOrderDetails: this.props.fromOrderDetails || false});

        //write the cart data to the local storage
        if (global.localStorage) {
            //chek if one exists and if so get it else store it
            if (localStorage.getItem('cart-data') !== null) {
                this.setState({cartItems : localStorage.getItem('cart-data')});
            } else {
                localStorage.setItem('cart-data', JSON.stringify(this.props.cartItems));
            }
        }

        // Warning before leaving the page (back button, or outgoinglink)
        window.onbeforeunload = function() {
            return "Are you sure you want to leave cart, items will be removed?";
        };
    }

    componentDidUpdate = () => {
        if(!this.state.iHaveUsedUtilityData && this.props.utilityData) {
            this.buildTheItemsInTheCart(this.props.cartItems.length > 0 ? this.props.cartItems : JSON.parse(localStorage.getItem('cart-data')), this.props.fromOrderDetails);

            this.setState({iHaveUsedUtilityData: true});
        }
    }

    render() {
        return this.state.showOrderDetailsPage ?
            <Redirect to='/OrderDetails'/>
        :
          (
              <div id='checkout-alpha-container'>
                  <PageLoading  styling={this.state.showPageLoading ? {display:'block'} : {display:'none'}}/>

                  {/* the modal */}
                  <div className='modal-background' style={this.state.showModal ? {display:'block'} : {display:'none'}}>
                      <div id='modal-container'>
                          <MdClear id='close-modal' style={this.state.responseAddNewOrder.status ? {display:'none'} : {display:'block'}} onClick={() => this.toggleModal()}/>

                          {
                              this.state.showPaymentInfoInModal ? 
                                  <div id='payment-modal-container'>
                                      <div className='sub-section-title center margin-top20' id='choose-payment-method-title'>Choose Payment Method</div>
                                      {this.state.savedCardsComponents}

                                      <div id='pay-for-item-button-container'>
                                          <button className='custom-button' id='pay-for-item-button' onClick={() => this.payForItems()}>Pay</button>
                                      </div>
                                  </div>
                              :
                                  this.state.showPleaseLoginInfoInModal ?
                                      <div>
                                          <div id='account-type-icon-container-sign-in-up'>
                                                <img src={client_logo} className='account-type-icon' alt='client_logo'/>
                                          </div>
                                          <div id='please-sign-in-sign-up-container'>
                                              <button className='custom-button sign-in-sign-up-button'><a href='\SignIn'>SIGN IN</a></button>
                                              <button className='custom-button sign-in-sign-up-button'><a href='\SignUp'>SIGN UP</a></button>
                                          </div>
                                      </div>
                                  :
                                      this.state.showPostAddNewOrderModalInfo ?
                                          this.state.responseAddNewOrder.status ? 
                                              <div>
                                                  <div id='response-from-order' className='valid-color'>Thank you, your Order has been processed!</div>
                                                  <div className='after-payment-process-button-div'>
                                                      <button className='custom-button after-payment-process-button' onClick={() => this.paymentWentThrough()}>Ok</button>
                                                  </div>
                                              </div>
                                          :
                                              <div>
                                                  <div id='response-from-order' className='bad-color'> Sorry! We couldnt process your order, {this.state.responseAddNewOrder.msg}</div>
                                                  <div className='after-payment-process-button-div'>
                                                      <button className='custom-button after-payment-process-button'  onClick={() => this.toggleModal()}>Ok</button>
                                                  </div>
                                              </div>
                                      :
                              ''
                          }
                      </div>
                  </div>

                  <div className='absolute-company-logo'>
                      <img src={company_logo} className='image-take-full-space' alt='company_logo'/>
                  </div>
                  
                  <div id='cart-goback-icons-container'>
                      <MdKeyboardBackspace className='cart-goback-icons icon-1' onClick={ () => this.handleGoBack()} />  
                      <MdCreditCard className='cart-goback-icons icon-2' onClick={() => this.proceedToPayment()}/>  
                  </div>

                  <div id='whole-container'>
                      <div id='order-items-title' className='title'>Order Items</div>

                      <div id='items-in-cart-alpha-container'>
                          <div className='item-name-in-page-cart bold'>Item name</div>
                          <div className='item-amount-in-page-cart bold'>Amount</div>
                          <div className='item-price-in-page-cart bold'>Price</div>
                          <div className='item-remove-in-page-cart bold'>Remove</div>
                          <div id='items-in-the-page-cart-container'>
                              {this.state.itemsInCartComponents}
                          </div>

                          <div id='destination-address-container'>
                              <div id='get-my-location-container'>
                                  <div className='title destination-address-text'>Destination Address</div>
                                  <GoogleSearchLocator />
                              </div>
                          </div>

                          <div id='proceed-to-payment-button-container'><button className='custom-button proceed-to-payment-button' onClick={() => this.proceedToPayment()}>Proceed to Payment</button></div>
                      </div>
                  </div>
              </div>
          )
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    socketData: state.auth.socketData,
    userData: state.auth.userData,
    utilityData: state.utility.utilityData,
    destinationAddressData: state.utility.destinationAddressData
});

export default connect(mapStateToProps, {})(Checkout);