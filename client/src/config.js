const production = false;   //set this to true when deploying to production

export const domain = production ? '159.203.29.203' : 'localhost:5000'; //points to digital ocean
export const apiURL = `${production ? 'https://delpries.com' : `http://${domain}`}`;
export const websocketUrl = `${production ? 'https://delpries.com' : `http://${domain}`}`;
