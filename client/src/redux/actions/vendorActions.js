import { 
    STORE_AMOUNT_OF_ORDERS,
    REMOVE_ONE_ORDER,
    ADD_ONE_ORDER,

    STORE_AMOUNT_OF_ONGOING_ORDERS_VENDORS,
    REMOVE_ONE_ONGOING_ORDER_VENDORS,
    ADD_ONE_ONGOING_ORDER_VENDORS
} from "../actions/types";


//send utility data
export const storeAmountOfOrder = (orderAmount) => dispatch => {
    dispatch({type: STORE_AMOUNT_OF_ORDERS, payload: orderAmount});
} 

export const removeOneOrder = () => dispatch => {
    dispatch({type: REMOVE_ONE_ORDER});
} 

export const addOneOrder = () => dispatch => {
    dispatch({type: ADD_ONE_ORDER});
} 



export const storeAmountOfOngoingOrder = (orderAmount) => dispatch => {
    dispatch({type: STORE_AMOUNT_OF_ONGOING_ORDERS_VENDORS, payload: orderAmount});
} 

export const removeOneOngoingOrder = () => dispatch => {
    dispatch({type: REMOVE_ONE_ONGOING_ORDER_VENDORS});
} 

export const addOneOngoingOrder = () => dispatch => {
    dispatch({type: ADD_ONE_ONGOING_ORDER_VENDORS});
} 

