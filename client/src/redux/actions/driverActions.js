import { 
    STORE_NEW_ORDER_REQUEST,
    REMOVE_NEW_ORDER_REQUEST,

    STORE_AMOUNT_OF_ONGOING_ORDERS,
    REMOVE_ONE_ONGOING_ORDER,
    ADD_ONE_ONGOING_ORDER
} from "../actions/types";


//send utility data
export const storeNewOrderRequest = (newOrderRequestData) => dispatch => {
    dispatch({type: STORE_NEW_ORDER_REQUEST, payload: newOrderRequestData});
} 

export const removeNewOrderRequest = (orderId) => dispatch => {
    dispatch({type: REMOVE_NEW_ORDER_REQUEST, payload: {orderId}});
} 


//
export const storeOngoingOrdersAmount = (orderAmount) => dispatch => {
    dispatch({type: STORE_AMOUNT_OF_ONGOING_ORDERS, payload: orderAmount});
} 

export const removeOneOngoingOrder = () => dispatch => {
    dispatch({type: REMOVE_ONE_ONGOING_ORDER});
} 

export const addOneOngoingOrder = () => dispatch => {
    dispatch({type: ADD_ONE_ONGOING_ORDER});
} 
