//this file would hold all types of actions that are to be performed
export const AUTH_ERROR = "AUTH_ERROR";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT_USER = "LOGOUT_USER";
export const USER_VALIDATED = "USER_VALIDATED";

//these have to do with the error reducers
export const GET_ERRORS = "GET_ERRORS";
export const CLEAR_ERRORS = "CLEAR_ERRORS";

//these would have to deal with the socket
export const STORE_SOCKET = "STORE_SOCKET";

//this would handle the storage of the utility data that holds the payment infor for specific location
export const STORE_UTILITY_DATA = "STORE_UTILITY_DATA";
export const STORE_ORDER_DESTINATION_ADDRESS_DATA = "STORE_ORDER_DESTINATION_ADDRESS_DATA";

//this is used by drivers to store a new order request
export const STORE_NEW_ORDER_REQUEST = "STORE_NEW_ORDER_REQUEST";
export const REMOVE_NEW_ORDER_REQUEST = "REMOVE_NEW_ORDER_REQUEST";

//this is used by the vendors
export const STORE_AMOUNT_OF_ORDERS = "STORE_NEW_ORDER_REQUEST";
export const REMOVE_ONE_ORDER = "REMOVE_ONE_ORDER";
export const ADD_ONE_ORDER = "ADD_ONE_ORDER";
export const STORE_AMOUNT_OF_ONGOING_ORDERS_VENDORS = "STORE_AMOUNT_OF_ONGOING_ORDERS_VENDORS";
export const REMOVE_ONE_ONGOING_ORDER_VENDORS = "REMOVE_ONE_ONGOING_ORDER_VENDORS";
export const ADD_ONE_ONGOING_ORDER_VENDORS = "ADD_ONE_ONGOING_ORDER_VENDORS";

//this is used by the drivers
export const STORE_AMOUNT_OF_ONGOING_ORDERS = "STORE_AMOUNT_OF_ONGOING_ORDERS";
export const REMOVE_ONE_ONGOING_ORDER = "REMOVE_ONE_ONGOING_ORDER";
export const ADD_ONE_ONGOING_ORDER = "ADD_ONE_ONGOING_ORDER";