import axios from 'axios';
import {apiURL} from '../../config.js';
import { 
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_USER,
    LOGIN_USER,
    USER_VALIDATED,
    STORE_SOCKET
} from "../actions/types";



//this runs when we want to login
export const login = ({email, password}) => dispatch => {
    //headers
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    //make body
    const body = JSON.stringify({email, password});

    axios.post(`${apiURL}/api/sign_in/sign_in`, body, config)
    .then(res => dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data
    }))
    .catch(err => { 
        dispatch({
            type: LOGIN_FAIL,
            payload: err.response.data
        });
    });
}


export const authenticateUserWithToken = () => (dispatch, getState) => {
    axios.get(`${apiURL}/api/auth/validate_user`, tokenConfig(getState))
    .then(res => dispatch({
        type: USER_VALIDATED,
        payload: res.data
    }))
    .catch(err => {
        dispatch({
            type: AUTH_ERROR,
            payload: err
        })
    })
} 

//send socket
export const storeSocket = (socket) => dispatch => {
    dispatch({type: STORE_SOCKET, payload: socket});
} 

//setup config/headers in token
export const tokenConfig = getState => {
    //its going to try to get the token from authReducer.initialState.token
    const token = getState().auth.token;

    //Headers
    const config = {
        headers: {
            "Content-type": "application/json"
        }
    }

    //if there is a token then add it to headers
    if(token) {
        config.headers['x-auth-token'] = token;
    }

    return config;
}


//Logout User
export const logout = () => {
    return{
        type: LOGOUT_USER
    };
}