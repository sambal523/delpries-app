import { 
    STORE_UTILITY_DATA,
    STORE_ORDER_DESTINATION_ADDRESS_DATA,
    ADDRESS_IS_INVALID
 } from "../actions/types";


//send utility data
export const storeUtilityData = (utilityData) => dispatch => {
    dispatch({type: STORE_UTILITY_DATA, payload: utilityData});
} 

//send address data
export const storeAddressData = (addressData) => dispatch => {
    dispatch({type: STORE_ORDER_DESTINATION_ADDRESS_DATA, payload: addressData});
} 

