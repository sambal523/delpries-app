import { 
    STORE_NEW_ORDER_REQUEST,
    REMOVE_NEW_ORDER_REQUEST,
    
    STORE_AMOUNT_OF_ONGOING_ORDERS,
    REMOVE_ONE_ONGOING_ORDER,
    ADD_ONE_ONGOING_ORDER
} from "../actions/types";

//definitions
const _ = require('lodash');


const initialState = {
    allNewOrderRequestData: [],
    orderRequestAmount: 0,
    amountOfOngoingOrder: 0,
    newOrderRemoved: false
}

//personal functions
const checkIfPropertyMatchesAnyInArray = (arrayName, propertyName, value) => {
    return _.findIndex(arrayName, {[propertyName]: value}) !== -1 ? true : false;
}

export default function(state = initialState, action) {
    switch(action.type) {
        case STORE_NEW_ORDER_REQUEST:
            //if the order already exists in the list dont add it but if it does not then you can add it
            if(checkIfPropertyMatchesAnyInArray(state.allNewOrderRequestData, 'orderId', action.payload.orderId)) {
                return {
                    ...state
                };
            }
            else {
                var currOrderRequest = state.allNewOrderRequestData;
                currOrderRequest.push(action.payload);
                return {
                    ...state,
                    allNewOrderRequestData: currOrderRequest,
                    orderRequestAmount: currOrderRequest.length
                };
            }


        case REMOVE_NEW_ORDER_REQUEST:
            //first check if that orderId is present, if it is then we can remove it, if it is not we do nothing
            if(checkIfPropertyMatchesAnyInArray(state.allNewOrderRequestData, 'orderId', action.payload.orderId)) {
                var currOrderRequest = state.allNewOrderRequestData;

                //we want to remove the order
                _.remove(currOrderRequest, function(order) {
                    return order.orderId === action.payload.orderId;
                });

                return {
                    ...state,
                    allNewOrderRequestData: currOrderRequest,
                    orderRequestAmount: currOrderRequest.length,
                    newOrderRemoved: true
                };
            } else {
                return {
                    ...state,
                    newOrderRemoved: false
                };
            }



        case STORE_AMOUNT_OF_ONGOING_ORDERS:
            return {
                ...state,
                amountOfOngoingOrder : action.payload
            };

        case REMOVE_ONE_ONGOING_ORDER:
            return {
                ...state,
                amountOfOngoingOrder : state.amountOfOngoingOrder === 0 ? 0 : state.amountOfOngoingOrder - 1
            };

        case ADD_ONE_ONGOING_ORDER:
            return {
                ...state,
                amountOfOngoingOrder : state.amountOfOngoingOrder + 1
            };      
        
        default: 
           return state;
    }
}