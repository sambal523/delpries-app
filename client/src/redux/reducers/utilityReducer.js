import { 
    STORE_UTILITY_DATA,
    STORE_ORDER_DESTINATION_ADDRESS_DATA
} from "../actions/types";

const initialState = {
    utilityData: null,
    destinationAddressData: {address:'', addressIsValid: false}
}


export default function(state = initialState, action) {
    switch(action.type) {
        case STORE_UTILITY_DATA:
            return {
                ...state,
                utilityData: action.payload
            };
        
        case STORE_ORDER_DESTINATION_ADDRESS_DATA:
            return {
                ...state,
                destinationAddressData: {address:action.payload.address, addressIsValid: action.payload.addressIsValid}
            };
        
        default: 
           return state;
    }
}