import { 
    STORE_AMOUNT_OF_ORDERS,
    REMOVE_ONE_ORDER,
    ADD_ONE_ORDER,

    STORE_AMOUNT_OF_ONGOING_ORDERS_VENDORS,
    REMOVE_ONE_ONGOING_ORDER_VENDORS,
    ADD_ONE_ONGOING_ORDER_VENDORS
} from "../actions/types";

const initialState = {
    amountOfNewOrders: 0,
    amountOfOngoingOrders: 0
}


export default function(state = initialState, action) {
    switch(action.type) {
        case STORE_AMOUNT_OF_ORDERS:
            return {
                ...state,
                amountOfNewOrders : action.payload
            };

        case REMOVE_ONE_ORDER:
            return {
                ...state,
                amountOfNewOrders : state.amountOfNewOrders === 0 ? 0 : state.amountOfNewOrders - 1
            };

        case ADD_ONE_ORDER:
            return {
                ...state,
                amountOfNewOrders : state.amountOfNewOrders + 1
            };   
            
        case STORE_AMOUNT_OF_ONGOING_ORDERS_VENDORS:
            return {
                ...state,
                amountOfOngoingOrders : action.payload
            };

        case REMOVE_ONE_ONGOING_ORDER_VENDORS:
            return {
                ...state,
                amountOfOngoingOrders : state.amountOfOngoingOrders === 0 ? 0 : state.amountOfOngoingOrders - 1
            };

        case ADD_ONE_ONGOING_ORDER_VENDORS:
            return {
                ...state,
                amountOfOngoingOrders : state.amountOfOngoingOrders + 1
            };  
        
        default: 
           return state;
    }
}