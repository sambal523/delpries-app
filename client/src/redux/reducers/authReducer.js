import { 
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_USER,
    USER_VALIDATED,
    STORE_SOCKET
} from "../actions/types";

const initialState = {
    token: localStorage.getItem('token'),
    isAuthenticated: false,
    userData : null,
    msg: null,
    socketData: null,
    userSpecifics : null
}


export default function(state = initialState, action) {
    switch(action.type) {
        case LOGIN_SUCCESS:
            localStorage.setItem('token', action.payload.token)
            return {
                ...state,
                ...action.payload,
                isAuthenticated: action.payload.canLogIn,
                userData : action.payload.user,
                msg: action.payload.msg,
                userSpecifics: action.payload.user_specifics
            };

        case AUTH_ERROR:
        case LOGOUT_USER:
            localStorage.removeItem('token');
            return {
                ...state,
                token: null,
                userData: null,
                isAuthenticated: false,
                isLoading: false,
                msg: '',
                userSpecifics: null
            };


        case LOGIN_FAIL:
            localStorage.removeItem('token');
            return {
                ...state,
                token: null,
                userData: null,
                isAuthenticated: action.payload.canLogIn,
                isLoading: false,
                msg: action.payload.msg,
                userSpecifics: null
            };
        
        
        case USER_VALIDATED: 
            return {
                ...state,
                isAuthenticated: true,
                userData: action.payload.user,
                userSpecifics: action.payload.user_specifics
            };
        
        case STORE_SOCKET: 
            return {
                ...state,
                socketData: action.payload
            }

        default: 
           return state;
    }
}