//this is a meeting place for all the things that we want to do
import {combineReducers} from 'redux';
import authReducer from './authReducer';
import utilityReducer from './utilityReducer';
import driverReducer from './driverReducer';
import vendorReducer from './vendorReducer';

//our itemReducer would be called item outside this file
export default combineReducers({
    auth: authReducer,
    utility: utilityReducer,
    driver: driverReducer,
    vendor: vendorReducer
})