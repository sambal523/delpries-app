/**
 * Date Created: 2nd September 2019.
 * 
 * Libraries and their purpose
 *  Express: This would help in running our backend framework
 *  Mongoose: This would be used in our ORM to interact with our database
 *  Path: used to get the path of files like ours
 *  Config: Used for keeping secured items
 * 
 * Written by Samuel Balogun
 */

//system defined imports
const express = require("express");
const mongoose = require("mongoose");
const config = require('config');
const app = express();
const path = require('path');
const _ = require('lodash');

//this would a ct as the middleware
//app.use(cors());
app.use(express.json());

//this is our connection string
const db = config.get('mongoURI');

//connect to mongo : this uses mongoose to connect to our mongoDB
mongoose.connect(db, { useNewUrlParser: true, useCreateIndex: true })
.then(() => console.log('MongoDB Connected...'))
.catch(err => console.log('error: '+err));

//reroute my routes
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use('/api/home', require('./routes/personal_api/home'));
app.use('/api/pick_items', require('./routes/personal_api/pick_vendor'));
app.use('/api/pick_products', require('./routes/personal_api/pick_products'));
app.use('/api/checkout', require('./routes/personal_api/checkout'));
app.use('/api/order_details', require('./routes/personal_api/order_details'));
app.use('/api/sign_up', require('./routes/personal_api/sign_up'));
app.use('/api/sign_in', require('./routes/personal_api/sign_in'));
app.use('/api/vendors', require('./routes/personal_api/vendor'));
app.use('/api/drivers', require('./routes/personal_api/driver'));
app.use('/api/auth', require('./routes/personal_api/auth'));
app.use('/api/profile', require('./routes/personal_api/profile'));
app.use('/api/admin', require('./routes/personal_api/admin'));

//Serve static assets if we are in production
if(process.env.NODE_ENV  === 'production'){
    //set static folder
    app.use(express.static('client/build'));
 
    //this says that any request that we get that is not '/api/items' should load this page, __dirname mans our current directory
    //and then it should open the client folder and then the build folder anbd then index.html
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
    });
}




/* 
    Socket --------------- definitions and work ------------------- 
    This section of the code contains Socket definitions and work
*/
const socketIO = require('socket.io');
const http = require('http');
let server = http.createServer(app);
let io = socketIO(server);
//io.origins(['https://159.203.29.203:5000']);
//io.set('origins', 'https://delpries.com:5000');

//class definitions
class Client {
    constructor(clientId, userName, userEmail, socketId) {
        this.clientId = clientId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.socketId = socketId;
    }
};

class Vendor {
    constructor(vendorManagerId , vendorManagerEmail, vendorName, socketId, vendorId) {
        this.vendorManagerId = vendorManagerId;
        this.vendorManagerEmail = vendorManagerEmail;
        this.vendorName = vendorName;
        this.socketId = socketId;
        this.vendorId = vendorId;
    }
};

class Driver {
    constructor(driverId, driverName, driverEmail, isOnAnOrder, socketId, pendingOrderRequest, pendingOrderId) {
        this.driverId = driverId;
        this.driverName = driverName;
        this.driverEmail = driverEmail;
        this.isOnAnOrder = isOnAnOrder;
        this.socketId = socketId;
        this.pendingOrderRequest = pendingOrderRequest;
        this.pendingOrderId = pendingOrderId;
    }
};

//this contains details about the order, like the user who made it, the driver who is sending it, the vendor it is going to 
class Order {
    constructor(orderId, clientId, clientLocation, vendorId, vendorName, vendorLocation, driverId) {
        this.orderId = orderId;
        this.clientId = clientId;
        this.clientLocation = clientLocation;
        this.vendorId = vendorId;
        this.vendorName = vendorName;
        this.vendorLocation = vendorLocation;
        this.driverId = driverId;
    }
}


//data definitions
var clients = [];
var vendors = [];
var drivers = [];
var orders = [];


//functions
checkIfPropertyMatchesAnyInArray = (arrayName, propertyName, value) => {
    return _.findIndex(arrayName, {[propertyName]: value}) !=-1 ? true : false;
}

io.on('connection', (socket) => {
    socket.on('createUserObject', userObject => {
        //verify the user type ? vendor
        if(userObject.can_access_vendors_page === true) { //this means that this user is a vendor   
            //if this cant be found that means that the vendor is not present -> if so we want to add the vendor to the list else we want ro replace the new socketID with the older one
            if(!checkIfPropertyMatchesAnyInArray(vendors, 'vendorManagerId', userObject._id)) {
                //check if this vendor exists in our array
                var vendor = new Vendor(userObject._id, userObject.user_email, (userObject.first_name+' '+userObject.last_name), socket.id, userObject.vendor_id_for_vendor);
                vendors.push(vendor);
            } else {
                //if it matches then we want to switch the socket id
                var idx = _.findIndex(vendors, {vendorManagerId: userObject._id});
                vendors[idx].socketId = socket.id
            }
        }

        //verify the user type ? driver
        if(userObject.can_access_drivers_page === true) { //this means that this user is a driver
            if(!checkIfPropertyMatchesAnyInArray(drivers, 'driverId', userObject._id)) {
                var driver = new Driver(userObject._id, (userObject.first_name+' '+userObject.last_name), userObject.user_email, userObject.on_an_order, socket.id, false, '');
                drivers.push(driver);
            } else {
                //if it matches then we want to switch the socket id
                var idx = _.findIndex(drivers, {driverId: userObject._id});
                drivers[idx].socketId = socket.id
            }

            //find the order request for this driver and resend them
            var ordersObj = _.filter(orders, function(order) {
                return order.driverId === userObject._id;
            });

            if(ordersObj.length > 0) {
                for(var i=0; i<ordersObj.length; i++) {
                    sendOrderRequestToSpecificDriver(ordersObj[i]);
                }
            }
        }

        //verify the user type ? client
        if(userObject.can_access_order_details_page === true) { //this means that this user is a client
            //if this cant be found that means that the client is not present -> if so we want to add the client to the list else we want to replace the new socketID with the older one
            if(!checkIfPropertyMatchesAnyInArray(clients, 'clientId', userObject._id)) {
                var client = new Client(userObject._id, (userObject.first_name+' '+userObject.last_name), userObject.user_email, socket.id);
                clients.push(client);
            } else {
                //if it matches then we want to switch the socket id
                var idx = _.findIndex(clients, {clientId: userObject._id});
                clients[idx].socketId = socket.id
            }
        }
    });






    /**
     * Vendor related transfers
     */
    //this sends a request to the vendor when an order is placed
    socket.on('verifyFromVendorOrderAvailability', (clientVendorAndOrderDetails) => {
        //get the data that we want
        var {orderId, userData, vendorId, cartItems, vendorsTotalMoney, orderNumForView} = clientVendorAndOrderDetails;

        //sometimes the data comes as an object, and if that is the case we want to stingify it
        if(typeof cartItems === 'object') {
            cartItems = JSON.stringify(cartItems);
        }

        //find the vendor and tell them that there is a new order
        var vendor = _.find(vendors, {'vendorId': vendorId});
        if(vendor) {
            io.to(vendor.socketId).emit('thereIsANewOrder', [{_id:orderId, userData, cartItems, vendors_total_money: vendorsTotalMoney, order_num_for_view:orderNumForView}]);
        }
    });

    //this connects when a vendor has responded to an order
    socket.on('notifyClientOfAvailability', (orderAvailability) => {
        //if the vendor has the order, we want to store it
        if(orderAvailability.orderIsAvailable) {
            //create an order if the order does not exist already
            if(!checkIfPropertyMatchesAnyInArray(orders, 'orderId'), orderAvailability.orderId) {
                var order = new Order(orderAvailability.orderId, 
                                    orderAvailability.clientId, 
                                    orderAvailability.clientLocation, 
                                    orderAvailability.vendorId, 
                                    orderAvailability.vendorName, 
                                    orderAvailability.vendorLocation, 
                                    '');
                orders.push(order);
            }
        }

        var client = _.find(clients, {'clientId': orderAvailability.clientId});
        if(client) {
            io.to(client.socketId).emit('vendorHasRespondedToOrderAvailability', orderAvailability);
        }
    });

    socket.on('findADriverForOrder', (orderDetails) => {
        //loop through drivers and send the order to any available driver
        findDriverForOrder(orderDetails);
    });


    


    /**
     * Responses we get from drivers
     */
    socket.on('driverCannotTakeOrder', (orderData) => {
        //set driver pending a request to false for the driver on the order
        var driverIdx = _.findIndex(drivers, {'driverId': orderData.driverId});
        if(driverIdx >= 0) {
            //if this is the last order that the driver is responding to let the pendingOrderRequest be false
            if(orderData.orderRequestAmount === 0) {
                drivers[driverIdx].pendingOrderRequest = false;
            }
            drivers[driverIdx].pendingOrderId = '';

            //make the order driverId to be nothing so it signifies that we are still searching for an order
            var orderId = _.findIndex(orders, {'orderId': orderData.orderId});
            if(orderId >= 0) {
                orders[orderId].driverId = '';
            }
        }

        //loop through drivers and send the order to any available driver
        findDriverForOrder(orderData);
    });

    socket.on('driverCanTakeOrder', (orderData) => { 
        //set driver pending a request to false for the driver on the order
        var driverIdx = _.findIndex(drivers, {'driverId': orderData.driverId});
        if(driverIdx >= 0) {
            //if this is the last order that the driver is responding to let the pendingOrderRequest be false
            if(orderData.orderRequestAmount === 0) {
                drivers[driverIdx].pendingOrderRequest = false;
            }
            drivers[driverIdx].pendingOrderId = '';   
            drivers[driverIdx].isOnAnOrder = true;

            //delete the order from the list of orders
            _.remove(orders, function(order) {
                return order.driverId === drivers[driverIdx].driverId;
            });
        }
    });

    socket.on('driverDeliveredOrder', (orderData) => {
        //set driver pending a request to false for the driver on the order
        var driverIdx = _.findIndex(drivers, {'driverId': orderData.driverId});
        if(driverIdx >= 0) {
            //if this is the last order that the driver is responding to let the pendingOrderRequest be false
            if(orderData.orderOngoingAmount === 0) {
                drivers[driverIdx].isOnAnOrder = false;
            }
        }
    });






    /**
     * determines what would happen when a client disconnects
     */
    socket.on('disconnect', () => {
        var socketId = socket.id;

        //find the person that disconnected
        var driverObj = _.find(drivers, {'socketId': socketId});
        var clientObj = _.find(clients, {'socketId': socketId});
        var vendorObj = _.find(vendors, {'socketId': socketId});

        if(driverObj) {
            //remove the driver
            _.remove(drivers, function(driver) {
                return driver.driverId === driverObj.driverId;
            });
        }else if(clientObj) {
            //if there is not driver then get the client to see if its a client
            _.remove(clients, function(client) {
                return client.clientId === clientObj.clientId;
            });
        }else if(vendorObj) {
            //make vendor not currently active


            //if there is no client then get the vendor to see if its a vendor
            _.remove(vendors, function(vendor) {
                return vendor.vendorId === vendorObj.vendorId;
            });
        }
    });







    /**
     * Function definitions
     */
    function sendOrderRequestToSpecificDriver (orderData) {
        //find the driver first
        var driverObj = _.find(drivers, {'driverId': orderData.driverId});
        if(driverObj) {
            io.to(driverObj.socketId).emit('thereIsARequestOnDelivery', {orderId: orderData.orderId, 
                                                                        clientId: orderData.clientId, 
                                                                        clientLocation: orderData.clientLocation, 
                                                                        vendorId: orderData.vendorId, 
                                                                        vendorName: orderData.vendorName, 
                                                                        vendorLocation: orderData.vendorLocation,
                                                                        driverId: orderData.driverId
                                                                        });
        }
    }

    function findDriverForOrder (orderAvailability) {        
        var driver = null;
        var driverFound = false;
        //loop through all the drivers to send them the request
        for(var i=0; i<drivers.length; i++) {
            driver = drivers[i];

            if(driver.isOnAnOrder === false && !driver.pendingOrderRequest) {
                driverFound = true;
                drivers[i].pendingOrderRequest = true;
                drivers[i].pendingOrderId = orderAvailability.orderId;

                //send request to the driver        
                if(driver) {
                    //find the order and update the driverId
                    var orderId = _.findIndex(orders, {'orderId': orderAvailability.orderId});
                    if(orderId >= 0) {
                        orders[orderId].driverId = driver.driverId;
                    }

                    io.to(driver.socketId).emit('thereIsARequestOnDelivery', {orderId: orderAvailability.orderId, 
                                                                              clientId: orderAvailability.clientId, 
                                                                              clientLocation: orderAvailability.clientLocation, 
                                                                              vendorId: orderAvailability.vendorId, 
                                                                              vendorName: orderAvailability.vendorName, 
                                                                              vendorLocation: orderAvailability.vendorLocation,
                                                                              driverId: driver.driverId
                                                                            });
                }

                //break
                break;
            }
        }

        //search for a random driver and send them the request if no driver has been found
        if(!driverFound) {
            var idx = Math.floor(Math.random()*drivers.length);
    
            if(drivers[idx]) {
                //find the order and update the driverId
                var orderIdIdx = _.findIndex(orders, {'orderId': orderAvailability.orderId});
                if(orderIdIdx >= 0) {
                    orders[orderIdIdx].driverId = drivers[idx].driverId;
                }

                drivers[idx].pendingOrderRequest = true;
                drivers[idx].pendingOrderId = orderAvailability.orderId;
                io.to(drivers[idx].socketId).emit('thereIsARequestOnDelivery', {    orderId: orderAvailability.orderId, 
                                                                                    clientId: orderAvailability.clientId, 
                                                                                    clientLocation: orderAvailability.clientLocation, 
                                                                                    vendorId: orderAvailability.vendorId, 
                                                                                    vendorName: orderAvailability.vendorName, 
                                                                                    vendorLocation: orderAvailability.vendorLocation,
                                                                                    driverId: driver.driverId
                                                                                });
            } else {
                //send a notification to the admin that a driver was not found for this order
            }
        }
    }
});











//connect and listen
const port = process.env.PORT || 5000;
server.listen(port, () => console.log(`Server Started on PORT ${port}`));

