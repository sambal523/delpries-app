//we need to only allow routes that send the token work that is why we need this middleware
const config = require('config');
const jwt = require('jsonwebtoken');


/**
 * This function is to get the token that is sent
 * when we are done with this middleware we call next to go to the next middleware
 */
function auth(req, res, next) {
    const token = req.header('x-auth-token');

    //check for a token
    if(!token) {
        return res.status(401).json({msg: "No token Access denied", grantAccess: false}); //401 means that you are not authorized to enter this route
    }

    try {
        //verify the token if we have one
        const decoded = jwt.verify(token, config.get('jwtSecret'));

        //we want to take the token and put in in request.user and then Add user from payload
        req.user = decoded; 

        //move to tge next middleware
        next();
    } catch(e) {
        res.status(400).json({msg: 'Token is not valid', grantAccess: false});
    }
}

module.exports = auth;